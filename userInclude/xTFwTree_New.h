#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

#include "AnaObjs.h"
#include "AnaTree.h"
#include "TLorentzVector.h"
#include "TVector3.h"

class xTFwTree_New : public AnaTree {
public:
    xTFwTree_New(TChain *chain) : AnaTree(chain) { Init(this->fChain); }

    // Declaration of leaf types
    Double_t AMI_cross_section;
    UInt_t HLT_e140_lhloose_nod0;
    UInt_t HLT_e26_lhtight_nod0_ivarloose;
    UInt_t HLT_e60_lhmedium_nod0;
    UInt_t HLT_mu26_ivarmedium;
    UInt_t HLT_mu50;
    Float_t NOMINAL_pileup_combined_weight;
    UInt_t NOMINAL_pileup_random_run_number;
    Float_t PRW_DATASF_1down_pileup_combined_weight;
    UInt_t PRW_DATASF_1down_pileup_random_run_number;
    Float_t PRW_DATASF_1up_pileup_combined_weight;
    UInt_t PRW_DATASF_1up_pileup_random_run_number;
    Double_t beamSpotWeight;
    Double_t cross_section;
    UInt_t event_clean_EC_LooseBad;
    UInt_t event_clean_EC_TightBad;
    Int_t event_clean_detector_core;
    Int_t event_is_bad_batman;
    ULong64_t event_number;
    Double_t filter_efficiency;
    Float_t jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT;
    Float_t jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT;
    Float_t jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT;
    Float_t jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT;
    Float_t jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT;
    Float_t jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT;
    Float_t jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT;
    Float_t jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT;
    Float_t jet_NOMINAL_central_jets_global_effSF_JVT;
    Float_t jet_NOMINAL_central_jets_global_ineffSF_JVT;
    Float_t jet_NOMINAL_forward_jets_global_effSF_JVT;
    Float_t jet_NOMINAL_forward_jets_global_ineffSF_JVT;
    Float_t jet_NOMINAL_global_effSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_NOMINAL_global_effSF_DL1dv00_FixedCutBEff_85;
    Float_t jet_NOMINAL_global_ineffSF_DL1dv00_FixedCutBEff_70;
    Float_t jet_NOMINAL_global_ineffSF_DL1dv00_FixedCutBEff_85;
    std::vector<unsigned int> *jets;
    std::vector<float> *jets_Bottom_DL1_score;
    std::vector<float> *jets_Charm_DL1_score;
    std::vector<float> *jets_DL1dv00_FixedCutBEff_70_weight;
    std::vector<float> *jets_DL1dv00_FixedCutBEff_85_weight;
    std::vector<float> *jets_Light_DL1_score;
    std::vector<int> *jets_b_tag_quantile;
    std::vector<float> *jets_b_tag_score;
    std::vector<int> *jets_b_tagged_DL1dv00;
    std::vector<float> *jets_fjvt;
    std::vector<int> *jets_flavorlabel;
    std::vector<int> *jets_flavorlabel_cone;
    std::vector<int> *jets_flavorlabel_part;
    std::vector<int> *jets_is_Jvt_HS;
    std::vector<float> *jets_jvt;
    std::vector<int> *jets_origin;
    std::vector<TLorentzVector> *jets_p4;
    std::vector<float> *jets_q;
    std::vector<int> *jets_type;
    std::vector<float> *jets_width;
    Double_t kfactor;
    UInt_t lep;
    Float_t lep_EL_CHARGEID_STAT_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose;
    Float_t lep_EL_CHARGEID_STAT_1up_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose;
    Float_t lep_EL_CHARGEID_SYStotal_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose;
    Float_t lep_EL_CHARGEID_SYStotal_1up_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose;
    Float_t lep_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_MediumLLH_d0z0_v13;
    Float_t lep_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_MediumLLH_d0z0_v13;
    Float_t lep_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose;
    Float_t lep_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose;
    Float_t lep_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_RecoTrk;
    Float_t lep_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_RecoTrk;
    Float_t
        lep_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose;
    Float_t
        lep_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose;
    Float_t lep_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_FixedRad;
    Float_t lep_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_FixedRad;
    Float_t lep_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_FixedRad;
    Float_t lep_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_FixedRad;
    Float_t lep_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium;
    Float_t lep_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium;
    Float_t lep_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium;
    Float_t lep_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium;
    Float_t lep_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium;
    Float_t lep_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium;
    Float_t lep_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium;
    Float_t lep_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium;
    Float_t lep_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA;
    Float_t lep_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA;
    Float_t lep_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA;
    Float_t lep_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA;
    Float_t lep_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;
    Float_t lep_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;
    Float_t lep_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;
    Float_t lep_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;
    Float_t lep_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;
    Float_t lep_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;
    Float_t lep_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;
    Float_t lep_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;
    Float_t lep_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose;
    Float_t lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose;
    Float_t
        lep_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose;
    Float_t lep_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v13;
    Float_t lep_NOMINAL_EleEffSF_offline_RecoTrk;
    Float_t lep_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;
    Float_t lep_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;
    Float_t lep_NOMINAL_MuEffSF_IsoTightTrackOnly_FixedRad;
    Float_t lep_NOMINAL_MuEffSF_Reco_QualMedium;
    Float_t lep_NOMINAL_MuEffSF_TTVA;
    Float_t lep_cluster_eta;
    Float_t lep_cluster_eta_be2;
    UInt_t lep_elec_trigmatch_HLT_e140_lhloose_nod0;
    UInt_t lep_elec_trigmatch_HLT_e26_lhtight_nod0_ivarloose;
    UInt_t lep_elec_trigmatch_HLT_e60_lhmedium_nod0;
    UInt_t lep_elec_trigmatch_trigger_matched;
    Int_t lep_id_bad;
    Int_t lep_id_charge;
    Int_t lep_id_loose;
    Int_t lep_id_medium;
    Int_t lep_id_tight;
    Int_t lep_id_veryloose;
    UInt_t lep_iso_FCHighPtCaloOnly;
    UInt_t lep_iso_FCLoose;
    UInt_t lep_iso_FCLoose_FixedRad;
    UInt_t lep_iso_FCTight;
    UInt_t lep_iso_FCTightTrackOnly;
    UInt_t lep_iso_FCTightTrackOnly_FixedRad;
    UInt_t lep_iso_FCTight_FixedRad;
    UInt_t lep_iso_FixedCutLoose;
    UInt_t lep_iso_FixedCutTight;
    UInt_t lep_iso_FixedCutTightCaloOnly;
    UInt_t lep_iso_HighPtCaloOnly;
    UInt_t lep_iso_HighPtTrackOnly;
    UInt_t lep_iso_Loose;
    UInt_t lep_iso_Loose_FixedRad;
    UInt_t lep_iso_Loose_VarRad;
    UInt_t lep_iso_Tight;
    UInt_t lep_iso_TightTrackOnly;
    UInt_t lep_iso_TightTrackOnly_FixedRad;
    UInt_t lep_iso_TightTrackOnly_VarRad;
    UInt_t lep_iso_Tight_FixedRad;
    UInt_t lep_iso_Tight_VarRad;
    UInt_t lep_matched;
    Int_t lep_matched_classifierParticleOrigin;
    Int_t lep_matched_classifierParticleType;
    Int_t lep_matched_isHad;
    Int_t lep_matched_mother_pdgId;
    Int_t lep_matched_mother_status;
    Int_t lep_matched_origin;
    TLorentzVector *lep_matched_p4;
    Int_t lep_matched_pdgId;
    Float_t lep_matched_q;
    Int_t lep_matched_status;
    Int_t lep_matched_type;
    Int_t lep_muonAuthor;
    Int_t lep_muonType;
    UInt_t lep_muon_trigmatch_HLT_mu26_ivarmedium;
    UInt_t lep_muon_trigmatch_HLT_mu50;
    UInt_t lep_muon_trigmatch_trigger_matched;
    Int_t lep_origin;
    TLorentzVector *lep_p4;
    Float_t lep_q;
    Float_t lep_trk_d0;
    Float_t lep_trk_d0_sig;
    TLorentzVector *lep_trk_p4;
    Float_t lep_trk_pt_error;
    Float_t lep_trk_pvx_z0;
    Float_t lep_trk_pvx_z0_sig;
    Float_t lep_trk_pvx_z0_sintheta;
    Int_t lep_trk_vx;
    TVector3 *lep_trk_vx_v;
    Float_t lep_trk_z0;
    Float_t lep_trk_z0_sig;
    Float_t lep_trk_z0_sintheta;
    Int_t lep_type;
    Int_t lephad;
    Int_t lephad_coll_approx;
    Float_t lephad_coll_approx_m;
    Float_t lephad_coll_approx_x0;
    Float_t lephad_coll_approx_x1;
    Float_t lephad_cosalpha;
    Float_t lephad_deta;
    Float_t lephad_dphi;
    Float_t lephad_dpt;
    Float_t lephad_dr;
    Int_t lephad_met_bisect;
    Float_t lephad_met_centrality;
    Float_t lephad_met_lep0_cos_dphi;
    Float_t lephad_met_lep1_cos_dphi;
    Float_t lephad_met_min_dphi;
    Float_t lephad_met_sum_cos_dphi;
    Float_t lephad_mt_lep0_met;
    Float_t lephad_mt_lep1_met;
    TLorentzVector *lephad_p4;
    Float_t lephad_qxq;
    Float_t lephad_scal_sum_pt;
    UInt_t mc_channel_number;
    TLorentzVector *met_reco_p4;
    Float_t met_reco_sig;
    Float_t met_reco_sig_tracks;
    Float_t met_reco_sumet;
    Float_t n_actual_int;
    Float_t n_actual_int_cor;
    Float_t n_avg_int;
    Float_t n_avg_int_cor;
    Int_t n_bjets_DL1dv00_FixedCutBEff_70;
    Int_t n_bjets_DL1dv00_FixedCutBEff_85;
    Int_t n_electrons;
    Int_t n_jets;
    Int_t n_jets_30;
    Int_t n_jets_40;
    Int_t n_jets_mc_hs;
    Int_t n_muons;
    Int_t n_muons_bad;
    Int_t n_muons_loose;
    Int_t n_muons_medium;
    Int_t n_muons_tight;
    Int_t n_muons_veryloose;
    Int_t n_photons;
    Int_t n_pvx;
    Int_t n_taus;
    Int_t n_taus_rnn_loose;
    Int_t n_taus_rnn_medium;
    Int_t n_taus_rnn_tight;
    Int_t n_taus_rnn_veryloose;
    UInt_t n_truth_gluon_jets;
    UInt_t n_truth_jets;
    UInt_t n_truth_jets_pt20_eta45;
    UInt_t n_truth_quark_jets;
    Int_t n_vx;
    UInt_t run_number;
    UInt_t tau;
    Float_t tau_NOMINAL_TauEffSF_JetRNNloose;
    Float_t tau_NOMINAL_TauEffSF_JetRNNmedium;
    Float_t tau_NOMINAL_TauEffSF_JetRNNtight;
    Float_t tau_NOMINAL_TauEffSF_reco;
    Float_t tau_NOMINAL_TauEffSF_selection;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNtight;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNloose;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNmedium;
    Float_t tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNtight;
    std::vector<float> *tau_charged_tracks_d0;
    std::vector<float> *tau_charged_tracks_d0_sig;
    std::vector<float> *tau_charged_tracks_eProbabilityHT;
    std::vector<int> *tau_charged_tracks_nInnermostPixelHits;
    std::vector<int> *tau_charged_tracks_nPixelHits;
    std::vector<int> *tau_charged_tracks_nSCTHits;
    std::vector<TLorentzVector> *tau_charged_tracks_p4;
    std::vector<float> *tau_charged_tracks_pt_err;
    std::vector<float> *tau_charged_tracks_z0;
    std::vector<float> *tau_charged_tracks_z0_sig;
    std::vector<float> *tau_charged_tracks_z0_sintheta;
    std::vector<float> *tau_charged_tracks_z0_sintheta_tjva;
    UInt_t tau_decay_mode;
    Int_t tau_ele_rnn_loose_retuned;
    Int_t tau_ele_rnn_medium_retuned;
    Float_t tau_ele_rnn_score;
    Float_t tau_ele_rnn_score_trans;
    Int_t tau_ele_rnn_tight_retuned;
    UInt_t tau_jet_rnn_loose;
    UInt_t tau_jet_rnn_medium;
    Float_t tau_jet_rnn_score;
    Float_t tau_jet_rnn_score_trans;
    UInt_t tau_jet_rnn_tight;
    UInt_t tau_jet_rnn_veryloose;
    Float_t tau_jet_width;
    UInt_t tau_matched;
    Int_t tau_matched_classifierParticleOrigin;
    Int_t tau_matched_classifierParticleType;
    Int_t tau_matched_decayVertex;
    TVector3 *tau_matched_decayVertex_v;
    Int_t tau_matched_decay_mode;
    UInt_t tau_matched_isEle;
    UInt_t tau_matched_isHadTau;
    UInt_t tau_matched_isJet;
    UInt_t tau_matched_isMuon;
    UInt_t tau_matched_isTau;
    UInt_t tau_matched_isTruthMatch;
    Int_t tau_matched_mother_pdgId;
    Int_t tau_matched_mother_status;
    Int_t tau_matched_n_charged;
    Int_t tau_matched_n_charged_pion;
    Int_t tau_matched_n_neutral;
    Int_t tau_matched_n_neutral_pion;
    Int_t tau_matched_origin;
    TLorentzVector *tau_matched_p4;
    TLorentzVector *tau_matched_p4_vis_charged_track0;
    TLorentzVector *tau_matched_p4_vis_charged_track1;
    TLorentzVector *tau_matched_p4_vis_charged_track2;
    Int_t tau_matched_pdgId;
    Int_t tau_matched_productionVertex;
    TVector3 *tau_matched_productionVertex_v;
    Float_t tau_matched_pz;
    Float_t tau_matched_q;
    Int_t tau_matched_status;
    Int_t tau_matched_type;
    TLorentzVector *tau_matched_vis_charged_p4;
    TLorentzVector *tau_matched_vis_neutral_others_p4;
    TLorentzVector *tau_matched_vis_neutral_p4;
    TLorentzVector *tau_matched_vis_neutral_pions_p4;
    TLorentzVector *tau_matched_vis_p4;
    UInt_t tau_n_all_tracks;
    UInt_t tau_n_charged_tracks;
    UInt_t tau_n_conversion_tracks;
    UInt_t tau_n_fake_tracks;
    UInt_t tau_n_isolation_tracks;
    UInt_t tau_n_unclassified_tracks;
    Int_t tau_origin;
    TLorentzVector *tau_p4;
    Int_t tau_pass_tst_muonolr;
    Float_t tau_q;
    Double_t tau_tes_alpha_pt_shift;
    Int_t tau_type;
    Float_t theory_weights_CT14_pdfset;
    Float_t theory_weights_MMHT_pdfset;
    Float_t theory_weights_alphaS_down;
    Float_t theory_weights_alphaS_up;
    Double_t weight_mc;
    Double_t Weight_mc;
    Int_t mergedRunNumber;
    Double_t upsilon;
    Bool_t isQCR;
    Bool_t isWCR;
    Bool_t isWCR_QCD;
    Bool_t isIQCR_Iso;
    Bool_t isIQCR_NotIso;
    Bool_t isTCR;
    Bool_t isSR;
    Bool_t isData;

    // List of branches
    TBranch *b_AMI_cross_section;                                                                          //!
    TBranch *b_HLT_e140_lhloose_nod0;                                                                      //!
    TBranch *b_HLT_e26_lhtight_nod0_ivarloose;                                                             //!
    TBranch *b_HLT_e60_lhmedium_nod0;                                                                      //!
    TBranch *b_HLT_mu26_ivarmedium;                                                                        //!
    TBranch *b_HLT_mu50;                                                                                   //!
    TBranch *b_NOMINAL_pileup_combined_weight;                                                             //!
    TBranch *b_NOMINAL_pileup_random_run_number;                                                           //!
    TBranch *b_PRW_DATASF_1down_pileup_combined_weight;                                                    //!
    TBranch *b_PRW_DATASF_1down_pileup_random_run_number;                                                  //!
    TBranch *b_PRW_DATASF_1up_pileup_combined_weight;                                                      //!
    TBranch *b_PRW_DATASF_1up_pileup_random_run_number;                                                    //!
    TBranch *b_beamSpotWeight;                                                                             //!
    TBranch *b_cross_section;                                                                              //!
    TBranch *b_event_clean_EC_LooseBad;                                                                    //!
    TBranch *b_event_clean_EC_TightBad;                                                                    //!
    TBranch *b_event_clean_detector_core;                                                                  //!
    TBranch *b_event_is_bad_batman;                                                                        //!
    TBranch *b_event_number;                                                                               //!
    TBranch *b_filter_efficiency;                                                                          //!
    TBranch *b_jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1dv00_FixedCutBEff_70;                            //!
    TBranch *b_jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1dv00_FixedCutBEff_85;                            //!
    TBranch *b_jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;                          //!
    TBranch *b_jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;                          //!
    TBranch *b_jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1dv00_FixedCutBEff_70;                              //!
    TBranch *b_jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1dv00_FixedCutBEff_85;                              //!
    TBranch *b_jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;                            //!
    TBranch *b_jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;                            //!
    TBranch *b_jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1dv00_FixedCutBEff_70;                            //!
    TBranch *b_jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1dv00_FixedCutBEff_85;                            //!
    TBranch *b_jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;                          //!
    TBranch *b_jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;                          //!
    TBranch *b_jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1dv00_FixedCutBEff_70;                              //!
    TBranch *b_jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1dv00_FixedCutBEff_85;                              //!
    TBranch *b_jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;                            //!
    TBranch *b_jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;                            //!
    TBranch *b_jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1dv00_FixedCutBEff_70;                            //!
    TBranch *b_jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1dv00_FixedCutBEff_85;                            //!
    TBranch *b_jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;                          //!
    TBranch *b_jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;                          //!
    TBranch *b_jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1dv00_FixedCutBEff_70;                              //!
    TBranch *b_jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1dv00_FixedCutBEff_85;                              //!
    TBranch *b_jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;                            //!
    TBranch *b_jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;                            //!
    TBranch *b_jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1dv00_FixedCutBEff_70;                            //!
    TBranch *b_jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1dv00_FixedCutBEff_85;                            //!
    TBranch *b_jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;                          //!
    TBranch *b_jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;                          //!
    TBranch *b_jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1dv00_FixedCutBEff_70;                              //!
    TBranch *b_jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1dv00_FixedCutBEff_85;                              //!
    TBranch *b_jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;                            //!
    TBranch *b_jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;                            //!
    TBranch *b_jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1dv00_FixedCutBEff_70;                        //!
    TBranch *b_jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1dv00_FixedCutBEff_85;                        //!
    TBranch *b_jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;                      //!
    TBranch *b_jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;                      //!
    TBranch *b_jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1dv00_FixedCutBEff_70;                          //!
    TBranch *b_jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1dv00_FixedCutBEff_85;                          //!
    TBranch *b_jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;                        //!
    TBranch *b_jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;                        //!
    TBranch *b_jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1dv00_FixedCutBEff_70;                        //!
    TBranch *b_jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1dv00_FixedCutBEff_85;                        //!
    TBranch *b_jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;                      //!
    TBranch *b_jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;                      //!
    TBranch *b_jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1dv00_FixedCutBEff_70;                          //!
    TBranch *b_jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1dv00_FixedCutBEff_85;                          //!
    TBranch *b_jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;                        //!
    TBranch *b_jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;                        //!
    TBranch *b_jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1dv00_FixedCutBEff_70;             //!
    TBranch *b_jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1dv00_FixedCutBEff_85;             //!
    TBranch *b_jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1dv00_FixedCutBEff_70;           //!
    TBranch *b_jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1dv00_FixedCutBEff_85;           //!
    TBranch *b_jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1dv00_FixedCutBEff_70;               //!
    TBranch *b_jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1dv00_FixedCutBEff_85;               //!
    TBranch *b_jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1dv00_FixedCutBEff_70;             //!
    TBranch *b_jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1dv00_FixedCutBEff_85;             //!
    TBranch *b_jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT;                                  //!
    TBranch *b_jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT;                                //!
    TBranch *b_jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT;                                    //!
    TBranch *b_jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT;                                  //!
    TBranch *b_jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT;                                 //!
    TBranch *b_jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT;                               //!
    TBranch *b_jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT;                                   //!
    TBranch *b_jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT;                                 //!
    TBranch *b_jet_NOMINAL_central_jets_global_effSF_JVT;                                                  //!
    TBranch *b_jet_NOMINAL_central_jets_global_ineffSF_JVT;                                                //!
    TBranch *b_jet_NOMINAL_forward_jets_global_effSF_JVT;                                                  //!
    TBranch *b_jet_NOMINAL_forward_jets_global_ineffSF_JVT;                                                //!
    TBranch *b_jet_NOMINAL_global_effSF_DL1dv00_FixedCutBEff_70;                                           //!
    TBranch *b_jet_NOMINAL_global_effSF_DL1dv00_FixedCutBEff_85;                                           //!
    TBranch *b_jet_NOMINAL_global_ineffSF_DL1dv00_FixedCutBEff_70;                                         //!
    TBranch *b_jet_NOMINAL_global_ineffSF_DL1dv00_FixedCutBEff_85;                                         //!
    TBranch *b_jets;                                                                                       //!
    TBranch *b_jets_Bottom_DL1_score;                                                                      //!
    TBranch *b_jets_Charm_DL1_score;                                                                       //!
    TBranch *b_jets_DL1dv00_FixedCutBEff_70_weight;                                                        //!
    TBranch *b_jets_DL1dv00_FixedCutBEff_85_weight;                                                        //!
    TBranch *b_jets_Light_DL1_score;                                                                       //!
    TBranch *b_jets_b_tag_quantile;                                                                        //!
    TBranch *b_jets_b_tag_score;                                                                           //!
    TBranch *b_jets_b_tagged_DL1dv00;                                                                      //!
    TBranch *b_jets_fjvt;                                                                                  //!
    TBranch *b_jets_flavorlabel;                                                                           //!
    TBranch *b_jets_flavorlabel_cone;                                                                      //!
    TBranch *b_jets_flavorlabel_part;                                                                      //!
    TBranch *b_jets_is_Jvt_HS;                                                                             //!
    TBranch *b_jets_jvt;                                                                                   //!
    TBranch *b_jets_origin;                                                                                //!
    TBranch *b_jets_p4;                                                                                    //!
    TBranch *b_jets_q;                                                                                     //!
    TBranch *b_jets_type;                                                                                  //!
    TBranch *b_jets_width;                                                                                 //!
    TBranch *b_kfactor;                                                                                    //!
    TBranch *b_lep;                                                                                        //!
    TBranch *b_lep_EL_CHARGEID_STAT_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose;                    //!
    TBranch *b_lep_EL_CHARGEID_STAT_1up_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose;                      //!
    TBranch *b_lep_EL_CHARGEID_SYStotal_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose;                //!
    TBranch *b_lep_EL_CHARGEID_SYStotal_1up_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose;                  //!
    TBranch *b_lep_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_MediumLLH_d0z0_v13;            //!
    TBranch *b_lep_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_MediumLLH_d0z0_v13;              //!
    TBranch *b_lep_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose; //!
    TBranch *b_lep_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose;   //!
    TBranch *b_lep_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_RecoTrk;                     //!
    TBranch *b_lep_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_RecoTrk;                       //!
    TBranch *
        b_lep_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose; //!
    TBranch *
        b_lep_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose; //!
    TBranch *b_lep_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_FixedRad;                               //!
    TBranch *b_lep_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_FixedRad;                                 //!
    TBranch *b_lep_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_FixedRad;                                //!
    TBranch *b_lep_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_FixedRad;                                  //!
    TBranch *b_lep_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium;                                         //!
    TBranch *b_lep_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium;                                           //!
    TBranch *b_lep_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium;                                   //!
    TBranch *b_lep_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium;                                     //!
    TBranch *b_lep_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium;                                          //!
    TBranch *b_lep_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium;                                            //!
    TBranch *b_lep_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium;                                    //!
    TBranch *b_lep_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium;                                      //!
    TBranch *b_lep_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA;                                                    //!
    TBranch *b_lep_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA;                                                      //!
    TBranch *b_lep_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA;                                                     //!
    TBranch *b_lep_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA;                                                       //!
    TBranch *b_lep_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium; //!
    TBranch *b_lep_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;    //!
    TBranch *b_lep_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;   //!
    TBranch *b_lep_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;      //!
    TBranch *b_lep_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium; //!
    TBranch *b_lep_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;    //!
    TBranch *b_lep_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;   //!
    TBranch *b_lep_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;      //!
    TBranch *b_lep_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose;                                     //!
    TBranch *b_lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose;                                    //!
    TBranch *
        b_lep_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose; //!
    TBranch *b_lep_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v13;                                //!
    TBranch *b_lep_NOMINAL_EleEffSF_offline_RecoTrk;                                           //!
    TBranch *b_lep_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;              //!
    TBranch *b_lep_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;                 //!
    TBranch *b_lep_NOMINAL_MuEffSF_IsoTightTrackOnly_FixedRad;                                 //!
    TBranch *b_lep_NOMINAL_MuEffSF_Reco_QualMedium;                                            //!
    TBranch *b_lep_NOMINAL_MuEffSF_TTVA;                                                       //!
    TBranch *b_lep_cluster_eta;                                                                //!
    TBranch *b_lep_cluster_eta_be2;                                                            //!
    TBranch *b_lep_elec_trigmatch_HLT_e140_lhloose_nod0;                                       //!
    TBranch *b_lep_elec_trigmatch_HLT_e26_lhtight_nod0_ivarloose;                              //!
    TBranch *b_lep_elec_trigmatch_HLT_e60_lhmedium_nod0;                                       //!
    TBranch *b_lep_elec_trigmatch_trigger_matched;                                             //!
    TBranch *b_lep_id_bad;                                                                     //!
    TBranch *b_lep_id_charge;                                                                  //!
    TBranch *b_lep_id_loose;                                                                   //!
    TBranch *b_lep_id_medium;                                                                  //!
    TBranch *b_lep_id_tight;                                                                   //!
    TBranch *b_lep_id_veryloose;                                                               //!
    TBranch *b_lep_iso_FCHighPtCaloOnly;                                                       //!
    TBranch *b_lep_iso_FCLoose;                                                                //!
    TBranch *b_lep_iso_FCLoose_FixedRad;                                                       //!
    TBranch *b_lep_iso_FCTight;                                                                //!
    TBranch *b_lep_iso_FCTightTrackOnly;                                                       //!
    TBranch *b_lep_iso_FCTightTrackOnly_FixedRad;                                              //!
    TBranch *b_lep_iso_FCTight_FixedRad;                                                       //!
    TBranch *b_lep_iso_FixedCutLoose;                                                          //!
    TBranch *b_lep_iso_FixedCutTight;                                                          //!
    TBranch *b_lep_iso_FixedCutTightCaloOnly;                                                  //!
    TBranch *b_lep_iso_HighPtCaloOnly;                                                         //!
    TBranch *b_lep_iso_HighPtTrackOnly;                                                        //!
    TBranch *b_lep_iso_Loose;                                                                  //!
    TBranch *b_lep_iso_Loose_FixedRad;                                                         //!
    TBranch *b_lep_iso_Loose_VarRad;                                                           //!
    TBranch *b_lep_iso_Tight;                                                                  //!
    TBranch *b_lep_iso_TightTrackOnly;                                                         //!
    TBranch *b_lep_iso_TightTrackOnly_FixedRad;                                                //!
    TBranch *b_lep_iso_TightTrackOnly_VarRad;                                                  //!
    TBranch *b_lep_iso_Tight_FixedRad;                                                         //!
    TBranch *b_lep_iso_Tight_VarRad;                                                           //!
    TBranch *b_lep_matched;                                                                    //!
    TBranch *b_lep_matched_classifierParticleOrigin;                                           //!
    TBranch *b_lep_matched_classifierParticleType;                                             //!
    TBranch *b_lep_matched_isHad;                                                              //!
    TBranch *b_lep_matched_mother_pdgId;                                                       //!
    TBranch *b_lep_matched_mother_status;                                                      //!
    TBranch *b_lep_matched_origin;                                                             //!
    TBranch *b_lep_matched_p4;                                                                 //!
    TBranch *b_lep_matched_pdgId;                                                              //!
    TBranch *b_lep_matched_q;                                                                  //!
    TBranch *b_lep_matched_status;                                                             //!
    TBranch *b_lep_matched_type;                                                               //!
    TBranch *b_lep_muonAuthor;                                                                 //!
    TBranch *b_lep_muonType;                                                                   //!
    TBranch *b_lep_muon_trigmatch_HLT_mu26_ivarmedium;                                         //!
    TBranch *b_lep_muon_trigmatch_HLT_mu50;                                                    //!
    TBranch *b_lep_muon_trigmatch_trigger_matched;                                             //!
    TBranch *b_lep_origin;                                                                     //!
    TBranch *b_lep_p4;                                                                         //!
    TBranch *b_lep_q;                                                                          //!
    TBranch *b_lep_trk_d0;                                                                     //!
    TBranch *b_lep_trk_d0_sig;                                                                 //!
    TBranch *b_lep_trk_p4;                                                                     //!
    TBranch *b_lep_trk_pt_error;                                                               //!
    TBranch *b_lep_trk_pvx_z0;                                                                 //!
    TBranch *b_lep_trk_pvx_z0_sig;                                                             //!
    TBranch *b_lep_trk_pvx_z0_sintheta;                                                        //!
    TBranch *b_lep_trk_vx;                                                                     //!
    TBranch *b_lep_trk_vx_v;                                                                   //!
    TBranch *b_lep_trk_z0;                                                                     //!
    TBranch *b_lep_trk_z0_sig;                                                                 //!
    TBranch *b_lep_trk_z0_sintheta;                                                            //!
    TBranch *b_lep_type;                                                                       //!
    TBranch *b_lephad;                                                                         //!
    TBranch *b_lephad_coll_approx;                                                             //!
    TBranch *b_lephad_coll_approx_m;                                                           //!
    TBranch *b_lephad_coll_approx_x0;                                                          //!
    TBranch *b_lephad_coll_approx_x1;                                                          //!
    TBranch *b_lephad_cosalpha;                                                                //!
    TBranch *b_lephad_deta;                                                                    //!
    TBranch *b_lephad_dphi;                                                                    //!
    TBranch *b_lephad_dpt;                                                                     //!
    TBranch *b_lephad_dr;                                                                      //!
    TBranch *b_lephad_met_bisect;                                                              //!
    TBranch *b_lephad_met_centrality;                                                          //!
    TBranch *b_lephad_met_lep0_cos_dphi;                                                       //!
    TBranch *b_lephad_met_lep1_cos_dphi;                                                       //!
    TBranch *b_lephad_met_min_dphi;                                                            //!
    TBranch *b_lephad_met_sum_cos_dphi;                                                        //!
    TBranch *b_lephad_mt_lep0_met;                                                             //!
    TBranch *b_lephad_mt_lep1_met;                                                             //!
    TBranch *b_lephad_p4;                                                                      //!
    TBranch *b_lephad_qxq;                                                                     //!
    TBranch *b_lephad_scal_sum_pt;                                                             //!
    TBranch *b_mc_channel_number;                                                              //!
    TBranch *b_met_reco_p4;                                                                    //!
    TBranch *b_met_reco_sig;                                                                   //!
    TBranch *b_met_reco_sig_tracks;                                                            //!
    TBranch *b_met_reco_sumet;                                                                 //!
    TBranch *b_n_actual_int;                                                                   //!
    TBranch *b_n_actual_int_cor;                                                               //!
    TBranch *b_n_avg_int;                                                                      //!
    TBranch *b_n_avg_int_cor;                                                                  //!
    TBranch *b_n_bjets_DL1dv00_FixedCutBEff_70;                                                //!
    TBranch *b_n_bjets_DL1dv00_FixedCutBEff_85;                                                //!
    TBranch *b_n_electrons;                                                                    //!
    TBranch *b_n_jets;                                                                         //!
    TBranch *b_n_jets_30;                                                                      //!
    TBranch *b_n_jets_40;                                                                      //!
    TBranch *b_n_jets_mc_hs;                                                                   //!
    TBranch *b_n_muons;                                                                        //!
    TBranch *b_n_muons_bad;                                                                    //!
    TBranch *b_n_muons_loose;                                                                  //!
    TBranch *b_n_muons_medium;                                                                 //!
    TBranch *b_n_muons_tight;                                                                  //!
    TBranch *b_n_muons_veryloose;                                                              //!
    TBranch *b_n_photons;                                                                      //!
    TBranch *b_n_pvx;                                                                          //!
    TBranch *b_n_taus;                                                                         //!
    TBranch *b_n_taus_rnn_loose;                                                               //!
    TBranch *b_n_taus_rnn_medium;                                                              //!
    TBranch *b_n_taus_rnn_tight;                                                               //!
    TBranch *b_n_taus_rnn_veryloose;                                                           //!
    TBranch *b_n_truth_gluon_jets;                                                             //!
    TBranch *b_n_truth_jets;                                                                   //!
    TBranch *b_n_truth_jets_pt20_eta45;                                                        //!
    TBranch *b_n_truth_quark_jets;                                                             //!
    TBranch *b_n_vx;                                                                           //!
    TBranch *b_run_number;                                                                     //!
    TBranch *b_tau;                                                                            //!
    TBranch *b_tau_NOMINAL_TauEffSF_JetRNNloose;                                               //!
    TBranch *b_tau_NOMINAL_TauEffSF_JetRNNmedium;                                              //!
    TBranch *b_tau_NOMINAL_TauEffSF_JetRNNtight;                                               //!
    TBranch *b_tau_NOMINAL_TauEffSF_reco;                                                      //!
    TBranch *b_tau_NOMINAL_TauEffSF_selection;                                                 //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco;                         //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection;                    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco;                           //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection;                      //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium; //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium;   //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium; //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium;   //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium; //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium;   //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium; //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium;   //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium; //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium;   //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium; //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium;   //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium; //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium;   //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium; //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight;  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium;   //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight;    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNloose;                //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNmedium;               //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNtight;                //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNloose;                  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNmedium;                 //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNtight;                  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNloose;                  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNmedium;                 //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNtight;                  //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNloose;                    //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNmedium;                   //!
    TBranch *b_tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNtight;                    //!
    TBranch *b_tau_charged_tracks_d0;                                                          //!
    TBranch *b_tau_charged_tracks_d0_sig;                                                      //!
    TBranch *b_tau_charged_tracks_eProbabilityHT;                                              //!
    TBranch *b_tau_charged_tracks_nInnermostPixelHits;                                         //!
    TBranch *b_tau_charged_tracks_nPixelHits;                                                  //!
    TBranch *b_tau_charged_tracks_nSCTHits;                                                    //!
    TBranch *b_tau_charged_tracks_p4;                                                          //!
    TBranch *b_tau_charged_tracks_pt_err;                                                      //!
    TBranch *b_tau_charged_tracks_z0;                                                          //!
    TBranch *b_tau_charged_tracks_z0_sig;                                                      //!
    TBranch *b_tau_charged_tracks_z0_sintheta;                                                 //!
    TBranch *b_tau_charged_tracks_z0_sintheta_tjva;                                            //!
    TBranch *b_tau_decay_mode;                                                                 //!
    TBranch *b_tau_ele_rnn_loose_retuned;                                                      //!
    TBranch *b_tau_ele_rnn_medium_retuned;                                                     //!
    TBranch *b_tau_ele_rnn_score;                                                              //!
    TBranch *b_tau_ele_rnn_score_trans;                                                        //!
    TBranch *b_tau_ele_rnn_tight_retuned;                                                      //!
    TBranch *b_tau_jet_rnn_loose;                                                              //!
    TBranch *b_tau_jet_rnn_medium;                                                             //!
    TBranch *b_tau_jet_rnn_score;                                                              //!
    TBranch *b_tau_jet_rnn_score_trans;                                                        //!
    TBranch *b_tau_jet_rnn_tight;                                                              //!
    TBranch *b_tau_jet_rnn_veryloose;                                                          //!
    TBranch *b_tau_jet_width;                                                                  //!
    TBranch *b_tau_matched;                                                                    //!
    TBranch *b_tau_matched_classifierParticleOrigin;                                           //!
    TBranch *b_tau_matched_classifierParticleType;                                             //!
    TBranch *b_tau_matched_decayVertex;                                                        //!
    TBranch *b_tau_matched_decayVertex_v;                                                      //!
    TBranch *b_tau_matched_decay_mode;                                                         //!
    TBranch *b_tau_matched_isEle;                                                              //!
    TBranch *b_tau_matched_isHadTau;                                                           //!
    TBranch *b_tau_matched_isJet;                                                              //!
    TBranch *b_tau_matched_isMuon;                                                             //!
    TBranch *b_tau_matched_isTau;                                                              //!
    TBranch *b_tau_matched_isTruthMatch;                                                       //!
    TBranch *b_tau_matched_mother_pdgId;                                                       //!
    TBranch *b_tau_matched_mother_status;                                                      //!
    TBranch *b_tau_matched_n_charged;                                                          //!
    TBranch *b_tau_matched_n_charged_pion;                                                     //!
    TBranch *b_tau_matched_n_neutral;                                                          //!
    TBranch *b_tau_matched_n_neutral_pion;                                                     //!
    TBranch *b_tau_matched_origin;                                                             //!
    TBranch *b_tau_matched_p4;                                                                 //!
    TBranch *b_tau_matched_p4_vis_charged_track0;                                              //!
    TBranch *b_tau_matched_p4_vis_charged_track1;                                              //!
    TBranch *b_tau_matched_p4_vis_charged_track2;                                              //!
    TBranch *b_tau_matched_pdgId;                                                              //!
    TBranch *b_tau_matched_productionVertex;                                                   //!
    TBranch *b_tau_matched_productionVertex_v;                                                 //!
    TBranch *b_tau_matched_pz;                                                                 //!
    TBranch *b_tau_matched_q;                                                                  //!
    TBranch *b_tau_matched_status;                                                             //!
    TBranch *b_tau_matched_type;                                                               //!
    TBranch *b_tau_matched_vis_charged_p4;                                                     //!
    TBranch *b_tau_matched_vis_neutral_others_p4;                                              //!
    TBranch *b_tau_matched_vis_neutral_p4;                                                     //!
    TBranch *b_tau_matched_vis_neutral_pions_p4;                                               //!
    TBranch *b_tau_matched_vis_p4;                                                             //!
    TBranch *b_tau_n_all_tracks;                                                               //!
    TBranch *b_tau_n_charged_tracks;                                                           //!
    TBranch *b_tau_n_conversion_tracks;                                                        //!
    TBranch *b_tau_n_fake_tracks;                                                              //!
    TBranch *b_tau_n_isolation_tracks;                                                         //!
    TBranch *b_tau_n_unclassified_tracks;                                                      //!
    TBranch *b_tau_origin;                                                                     //!
    TBranch *b_tau_p4;                                                                         //!
    TBranch *b_tau_pass_tst_muonolr;                                                           //!
    TBranch *b_tau_q;                                                                          //!
    TBranch *b_tau_tes_alpha_pt_shift;                                                         //!
    TBranch *b_tau_type;                                                                       //!
    TBranch *b_theory_weights_CT14_pdfset;                                                     //!
    TBranch *b_theory_weights_MMHT_pdfset;                                                     //!
    TBranch *b_theory_weights_alphaS_down;                                                     //!
    TBranch *b_theory_weights_alphaS_up;                                                       //!
    TBranch *b_weight_mc;                                                                      //!
    TBranch *b_Weight_mc;                                                                      //!
    TBranch *b_mergedRunNumber;                                                                //!
    TBranch *b_upsilon;                                                                        //!
    TBranch *b_isQCR;                                                                          //!
    TBranch *b_isWCR;                                                                          //!
    TBranch *b_isWCR_QCD;                                                                      //!
    TBranch *b_isIQCR_Iso;                                                                     //!
    TBranch *b_isIQCR_NotIso;                                                                  //!
    TBranch *b_isTCR;                                                                          //!
    TBranch *b_isSR;                                                                           //!
    TBranch *b_isData;                                                                         //!

    void Init(TChain *tree) override {
        std::lock_guard<std::mutex> lock(_mutex);
        // The Init() function is called when the selector needs to initialize
        // a new tree or chain. Typically here the branch addresses and branch
        // pointers of the tree will be set.
        // It is normally not necessary to make changes to the generated
        // code, but the routine can be extended by the user if needed.
        // Init() will be called many times when running on PROOF
        // (once per file to be processed).

        // Set object pointer
        // jet_0_matched_p4 = 0;
        // jet_0_p4 = 0;
        // lep_0_matched_leptonic_tau_decay_invis_p4 = 0;
        // lep_0_matched_leptonic_tau_decay_p4 = 0;
        // lep_0_matched_leptonic_tau_decay_vis_p4 = 0;
        // lep_0_matched_p4 = 0;
        // lep_0_p4 = 0;
        // lep_0_trk_p4 = 0;
        // lep_0_trk_vx_v = 0;
        // lephad_p4 = 0;
        // met_reco_p4 = 0;
        // met_truth_p4 = 0;
        // tau_0_charged_tracks_d0 = 0;
        // tau_0_charged_tracks_d0_sig = 0;
        // tau_0_charged_tracks_eProbabilityHT = 0;
        // tau_0_charged_tracks_nInnermostPixelHits = 0;
        // tau_0_charged_tracks_nPixelHits = 0;
        // tau_0_charged_tracks_nSCTHits = 0;
        // tau_0_charged_tracks_p4 = 0;
        // tau_0_charged_tracks_pt_err = 0;
        // tau_0_charged_tracks_z0 = 0;
        // tau_0_charged_tracks_z0_sig = 0;
        // tau_0_charged_tracks_z0_sintheta = 0;
        // tau_0_charged_tracks_z0_sintheta_tjva = 0;
        // tau_0_conversion_tracks_p4 = 0;
        // tau_0_matched_decayVertex_v = 0;
        // tau_0_matched_p4 = 0;
        // tau_0_matched_p4_vis_charged_track0 = 0;
        // tau_0_matched_p4_vis_charged_track1 = 0;
        // tau_0_matched_p4_vis_charged_track2 = 0;
        // tau_0_matched_productionVertex_v = 0;
        // tau_0_matched_vis_charged_p4 = 0;
        // tau_0_matched_vis_neutral_others_p4 = 0;
        // tau_0_matched_vis_neutral_p4 = 0;
        // tau_0_matched_vis_neutral_pions_p4 = 0;
        // tau_0_matched_vis_p4 = 0;
        // tau_0_p4 = 0;
        // tau_0_primary_vertex_v = 0;
        // tau_0_secondary_vertex_v = 0;
        jets = 0;
        jets_Bottom_DL1_score = 0;
        jets_Charm_DL1_score = 0;
        jets_DL1dv00_FixedCutBEff_70_weight = 0;
        jets_DL1dv00_FixedCutBEff_85_weight = 0;
        jets_Light_DL1_score = 0;
        jets_b_tag_quantile = 0;
        jets_b_tag_score = 0;
        jets_b_tagged_DL1dv00 = 0;
        jets_fjvt = 0;
        jets_flavorlabel = 0;
        jets_flavorlabel_cone = 0;
        jets_flavorlabel_part = 0;
        jets_is_Jvt_HS = 0;
        jets_jvt = 0;
        jets_origin = 0;
        jets_p4 = 0;
        jets_q = 0;
        jets_type = 0;
        jets_width = 0;
        lep_matched_p4 = 0;
        lep_p4 = 0;
        lep_trk_p4 = 0;
        lep_trk_vx_v = 0;
        lephad_p4 = 0;
        met_reco_p4 = 0;
        tau_charged_tracks_d0 = 0;
        tau_charged_tracks_d0_sig = 0;
        tau_charged_tracks_eProbabilityHT = 0;
        tau_charged_tracks_nInnermostPixelHits = 0;
        tau_charged_tracks_nPixelHits = 0;
        tau_charged_tracks_nSCTHits = 0;
        tau_charged_tracks_p4 = 0;
        tau_charged_tracks_pt_err = 0;
        tau_charged_tracks_z0 = 0;
        tau_charged_tracks_z0_sig = 0;
        tau_charged_tracks_z0_sintheta = 0;
        tau_charged_tracks_z0_sintheta_tjva = 0;
        tau_matched_decayVertex_v = 0;
        tau_matched_p4 = 0;
        tau_matched_p4_vis_charged_track0 = 0;
        tau_matched_p4_vis_charged_track1 = 0;
        tau_matched_p4_vis_charged_track2 = 0;
        tau_matched_productionVertex_v = 0;
        tau_matched_vis_charged_p4 = 0;
        tau_matched_vis_neutral_others_p4 = 0;
        tau_matched_vis_neutral_p4 = 0;
        tau_matched_vis_neutral_pions_p4 = 0;
        tau_matched_vis_p4 = 0;
        tau_p4 = 0;
        isQCR = false;
        isWCR = false;
        isWCR_QCD = false;
        isIQCR_Iso = false;
        isIQCR_NotIso = false;
        isTCR = false;
        isSR = false;
        isData = false;

        // Set branch addresses and branch pointers
        if (!tree) return;
        fChain = tree;
        fChain->SetMakeClass(1);

        fChain->SetBranchAddress("AMI_cross_section", &AMI_cross_section, &b_AMI_cross_section);
        fChain->SetBranchAddress("HLT_e140_lhloose_nod0", &HLT_e140_lhloose_nod0, &b_HLT_e140_lhloose_nod0);
        fChain->SetBranchAddress("HLT_e26_lhtight_nod0_ivarloose", &HLT_e26_lhtight_nod0_ivarloose, &b_HLT_e26_lhtight_nod0_ivarloose);
        fChain->SetBranchAddress("HLT_e60_lhmedium_nod0", &HLT_e60_lhmedium_nod0, &b_HLT_e60_lhmedium_nod0);
        fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
        fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
        fChain->SetBranchAddress("NOMINAL_pileup_combined_weight", &NOMINAL_pileup_combined_weight, &b_NOMINAL_pileup_combined_weight);
        fChain->SetBranchAddress("NOMINAL_pileup_random_run_number", &NOMINAL_pileup_random_run_number, &b_NOMINAL_pileup_random_run_number);
        fChain->SetBranchAddress("PRW_DATASF_1down_pileup_combined_weight", &PRW_DATASF_1down_pileup_combined_weight,
                                 &b_PRW_DATASF_1down_pileup_combined_weight);
        fChain->SetBranchAddress("PRW_DATASF_1down_pileup_random_run_number", &PRW_DATASF_1down_pileup_random_run_number,
                                 &b_PRW_DATASF_1down_pileup_random_run_number);
        fChain->SetBranchAddress("PRW_DATASF_1up_pileup_combined_weight", &PRW_DATASF_1up_pileup_combined_weight,
                                 &b_PRW_DATASF_1up_pileup_combined_weight);
        fChain->SetBranchAddress("PRW_DATASF_1up_pileup_random_run_number", &PRW_DATASF_1up_pileup_random_run_number,
                                 &b_PRW_DATASF_1up_pileup_random_run_number);
        fChain->SetBranchAddress("beamSpotWeight", &beamSpotWeight, &b_beamSpotWeight);
        fChain->SetBranchAddress("cross_section", &cross_section, &b_cross_section);
        fChain->SetBranchAddress("event_clean_EC_LooseBad", &event_clean_EC_LooseBad, &b_event_clean_EC_LooseBad);
        fChain->SetBranchAddress("event_clean_EC_TightBad", &event_clean_EC_TightBad, &b_event_clean_EC_TightBad);
        fChain->SetBranchAddress("event_clean_detector_core", &event_clean_detector_core, &b_event_clean_detector_core);
        fChain->SetBranchAddress("event_is_bad_batman", &event_is_bad_batman, &b_event_is_bad_batman);
        fChain->SetBranchAddress("event_number", &event_number, &b_event_number);
        fChain->SetBranchAddress("filter_efficiency", &filter_efficiency, &b_filter_efficiency);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1dv00_FixedCutBEff_70",
                                 &jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1dv00_FixedCutBEff_85",
                                 &jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT",
                                 &jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT,
                                 &b_jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT",
                                 &jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT,
                                 &b_jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT", &jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT,
                                 &b_jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT",
                                 &jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT,
                                 &b_jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT",
                                 &jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT,
                                 &b_jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT",
                                 &jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT,
                                 &b_jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT",
                                 &jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT,
                                 &b_jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT",
                                 &jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT,
                                 &b_jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_central_jets_global_effSF_JVT", &jet_NOMINAL_central_jets_global_effSF_JVT,
                                 &b_jet_NOMINAL_central_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_central_jets_global_ineffSF_JVT", &jet_NOMINAL_central_jets_global_ineffSF_JVT,
                                 &b_jet_NOMINAL_central_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_forward_jets_global_effSF_JVT", &jet_NOMINAL_forward_jets_global_effSF_JVT,
                                 &b_jet_NOMINAL_forward_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_forward_jets_global_ineffSF_JVT", &jet_NOMINAL_forward_jets_global_ineffSF_JVT,
                                 &b_jet_NOMINAL_forward_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_global_effSF_DL1dv00_FixedCutBEff_70", &jet_NOMINAL_global_effSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_NOMINAL_global_effSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_NOMINAL_global_effSF_DL1dv00_FixedCutBEff_85", &jet_NOMINAL_global_effSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_NOMINAL_global_effSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jet_NOMINAL_global_ineffSF_DL1dv00_FixedCutBEff_70", &jet_NOMINAL_global_ineffSF_DL1dv00_FixedCutBEff_70,
                                 &b_jet_NOMINAL_global_ineffSF_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_NOMINAL_global_ineffSF_DL1dv00_FixedCutBEff_85", &jet_NOMINAL_global_ineffSF_DL1dv00_FixedCutBEff_85,
                                 &b_jet_NOMINAL_global_ineffSF_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("jets", &jets, &b_jets);
        fChain->SetBranchAddress("jets_Bottom_DL1_score", &jets_Bottom_DL1_score, &b_jets_Bottom_DL1_score);
        fChain->SetBranchAddress("jets_Charm_DL1_score", &jets_Charm_DL1_score, &b_jets_Charm_DL1_score);
        fChain->SetBranchAddress("jets_DL1dv00_FixedCutBEff_70_weight", &jets_DL1dv00_FixedCutBEff_70_weight, &b_jets_DL1dv00_FixedCutBEff_70_weight);
        fChain->SetBranchAddress("jets_DL1dv00_FixedCutBEff_85_weight", &jets_DL1dv00_FixedCutBEff_85_weight, &b_jets_DL1dv00_FixedCutBEff_85_weight);
        fChain->SetBranchAddress("jets_Light_DL1_score", &jets_Light_DL1_score, &b_jets_Light_DL1_score);
        fChain->SetBranchAddress("jets_b_tag_quantile", &jets_b_tag_quantile, &b_jets_b_tag_quantile);
        fChain->SetBranchAddress("jets_b_tag_score", &jets_b_tag_score, &b_jets_b_tag_score);
        fChain->SetBranchAddress("jets_b_tagged_DL1dv00", &jets_b_tagged_DL1dv00, &b_jets_b_tagged_DL1dv00);
        fChain->SetBranchAddress("jets_fjvt", &jets_fjvt, &b_jets_fjvt);
        fChain->SetBranchAddress("jets_flavorlabel", &jets_flavorlabel, &b_jets_flavorlabel);
        fChain->SetBranchAddress("jets_flavorlabel_cone", &jets_flavorlabel_cone, &b_jets_flavorlabel_cone);
        fChain->SetBranchAddress("jets_flavorlabel_part", &jets_flavorlabel_part, &b_jets_flavorlabel_part);
        fChain->SetBranchAddress("jets_is_Jvt_HS", &jets_is_Jvt_HS, &b_jets_is_Jvt_HS);
        fChain->SetBranchAddress("jets_jvt", &jets_jvt, &b_jets_jvt);
        fChain->SetBranchAddress("jets_origin", &jets_origin, &b_jets_origin);
        fChain->SetBranchAddress("jets_p4", &jets_p4, &b_jets_p4);
        fChain->SetBranchAddress("jets_q", &jets_q, &b_jets_q);
        fChain->SetBranchAddress("jets_type", &jets_type, &b_jets_type);
        fChain->SetBranchAddress("jets_width", &jets_width, &b_jets_width);
        fChain->SetBranchAddress("kfactor", &kfactor, &b_kfactor);
        fChain->SetBranchAddress("lep", &lep, &b_lep);
        fChain->SetBranchAddress("lep_EL_CHARGEID_STAT_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose",
                                 &lep_EL_CHARGEID_STAT_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose,
                                 &b_lep_EL_CHARGEID_STAT_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose);
        fChain->SetBranchAddress("lep_EL_CHARGEID_STAT_1up_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose",
                                 &lep_EL_CHARGEID_STAT_1up_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose,
                                 &b_lep_EL_CHARGEID_STAT_1up_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose);
        fChain->SetBranchAddress("lep_EL_CHARGEID_SYStotal_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose",
                                 &lep_EL_CHARGEID_SYStotal_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose,
                                 &b_lep_EL_CHARGEID_SYStotal_1down_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose);
        fChain->SetBranchAddress("lep_EL_CHARGEID_SYStotal_1up_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose",
                                 &lep_EL_CHARGEID_SYStotal_1up_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose,
                                 &b_lep_EL_CHARGEID_SYStotal_1up_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose);
        fChain->SetBranchAddress("lep_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_MediumLLH_d0z0_v13",
                                 &lep_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_MediumLLH_d0z0_v13,
                                 &b_lep_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_MediumLLH_d0z0_v13);
        fChain->SetBranchAddress("lep_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_MediumLLH_d0z0_v13",
                                 &lep_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_MediumLLH_d0z0_v13,
                                 &b_lep_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_MediumLLH_d0z0_v13);
        fChain->SetBranchAddress("lep_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose",
                                 &lep_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose,
                                 &b_lep_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose);
        fChain->SetBranchAddress("lep_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose",
                                 &lep_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose,
                                 &b_lep_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose);
        fChain->SetBranchAddress("lep_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_RecoTrk",
                                 &lep_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_RecoTrk,
                                 &b_lep_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_offline_RecoTrk);
        fChain->SetBranchAddress("lep_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_RecoTrk",
                                 &lep_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_RecoTrk,
                                 &b_lep_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_offline_RecoTrk);
        fChain->SetBranchAddress(
            "lep_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_"
            "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose",
            &lep_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose,
            &b_lep_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1down_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose);
        fChain->SetBranchAddress(
            "lep_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_"
            "e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose",
            &lep_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose,
            &b_lep_EL_EFF_Trigger_TOTAL_1NPCOR_PLUS_UNCOR_1up_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose);
        fChain->SetBranchAddress("lep_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_FixedRad",
                                 &lep_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_FixedRad,
                                 &b_lep_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_FixedRad",
                                 &lep_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_FixedRad,
                                 &b_lep_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_FixedRad",
                                 &lep_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_FixedRad,
                                 &b_lep_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_FixedRad",
                                 &lep_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_FixedRad,
                                 &b_lep_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium", &lep_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium,
                                 &b_lep_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium", &lep_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium,
                                 &b_lep_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium",
                                 &lep_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium,
                                 &b_lep_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium",
                                 &lep_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium,
                                 &b_lep_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium", &lep_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium,
                                 &b_lep_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium", &lep_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium,
                                 &b_lep_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium",
                                 &lep_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium,
                                 &b_lep_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium", &lep_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium,
                                 &b_lep_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA", &lep_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA,
                                 &b_lep_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA", &lep_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA,
                                 &b_lep_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA", &lep_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA,
                                 &b_lep_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA", &lep_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA,
                                 &b_lep_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium",
                                 &lep_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium,
                                 &b_lep_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium",
                                 &lep_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium,
                                 &b_lep_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium",
                                 &lep_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium,
                                 &b_lep_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium",
                                 &lep_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium,
                                 &b_lep_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium",
                                 &lep_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium,
                                 &b_lep_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium",
                                 &lep_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium,
                                 &b_lep_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium",
                                 &lep_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium,
                                 &b_lep_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium",
                                 &lep_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium,
                                 &b_lep_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose",
                                 &lep_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose,
                                 &b_lep_NOMINAL_EleEffSF_ChargeID_MediumLLH_d0z0_v13_FCLoose);
        fChain->SetBranchAddress("lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose",
                                 &lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose,
                                 &b_lep_NOMINAL_EleEffSF_Isolation_MediumLLH_d0z0_v13_FCLoose);
        fChain->SetBranchAddress(
            "lep_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_"
            "lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose",
            &lep_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose,
            &b_lep_NOMINAL_EleEffSF_SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_2018_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0_MediumLLH_d0z0_v13_isolFCLoose);
        fChain->SetBranchAddress("lep_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v13", &lep_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v13,
                                 &b_lep_NOMINAL_EleEffSF_offline_MediumLLH_d0z0_v13);
        fChain->SetBranchAddress("lep_NOMINAL_EleEffSF_offline_RecoTrk", &lep_NOMINAL_EleEffSF_offline_RecoTrk,
                                 &b_lep_NOMINAL_EleEffSF_offline_RecoTrk);
        fChain->SetBranchAddress("lep_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium",
                                 &lep_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium,
                                 &b_lep_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium",
                                 &lep_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium,
                                 &b_lep_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_NOMINAL_MuEffSF_IsoTightTrackOnly_FixedRad", &lep_NOMINAL_MuEffSF_IsoTightTrackOnly_FixedRad,
                                 &b_lep_NOMINAL_MuEffSF_IsoTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_NOMINAL_MuEffSF_Reco_QualMedium", &lep_NOMINAL_MuEffSF_Reco_QualMedium, &b_lep_NOMINAL_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_NOMINAL_MuEffSF_TTVA", &lep_NOMINAL_MuEffSF_TTVA, &b_lep_NOMINAL_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_cluster_eta", &lep_cluster_eta, &b_lep_cluster_eta);
        fChain->SetBranchAddress("lep_cluster_eta_be2", &lep_cluster_eta_be2, &b_lep_cluster_eta_be2);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e140_lhloose_nod0", &lep_elec_trigmatch_HLT_e140_lhloose_nod0,
                                 &b_lep_elec_trigmatch_HLT_e140_lhloose_nod0);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e26_lhtight_nod0_ivarloose", &lep_elec_trigmatch_HLT_e26_lhtight_nod0_ivarloose,
                                 &b_lep_elec_trigmatch_HLT_e26_lhtight_nod0_ivarloose);
        fChain->SetBranchAddress("lep_elec_trigmatch_HLT_e60_lhmedium_nod0", &lep_elec_trigmatch_HLT_e60_lhmedium_nod0,
                                 &b_lep_elec_trigmatch_HLT_e60_lhmedium_nod0);
        fChain->SetBranchAddress("lep_elec_trigmatch_trigger_matched", &lep_elec_trigmatch_trigger_matched, &b_lep_elec_trigmatch_trigger_matched);
        fChain->SetBranchAddress("lep_id_bad", &lep_id_bad, &b_lep_id_bad);
        fChain->SetBranchAddress("lep_id_charge", &lep_id_charge, &b_lep_id_charge);
        fChain->SetBranchAddress("lep_id_loose", &lep_id_loose, &b_lep_id_loose);
        fChain->SetBranchAddress("lep_id_medium", &lep_id_medium, &b_lep_id_medium);
        fChain->SetBranchAddress("lep_id_tight", &lep_id_tight, &b_lep_id_tight);
        fChain->SetBranchAddress("lep_id_veryloose", &lep_id_veryloose, &b_lep_id_veryloose);
        fChain->SetBranchAddress("lep_iso_FCHighPtCaloOnly", &lep_iso_FCHighPtCaloOnly, &b_lep_iso_FCHighPtCaloOnly);
        fChain->SetBranchAddress("lep_iso_FCLoose", &lep_iso_FCLoose, &b_lep_iso_FCLoose);
        fChain->SetBranchAddress("lep_iso_FCLoose_FixedRad", &lep_iso_FCLoose_FixedRad, &b_lep_iso_FCLoose_FixedRad);
        fChain->SetBranchAddress("lep_iso_FCTight", &lep_iso_FCTight, &b_lep_iso_FCTight);
        fChain->SetBranchAddress("lep_iso_FCTightTrackOnly", &lep_iso_FCTightTrackOnly, &b_lep_iso_FCTightTrackOnly);
        fChain->SetBranchAddress("lep_iso_FCTightTrackOnly_FixedRad", &lep_iso_FCTightTrackOnly_FixedRad, &b_lep_iso_FCTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_iso_FCTight_FixedRad", &lep_iso_FCTight_FixedRad, &b_lep_iso_FCTight_FixedRad);
        fChain->SetBranchAddress("lep_iso_FixedCutLoose", &lep_iso_FixedCutLoose, &b_lep_iso_FixedCutLoose);
        fChain->SetBranchAddress("lep_iso_FixedCutTight", &lep_iso_FixedCutTight, &b_lep_iso_FixedCutTight);
        fChain->SetBranchAddress("lep_iso_FixedCutTightCaloOnly", &lep_iso_FixedCutTightCaloOnly, &b_lep_iso_FixedCutTightCaloOnly);
        fChain->SetBranchAddress("lep_iso_HighPtCaloOnly", &lep_iso_HighPtCaloOnly, &b_lep_iso_HighPtCaloOnly);
        fChain->SetBranchAddress("lep_iso_HighPtTrackOnly", &lep_iso_HighPtTrackOnly, &b_lep_iso_HighPtTrackOnly);
        fChain->SetBranchAddress("lep_iso_Loose", &lep_iso_Loose, &b_lep_iso_Loose);
        fChain->SetBranchAddress("lep_iso_Loose_FixedRad", &lep_iso_Loose_FixedRad, &b_lep_iso_Loose_FixedRad);
        fChain->SetBranchAddress("lep_iso_Loose_VarRad", &lep_iso_Loose_VarRad, &b_lep_iso_Loose_VarRad);
        fChain->SetBranchAddress("lep_iso_Tight", &lep_iso_Tight, &b_lep_iso_Tight);
        fChain->SetBranchAddress("lep_iso_TightTrackOnly", &lep_iso_TightTrackOnly, &b_lep_iso_TightTrackOnly);
        fChain->SetBranchAddress("lep_iso_TightTrackOnly_FixedRad", &lep_iso_TightTrackOnly_FixedRad, &b_lep_iso_TightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_iso_TightTrackOnly_VarRad", &lep_iso_TightTrackOnly_VarRad, &b_lep_iso_TightTrackOnly_VarRad);
        fChain->SetBranchAddress("lep_iso_Tight_FixedRad", &lep_iso_Tight_FixedRad, &b_lep_iso_Tight_FixedRad);
        fChain->SetBranchAddress("lep_iso_Tight_VarRad", &lep_iso_Tight_VarRad, &b_lep_iso_Tight_VarRad);
        fChain->SetBranchAddress("lep_matched", &lep_matched, &b_lep_matched);
        fChain->SetBranchAddress("lep_matched_classifierParticleOrigin", &lep_matched_classifierParticleOrigin,
                                 &b_lep_matched_classifierParticleOrigin);
        fChain->SetBranchAddress("lep_matched_classifierParticleType", &lep_matched_classifierParticleType, &b_lep_matched_classifierParticleType);
        fChain->SetBranchAddress("lep_matched_isHad", &lep_matched_isHad, &b_lep_matched_isHad);
        fChain->SetBranchAddress("lep_matched_mother_pdgId", &lep_matched_mother_pdgId, &b_lep_matched_mother_pdgId);
        fChain->SetBranchAddress("lep_matched_mother_status", &lep_matched_mother_status, &b_lep_matched_mother_status);
        fChain->SetBranchAddress("lep_matched_origin", &lep_matched_origin, &b_lep_matched_origin);
        fChain->SetBranchAddress("lep_matched_p4", &lep_matched_p4, &b_lep_matched_p4);
        fChain->SetBranchAddress("lep_matched_pdgId", &lep_matched_pdgId, &b_lep_matched_pdgId);
        fChain->SetBranchAddress("lep_matched_q", &lep_matched_q, &b_lep_matched_q);
        fChain->SetBranchAddress("lep_matched_status", &lep_matched_status, &b_lep_matched_status);
        fChain->SetBranchAddress("lep_matched_type", &lep_matched_type, &b_lep_matched_type);
        fChain->SetBranchAddress("lep_muonAuthor", &lep_muonAuthor, &b_lep_muonAuthor);
        fChain->SetBranchAddress("lep_muonType", &lep_muonType, &b_lep_muonType);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu26_ivarmedium", &lep_muon_trigmatch_HLT_mu26_ivarmedium,
                                 &b_lep_muon_trigmatch_HLT_mu26_ivarmedium);
        fChain->SetBranchAddress("lep_muon_trigmatch_HLT_mu50", &lep_muon_trigmatch_HLT_mu50, &b_lep_muon_trigmatch_HLT_mu50);
        fChain->SetBranchAddress("lep_muon_trigmatch_trigger_matched", &lep_muon_trigmatch_trigger_matched, &b_lep_muon_trigmatch_trigger_matched);
        fChain->SetBranchAddress("lep_origin", &lep_origin, &b_lep_origin);
        fChain->SetBranchAddress("lep_p4", &lep_p4, &b_lep_p4);
        fChain->SetBranchAddress("lep_q", &lep_q, &b_lep_q);
        fChain->SetBranchAddress("lep_trk_d0", &lep_trk_d0, &b_lep_trk_d0);
        fChain->SetBranchAddress("lep_trk_d0_sig", &lep_trk_d0_sig, &b_lep_trk_d0_sig);
        fChain->SetBranchAddress("lep_trk_p4", &lep_trk_p4, &b_lep_trk_p4);
        fChain->SetBranchAddress("lep_trk_pt_error", &lep_trk_pt_error, &b_lep_trk_pt_error);
        fChain->SetBranchAddress("lep_trk_pvx_z0", &lep_trk_pvx_z0, &b_lep_trk_pvx_z0);
        fChain->SetBranchAddress("lep_trk_pvx_z0_sig", &lep_trk_pvx_z0_sig, &b_lep_trk_pvx_z0_sig);
        fChain->SetBranchAddress("lep_trk_pvx_z0_sintheta", &lep_trk_pvx_z0_sintheta, &b_lep_trk_pvx_z0_sintheta);
        fChain->SetBranchAddress("lep_trk_vx", &lep_trk_vx, &b_lep_trk_vx);
        fChain->SetBranchAddress("lep_trk_vx_v", &lep_trk_vx_v, &b_lep_trk_vx_v);
        fChain->SetBranchAddress("lep_trk_z0", &lep_trk_z0, &b_lep_trk_z0);
        fChain->SetBranchAddress("lep_trk_z0_sig", &lep_trk_z0_sig, &b_lep_trk_z0_sig);
        fChain->SetBranchAddress("lep_trk_z0_sintheta", &lep_trk_z0_sintheta, &b_lep_trk_z0_sintheta);
        fChain->SetBranchAddress("lep_type", &lep_type, &b_lep_type);
        fChain->SetBranchAddress("lephad", &lephad, &b_lephad);
        fChain->SetBranchAddress("lephad_coll_approx", &lephad_coll_approx, &b_lephad_coll_approx);
        fChain->SetBranchAddress("lephad_coll_approx_m", &lephad_coll_approx_m, &b_lephad_coll_approx_m);
        fChain->SetBranchAddress("lephad_coll_approx_x0", &lephad_coll_approx_x0, &b_lephad_coll_approx_x0);
        fChain->SetBranchAddress("lephad_coll_approx_x1", &lephad_coll_approx_x1, &b_lephad_coll_approx_x1);
        fChain->SetBranchAddress("lephad_cosalpha", &lephad_cosalpha, &b_lephad_cosalpha);
        fChain->SetBranchAddress("lephad_deta", &lephad_deta, &b_lephad_deta);
        fChain->SetBranchAddress("lephad_dphi", &lephad_dphi, &b_lephad_dphi);
        fChain->SetBranchAddress("lephad_dpt", &lephad_dpt, &b_lephad_dpt);
        fChain->SetBranchAddress("lephad_dr", &lephad_dr, &b_lephad_dr);
        fChain->SetBranchAddress("lephad_met_bisect", &lephad_met_bisect, &b_lephad_met_bisect);
        fChain->SetBranchAddress("lephad_met_centrality", &lephad_met_centrality, &b_lephad_met_centrality);
        fChain->SetBranchAddress("lephad_met_lep0_cos_dphi", &lephad_met_lep0_cos_dphi, &b_lephad_met_lep0_cos_dphi);
        fChain->SetBranchAddress("lephad_met_lep1_cos_dphi", &lephad_met_lep1_cos_dphi, &b_lephad_met_lep1_cos_dphi);
        fChain->SetBranchAddress("lephad_met_min_dphi", &lephad_met_min_dphi, &b_lephad_met_min_dphi);
        fChain->SetBranchAddress("lephad_met_sum_cos_dphi", &lephad_met_sum_cos_dphi, &b_lephad_met_sum_cos_dphi);
        fChain->SetBranchAddress("lephad_mt_lep0_met", &lephad_mt_lep0_met, &b_lephad_mt_lep0_met);
        fChain->SetBranchAddress("lephad_mt_lep1_met", &lephad_mt_lep1_met, &b_lephad_mt_lep1_met);
        fChain->SetBranchAddress("lephad_p4", &lephad_p4, &b_lephad_p4);
        fChain->SetBranchAddress("lephad_qxq", &lephad_qxq, &b_lephad_qxq);
        fChain->SetBranchAddress("lephad_scal_sum_pt", &lephad_scal_sum_pt, &b_lephad_scal_sum_pt);
        fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
        fChain->SetBranchAddress("met_reco_p4", &met_reco_p4, &b_met_reco_p4);
        fChain->SetBranchAddress("met_reco_sig", &met_reco_sig, &b_met_reco_sig);
        fChain->SetBranchAddress("met_reco_sig_tracks", &met_reco_sig_tracks, &b_met_reco_sig_tracks);
        fChain->SetBranchAddress("met_reco_sumet", &met_reco_sumet, &b_met_reco_sumet);
        fChain->SetBranchAddress("n_actual_int", &n_actual_int, &b_n_actual_int);
        fChain->SetBranchAddress("n_actual_int_cor", &n_actual_int_cor, &b_n_actual_int_cor);
        fChain->SetBranchAddress("n_avg_int", &n_avg_int, &b_n_avg_int);
        fChain->SetBranchAddress("n_avg_int_cor", &n_avg_int_cor, &b_n_avg_int_cor);
        fChain->SetBranchAddress("n_bjets_DL1dv00_FixedCutBEff_70", &n_bjets_DL1dv00_FixedCutBEff_70, &b_n_bjets_DL1dv00_FixedCutBEff_70);
        fChain->SetBranchAddress("n_bjets_DL1dv00_FixedCutBEff_85", &n_bjets_DL1dv00_FixedCutBEff_85, &b_n_bjets_DL1dv00_FixedCutBEff_85);
        fChain->SetBranchAddress("n_electrons", &n_electrons, &b_n_electrons);
        fChain->SetBranchAddress("n_jets", &n_jets, &b_n_jets);
        fChain->SetBranchAddress("n_jets_30", &n_jets_30, &b_n_jets_30);
        fChain->SetBranchAddress("n_jets_40", &n_jets_40, &b_n_jets_40);
        fChain->SetBranchAddress("n_jets_mc_hs", &n_jets_mc_hs, &b_n_jets_mc_hs);
        fChain->SetBranchAddress("n_muons", &n_muons, &b_n_muons);
        fChain->SetBranchAddress("n_muons_bad", &n_muons_bad, &b_n_muons_bad);
        fChain->SetBranchAddress("n_muons_loose", &n_muons_loose, &b_n_muons_loose);
        fChain->SetBranchAddress("n_muons_medium", &n_muons_medium, &b_n_muons_medium);
        fChain->SetBranchAddress("n_muons_tight", &n_muons_tight, &b_n_muons_tight);
        fChain->SetBranchAddress("n_muons_veryloose", &n_muons_veryloose, &b_n_muons_veryloose);
        fChain->SetBranchAddress("n_photons", &n_photons, &b_n_photons);
        fChain->SetBranchAddress("n_pvx", &n_pvx, &b_n_pvx);
        fChain->SetBranchAddress("n_taus", &n_taus, &b_n_taus);
        fChain->SetBranchAddress("n_taus_rnn_loose", &n_taus_rnn_loose, &b_n_taus_rnn_loose);
        fChain->SetBranchAddress("n_taus_rnn_medium", &n_taus_rnn_medium, &b_n_taus_rnn_medium);
        fChain->SetBranchAddress("n_taus_rnn_tight", &n_taus_rnn_tight, &b_n_taus_rnn_tight);
        fChain->SetBranchAddress("n_taus_rnn_veryloose", &n_taus_rnn_veryloose, &b_n_taus_rnn_veryloose);
        fChain->SetBranchAddress("n_truth_gluon_jets", &n_truth_gluon_jets, &b_n_truth_gluon_jets);
        fChain->SetBranchAddress("n_truth_jets", &n_truth_jets, &b_n_truth_jets);
        fChain->SetBranchAddress("n_truth_jets_pt20_eta45", &n_truth_jets_pt20_eta45, &b_n_truth_jets_pt20_eta45);
        fChain->SetBranchAddress("n_truth_quark_jets", &n_truth_quark_jets, &b_n_truth_quark_jets);
        fChain->SetBranchAddress("n_vx", &n_vx, &b_n_vx);
        fChain->SetBranchAddress("run_number", &run_number, &b_run_number);
        fChain->SetBranchAddress("tau", &tau, &b_tau);
        fChain->SetBranchAddress("tau_NOMINAL_TauEffSF_JetRNNloose", &tau_NOMINAL_TauEffSF_JetRNNloose, &b_tau_NOMINAL_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_NOMINAL_TauEffSF_JetRNNmedium", &tau_NOMINAL_TauEffSF_JetRNNmedium, &b_tau_NOMINAL_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_NOMINAL_TauEffSF_JetRNNtight", &tau_NOMINAL_TauEffSF_JetRNNtight, &b_tau_NOMINAL_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_NOMINAL_TauEffSF_reco", &tau_NOMINAL_TauEffSF_reco, &b_tau_NOMINAL_TauEffSF_reco);
        fChain->SetBranchAddress("tau_NOMINAL_TauEffSF_selection", &tau_NOMINAL_TauEffSF_selection, &b_tau_NOMINAL_TauEffSF_selection);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco", &tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection",
                                 &tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco", &tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection",
                                 &tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNloose",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNloose,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNmedium",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNtight",
                                 &tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNtight,
                                 &b_tau_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_charged_tracks_d0", &tau_charged_tracks_d0, &b_tau_charged_tracks_d0);
        fChain->SetBranchAddress("tau_charged_tracks_d0_sig", &tau_charged_tracks_d0_sig, &b_tau_charged_tracks_d0_sig);
        fChain->SetBranchAddress("tau_charged_tracks_eProbabilityHT", &tau_charged_tracks_eProbabilityHT, &b_tau_charged_tracks_eProbabilityHT);
        fChain->SetBranchAddress("tau_charged_tracks_nInnermostPixelHits", &tau_charged_tracks_nInnermostPixelHits,
                                 &b_tau_charged_tracks_nInnermostPixelHits);
        fChain->SetBranchAddress("tau_charged_tracks_nPixelHits", &tau_charged_tracks_nPixelHits, &b_tau_charged_tracks_nPixelHits);
        fChain->SetBranchAddress("tau_charged_tracks_nSCTHits", &tau_charged_tracks_nSCTHits, &b_tau_charged_tracks_nSCTHits);
        fChain->SetBranchAddress("tau_charged_tracks_p4", &tau_charged_tracks_p4, &b_tau_charged_tracks_p4);
        fChain->SetBranchAddress("tau_charged_tracks_pt_err", &tau_charged_tracks_pt_err, &b_tau_charged_tracks_pt_err);
        fChain->SetBranchAddress("tau_charged_tracks_z0", &tau_charged_tracks_z0, &b_tau_charged_tracks_z0);
        fChain->SetBranchAddress("tau_charged_tracks_z0_sig", &tau_charged_tracks_z0_sig, &b_tau_charged_tracks_z0_sig);
        fChain->SetBranchAddress("tau_charged_tracks_z0_sintheta", &tau_charged_tracks_z0_sintheta, &b_tau_charged_tracks_z0_sintheta);
        fChain->SetBranchAddress("tau_charged_tracks_z0_sintheta_tjva", &tau_charged_tracks_z0_sintheta_tjva, &b_tau_charged_tracks_z0_sintheta_tjva);
        fChain->SetBranchAddress("tau_decay_mode", &tau_decay_mode, &b_tau_decay_mode);
        fChain->SetBranchAddress("tau_ele_rnn_loose_retuned", &tau_ele_rnn_loose_retuned, &b_tau_ele_rnn_loose_retuned);
        fChain->SetBranchAddress("tau_ele_rnn_medium_retuned", &tau_ele_rnn_medium_retuned, &b_tau_ele_rnn_medium_retuned);
        fChain->SetBranchAddress("tau_ele_rnn_score", &tau_ele_rnn_score, &b_tau_ele_rnn_score);
        fChain->SetBranchAddress("tau_ele_rnn_score_trans", &tau_ele_rnn_score_trans, &b_tau_ele_rnn_score_trans);
        fChain->SetBranchAddress("tau_ele_rnn_tight_retuned", &tau_ele_rnn_tight_retuned, &b_tau_ele_rnn_tight_retuned);
        fChain->SetBranchAddress("tau_jet_rnn_loose", &tau_jet_rnn_loose, &b_tau_jet_rnn_loose);
        fChain->SetBranchAddress("tau_jet_rnn_medium", &tau_jet_rnn_medium, &b_tau_jet_rnn_medium);
        fChain->SetBranchAddress("tau_jet_rnn_score", &tau_jet_rnn_score, &b_tau_jet_rnn_score);
        fChain->SetBranchAddress("tau_jet_rnn_score_trans", &tau_jet_rnn_score_trans, &b_tau_jet_rnn_score_trans);
        fChain->SetBranchAddress("tau_jet_rnn_tight", &tau_jet_rnn_tight, &b_tau_jet_rnn_tight);
        fChain->SetBranchAddress("tau_jet_rnn_veryloose", &tau_jet_rnn_veryloose, &b_tau_jet_rnn_veryloose);
        fChain->SetBranchAddress("tau_jet_width", &tau_jet_width, &b_tau_jet_width);
        fChain->SetBranchAddress("tau_matched", &tau_matched, &b_tau_matched);
        fChain->SetBranchAddress("tau_matched_classifierParticleOrigin", &tau_matched_classifierParticleOrigin,
                                 &b_tau_matched_classifierParticleOrigin);
        fChain->SetBranchAddress("tau_matched_classifierParticleType", &tau_matched_classifierParticleType, &b_tau_matched_classifierParticleType);
        fChain->SetBranchAddress("tau_matched_decayVertex", &tau_matched_decayVertex, &b_tau_matched_decayVertex);
        fChain->SetBranchAddress("tau_matched_decayVertex_v", &tau_matched_decayVertex_v, &b_tau_matched_decayVertex_v);
        fChain->SetBranchAddress("tau_matched_decay_mode", &tau_matched_decay_mode, &b_tau_matched_decay_mode);
        fChain->SetBranchAddress("tau_matched_isEle", &tau_matched_isEle, &b_tau_matched_isEle);
        fChain->SetBranchAddress("tau_matched_isHadTau", &tau_matched_isHadTau, &b_tau_matched_isHadTau);
        fChain->SetBranchAddress("tau_matched_isJet", &tau_matched_isJet, &b_tau_matched_isJet);
        fChain->SetBranchAddress("tau_matched_isMuon", &tau_matched_isMuon, &b_tau_matched_isMuon);
        fChain->SetBranchAddress("tau_matched_isTau", &tau_matched_isTau, &b_tau_matched_isTau);
        fChain->SetBranchAddress("tau_matched_isTruthMatch", &tau_matched_isTruthMatch, &b_tau_matched_isTruthMatch);
        fChain->SetBranchAddress("tau_matched_mother_pdgId", &tau_matched_mother_pdgId, &b_tau_matched_mother_pdgId);
        fChain->SetBranchAddress("tau_matched_mother_status", &tau_matched_mother_status, &b_tau_matched_mother_status);
        fChain->SetBranchAddress("tau_matched_n_charged", &tau_matched_n_charged, &b_tau_matched_n_charged);
        fChain->SetBranchAddress("tau_matched_n_charged_pion", &tau_matched_n_charged_pion, &b_tau_matched_n_charged_pion);
        fChain->SetBranchAddress("tau_matched_n_neutral", &tau_matched_n_neutral, &b_tau_matched_n_neutral);
        fChain->SetBranchAddress("tau_matched_n_neutral_pion", &tau_matched_n_neutral_pion, &b_tau_matched_n_neutral_pion);
        fChain->SetBranchAddress("tau_matched_origin", &tau_matched_origin, &b_tau_matched_origin);
        fChain->SetBranchAddress("tau_matched_p4", &tau_matched_p4, &b_tau_matched_p4);
        fChain->SetBranchAddress("tau_matched_p4_vis_charged_track0", &tau_matched_p4_vis_charged_track0, &b_tau_matched_p4_vis_charged_track0);
        fChain->SetBranchAddress("tau_matched_p4_vis_charged_track1", &tau_matched_p4_vis_charged_track1, &b_tau_matched_p4_vis_charged_track1);
        fChain->SetBranchAddress("tau_matched_p4_vis_charged_track2", &tau_matched_p4_vis_charged_track2, &b_tau_matched_p4_vis_charged_track2);
        fChain->SetBranchAddress("tau_matched_pdgId", &tau_matched_pdgId, &b_tau_matched_pdgId);
        fChain->SetBranchAddress("tau_matched_productionVertex", &tau_matched_productionVertex, &b_tau_matched_productionVertex);
        fChain->SetBranchAddress("tau_matched_productionVertex_v", &tau_matched_productionVertex_v, &b_tau_matched_productionVertex_v);
        fChain->SetBranchAddress("tau_matched_pz", &tau_matched_pz, &b_tau_matched_pz);
        fChain->SetBranchAddress("tau_matched_q", &tau_matched_q, &b_tau_matched_q);
        fChain->SetBranchAddress("tau_matched_status", &tau_matched_status, &b_tau_matched_status);
        fChain->SetBranchAddress("tau_matched_type", &tau_matched_type, &b_tau_matched_type);
        fChain->SetBranchAddress("tau_matched_vis_charged_p4", &tau_matched_vis_charged_p4, &b_tau_matched_vis_charged_p4);
        fChain->SetBranchAddress("tau_matched_vis_neutral_others_p4", &tau_matched_vis_neutral_others_p4, &b_tau_matched_vis_neutral_others_p4);
        fChain->SetBranchAddress("tau_matched_vis_neutral_p4", &tau_matched_vis_neutral_p4, &b_tau_matched_vis_neutral_p4);
        fChain->SetBranchAddress("tau_matched_vis_neutral_pions_p4", &tau_matched_vis_neutral_pions_p4, &b_tau_matched_vis_neutral_pions_p4);
        fChain->SetBranchAddress("tau_matched_vis_p4", &tau_matched_vis_p4, &b_tau_matched_vis_p4);
        fChain->SetBranchAddress("tau_n_all_tracks", &tau_n_all_tracks, &b_tau_n_all_tracks);
        fChain->SetBranchAddress("tau_n_charged_tracks", &tau_n_charged_tracks, &b_tau_n_charged_tracks);
        fChain->SetBranchAddress("tau_n_conversion_tracks", &tau_n_conversion_tracks, &b_tau_n_conversion_tracks);
        fChain->SetBranchAddress("tau_n_fake_tracks", &tau_n_fake_tracks, &b_tau_n_fake_tracks);
        fChain->SetBranchAddress("tau_n_isolation_tracks", &tau_n_isolation_tracks, &b_tau_n_isolation_tracks);
        fChain->SetBranchAddress("tau_n_unclassified_tracks", &tau_n_unclassified_tracks, &b_tau_n_unclassified_tracks);
        fChain->SetBranchAddress("tau_origin", &tau_origin, &b_tau_origin);
        fChain->SetBranchAddress("tau_p4", &tau_p4, &b_tau_p4);
        fChain->SetBranchAddress("tau_pass_tst_muonolr", &tau_pass_tst_muonolr, &b_tau_pass_tst_muonolr);
        fChain->SetBranchAddress("tau_q", &tau_q, &b_tau_q);
        fChain->SetBranchAddress("tau_tes_alpha_pt_shift", &tau_tes_alpha_pt_shift, &b_tau_tes_alpha_pt_shift);
        fChain->SetBranchAddress("tau_type", &tau_type, &b_tau_type);
        fChain->SetBranchAddress("theory_weights_CT14_pdfset", &theory_weights_CT14_pdfset, &b_theory_weights_CT14_pdfset);
        fChain->SetBranchAddress("theory_weights_MMHT_pdfset", &theory_weights_MMHT_pdfset, &b_theory_weights_MMHT_pdfset);
        fChain->SetBranchAddress("theory_weights_alphaS_down", &theory_weights_alphaS_down, &b_theory_weights_alphaS_down);
        fChain->SetBranchAddress("theory_weights_alphaS_up", &theory_weights_alphaS_up, &b_theory_weights_alphaS_up);
        fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
        fChain->SetBranchAddress("Weight_mc", &Weight_mc, &b_Weight_mc);
        fChain->SetBranchAddress("mergedRunNumber", &mergedRunNumber, &b_mergedRunNumber);
        fChain->SetBranchAddress("upsilon", &upsilon, &b_upsilon);
        fChain->SetBranchAddress("isQCR", &isQCR, &b_isQCR);
        fChain->SetBranchAddress("isWCR", &isWCR, &b_isWCR);
        fChain->SetBranchAddress("isWCR_QCD", &isWCR_QCD, &b_isWCR_QCD);
        fChain->SetBranchAddress("isIQCR_Iso", &isIQCR_Iso, &b_isIQCR_Iso);
        fChain->SetBranchAddress("isIQCR_NotIso", &isIQCR_NotIso, &b_isIQCR_NotIso);
        fChain->SetBranchAddress("isTCR", &isTCR, &b_isTCR);
        fChain->SetBranchAddress("isSR", &isSR, &b_isSR);
        fChain->SetBranchAddress("isData", &isData, &b_isData);
    }
};
