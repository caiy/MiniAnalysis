#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

#include <vector>

#include "AnaTree.h"

class OneLepTree : public AnaTree {
public:
    OneLepTree(TChain *chain) : AnaTree(chain) { Init(this->fChain); }

    // Declaration of leaf types
    Double_t trigWeight_metTrig;
    Bool_t trigMatch_metTrig;
    Double_t trigWeight_singleLepTrig;
    Bool_t trigMatch_singleLepTrig;
    Int_t nLep_base;
    Int_t nLep_signal;
    Float_t mu;
    Float_t actual_mu;
    Int_t AnalysisType;
    Int_t lep1Charge;
    Bool_t isBadMuon;
    Float_t muQOverP;
    Float_t muQOverPErr;
    Int_t muQuality;
    Int_t nVtx;
    Int_t nJet30;
    Int_t nJet25;
    Int_t nBJet30_MV2c10;
    Int_t nBJet25_MV2c10;
    Int_t NPairs;
    Float_t met;
    Float_t mt;
    Float_t mct;
    Float_t mct2;
    Float_t mlb1;
    Float_t mlj1;
    Float_t mbb;
    Float_t meffInc30;
    Float_t Ht30;
    Float_t lep1Pt;
    Float_t lep1Eta;
    Float_t lep1Phi;
    Float_t lep2Pt;
    Float_t lep2Eta;
    Float_t lep2Phi;
    Float_t LepAplanarity;
    Float_t JetAplanarity;
    Float_t dRJet;
    Float_t jet1Pt;
    Float_t jet1Eta;
    Float_t jet1Phi;
    Float_t jet1MV2c10;
    Float_t jet2Pt;
    Float_t jet2Eta;
    Float_t jet2Phi;
    Float_t jet3Pt;
    Float_t jet3Eta;
    Float_t jet3Phi;
    Float_t jet4Pt;
    Float_t jet4Eta;
    Float_t jet4Phi;
    Float_t jet5Pt;
    Float_t jet6Pt;
    Float_t bJet1Pt;
    Float_t bJet2Pt;
    Float_t TST_Et;
    Float_t TST_Phi;
    Float_t met_Phi;
    Float_t met_Signif;
    Float_t deltaPhi_MET_TST_Phi;
    Float_t leptonTrigWeight;
    Double_t pileupWeight;
    Double_t leptonWeight;
    Double_t eventWeight;
    Double_t jvtWeight;
    Double_t bTagWeight;
    Double_t genWeight;
    Double_t genWeightUp;
    Double_t genWeightDown;
    Double_t SherpaVjetsNjetsWeight;
    Bool_t HLT_xe70;
    Bool_t HLT_xe70_mht;
    Bool_t HLT_xe70_mht_wEFMu;
    Bool_t HLT_xe70_tc_lcw;
    Bool_t HLT_xe70_tc_lcw_wEFMu;
    Bool_t HLT_xe80_tc_lcw_L1XE50;
    Bool_t HLT_xe90_tc_lcw_L1XE50;
    Bool_t HLT_xe90_mht_L1XE50;
    Bool_t HLT_xe90_tc_lcw_wEFMu_L1XE50;
    Bool_t HLT_xe90_mht_wEFMu_L1XE50;
    Bool_t HLT_xe100_L1XE50;
    Bool_t HLT_xe100_wEFMu_L1XE50;
    Bool_t HLT_xe100_tc_lcw_L1XE50;
    Bool_t HLT_xe100_mht_L1XE50;
    Bool_t HLT_xe100_tc_lcw_wEFMu_L1XE50;
    Bool_t HLT_xe100_mht_wEFMu_L1XE50;
    Bool_t HLT_xe110_L1XE50;
    Bool_t HLT_xe110_tc_em_L1XE50;
    Bool_t HLT_xe110_wEFMu_L1XE50;
    Bool_t HLT_xe110_tc_em_wEFMu_L1XE50;
    Bool_t HLT_xe110_mht_L1XE50;
    Bool_t HLT_xe110_pufit_L1XE55;
    Bool_t HLT_xe110_pufit_xe70_L1XE50;
    Bool_t HLT_noalg_L1J400;
    Bool_t match_HLT_mu26_ivarmedium;
    Bool_t match_HLT_mu50;
    Bool_t match_HLT_mu60_0eta105_msonly;
    Bool_t match_HLT_e26_lhtight_nod0_ivarloose;
    Bool_t match_HLT_e60_lhmedium_nod0;
    Bool_t match_HLT_e140_lhloose_nod0;
    Bool_t match_HLT_e300_etcut;
    Int_t DecayModeTTbar;
    Double_t ttbarNNLOWeight;
    Double_t ttbarNNLOWeightUp;
    Double_t ttbarNNLOWeightDown;
    Float_t truthTopPt;
    Float_t truthAntiTopPt;
    Float_t truthTtbarPt;
    Float_t truthTtbarM;
    Float_t x1;
    Float_t x2;
    Float_t pdf1;
    Float_t pdf2;
    Float_t scalePDF;
    Int_t id1;
    Int_t id2;
    Int_t lep1Type;
    Int_t lep1Origin;
    Float_t ptcone20;
    Bool_t IsoFCTight;
    Bool_t IsoFCLoose;
    Bool_t IsoGradient;
    Bool_t IsoFCTightTrackOnly;
    Bool_t IsoFixedCutHighPtTrackOnly;
    Bool_t IsoFCHighPtCaloOnly;
    Float_t GenHt;
    Float_t GenMET;
    Float_t leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;
    Float_t leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;
    Float_t leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;
    Float_t leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;
    Float_t leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down;
    Float_t leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;
    Float_t leptonWeight_MUON_EFF_BADMUON_STAT__1down;
    Float_t leptonWeight_MUON_EFF_BADMUON_STAT__1up;
    Float_t leptonWeight_MUON_EFF_BADMUON_SYS__1down;
    Float_t leptonWeight_MUON_EFF_BADMUON_SYS__1up;
    Float_t leptonWeight_MUON_EFF_ISO_STAT__1down;
    Float_t leptonWeight_MUON_EFF_ISO_STAT__1up;
    Float_t leptonWeight_MUON_EFF_ISO_SYS__1down;
    Float_t leptonWeight_MUON_EFF_ISO_SYS__1up;
    Float_t leptonWeight_MUON_EFF_RECO_STAT__1down;
    Float_t leptonWeight_MUON_EFF_RECO_STAT__1up;
    Float_t leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down;
    Float_t leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up;
    Float_t leptonWeight_MUON_EFF_RECO_SYS__1down;
    Float_t leptonWeight_MUON_EFF_RECO_SYS__1up;
    Float_t leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down;
    Float_t leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up;
    Float_t leptonWeight_MUON_EFF_TTVA_STAT__1down;
    Float_t leptonWeight_MUON_EFF_TTVA_STAT__1up;
    Float_t leptonWeight_MUON_EFF_TTVA_SYS__1down;
    Float_t leptonWeight_MUON_EFF_TTVA_SYS__1up;
    Float_t bTagWeight_FT_EFF_B_systematics__1down;
    Float_t bTagWeight_FT_EFF_B_systematics__1up;
    Float_t bTagWeight_FT_EFF_C_systematics__1down;
    Float_t bTagWeight_FT_EFF_C_systematics__1up;
    Float_t bTagWeight_FT_EFF_Light_systematics__1down;
    Float_t bTagWeight_FT_EFF_Light_systematics__1up;
    Float_t bTagWeight_FT_EFF_extrapolation__1down;
    Float_t bTagWeight_FT_EFF_extrapolation__1up;
    Float_t bTagWeight_FT_EFF_extrapolation_from_charm__1down;
    Float_t bTagWeight_FT_EFF_extrapolation_from_charm__1up;
    Float_t jvtWeight_JET_JvtEfficiency__1down;
    Float_t jvtWeight_JET_JvtEfficiency__1up;
    Float_t pileupWeightUp;
    Float_t pileupWeightDown;
    ULong64_t PRWHash;
    ULong64_t EventNumber;
    Float_t xsec;
    Float_t TrueHt;
    Int_t DatasetNumber;
    Int_t RunNumber;
    Int_t RandomRunNumber;
    Int_t FS;
    Float_t LHE3Weight_MUR0_5_MUF0_5_PDF261000;
    Float_t LHE3Weight_MUR0_5_MUF1_PDF261000;
    Float_t LHE3Weight_MUR1_MUF0_5_PDF261000;
    Float_t LHE3Weight_MUR1_MUF1_PDF13000;
    Float_t LHE3Weight_MUR1_MUF1_PDF25300;
    Float_t LHE3Weight_MUR1_MUF1_PDF261000;
    Float_t LHE3Weight_MUR1_MUF1_PDF261001;
    Float_t LHE3Weight_MUR1_MUF1_PDF261002;
    Float_t LHE3Weight_MUR1_MUF1_PDF261003;
    Float_t LHE3Weight_MUR1_MUF1_PDF261004;
    Float_t LHE3Weight_MUR1_MUF1_PDF261005;
    Float_t LHE3Weight_MUR1_MUF1_PDF261006;
    Float_t LHE3Weight_MUR1_MUF1_PDF261007;
    Float_t LHE3Weight_MUR1_MUF1_PDF261008;
    Float_t LHE3Weight_MUR1_MUF1_PDF261009;
    Float_t LHE3Weight_MUR1_MUF1_PDF261010;
    Float_t LHE3Weight_MUR1_MUF1_PDF261011;
    Float_t LHE3Weight_MUR1_MUF1_PDF261012;
    Float_t LHE3Weight_MUR1_MUF1_PDF261013;
    Float_t LHE3Weight_MUR1_MUF1_PDF261014;
    Float_t LHE3Weight_MUR1_MUF1_PDF261015;
    Float_t LHE3Weight_MUR1_MUF1_PDF261016;
    Float_t LHE3Weight_MUR1_MUF1_PDF261017;
    Float_t LHE3Weight_MUR1_MUF1_PDF261018;
    Float_t LHE3Weight_MUR1_MUF1_PDF261019;
    Float_t LHE3Weight_MUR1_MUF1_PDF261020;
    Float_t LHE3Weight_MUR1_MUF1_PDF261021;
    Float_t LHE3Weight_MUR1_MUF1_PDF261022;
    Float_t LHE3Weight_MUR1_MUF1_PDF261023;
    Float_t LHE3Weight_MUR1_MUF1_PDF261024;
    Float_t LHE3Weight_MUR1_MUF1_PDF261025;
    Float_t LHE3Weight_MUR1_MUF1_PDF261026;
    Float_t LHE3Weight_MUR1_MUF1_PDF261027;
    Float_t LHE3Weight_MUR1_MUF1_PDF261028;
    Float_t LHE3Weight_MUR1_MUF1_PDF261029;
    Float_t LHE3Weight_MUR1_MUF1_PDF261030;
    Float_t LHE3Weight_MUR1_MUF1_PDF261031;
    Float_t LHE3Weight_MUR1_MUF1_PDF261032;
    Float_t LHE3Weight_MUR1_MUF1_PDF261033;
    Float_t LHE3Weight_MUR1_MUF1_PDF261034;
    Float_t LHE3Weight_MUR1_MUF1_PDF261035;
    Float_t LHE3Weight_MUR1_MUF1_PDF261036;
    Float_t LHE3Weight_MUR1_MUF1_PDF261037;
    Float_t LHE3Weight_MUR1_MUF1_PDF261038;
    Float_t LHE3Weight_MUR1_MUF1_PDF261039;
    Float_t LHE3Weight_MUR1_MUF1_PDF261040;
    Float_t LHE3Weight_MUR1_MUF1_PDF261041;
    Float_t LHE3Weight_MUR1_MUF1_PDF261042;
    Float_t LHE3Weight_MUR1_MUF1_PDF261043;
    Float_t LHE3Weight_MUR1_MUF1_PDF261044;
    Float_t LHE3Weight_MUR1_MUF1_PDF261045;
    Float_t LHE3Weight_MUR1_MUF1_PDF261046;
    Float_t LHE3Weight_MUR1_MUF1_PDF261047;
    Float_t LHE3Weight_MUR1_MUF1_PDF261048;
    Float_t LHE3Weight_MUR1_MUF1_PDF261049;
    Float_t LHE3Weight_MUR1_MUF1_PDF261050;
    Float_t LHE3Weight_MUR1_MUF1_PDF261051;
    Float_t LHE3Weight_MUR1_MUF1_PDF261052;
    Float_t LHE3Weight_MUR1_MUF1_PDF261053;
    Float_t LHE3Weight_MUR1_MUF1_PDF261054;
    Float_t LHE3Weight_MUR1_MUF1_PDF261055;
    Float_t LHE3Weight_MUR1_MUF1_PDF261056;
    Float_t LHE3Weight_MUR1_MUF1_PDF261057;
    Float_t LHE3Weight_MUR1_MUF1_PDF261058;
    Float_t LHE3Weight_MUR1_MUF1_PDF261059;
    Float_t LHE3Weight_MUR1_MUF1_PDF261060;
    Float_t LHE3Weight_MUR1_MUF1_PDF261061;
    Float_t LHE3Weight_MUR1_MUF1_PDF261062;
    Float_t LHE3Weight_MUR1_MUF1_PDF261063;
    Float_t LHE3Weight_MUR1_MUF1_PDF261064;
    Float_t LHE3Weight_MUR1_MUF1_PDF261065;
    Float_t LHE3Weight_MUR1_MUF1_PDF261066;
    Float_t LHE3Weight_MUR1_MUF1_PDF261067;
    Float_t LHE3Weight_MUR1_MUF1_PDF261068;
    Float_t LHE3Weight_MUR1_MUF1_PDF261069;
    Float_t LHE3Weight_MUR1_MUF1_PDF261070;
    Float_t LHE3Weight_MUR1_MUF1_PDF261071;
    Float_t LHE3Weight_MUR1_MUF1_PDF261072;
    Float_t LHE3Weight_MUR1_MUF1_PDF261073;
    Float_t LHE3Weight_MUR1_MUF1_PDF261074;
    Float_t LHE3Weight_MUR1_MUF1_PDF261075;
    Float_t LHE3Weight_MUR1_MUF1_PDF261076;
    Float_t LHE3Weight_MUR1_MUF1_PDF261077;
    Float_t LHE3Weight_MUR1_MUF1_PDF261078;
    Float_t LHE3Weight_MUR1_MUF1_PDF261079;
    Float_t LHE3Weight_MUR1_MUF1_PDF261080;
    Float_t LHE3Weight_MUR1_MUF1_PDF261081;
    Float_t LHE3Weight_MUR1_MUF1_PDF261082;
    Float_t LHE3Weight_MUR1_MUF1_PDF261083;
    Float_t LHE3Weight_MUR1_MUF1_PDF261084;
    Float_t LHE3Weight_MUR1_MUF1_PDF261085;
    Float_t LHE3Weight_MUR1_MUF1_PDF261086;
    Float_t LHE3Weight_MUR1_MUF1_PDF261087;
    Float_t LHE3Weight_MUR1_MUF1_PDF261088;
    Float_t LHE3Weight_MUR1_MUF1_PDF261089;
    Float_t LHE3Weight_MUR1_MUF1_PDF261090;
    Float_t LHE3Weight_MUR1_MUF1_PDF261091;
    Float_t LHE3Weight_MUR1_MUF1_PDF261092;
    Float_t LHE3Weight_MUR1_MUF1_PDF261093;
    Float_t LHE3Weight_MUR1_MUF1_PDF261094;
    Float_t LHE3Weight_MUR1_MUF1_PDF261095;
    Float_t LHE3Weight_MUR1_MUF1_PDF261096;
    Float_t LHE3Weight_MUR1_MUF1_PDF261097;
    Float_t LHE3Weight_MUR1_MUF1_PDF261098;
    Float_t LHE3Weight_MUR1_MUF1_PDF261099;
    Float_t LHE3Weight_MUR1_MUF1_PDF261100;
    Float_t LHE3Weight_MUR1_MUF1_PDF269000;
    Float_t LHE3Weight_MUR1_MUF1_PDF270000;
    Float_t LHE3Weight_MUR1_MUF2_PDF261000;
    Float_t LHE3Weight_MUR2_MUF1_PDF261000;
    Float_t LHE3Weight_MUR2_MUF2_PDF261000;

    // List of branches
    TBranch *b_trigWeight_metTrig;                                      //!
    TBranch *b_trigMatch_metTrig;                                       //!
    TBranch *b_trigWeight_singleLepTrig;                                //!
    TBranch *b_trigMatch_singleLepTrig;                                 //!
    TBranch *b_nLep_base;                                               //!
    TBranch *b_nLep_signal;                                             //!
    TBranch *b_mu;                                                      //!
    TBranch *b_actual_mu;                                               //!
    TBranch *b_AnalysisType;                                            //!
    TBranch *b_lep1Charge;                                              //!
    TBranch *b_isBadMuon;                                               //!
    TBranch *b_muQOverP;                                                //!
    TBranch *b_muQOverPErr;                                             //!
    TBranch *b_muQuality;                                               //!
    TBranch *b_nVtx;                                                    //!
    TBranch *b_nJet30;                                                  //!
    TBranch *b_nJet25;                                                  //!
    TBranch *b_nBJet30_MV2c10;                                          //!
    TBranch *b_nBJet25_MV2c10;                                          //!
    TBranch *b_NPairs;                                                  //!
    TBranch *b_met;                                                     //!
    TBranch *b_mt;                                                      //!
    TBranch *b_mct;                                                     //!
    TBranch *b_mct2;                                                    //!
    TBranch *b_mlb1;                                                    //!
    TBranch *b_mlj1;                                                    //!
    TBranch *b_mbb;                                                     //!
    TBranch *b_meffInc30;                                               //!
    TBranch *b_Ht30;                                                    //!
    TBranch *b_lep1Pt;                                                  //!
    TBranch *b_lep1Eta;                                                 //!
    TBranch *b_lep1Phi;                                                 //!
    TBranch *b_lep2Pt;                                                  //!
    TBranch *b_lep2Eta;                                                 //!
    TBranch *b_lep2Phi;                                                 //!
    TBranch *b_LepAplanarity;                                           //!
    TBranch *b_JetAplanarity;                                           //!
    TBranch *b_dRJet;                                                   //!
    TBranch *b_jet1Pt;                                                  //!
    TBranch *b_jet1Eta;                                                 //!
    TBranch *b_jet1Phi;                                                 //!
    TBranch *b_jet1MV2c10;                                              //!
    TBranch *b_jet2Pt;                                                  //!
    TBranch *b_jet2Eta;                                                 //!
    TBranch *b_jet2Phi;                                                 //!
    TBranch *b_jet3Pt;                                                  //!
    TBranch *b_jet3Eta;                                                 //!
    TBranch *b_jet3Phi;                                                 //!
    TBranch *b_jet4Pt;                                                  //!
    TBranch *b_jet4Eta;                                                 //!
    TBranch *b_jet4Phi;                                                 //!
    TBranch *b_jet5Pt;                                                  //!
    TBranch *b_jet6Pt;                                                  //!
    TBranch *b_bJet1Pt;                                                 //!
    TBranch *b_bJet2Pt;                                                 //!
    TBranch *b_TST_Et;                                                  //!
    TBranch *b_TST_Phi;                                                 //!
    TBranch *b_met_Phi;                                                 //!
    TBranch *b_met_Signif;                                              //!
    TBranch *b_deltaPhi_MET_TST_Phi;                                    //!
    TBranch *b_leptonTrigWeight;                                        //!
    TBranch *b_pileupWeight;                                            //!
    TBranch *b_leptonWeight;                                            //!
    TBranch *b_eventWeight;                                             //!
    TBranch *b_jvtWeight;                                               //!
    TBranch *b_bTagWeight;                                              //!
    TBranch *b_genWeight;                                               //!
    TBranch *b_genWeightUp;                                             //!
    TBranch *b_genWeightDown;                                           //!
    TBranch *b_SherpaVjetsNjetsWeight;                                  //!
    TBranch *b_HLT_xe70;                                                //!
    TBranch *b_HLT_xe70_mht;                                            //!
    TBranch *b_HLT_xe70_mht_wEFMu;                                      //!
    TBranch *b_HLT_xe70_tc_lcw;                                         //!
    TBranch *b_HLT_xe70_tc_lcw_wEFMu;                                   //!
    TBranch *b_HLT_xe80_tc_lcw_L1XE50;                                  //!
    TBranch *b_HLT_xe90_tc_lcw_L1XE50;                                  //!
    TBranch *b_HLT_xe90_mht_L1XE50;                                     //!
    TBranch *b_HLT_xe90_tc_lcw_wEFMu_L1XE50;                            //!
    TBranch *b_HLT_xe90_mht_wEFMu_L1XE50;                               //!
    TBranch *b_HLT_xe100_L1XE50;                                        //!
    TBranch *b_HLT_xe100_wEFMu_L1XE50;                                  //!
    TBranch *b_HLT_xe100_tc_lcw_L1XE50;                                 //!
    TBranch *b_HLT_xe100_mht_L1XE50;                                    //!
    TBranch *b_HLT_xe100_tc_lcw_wEFMu_L1XE50;                           //!
    TBranch *b_HLT_xe100_mht_wEFMu_L1XE50;                              //!
    TBranch *b_HLT_xe110_L1XE50;                                        //!
    TBranch *b_HLT_xe110_tc_em_L1XE50;                                  //!
    TBranch *b_HLT_xe110_wEFMu_L1XE50;                                  //!
    TBranch *b_HLT_xe110_tc_em_wEFMu_L1XE50;                            //!
    TBranch *b_HLT_xe110_mht_L1XE50;                                    //!
    TBranch *b_HLT_xe110_pufit_L1XE55;                                  //!
    TBranch *b_HLT_xe110_pufit_xe70_L1XE50;                             //!
    TBranch *b_HLT_noalg_L1J400;                                        //!
    TBranch *b_match_HLT_mu26_ivarmedium;                               //!
    TBranch *b_match_HLT_mu50;                                          //!
    TBranch *b_match_HLT_mu60_0eta105_msonly;                           //!
    TBranch *b_match_HLT_e26_lhtight_nod0_ivarloose;                    //!
    TBranch *b_match_HLT_e60_lhmedium_nod0;                             //!
    TBranch *b_match_HLT_e140_lhloose_nod0;                             //!
    TBranch *b_match_HLT_e300_etcut;                                    //!
    TBranch *b_DecayModeTTbar;                                          //!
    TBranch *b_ttbarNNLOWeight;                                         //!
    TBranch *b_ttbarNNLOWeightUp;                                       //!
    TBranch *b_ttbarNNLOWeightDown;                                     //!
    TBranch *b_truthTopPt;                                              //!
    TBranch *b_truthAntiTopPt;                                          //!
    TBranch *b_truthTtbarPt;                                            //!
    TBranch *b_truthTtbarM;                                             //!
    TBranch *b_x1;                                                      //!
    TBranch *b_x2;                                                      //!
    TBranch *b_pdf1;                                                    //!
    TBranch *b_pdf2;                                                    //!
    TBranch *b_scalePDF;                                                //!
    TBranch *b_id1;                                                     //!
    TBranch *b_id2;                                                     //!
    TBranch *b_lep1Type;                                                //!
    TBranch *b_lep1Origin;                                              //!
    TBranch *b_ptcone20;                                                //!
    TBranch *b_IsoFCTight;                                              //!
    TBranch *b_IsoFCLoose;                                              //!
    TBranch *b_IsoGradient;                                             //!
    TBranch *b_IsoFCTightTrackOnly;                                     //!
    TBranch *b_IsoFixedCutHighPtTrackOnly;                              //!
    TBranch *b_IsoFCHighPtCaloOnly;                                     //!
    TBranch *b_GenHt;                                                   //!
    TBranch *b_GenMET;                                                  //!
    TBranch *b_leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down;   //!
    TBranch *b_leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up;     //!
    TBranch *b_leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down;  //!
    TBranch *b_leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up;    //!
    TBranch *b_leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down; //!
    TBranch *b_leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up;   //!
    TBranch *b_leptonWeight_MUON_EFF_BADMUON_STAT__1down;               //!
    TBranch *b_leptonWeight_MUON_EFF_BADMUON_STAT__1up;                 //!
    TBranch *b_leptonWeight_MUON_EFF_BADMUON_SYS__1down;                //!
    TBranch *b_leptonWeight_MUON_EFF_BADMUON_SYS__1up;                  //!
    TBranch *b_leptonWeight_MUON_EFF_ISO_STAT__1down;                   //!
    TBranch *b_leptonWeight_MUON_EFF_ISO_STAT__1up;                     //!
    TBranch *b_leptonWeight_MUON_EFF_ISO_SYS__1down;                    //!
    TBranch *b_leptonWeight_MUON_EFF_ISO_SYS__1up;                      //!
    TBranch *b_leptonWeight_MUON_EFF_RECO_STAT__1down;                  //!
    TBranch *b_leptonWeight_MUON_EFF_RECO_STAT__1up;                    //!
    TBranch *b_leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down;            //!
    TBranch *b_leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up;              //!
    TBranch *b_leptonWeight_MUON_EFF_RECO_SYS__1down;                   //!
    TBranch *b_leptonWeight_MUON_EFF_RECO_SYS__1up;                     //!
    TBranch *b_leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down;             //!
    TBranch *b_leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up;               //!
    TBranch *b_leptonWeight_MUON_EFF_TTVA_STAT__1down;                  //!
    TBranch *b_leptonWeight_MUON_EFF_TTVA_STAT__1up;                    //!
    TBranch *b_leptonWeight_MUON_EFF_TTVA_SYS__1down;                   //!
    TBranch *b_leptonWeight_MUON_EFF_TTVA_SYS__1up;                     //!
    TBranch *b_bTagWeight_FT_EFF_B_systematics__1down;                  //!
    TBranch *b_bTagWeight_FT_EFF_B_systematics__1up;                    //!
    TBranch *b_bTagWeight_FT_EFF_C_systematics__1down;                  //!
    TBranch *b_bTagWeight_FT_EFF_C_systematics__1up;                    //!
    TBranch *b_bTagWeight_FT_EFF_Light_systematics__1down;              //!
    TBranch *b_bTagWeight_FT_EFF_Light_systematics__1up;                //!
    TBranch *b_bTagWeight_FT_EFF_extrapolation__1down;                  //!
    TBranch *b_bTagWeight_FT_EFF_extrapolation__1up;                    //!
    TBranch *b_bTagWeight_FT_EFF_extrapolation_from_charm__1down;       //!
    TBranch *b_bTagWeight_FT_EFF_extrapolation_from_charm__1up;         //!
    TBranch *b_jvtWeight_JET_JvtEfficiency__1down;                      //!
    TBranch *b_jvtWeight_JET_JvtEfficiency__1up;                        //!
    TBranch *b_pileupWeightUp;                                          //!
    TBranch *b_pileupWeightDown;                                        //!
    TBranch *b_PRWHash;                                                 //!
    TBranch *b_EventNumber;                                             //!
    TBranch *b_xsec;                                                    //!
    TBranch *b_TrueHt;                                                  //!
    TBranch *b_DatasetNumber;                                           //!
    TBranch *b_RunNumber;                                               //!
    TBranch *b_RandomRunNumber;                                         //!
    TBranch *b_FS;                                                      //!
    TBranch *b_LHE3Weight_MUR0_5_MUF0_5_PDF261000;                      //!
    TBranch *b_LHE3Weight_MUR0_5_MUF1_PDF261000;                        //!
    TBranch *b_LHE3Weight_MUR1_MUF0_5_PDF261000;                        //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF13000;                           //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF25300;                           //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261000;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261001;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261002;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261003;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261004;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261005;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261006;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261007;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261008;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261009;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261010;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261011;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261012;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261013;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261014;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261015;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261016;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261017;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261018;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261019;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261020;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261021;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261022;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261023;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261024;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261025;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261026;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261027;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261028;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261029;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261030;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261031;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261032;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261033;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261034;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261035;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261036;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261037;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261038;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261039;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261040;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261041;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261042;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261043;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261044;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261045;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261046;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261047;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261048;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261049;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261050;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261051;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261052;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261053;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261054;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261055;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261056;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261057;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261058;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261059;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261060;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261061;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261062;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261063;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261064;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261065;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261066;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261067;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261068;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261069;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261070;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261071;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261072;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261073;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261074;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261075;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261076;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261077;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261078;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261079;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261080;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261081;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261082;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261083;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261084;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261085;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261086;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261087;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261088;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261089;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261090;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261091;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261092;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261093;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261094;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261095;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261096;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261097;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261098;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261099;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF261100;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF269000;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF1_PDF270000;                          //!
    TBranch *b_LHE3Weight_MUR1_MUF2_PDF261000;                          //!
    TBranch *b_LHE3Weight_MUR2_MUF1_PDF261000;                          //!
    TBranch *b_LHE3Weight_MUR2_MUF2_PDF261000;                          //!

    void Init(TChain *tree) {
        std::lock_guard<std::mutex> lock(_mutex);
        // The Init() function is called when the AnaTree needs to initialize
        // a new tree or chain. Typically here the branch addresses and branch
        // pointers of the tree will be set.
        // It is normally not necessary to make changes to the generated
        // code, but the routine can be extended by the user if needed.
        // Init() will be called many times when running on PROOF
        // (once per file to be processed).

        // Set branch addresses and branch pointers
        fChain = tree;

        fChain->SetBranchAddress("trigWeight_metTrig", &trigWeight_metTrig, &b_trigWeight_metTrig);
        fChain->SetBranchAddress("trigMatch_metTrig", &trigMatch_metTrig, &b_trigMatch_metTrig);
        fChain->SetBranchAddress("trigWeight_singleLepTrig", &trigWeight_singleLepTrig, &b_trigWeight_singleLepTrig);
        fChain->SetBranchAddress("trigMatch_singleLepTrig", &trigMatch_singleLepTrig, &b_trigMatch_singleLepTrig);
        fChain->SetBranchAddress("nLep_base", &nLep_base, &b_nLep_base);
        fChain->SetBranchAddress("nLep_signal", &nLep_signal, &b_nLep_signal);
        fChain->SetBranchAddress("mu", &mu, &b_mu);
        fChain->SetBranchAddress("actual_mu", &actual_mu, &b_actual_mu);
        fChain->SetBranchAddress("AnalysisType", &AnalysisType, &b_AnalysisType);
        fChain->SetBranchAddress("lep1Charge", &lep1Charge, &b_lep1Charge);
        fChain->SetBranchAddress("isBadMuon", &isBadMuon, &b_isBadMuon);
        fChain->SetBranchAddress("muQOverP", &muQOverP, &b_muQOverP);
        fChain->SetBranchAddress("muQOverPErr", &muQOverPErr, &b_muQOverPErr);
        fChain->SetBranchAddress("muQuality", &muQuality, &b_muQuality);
        fChain->SetBranchAddress("nVtx", &nVtx, &b_nVtx);
        fChain->SetBranchAddress("nJet30", &nJet30, &b_nJet30);
        fChain->SetBranchAddress("nJet25", &nJet25, &b_nJet25);
        fChain->SetBranchAddress("nBJet30_MV2c10", &nBJet30_MV2c10, &b_nBJet30_MV2c10);
        fChain->SetBranchAddress("nBJet25_MV2c10", &nBJet25_MV2c10, &b_nBJet25_MV2c10);
        fChain->SetBranchAddress("NPairs", &NPairs, &b_NPairs);
        fChain->SetBranchAddress("met", &met, &b_met);
        fChain->SetBranchAddress("mt", &mt, &b_mt);
        fChain->SetBranchAddress("mct", &mct, &b_mct);
        fChain->SetBranchAddress("mct2", &mct2, &b_mct2);
        fChain->SetBranchAddress("mlb1", &mlb1, &b_mlb1);
        fChain->SetBranchAddress("mlj1", &mlj1, &b_mlj1);
        fChain->SetBranchAddress("mbb", &mbb, &b_mbb);
        fChain->SetBranchAddress("meffInc30", &meffInc30, &b_meffInc30);
        fChain->SetBranchAddress("Ht30", &Ht30, &b_Ht30);
        fChain->SetBranchAddress("lep1Pt", &lep1Pt, &b_lep1Pt);
        fChain->SetBranchAddress("lep1Eta", &lep1Eta, &b_lep1Eta);
        fChain->SetBranchAddress("lep1Phi", &lep1Phi, &b_lep1Phi);
        fChain->SetBranchAddress("lep2Pt", &lep2Pt, &b_lep2Pt);
        fChain->SetBranchAddress("lep2Eta", &lep2Eta, &b_lep2Eta);
        fChain->SetBranchAddress("lep2Phi", &lep2Phi, &b_lep2Phi);
        fChain->SetBranchAddress("LepAplanarity", &LepAplanarity, &b_LepAplanarity);
        fChain->SetBranchAddress("JetAplanarity", &JetAplanarity, &b_JetAplanarity);
        fChain->SetBranchAddress("dRJet", &dRJet, &b_dRJet);
        fChain->SetBranchAddress("jet1Pt", &jet1Pt, &b_jet1Pt);
        fChain->SetBranchAddress("jet1Eta", &jet1Eta, &b_jet1Eta);
        fChain->SetBranchAddress("jet1Phi", &jet1Phi, &b_jet1Phi);
        fChain->SetBranchAddress("jet1MV2c10", &jet1MV2c10, &b_jet1MV2c10);
        fChain->SetBranchAddress("jet2Pt", &jet2Pt, &b_jet2Pt);
        fChain->SetBranchAddress("jet2Eta", &jet2Eta, &b_jet2Eta);
        fChain->SetBranchAddress("jet2Phi", &jet2Phi, &b_jet2Phi);
        fChain->SetBranchAddress("jet3Pt", &jet3Pt, &b_jet3Pt);
        fChain->SetBranchAddress("jet3Eta", &jet3Eta, &b_jet3Eta);
        fChain->SetBranchAddress("jet3Phi", &jet3Phi, &b_jet3Phi);
        fChain->SetBranchAddress("jet4Pt", &jet4Pt, &b_jet4Pt);
        fChain->SetBranchAddress("jet4Eta", &jet4Eta, &b_jet4Eta);
        fChain->SetBranchAddress("jet4Phi", &jet4Phi, &b_jet4Phi);
        fChain->SetBranchAddress("jet5Pt", &jet5Pt, &b_jet5Pt);
        fChain->SetBranchAddress("jet6Pt", &jet6Pt, &b_jet6Pt);
        fChain->SetBranchAddress("bJet1Pt", &bJet1Pt, &b_bJet1Pt);
        fChain->SetBranchAddress("bJet2Pt", &bJet2Pt, &b_bJet2Pt);
        fChain->SetBranchAddress("TST_Et", &TST_Et, &b_TST_Et);
        fChain->SetBranchAddress("TST_Phi", &TST_Phi, &b_TST_Phi);
        fChain->SetBranchAddress("met_Phi", &met_Phi, &b_met_Phi);
        fChain->SetBranchAddress("met_Signif", &met_Signif, &b_met_Signif);
        fChain->SetBranchAddress("deltaPhi_MET_TST_Phi", &deltaPhi_MET_TST_Phi, &b_deltaPhi_MET_TST_Phi);
        fChain->SetBranchAddress("leptonTrigWeight", &leptonTrigWeight, &b_leptonTrigWeight);
        fChain->SetBranchAddress("pileupWeight", &pileupWeight, &b_pileupWeight);
        fChain->SetBranchAddress("leptonWeight", &leptonWeight, &b_leptonWeight);
        fChain->SetBranchAddress("eventWeight", &eventWeight, &b_eventWeight);
        fChain->SetBranchAddress("jvtWeight", &jvtWeight, &b_jvtWeight);
        fChain->SetBranchAddress("bTagWeight", &bTagWeight, &b_bTagWeight);
        fChain->SetBranchAddress("genWeight", &genWeight, &b_genWeight);
        fChain->SetBranchAddress("genWeightUp", &genWeightUp, &b_genWeightUp);
        fChain->SetBranchAddress("genWeightDown", &genWeightDown, &b_genWeightDown);
        fChain->SetBranchAddress("SherpaVjetsNjetsWeight", &SherpaVjetsNjetsWeight, &b_SherpaVjetsNjetsWeight);
        fChain->SetBranchAddress("HLT_xe70", &HLT_xe70, &b_HLT_xe70);
        fChain->SetBranchAddress("HLT_xe70_mht", &HLT_xe70_mht, &b_HLT_xe70_mht);
        fChain->SetBranchAddress("HLT_xe70_mht_wEFMu", &HLT_xe70_mht_wEFMu, &b_HLT_xe70_mht_wEFMu);
        fChain->SetBranchAddress("HLT_xe70_tc_lcw", &HLT_xe70_tc_lcw, &b_HLT_xe70_tc_lcw);
        fChain->SetBranchAddress("HLT_xe70_tc_lcw_wEFMu", &HLT_xe70_tc_lcw_wEFMu, &b_HLT_xe70_tc_lcw_wEFMu);
        fChain->SetBranchAddress("HLT_xe80_tc_lcw_L1XE50", &HLT_xe80_tc_lcw_L1XE50, &b_HLT_xe80_tc_lcw_L1XE50);
        fChain->SetBranchAddress("HLT_xe90_tc_lcw_L1XE50", &HLT_xe90_tc_lcw_L1XE50, &b_HLT_xe90_tc_lcw_L1XE50);
        fChain->SetBranchAddress("HLT_xe90_mht_L1XE50", &HLT_xe90_mht_L1XE50, &b_HLT_xe90_mht_L1XE50);
        fChain->SetBranchAddress("HLT_xe90_tc_lcw_wEFMu_L1XE50", &HLT_xe90_tc_lcw_wEFMu_L1XE50, &b_HLT_xe90_tc_lcw_wEFMu_L1XE50);
        fChain->SetBranchAddress("HLT_xe90_mht_wEFMu_L1XE50", &HLT_xe90_mht_wEFMu_L1XE50, &b_HLT_xe90_mht_wEFMu_L1XE50);
        fChain->SetBranchAddress("HLT_xe100_L1XE50", &HLT_xe100_L1XE50, &b_HLT_xe100_L1XE50);
        fChain->SetBranchAddress("HLT_xe100_wEFMu_L1XE50", &HLT_xe100_wEFMu_L1XE50, &b_HLT_xe100_wEFMu_L1XE50);
        fChain->SetBranchAddress("HLT_xe100_tc_lcw_L1XE50", &HLT_xe100_tc_lcw_L1XE50, &b_HLT_xe100_tc_lcw_L1XE50);
        fChain->SetBranchAddress("HLT_xe100_mht_L1XE50", &HLT_xe100_mht_L1XE50, &b_HLT_xe100_mht_L1XE50);
        fChain->SetBranchAddress("HLT_xe100_tc_lcw_wEFMu_L1XE50", &HLT_xe100_tc_lcw_wEFMu_L1XE50, &b_HLT_xe100_tc_lcw_wEFMu_L1XE50);
        fChain->SetBranchAddress("HLT_xe100_mht_wEFMu_L1XE50", &HLT_xe100_mht_wEFMu_L1XE50, &b_HLT_xe100_mht_wEFMu_L1XE50);
        fChain->SetBranchAddress("HLT_xe110_L1XE50", &HLT_xe110_L1XE50, &b_HLT_xe110_L1XE50);
        fChain->SetBranchAddress("HLT_xe110_tc_em_L1XE50", &HLT_xe110_tc_em_L1XE50, &b_HLT_xe110_tc_em_L1XE50);
        fChain->SetBranchAddress("HLT_xe110_wEFMu_L1XE50", &HLT_xe110_wEFMu_L1XE50, &b_HLT_xe110_wEFMu_L1XE50);
        fChain->SetBranchAddress("HLT_xe110_tc_em_wEFMu_L1XE50", &HLT_xe110_tc_em_wEFMu_L1XE50, &b_HLT_xe110_tc_em_wEFMu_L1XE50);
        fChain->SetBranchAddress("HLT_xe110_mht_L1XE50", &HLT_xe110_mht_L1XE50, &b_HLT_xe110_mht_L1XE50);
        fChain->SetBranchAddress("HLT_xe110_pufit_L1XE55", &HLT_xe110_pufit_L1XE55, &b_HLT_xe110_pufit_L1XE55);
        fChain->SetBranchAddress("HLT_xe110_pufit_xe70_L1XE50", &HLT_xe110_pufit_xe70_L1XE50, &b_HLT_xe110_pufit_xe70_L1XE50);
        fChain->SetBranchAddress("HLT_noalg_L1J400", &HLT_noalg_L1J400, &b_HLT_noalg_L1J400);
        fChain->SetBranchAddress("match_HLT_mu26_ivarmedium", &match_HLT_mu26_ivarmedium, &b_match_HLT_mu26_ivarmedium);
        fChain->SetBranchAddress("match_HLT_mu50", &match_HLT_mu50, &b_match_HLT_mu50);
        fChain->SetBranchAddress("match_HLT_mu60_0eta105_msonly", &match_HLT_mu60_0eta105_msonly, &b_match_HLT_mu60_0eta105_msonly);
        fChain->SetBranchAddress("match_HLT_e26_lhtight_nod0_ivarloose", &match_HLT_e26_lhtight_nod0_ivarloose,
                                 &b_match_HLT_e26_lhtight_nod0_ivarloose);
        fChain->SetBranchAddress("match_HLT_e60_lhmedium_nod0", &match_HLT_e60_lhmedium_nod0, &b_match_HLT_e60_lhmedium_nod0);
        fChain->SetBranchAddress("match_HLT_e140_lhloose_nod0", &match_HLT_e140_lhloose_nod0, &b_match_HLT_e140_lhloose_nod0);
        fChain->SetBranchAddress("match_HLT_e300_etcut", &match_HLT_e300_etcut, &b_match_HLT_e300_etcut);
        fChain->SetBranchAddress("DecayModeTTbar", &DecayModeTTbar, &b_DecayModeTTbar);
        fChain->SetBranchAddress("ttbarNNLOWeight", &ttbarNNLOWeight, &b_ttbarNNLOWeight);
        fChain->SetBranchAddress("ttbarNNLOWeightUp", &ttbarNNLOWeightUp, &b_ttbarNNLOWeightUp);
        fChain->SetBranchAddress("ttbarNNLOWeightDown", &ttbarNNLOWeightDown, &b_ttbarNNLOWeightDown);
        fChain->SetBranchAddress("truthTopPt", &truthTopPt, &b_truthTopPt);
        fChain->SetBranchAddress("truthAntiTopPt", &truthAntiTopPt, &b_truthAntiTopPt);
        fChain->SetBranchAddress("truthTtbarPt", &truthTtbarPt, &b_truthTtbarPt);
        fChain->SetBranchAddress("truthTtbarM", &truthTtbarM, &b_truthTtbarM);
        fChain->SetBranchAddress("x1", &x1, &b_x1);
        fChain->SetBranchAddress("x2", &x2, &b_x2);
        fChain->SetBranchAddress("pdf1", &pdf1, &b_pdf1);
        fChain->SetBranchAddress("pdf2", &pdf2, &b_pdf2);
        fChain->SetBranchAddress("scalePDF", &scalePDF, &b_scalePDF);
        fChain->SetBranchAddress("id1", &id1, &b_id1);
        fChain->SetBranchAddress("id2", &id2, &b_id2);
        fChain->SetBranchAddress("lep1Type", &lep1Type, &b_lep1Type);
        fChain->SetBranchAddress("lep1Origin", &lep1Origin, &b_lep1Origin);
        fChain->SetBranchAddress("ptcone20", &ptcone20, &b_ptcone20);
        fChain->SetBranchAddress("IsoFCTight", &IsoFCTight, &b_IsoFCTight);
        fChain->SetBranchAddress("IsoFCLoose", &IsoFCLoose, &b_IsoFCLoose);
        fChain->SetBranchAddress("IsoGradient", &IsoGradient, &b_IsoGradient);
        fChain->SetBranchAddress("IsoFCTightTrackOnly", &IsoFCTightTrackOnly, &b_IsoFCTightTrackOnly);
        fChain->SetBranchAddress("IsoFixedCutHighPtTrackOnly", &IsoFixedCutHighPtTrackOnly, &b_IsoFixedCutHighPtTrackOnly);
        fChain->SetBranchAddress("IsoFCHighPtCaloOnly", &IsoFCHighPtCaloOnly, &b_IsoFCHighPtCaloOnly);
        fChain->SetBranchAddress("GenHt", &GenHt, &b_GenHt);
        fChain->SetBranchAddress("GenMET", &GenMET, &b_GenMET);
        fChain->SetBranchAddress("leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down", &leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down,
                                 &b_leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        fChain->SetBranchAddress("leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up", &leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up,
                                 &b_leptonWeight_EL_EFF_ID_TOTAL_1NPCOR_PLUS_UNCOR__1up);
        fChain->SetBranchAddress("leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down", &leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down,
                                 &b_leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        fChain->SetBranchAddress("leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up", &leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up,
                                 &b_leptonWeight_EL_EFF_Iso_TOTAL_1NPCOR_PLUS_UNCOR__1up);
        fChain->SetBranchAddress("leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down", &leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down,
                                 &b_leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1down);
        fChain->SetBranchAddress("leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up", &leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up,
                                 &b_leptonWeight_EL_EFF_Reco_TOTAL_1NPCOR_PLUS_UNCOR__1up);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_BADMUON_STAT__1down", &leptonWeight_MUON_EFF_BADMUON_STAT__1down,
                                 &b_leptonWeight_MUON_EFF_BADMUON_STAT__1down);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_BADMUON_STAT__1up", &leptonWeight_MUON_EFF_BADMUON_STAT__1up,
                                 &b_leptonWeight_MUON_EFF_BADMUON_STAT__1up);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_BADMUON_SYS__1down", &leptonWeight_MUON_EFF_BADMUON_SYS__1down,
                                 &b_leptonWeight_MUON_EFF_BADMUON_SYS__1down);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_BADMUON_SYS__1up", &leptonWeight_MUON_EFF_BADMUON_SYS__1up,
                                 &b_leptonWeight_MUON_EFF_BADMUON_SYS__1up);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_ISO_STAT__1down", &leptonWeight_MUON_EFF_ISO_STAT__1down,
                                 &b_leptonWeight_MUON_EFF_ISO_STAT__1down);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_ISO_STAT__1up", &leptonWeight_MUON_EFF_ISO_STAT__1up, &b_leptonWeight_MUON_EFF_ISO_STAT__1up);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_ISO_SYS__1down", &leptonWeight_MUON_EFF_ISO_SYS__1down,
                                 &b_leptonWeight_MUON_EFF_ISO_SYS__1down);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_ISO_SYS__1up", &leptonWeight_MUON_EFF_ISO_SYS__1up, &b_leptonWeight_MUON_EFF_ISO_SYS__1up);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_RECO_STAT__1down", &leptonWeight_MUON_EFF_RECO_STAT__1down,
                                 &b_leptonWeight_MUON_EFF_RECO_STAT__1down);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_RECO_STAT__1up", &leptonWeight_MUON_EFF_RECO_STAT__1up,
                                 &b_leptonWeight_MUON_EFF_RECO_STAT__1up);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down", &leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down,
                                 &b_leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1down);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up", &leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up,
                                 &b_leptonWeight_MUON_EFF_RECO_STAT_LOWPT__1up);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_RECO_SYS__1down", &leptonWeight_MUON_EFF_RECO_SYS__1down,
                                 &b_leptonWeight_MUON_EFF_RECO_SYS__1down);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_RECO_SYS__1up", &leptonWeight_MUON_EFF_RECO_SYS__1up, &b_leptonWeight_MUON_EFF_RECO_SYS__1up);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down", &leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down,
                                 &b_leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1down);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up", &leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up,
                                 &b_leptonWeight_MUON_EFF_RECO_SYS_LOWPT__1up);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_TTVA_STAT__1down", &leptonWeight_MUON_EFF_TTVA_STAT__1down,
                                 &b_leptonWeight_MUON_EFF_TTVA_STAT__1down);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_TTVA_STAT__1up", &leptonWeight_MUON_EFF_TTVA_STAT__1up,
                                 &b_leptonWeight_MUON_EFF_TTVA_STAT__1up);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_TTVA_SYS__1down", &leptonWeight_MUON_EFF_TTVA_SYS__1down,
                                 &b_leptonWeight_MUON_EFF_TTVA_SYS__1down);
        fChain->SetBranchAddress("leptonWeight_MUON_EFF_TTVA_SYS__1up", &leptonWeight_MUON_EFF_TTVA_SYS__1up, &b_leptonWeight_MUON_EFF_TTVA_SYS__1up);
        fChain->SetBranchAddress("bTagWeight_FT_EFF_B_systematics__1down", &bTagWeight_FT_EFF_B_systematics__1down,
                                 &b_bTagWeight_FT_EFF_B_systematics__1down);
        fChain->SetBranchAddress("bTagWeight_FT_EFF_B_systematics__1up", &bTagWeight_FT_EFF_B_systematics__1up,
                                 &b_bTagWeight_FT_EFF_B_systematics__1up);
        fChain->SetBranchAddress("bTagWeight_FT_EFF_C_systematics__1down", &bTagWeight_FT_EFF_C_systematics__1down,
                                 &b_bTagWeight_FT_EFF_C_systematics__1down);
        fChain->SetBranchAddress("bTagWeight_FT_EFF_C_systematics__1up", &bTagWeight_FT_EFF_C_systematics__1up,
                                 &b_bTagWeight_FT_EFF_C_systematics__1up);
        fChain->SetBranchAddress("bTagWeight_FT_EFF_Light_systematics__1down", &bTagWeight_FT_EFF_Light_systematics__1down,
                                 &b_bTagWeight_FT_EFF_Light_systematics__1down);
        fChain->SetBranchAddress("bTagWeight_FT_EFF_Light_systematics__1up", &bTagWeight_FT_EFF_Light_systematics__1up,
                                 &b_bTagWeight_FT_EFF_Light_systematics__1up);
        fChain->SetBranchAddress("bTagWeight_FT_EFF_extrapolation__1down", &bTagWeight_FT_EFF_extrapolation__1down,
                                 &b_bTagWeight_FT_EFF_extrapolation__1down);
        fChain->SetBranchAddress("bTagWeight_FT_EFF_extrapolation__1up", &bTagWeight_FT_EFF_extrapolation__1up,
                                 &b_bTagWeight_FT_EFF_extrapolation__1up);
        fChain->SetBranchAddress("bTagWeight_FT_EFF_extrapolation_from_charm__1down", &bTagWeight_FT_EFF_extrapolation_from_charm__1down,
                                 &b_bTagWeight_FT_EFF_extrapolation_from_charm__1down);
        fChain->SetBranchAddress("bTagWeight_FT_EFF_extrapolation_from_charm__1up", &bTagWeight_FT_EFF_extrapolation_from_charm__1up,
                                 &b_bTagWeight_FT_EFF_extrapolation_from_charm__1up);
        fChain->SetBranchAddress("jvtWeight_JET_JvtEfficiency__1down", &jvtWeight_JET_JvtEfficiency__1down, &b_jvtWeight_JET_JvtEfficiency__1down);
        fChain->SetBranchAddress("jvtWeight_JET_JvtEfficiency__1up", &jvtWeight_JET_JvtEfficiency__1up, &b_jvtWeight_JET_JvtEfficiency__1up);
        fChain->SetBranchAddress("pileupWeightUp", &pileupWeightUp, &b_pileupWeightUp);
        fChain->SetBranchAddress("pileupWeightDown", &pileupWeightDown, &b_pileupWeightDown);
        fChain->SetBranchAddress("PRWHash", &PRWHash, &b_PRWHash);
        fChain->SetBranchAddress("EventNumber", &EventNumber, &b_EventNumber);
        fChain->SetBranchAddress("xsec", &xsec, &b_xsec);
        fChain->SetBranchAddress("TrueHt", &TrueHt, &b_TrueHt);
        fChain->SetBranchAddress("DatasetNumber", &DatasetNumber, &b_DatasetNumber);
        fChain->SetBranchAddress("RunNumber", &RunNumber, &b_RunNumber);
        fChain->SetBranchAddress("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
        fChain->SetBranchAddress("FS", &FS, &b_FS);
        fChain->SetBranchAddress("LHE3Weight_MUR0.5_MUF0.5_PDF261000", &LHE3Weight_MUR0_5_MUF0_5_PDF261000, &b_LHE3Weight_MUR0_5_MUF0_5_PDF261000);
        fChain->SetBranchAddress("LHE3Weight_MUR0.5_MUF1_PDF261000", &LHE3Weight_MUR0_5_MUF1_PDF261000, &b_LHE3Weight_MUR0_5_MUF1_PDF261000);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF0.5_PDF261000", &LHE3Weight_MUR1_MUF0_5_PDF261000, &b_LHE3Weight_MUR1_MUF0_5_PDF261000);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF13000", &LHE3Weight_MUR1_MUF1_PDF13000, &b_LHE3Weight_MUR1_MUF1_PDF13000);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF25300", &LHE3Weight_MUR1_MUF1_PDF25300, &b_LHE3Weight_MUR1_MUF1_PDF25300);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261000", &LHE3Weight_MUR1_MUF1_PDF261000, &b_LHE3Weight_MUR1_MUF1_PDF261000);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261001", &LHE3Weight_MUR1_MUF1_PDF261001, &b_LHE3Weight_MUR1_MUF1_PDF261001);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261002", &LHE3Weight_MUR1_MUF1_PDF261002, &b_LHE3Weight_MUR1_MUF1_PDF261002);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261003", &LHE3Weight_MUR1_MUF1_PDF261003, &b_LHE3Weight_MUR1_MUF1_PDF261003);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261004", &LHE3Weight_MUR1_MUF1_PDF261004, &b_LHE3Weight_MUR1_MUF1_PDF261004);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261005", &LHE3Weight_MUR1_MUF1_PDF261005, &b_LHE3Weight_MUR1_MUF1_PDF261005);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261006", &LHE3Weight_MUR1_MUF1_PDF261006, &b_LHE3Weight_MUR1_MUF1_PDF261006);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261007", &LHE3Weight_MUR1_MUF1_PDF261007, &b_LHE3Weight_MUR1_MUF1_PDF261007);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261008", &LHE3Weight_MUR1_MUF1_PDF261008, &b_LHE3Weight_MUR1_MUF1_PDF261008);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261009", &LHE3Weight_MUR1_MUF1_PDF261009, &b_LHE3Weight_MUR1_MUF1_PDF261009);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261010", &LHE3Weight_MUR1_MUF1_PDF261010, &b_LHE3Weight_MUR1_MUF1_PDF261010);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261011", &LHE3Weight_MUR1_MUF1_PDF261011, &b_LHE3Weight_MUR1_MUF1_PDF261011);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261012", &LHE3Weight_MUR1_MUF1_PDF261012, &b_LHE3Weight_MUR1_MUF1_PDF261012);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261013", &LHE3Weight_MUR1_MUF1_PDF261013, &b_LHE3Weight_MUR1_MUF1_PDF261013);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261014", &LHE3Weight_MUR1_MUF1_PDF261014, &b_LHE3Weight_MUR1_MUF1_PDF261014);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261015", &LHE3Weight_MUR1_MUF1_PDF261015, &b_LHE3Weight_MUR1_MUF1_PDF261015);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261016", &LHE3Weight_MUR1_MUF1_PDF261016, &b_LHE3Weight_MUR1_MUF1_PDF261016);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261017", &LHE3Weight_MUR1_MUF1_PDF261017, &b_LHE3Weight_MUR1_MUF1_PDF261017);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261018", &LHE3Weight_MUR1_MUF1_PDF261018, &b_LHE3Weight_MUR1_MUF1_PDF261018);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261019", &LHE3Weight_MUR1_MUF1_PDF261019, &b_LHE3Weight_MUR1_MUF1_PDF261019);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261020", &LHE3Weight_MUR1_MUF1_PDF261020, &b_LHE3Weight_MUR1_MUF1_PDF261020);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261021", &LHE3Weight_MUR1_MUF1_PDF261021, &b_LHE3Weight_MUR1_MUF1_PDF261021);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261022", &LHE3Weight_MUR1_MUF1_PDF261022, &b_LHE3Weight_MUR1_MUF1_PDF261022);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261023", &LHE3Weight_MUR1_MUF1_PDF261023, &b_LHE3Weight_MUR1_MUF1_PDF261023);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261024", &LHE3Weight_MUR1_MUF1_PDF261024, &b_LHE3Weight_MUR1_MUF1_PDF261024);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261025", &LHE3Weight_MUR1_MUF1_PDF261025, &b_LHE3Weight_MUR1_MUF1_PDF261025);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261026", &LHE3Weight_MUR1_MUF1_PDF261026, &b_LHE3Weight_MUR1_MUF1_PDF261026);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261027", &LHE3Weight_MUR1_MUF1_PDF261027, &b_LHE3Weight_MUR1_MUF1_PDF261027);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261028", &LHE3Weight_MUR1_MUF1_PDF261028, &b_LHE3Weight_MUR1_MUF1_PDF261028);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261029", &LHE3Weight_MUR1_MUF1_PDF261029, &b_LHE3Weight_MUR1_MUF1_PDF261029);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261030", &LHE3Weight_MUR1_MUF1_PDF261030, &b_LHE3Weight_MUR1_MUF1_PDF261030);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261031", &LHE3Weight_MUR1_MUF1_PDF261031, &b_LHE3Weight_MUR1_MUF1_PDF261031);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261032", &LHE3Weight_MUR1_MUF1_PDF261032, &b_LHE3Weight_MUR1_MUF1_PDF261032);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261033", &LHE3Weight_MUR1_MUF1_PDF261033, &b_LHE3Weight_MUR1_MUF1_PDF261033);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261034", &LHE3Weight_MUR1_MUF1_PDF261034, &b_LHE3Weight_MUR1_MUF1_PDF261034);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261035", &LHE3Weight_MUR1_MUF1_PDF261035, &b_LHE3Weight_MUR1_MUF1_PDF261035);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261036", &LHE3Weight_MUR1_MUF1_PDF261036, &b_LHE3Weight_MUR1_MUF1_PDF261036);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261037", &LHE3Weight_MUR1_MUF1_PDF261037, &b_LHE3Weight_MUR1_MUF1_PDF261037);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261038", &LHE3Weight_MUR1_MUF1_PDF261038, &b_LHE3Weight_MUR1_MUF1_PDF261038);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261039", &LHE3Weight_MUR1_MUF1_PDF261039, &b_LHE3Weight_MUR1_MUF1_PDF261039);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261040", &LHE3Weight_MUR1_MUF1_PDF261040, &b_LHE3Weight_MUR1_MUF1_PDF261040);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261041", &LHE3Weight_MUR1_MUF1_PDF261041, &b_LHE3Weight_MUR1_MUF1_PDF261041);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261042", &LHE3Weight_MUR1_MUF1_PDF261042, &b_LHE3Weight_MUR1_MUF1_PDF261042);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261043", &LHE3Weight_MUR1_MUF1_PDF261043, &b_LHE3Weight_MUR1_MUF1_PDF261043);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261044", &LHE3Weight_MUR1_MUF1_PDF261044, &b_LHE3Weight_MUR1_MUF1_PDF261044);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261045", &LHE3Weight_MUR1_MUF1_PDF261045, &b_LHE3Weight_MUR1_MUF1_PDF261045);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261046", &LHE3Weight_MUR1_MUF1_PDF261046, &b_LHE3Weight_MUR1_MUF1_PDF261046);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261047", &LHE3Weight_MUR1_MUF1_PDF261047, &b_LHE3Weight_MUR1_MUF1_PDF261047);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261048", &LHE3Weight_MUR1_MUF1_PDF261048, &b_LHE3Weight_MUR1_MUF1_PDF261048);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261049", &LHE3Weight_MUR1_MUF1_PDF261049, &b_LHE3Weight_MUR1_MUF1_PDF261049);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261050", &LHE3Weight_MUR1_MUF1_PDF261050, &b_LHE3Weight_MUR1_MUF1_PDF261050);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261051", &LHE3Weight_MUR1_MUF1_PDF261051, &b_LHE3Weight_MUR1_MUF1_PDF261051);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261052", &LHE3Weight_MUR1_MUF1_PDF261052, &b_LHE3Weight_MUR1_MUF1_PDF261052);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261053", &LHE3Weight_MUR1_MUF1_PDF261053, &b_LHE3Weight_MUR1_MUF1_PDF261053);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261054", &LHE3Weight_MUR1_MUF1_PDF261054, &b_LHE3Weight_MUR1_MUF1_PDF261054);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261055", &LHE3Weight_MUR1_MUF1_PDF261055, &b_LHE3Weight_MUR1_MUF1_PDF261055);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261056", &LHE3Weight_MUR1_MUF1_PDF261056, &b_LHE3Weight_MUR1_MUF1_PDF261056);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261057", &LHE3Weight_MUR1_MUF1_PDF261057, &b_LHE3Weight_MUR1_MUF1_PDF261057);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261058", &LHE3Weight_MUR1_MUF1_PDF261058, &b_LHE3Weight_MUR1_MUF1_PDF261058);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261059", &LHE3Weight_MUR1_MUF1_PDF261059, &b_LHE3Weight_MUR1_MUF1_PDF261059);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261060", &LHE3Weight_MUR1_MUF1_PDF261060, &b_LHE3Weight_MUR1_MUF1_PDF261060);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261061", &LHE3Weight_MUR1_MUF1_PDF261061, &b_LHE3Weight_MUR1_MUF1_PDF261061);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261062", &LHE3Weight_MUR1_MUF1_PDF261062, &b_LHE3Weight_MUR1_MUF1_PDF261062);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261063", &LHE3Weight_MUR1_MUF1_PDF261063, &b_LHE3Weight_MUR1_MUF1_PDF261063);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261064", &LHE3Weight_MUR1_MUF1_PDF261064, &b_LHE3Weight_MUR1_MUF1_PDF261064);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261065", &LHE3Weight_MUR1_MUF1_PDF261065, &b_LHE3Weight_MUR1_MUF1_PDF261065);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261066", &LHE3Weight_MUR1_MUF1_PDF261066, &b_LHE3Weight_MUR1_MUF1_PDF261066);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261067", &LHE3Weight_MUR1_MUF1_PDF261067, &b_LHE3Weight_MUR1_MUF1_PDF261067);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261068", &LHE3Weight_MUR1_MUF1_PDF261068, &b_LHE3Weight_MUR1_MUF1_PDF261068);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261069", &LHE3Weight_MUR1_MUF1_PDF261069, &b_LHE3Weight_MUR1_MUF1_PDF261069);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261070", &LHE3Weight_MUR1_MUF1_PDF261070, &b_LHE3Weight_MUR1_MUF1_PDF261070);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261071", &LHE3Weight_MUR1_MUF1_PDF261071, &b_LHE3Weight_MUR1_MUF1_PDF261071);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261072", &LHE3Weight_MUR1_MUF1_PDF261072, &b_LHE3Weight_MUR1_MUF1_PDF261072);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261073", &LHE3Weight_MUR1_MUF1_PDF261073, &b_LHE3Weight_MUR1_MUF1_PDF261073);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261074", &LHE3Weight_MUR1_MUF1_PDF261074, &b_LHE3Weight_MUR1_MUF1_PDF261074);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261075", &LHE3Weight_MUR1_MUF1_PDF261075, &b_LHE3Weight_MUR1_MUF1_PDF261075);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261076", &LHE3Weight_MUR1_MUF1_PDF261076, &b_LHE3Weight_MUR1_MUF1_PDF261076);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261077", &LHE3Weight_MUR1_MUF1_PDF261077, &b_LHE3Weight_MUR1_MUF1_PDF261077);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261078", &LHE3Weight_MUR1_MUF1_PDF261078, &b_LHE3Weight_MUR1_MUF1_PDF261078);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261079", &LHE3Weight_MUR1_MUF1_PDF261079, &b_LHE3Weight_MUR1_MUF1_PDF261079);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261080", &LHE3Weight_MUR1_MUF1_PDF261080, &b_LHE3Weight_MUR1_MUF1_PDF261080);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261081", &LHE3Weight_MUR1_MUF1_PDF261081, &b_LHE3Weight_MUR1_MUF1_PDF261081);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261082", &LHE3Weight_MUR1_MUF1_PDF261082, &b_LHE3Weight_MUR1_MUF1_PDF261082);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261083", &LHE3Weight_MUR1_MUF1_PDF261083, &b_LHE3Weight_MUR1_MUF1_PDF261083);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261084", &LHE3Weight_MUR1_MUF1_PDF261084, &b_LHE3Weight_MUR1_MUF1_PDF261084);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261085", &LHE3Weight_MUR1_MUF1_PDF261085, &b_LHE3Weight_MUR1_MUF1_PDF261085);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261086", &LHE3Weight_MUR1_MUF1_PDF261086, &b_LHE3Weight_MUR1_MUF1_PDF261086);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261087", &LHE3Weight_MUR1_MUF1_PDF261087, &b_LHE3Weight_MUR1_MUF1_PDF261087);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261088", &LHE3Weight_MUR1_MUF1_PDF261088, &b_LHE3Weight_MUR1_MUF1_PDF261088);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261089", &LHE3Weight_MUR1_MUF1_PDF261089, &b_LHE3Weight_MUR1_MUF1_PDF261089);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261090", &LHE3Weight_MUR1_MUF1_PDF261090, &b_LHE3Weight_MUR1_MUF1_PDF261090);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261091", &LHE3Weight_MUR1_MUF1_PDF261091, &b_LHE3Weight_MUR1_MUF1_PDF261091);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261092", &LHE3Weight_MUR1_MUF1_PDF261092, &b_LHE3Weight_MUR1_MUF1_PDF261092);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261093", &LHE3Weight_MUR1_MUF1_PDF261093, &b_LHE3Weight_MUR1_MUF1_PDF261093);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261094", &LHE3Weight_MUR1_MUF1_PDF261094, &b_LHE3Weight_MUR1_MUF1_PDF261094);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261095", &LHE3Weight_MUR1_MUF1_PDF261095, &b_LHE3Weight_MUR1_MUF1_PDF261095);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261096", &LHE3Weight_MUR1_MUF1_PDF261096, &b_LHE3Weight_MUR1_MUF1_PDF261096);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261097", &LHE3Weight_MUR1_MUF1_PDF261097, &b_LHE3Weight_MUR1_MUF1_PDF261097);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261098", &LHE3Weight_MUR1_MUF1_PDF261098, &b_LHE3Weight_MUR1_MUF1_PDF261098);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261099", &LHE3Weight_MUR1_MUF1_PDF261099, &b_LHE3Weight_MUR1_MUF1_PDF261099);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF261100", &LHE3Weight_MUR1_MUF1_PDF261100, &b_LHE3Weight_MUR1_MUF1_PDF261100);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF269000", &LHE3Weight_MUR1_MUF1_PDF269000, &b_LHE3Weight_MUR1_MUF1_PDF269000);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF1_PDF270000", &LHE3Weight_MUR1_MUF1_PDF270000, &b_LHE3Weight_MUR1_MUF1_PDF270000);
        fChain->SetBranchAddress("LHE3Weight_MUR1_MUF2_PDF261000", &LHE3Weight_MUR1_MUF2_PDF261000, &b_LHE3Weight_MUR1_MUF2_PDF261000);
        fChain->SetBranchAddress("LHE3Weight_MUR2_MUF1_PDF261000", &LHE3Weight_MUR2_MUF1_PDF261000, &b_LHE3Weight_MUR2_MUF1_PDF261000);
        fChain->SetBranchAddress("LHE3Weight_MUR2_MUF2_PDF261000", &LHE3Weight_MUR2_MUF2_PDF261000, &b_LHE3Weight_MUR2_MUF2_PDF261000);
    }
};
