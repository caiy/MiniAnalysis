#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

// Header file for the classes stored in the TTree if any.
#include <vector>

#include "AnaObjs.h"
#include "AnaTree.h"
#include "TLorentzVector.h"
#include "TVector3.h"

class xTFwTree : public AnaTree {
public:
    xTFwTree(TChain *chain) : AnaTree(chain) { Init(this->fChain); }

    // Declaration of leaf types
    Double_t AMI_cross_section;
    UInt_t HLT_mu20_iloose_L1MU15;
    UInt_t HLT_mu26_ivarmedium;
    UInt_t HLT_mu50;
    Float_t NOMINAL_pileup_combined_weight;
    UInt_t NOMINAL_pileup_random_run_number;
    Float_t PRW_DATASF_1down_pileup_combined_weight;
    UInt_t PRW_DATASF_1down_pileup_random_run_number;
    Float_t PRW_DATASF_1up_pileup_combined_weight;
    UInt_t PRW_DATASF_1up_pileup_random_run_number;
    Double_t cross_section;
    UInt_t event_clean_EC_LooseBad;
    Int_t event_clean_detector_core;
    Int_t event_is_bad_batman;
    ULong64_t event_number;
    Double_t filter_efficiency;
    UInt_t jet_0;
    Float_t jet_0_Bottom_DL1_score;
    Float_t jet_0_Charm_DL1_score;
    Float_t jet_0_DL1r_FixedCutBEff_70_weight;
    Float_t jet_0_Light_DL1_score;
    Int_t jet_0_b_tag_quantile;
    Float_t jet_0_b_tag_score;
    Int_t jet_0_b_tagged_DL1r_FixedCutBEff_70;
    UInt_t jet_0_cleanJet_EC_LooseBad;
    Float_t jet_0_fjvt;
    Int_t jet_0_flavorlabel;
    Int_t jet_0_flavorlabel_cone;
    Int_t jet_0_flavorlabel_part;
    Int_t jet_0_is_Jvt_HS;
    Float_t jet_0_jvt;
    TLorentzVector *jet_0_matched_p4;
    Float_t jet_0_matched_pdgid;
    Int_t jet_0_origin;
    TLorentzVector *jet_0_p4;
    Float_t jet_0_q;
    Int_t jet_0_type;
    Float_t jet_0_width;
    Float_t jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_2_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_2_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_2_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_B_2_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_2_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_2_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_2_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_2_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_3_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_3_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_3_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_C_3_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_2_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_2_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_2_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_2_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_3_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_3_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_3_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_3_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_4_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_4_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_4_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_Eigen_Light_4_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_extrapolation_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_extrapolation_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_extrapolation_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_extrapolation_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1r_FixedCutBEff_70;
    Float_t jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT;
    Float_t jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT;
    Float_t jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT;
    Float_t jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT;
    Float_t jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT;
    Float_t jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT;
    Float_t jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT;
    Float_t jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT;
    Float_t jet_NOMINAL_central_jets_global_effSF_JVT;
    Float_t jet_NOMINAL_central_jets_global_ineffSF_JVT;
    Float_t jet_NOMINAL_forward_jets_global_effSF_JVT;
    Float_t jet_NOMINAL_forward_jets_global_ineffSF_JVT;
    Float_t jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_70;
    Float_t jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_70;
    Double_t kfactor;
    UInt_t lep_0;
    Float_t lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose_VarRad;
    Float_t lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_FixedRad;
    Float_t lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_VarRad;
    Float_t lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose_VarRad;
    Float_t lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_FixedRad;
    Float_t lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_VarRad;
    Float_t lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose_VarRad;
    Float_t lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_FixedRad;
    Float_t lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_VarRad;
    Float_t lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose_VarRad;
    Float_t lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_FixedRad;
    Float_t lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_VarRad;
    Float_t lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium;
    Float_t lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA;
    Float_t lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA;
    Float_t lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA;
    Float_t lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA;
    Float_t lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;
    Float_t lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;
    Float_t lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;
    Float_t lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;
    Float_t lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;
    Float_t lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;
    Float_t lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;
    Float_t lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;
    Float_t lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;
    Float_t lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;
    Float_t lep_0_NOMINAL_MuEffSF_IsoLoose_VarRad;
    Float_t lep_0_NOMINAL_MuEffSF_IsoTightTrackOnly_FixedRad;
    Float_t lep_0_NOMINAL_MuEffSF_IsoTightTrackOnly_VarRad;
    Float_t lep_0_NOMINAL_MuEffSF_Reco_QualMedium;
    Float_t lep_0_NOMINAL_MuEffSF_TTVA;
    Float_t lep_0_cluster_eta;
    Float_t lep_0_cluster_eta_be2;
    Int_t lep_0_id_bad;
    Int_t lep_0_id_charge;
    Int_t lep_0_id_loose;
    Int_t lep_0_id_medium;
    Int_t lep_0_id_tight;
    Int_t lep_0_id_veryloose;
    UInt_t lep_0_iso_HighPtTrackOnly;
    UInt_t lep_0_iso_Loose_FixedRad;
    UInt_t lep_0_iso_Loose_VarRad;
    UInt_t lep_0_iso_PflowLoose_FixedRad;
    UInt_t lep_0_iso_PflowLoose_VarRad;
    UInt_t lep_0_iso_PflowTight_FixedRad;
    UInt_t lep_0_iso_PflowTight_VarRad;
    UInt_t lep_0_iso_TightTrackOnly_FixedRad;
    UInt_t lep_0_iso_TightTrackOnly_VarRad;
    UInt_t lep_0_iso_Tight_FixedRad;
    UInt_t lep_0_iso_Tight_VarRad;
    Float_t lep_0_iso_neflowisol20;
    Float_t lep_0_iso_ptcone20_TightTTVA_pt1000;
    Float_t lep_0_iso_ptcone20_TightTTVA_pt500;
    Float_t lep_0_iso_ptvarcone30_TightTTVA_pt1000;
    Float_t lep_0_iso_ptvarcone30_TightTTVA_pt500;
    Float_t lep_0_iso_topoetcone20;
    UInt_t lep_0_matched;
    Int_t lep_0_matched_classifierParticleOrigin;
    Int_t lep_0_matched_classifierParticleType;
    Int_t lep_0_matched_isHad;
    UInt_t lep_0_matched_leptonic_tau_decay;
    Int_t lep_0_matched_leptonic_tau_decay_classifierParticleOrigin;
    Int_t lep_0_matched_leptonic_tau_decay_classifierParticleType;
    TLorentzVector *lep_0_matched_leptonic_tau_decay_invis_p4;
    Int_t lep_0_matched_leptonic_tau_decay_mother_pdgId;
    Int_t lep_0_matched_leptonic_tau_decay_mother_status;
    Int_t lep_0_matched_leptonic_tau_decay_origin;
    TLorentzVector *lep_0_matched_leptonic_tau_decay_p4;
    Int_t lep_0_matched_leptonic_tau_decay_pdgId;
    Float_t lep_0_matched_leptonic_tau_decay_pz;
    Float_t lep_0_matched_leptonic_tau_decay_q;
    Int_t lep_0_matched_leptonic_tau_decay_status;
    Int_t lep_0_matched_leptonic_tau_decay_type;
    TLorentzVector *lep_0_matched_leptonic_tau_decay_vis_p4;
    Int_t lep_0_matched_mother_pdgId;
    Int_t lep_0_matched_mother_status;
    Int_t lep_0_matched_origin;
    TLorentzVector *lep_0_matched_p4;
    Int_t lep_0_matched_pdgId;
    Float_t lep_0_matched_q;
    Int_t lep_0_matched_status;
    Int_t lep_0_matched_type;
    Int_t lep_0_muonAuthor;
    Int_t lep_0_muonType;
    Int_t lep_0_origin;
    TLorentzVector *lep_0_p4;
    Float_t lep_0_q;
    Float_t lep_0_trk_d0;
    Float_t lep_0_trk_d0_sig;
    TLorentzVector *lep_0_trk_p4;
    Float_t lep_0_trk_pt_error;
    Float_t lep_0_trk_pvx_z0;
    Float_t lep_0_trk_pvx_z0_sig;
    Float_t lep_0_trk_pvx_z0_sintheta;
    Int_t lep_0_trk_vx;
    TVector3 *lep_0_trk_vx_v;
    Float_t lep_0_trk_z0;
    Float_t lep_0_trk_z0_sig;
    Float_t lep_0_trk_z0_sintheta;
    Int_t lep_0_type;
    Int_t lephad;
    Int_t lephad_coll_approx;
    Float_t lephad_coll_approx_m;
    Float_t lephad_coll_approx_x0;
    Float_t lephad_coll_approx_x1;
    Float_t lephad_cosalpha;
    Float_t lephad_deta;
    Float_t lephad_dphi;
    Float_t lephad_dpt;
    Float_t lephad_dr;
    Int_t lephad_met_bisect;
    Float_t lephad_met_centrality;
    Float_t lephad_met_lep0_cos_dphi;
    Float_t lephad_met_lep1_cos_dphi;
    Float_t lephad_met_min_dphi;
    Float_t lephad_met_sum_cos_dphi;
    Float_t lephad_mt_lep0_met;
    Float_t lephad_mt_lep1_met;
    TLorentzVector *lephad_p4;
    Float_t lephad_qxq;
    Float_t lephad_scal_sum_pt;
    UInt_t mc_channel_number;
    Float_t met_more_met_et_ele;
    Float_t met_more_met_et_jet;
    Float_t met_more_met_et_muon;
    Float_t met_more_met_et_pho;
    Float_t met_more_met_et_soft;
    Float_t met_more_met_et_tau;
    Float_t met_more_met_phi_ele;
    Float_t met_more_met_phi_jet;
    Float_t met_more_met_phi_muon;
    Float_t met_more_met_phi_pho;
    Float_t met_more_met_phi_soft;
    Float_t met_more_met_phi_tau;
    Float_t met_more_met_sumet_ele;
    Float_t met_more_met_sumet_jet;
    Float_t met_more_met_sumet_muon;
    Float_t met_more_met_sumet_pho;
    Float_t met_more_met_sumet_soft;
    Float_t met_more_met_sumet_tau;
    TLorentzVector *met_reco_p4;
    Float_t met_reco_sig;
    Float_t met_reco_sig_tracks;
    Float_t met_reco_sumet;
    TLorentzVector *met_truth_p4;
    Float_t met_truth_sig;
    Float_t met_truth_sig_tracks;
    Float_t met_truth_sumet;
    UInt_t muTrigMatch_0_HLT_mu20_iloose_L1MU15;
    UInt_t muTrigMatch_0_HLT_mu26_ivarmedium;
    UInt_t muTrigMatch_0_HLT_mu50;
    UInt_t muTrigMatch_0_trigger_matched;
    Float_t n_actual_int;
    Float_t n_actual_int_cor;
    Float_t n_avg_int;
    Float_t n_avg_int_cor;
    Int_t n_bjets_DL1r_FixedCutBEff_70;
    Int_t n_electrons;
    Int_t n_electrons_met;
    Int_t n_electrons_olr;
    Int_t n_jets;
    Int_t n_jets_30;
    Int_t n_jets_40;
    Int_t n_jets_bad;
    Int_t n_jets_mc_hs;
    Int_t n_jets_met;
    Int_t n_jets_olr;
    Int_t n_muons;
    Int_t n_muons_bad;
    Int_t n_muons_loose;
    Int_t n_muons_medium;
    Int_t n_muons_met;
    Int_t n_muons_olr;
    Int_t n_muons_tight;
    Int_t n_muons_veryloose;
    Int_t n_photons;
    Int_t n_photons_met;
    Int_t n_photons_olr;
    Int_t n_pvx;
    Int_t n_taus;
    Int_t n_taus_met;
    Int_t n_taus_olr;
    Int_t n_taus_rnn_loose;
    Int_t n_taus_rnn_medium;
    Int_t n_taus_rnn_tight;
    Int_t n_taus_rnn_veryloose;
    UInt_t n_truth_gluon_jets;
    UInt_t n_truth_jets;
    UInt_t n_truth_jets_pt20_eta45;
    UInt_t n_truth_quark_jets;
    Int_t n_vx;
    Float_t pmg_truth_weight_FSRHi;
    Float_t pmg_truth_weight_FSRLo;
    Float_t pmg_truth_weight_ISRLo;
    Float_t pmg_truth_weight_pdf_signal_weight_0;
    Float_t pmg_truth_weight_pdf_signal_weight_1;
    Float_t pmg_truth_weight_pdf_signal_weight_10;
    Float_t pmg_truth_weight_pdf_signal_weight_11;
    Float_t pmg_truth_weight_pdf_signal_weight_12;
    Float_t pmg_truth_weight_pdf_signal_weight_13;
    Float_t pmg_truth_weight_pdf_signal_weight_14;
    Float_t pmg_truth_weight_pdf_signal_weight_15;
    Float_t pmg_truth_weight_pdf_signal_weight_16;
    Float_t pmg_truth_weight_pdf_signal_weight_17;
    Float_t pmg_truth_weight_pdf_signal_weight_18;
    Float_t pmg_truth_weight_pdf_signal_weight_19;
    Float_t pmg_truth_weight_pdf_signal_weight_2;
    Float_t pmg_truth_weight_pdf_signal_weight_20;
    Float_t pmg_truth_weight_pdf_signal_weight_21;
    Float_t pmg_truth_weight_pdf_signal_weight_22;
    Float_t pmg_truth_weight_pdf_signal_weight_23;
    Float_t pmg_truth_weight_pdf_signal_weight_24;
    Float_t pmg_truth_weight_pdf_signal_weight_25;
    Float_t pmg_truth_weight_pdf_signal_weight_26;
    Float_t pmg_truth_weight_pdf_signal_weight_27;
    Float_t pmg_truth_weight_pdf_signal_weight_28;
    Float_t pmg_truth_weight_pdf_signal_weight_29;
    Float_t pmg_truth_weight_pdf_signal_weight_3;
    Float_t pmg_truth_weight_pdf_signal_weight_30;
    Float_t pmg_truth_weight_pdf_signal_weight_4;
    Float_t pmg_truth_weight_pdf_signal_weight_5;
    Float_t pmg_truth_weight_pdf_signal_weight_6;
    Float_t pmg_truth_weight_pdf_signal_weight_7;
    Float_t pmg_truth_weight_pdf_signal_weight_8;
    Float_t pmg_truth_weight_pdf_signal_weight_9;
    UInt_t run_number;
    UInt_t tau_0;
    Float_t tau_0_EleID_AbsDEtaLeadTrk;
    Float_t tau_0_EleID_AbsDPhiLeadTrk;
    Float_t tau_0_EleID_AbsEtaLeadTrk;
    Float_t tau_0_EleID_EMFracAtEMScale;
    Float_t tau_0_EleID_EMFracFixed;
    Float_t tau_0_EleID_PSSFraction;
    Float_t tau_0_EleID_centFrac;
    Float_t tau_0_EleID_clustersMeanCenterLambda;
    Float_t tau_0_EleID_clustersMeanEMProbability;
    Float_t tau_0_EleID_clustersMeanFirstEngDens;
    Float_t tau_0_EleID_clustersMeanPresamplerFrac;
    Float_t tau_0_EleID_clustersMeanSecondLambda;
    Float_t tau_0_EleID_etHotShotWinOverPtLeadTrk;
    Float_t tau_0_EleID_etOverPtLeadTrk;
    Float_t tau_0_EleID_hadLeakEt;
    Float_t tau_0_EleID_hadLeakFracFixed;
    Float_t tau_0_EleID_isolFrac;
    Float_t tau_0_EleID_leadTrackProbHT;
    Float_t tau_0_EleID_secMaxStripEt;
    Float_t tau_0_EleID_secMaxStripEtOverPtLeadTrk;
    Float_t tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad;
    Float_t tau_0_NOMINAL_TauEffSF_JetRNNloose;
    Float_t tau_0_NOMINAL_TauEffSF_JetRNNmedium;
    Float_t tau_0_NOMINAL_TauEffSF_JetRNNtight;
    Float_t tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron;
    Float_t tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron;
    Float_t tau_0_NOMINAL_TauEffSF_TightEleBDT_electron;
    Float_t tau_0_NOMINAL_TauEffSF_reco;
    Float_t tau_0_NOMINAL_TauEffSF_selection;
    Float_t tau_0_PanTau_BDTValue_1p0n_vs_1p1n;
    Float_t tau_0_PanTau_BDTValue_1p1n_vs_1pXn;
    Float_t tau_0_PanTau_BDTValue_3p0n_vs_3pXn;
    Int_t tau_0_PanTau_BDTVar_Basic_NNeutralConsts;
    Float_t tau_0_PanTau_BDTVar_Charged_HLV_SumM;
    Float_t tau_0_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt;
    Float_t tau_0_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts;
    Float_t tau_0_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged;
    Float_t tau_0_PanTau_BDTVar_Neutral_HLV_SumM;
    Float_t tau_0_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1;
    Float_t tau_0_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2;
    Float_t tau_0_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts;
    Float_t tau_0_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts;
    Float_t tau_0_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed;
    Int_t tau_0_PanTau_DecayModeProto;
    Int_t tau_0_PanTau_isPanTauCandidate;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_LooseEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_MediumEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_TightEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_LooseEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_MediumEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_TightEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_TightEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_TightEleBDT_electron;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_selection;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNloose;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNmedium;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNtight;
    Float_t tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_selection;
    Float_t tau_0_TauID_ChPiEMEOverCaloEME;
    Float_t tau_0_TauID_EMPOverTrkSysP;
    Float_t tau_0_TauID_SumPtTrkFrac;
    Float_t tau_0_TauID_centFrac;
    Float_t tau_0_TauID_dRmax;
    Float_t tau_0_TauID_etOverPtLeadTrk;
    Float_t tau_0_TauID_innerTrkAvgDist;
    Float_t tau_0_TauID_ipSigLeadTrk;
    Float_t tau_0_TauID_mEflowApprox;
    Float_t tau_0_TauID_massTrkSys;
    Float_t tau_0_TauID_ptIntermediateAxis;
    Float_t tau_0_TauID_ptRatioEflowApprox;
    Float_t tau_0_TauID_trFlightPathSig;
    Float_t tau_0_allTrk_eta;
    UInt_t tau_0_allTrk_n;
    Float_t tau_0_allTrk_phi;
    Float_t tau_0_allTrk_pt;
    Float_t tau_0_b_tag_score;
    Int_t tau_0_b_tagged;
    std::vector<float> *tau_0_charged_tracks_d0;
    std::vector<float> *tau_0_charged_tracks_d0_sig;
    std::vector<float> *tau_0_charged_tracks_eProbabilityHT;
    std::vector<int> *tau_0_charged_tracks_nInnermostPixelHits;
    std::vector<int> *tau_0_charged_tracks_nPixelHits;
    std::vector<int> *tau_0_charged_tracks_nSCTHits;
    std::vector<TLorentzVector> *tau_0_charged_tracks_p4;
    std::vector<float> *tau_0_charged_tracks_pt_err;
    std::vector<float> *tau_0_charged_tracks_z0;
    std::vector<float> *tau_0_charged_tracks_z0_sig;
    std::vector<float> *tau_0_charged_tracks_z0_sintheta;
    std::vector<float> *tau_0_charged_tracks_z0_sintheta_tjva;
    std::vector<TLorentzVector> *tau_0_conversion_tracks_p4;
    UInt_t tau_0_decay_mode;
    Int_t tau_0_ele_bdt_loose_retuned;
    Int_t tau_0_ele_bdt_medium_retuned;
    Float_t tau_0_ele_bdt_score_retuned;
    Float_t tau_0_ele_bdt_score_trans_retuned;
    Int_t tau_0_ele_bdt_tight_retuned;
    Float_t tau_0_ele_match_lhscore;
    UInt_t tau_0_ele_olr_pass;
    Float_t tau_0_jet_bdt_score;
    Float_t tau_0_jet_bdt_score_trans;
    Float_t tau_0_jet_jvt;
    UInt_t tau_0_jet_rnn_loose;
    UInt_t tau_0_jet_rnn_medium;
    Float_t tau_0_jet_rnn_score;
    Float_t tau_0_jet_rnn_score_trans;
    UInt_t tau_0_jet_rnn_tight;
    UInt_t tau_0_jet_rnn_veryloose;
    Float_t tau_0_jet_seed_TrackWidthPt1000_wrt_PV;
    Float_t tau_0_jet_seed_TrackWidthPt1000_wrt_TV;
    Float_t tau_0_jet_width;
    UInt_t tau_0_matched;
    Int_t tau_0_matched_classifierParticleOrigin;
    Int_t tau_0_matched_classifierParticleType;
    Int_t tau_0_matched_decayVertex;
    TVector3 *tau_0_matched_decayVertex_v;
    Int_t tau_0_matched_decay_mode;
    UInt_t tau_0_matched_isEle;
    UInt_t tau_0_matched_isHadTau;
    UInt_t tau_0_matched_isJet;
    UInt_t tau_0_matched_isMuon;
    UInt_t tau_0_matched_isTau;
    UInt_t tau_0_matched_isTruthMatch;
    Int_t tau_0_matched_mother_pdgId;
    Int_t tau_0_matched_mother_status;
    Int_t tau_0_matched_n_charged;
    Int_t tau_0_matched_n_charged_pion;
    Int_t tau_0_matched_n_neutral;
    Int_t tau_0_matched_n_neutral_pion;
    Int_t tau_0_matched_origin;
    TLorentzVector *tau_0_matched_p4;
    TLorentzVector *tau_0_matched_p4_vis_charged_track0;
    TLorentzVector *tau_0_matched_p4_vis_charged_track1;
    TLorentzVector *tau_0_matched_p4_vis_charged_track2;
    Int_t tau_0_matched_pdgId;
    Int_t tau_0_matched_productionVertex;
    TVector3 *tau_0_matched_productionVertex_v;
    Float_t tau_0_matched_pz;
    Float_t tau_0_matched_q;
    Int_t tau_0_matched_status;
    Int_t tau_0_matched_type;
    TLorentzVector *tau_0_matched_vis_charged_p4;
    TLorentzVector *tau_0_matched_vis_neutral_others_p4;
    TLorentzVector *tau_0_matched_vis_neutral_p4;
    TLorentzVector *tau_0_matched_vis_neutral_pions_p4;
    TLorentzVector *tau_0_matched_vis_p4;
    UInt_t tau_0_n_all_tracks;
    UInt_t tau_0_n_charged_tracks;
    UInt_t tau_0_n_conversion_tracks;
    UInt_t tau_0_n_core_tracks;
    UInt_t tau_0_n_failTrackFilter_tracks;
    UInt_t tau_0_n_fake_tracks;
    UInt_t tau_0_n_isolation_tracks;
    UInt_t tau_0_n_modified_isolation_tracks;
    UInt_t tau_0_n_old_tracks;
    UInt_t tau_0_n_passTrkSelectionTight_tracks;
    UInt_t tau_0_n_passTrkSelector_tracks;
    UInt_t tau_0_n_unclassified_tracks;
    UInt_t tau_0_n_wide_tracks;
    Int_t tau_0_origin;
    TLorentzVector *tau_0_p4;
    Int_t tau_0_pass_tst_muonolr;
    Int_t tau_0_primary_vertex;
    TVector3 *tau_0_primary_vertex_v;
    Float_t tau_0_q;
    Int_t tau_0_secondary_vertex;
    TVector3 *tau_0_secondary_vertex_v;
    UInt_t tau_0_trk_multi_diag_n_had_tracks;
    UInt_t tau_0_trk_multi_diag_n_spu_cv_tracks;
    UInt_t tau_0_trk_multi_diag_n_spu_fk_tracks;
    UInt_t tau_0_trk_multi_diag_n_spu_pu_tracks;
    UInt_t tau_0_trk_multi_diag_n_spu_tracks;
    UInt_t tau_0_trk_multi_diag_n_spu_uc_tracks;
    UInt_t tau_0_trk_multi_diag_n_spu_ue_tracks;
    UInt_t tau_0_trk_multi_diag_n_tau_tracks;
    Float_t tau_0_trk_multi_diag_purity;
    Int_t tau_0_type;
    Double_t tau_tes_alpha_pt_shift;
    Double_t weight_mc;
    Double_t Weight_mc;
    Int_t mergedRunNumber;
    Double_t upsilon;
    Bool_t isQCR;
    Bool_t isWCR;
    Bool_t isWCR_QCD;
    Bool_t isIQCR_Iso;
    Bool_t isIQCR_NotIso;
    Bool_t isTCR;
    Bool_t isSR;
    Bool_t isData;

    // List of branches
    TBranch *b_AMI_cross_section;                                                                              //!
    TBranch *b_HLT_mu20_iloose_L1MU15;                                                                         //!
    TBranch *b_HLT_mu26_ivarmedium;                                                                            //!
    TBranch *b_HLT_mu50;                                                                                       //!
    TBranch *b_NOMINAL_pileup_combined_weight;                                                                 //!
    TBranch *b_NOMINAL_pileup_random_run_number;                                                               //!
    TBranch *b_PRW_DATASF_1down_pileup_combined_weight;                                                        //!
    TBranch *b_PRW_DATASF_1down_pileup_random_run_number;                                                      //!
    TBranch *b_PRW_DATASF_1up_pileup_combined_weight;                                                          //!
    TBranch *b_PRW_DATASF_1up_pileup_random_run_number;                                                        //!
    TBranch *b_cross_section;                                                                                  //!
    TBranch *b_event_clean_EC_LooseBad;                                                                        //!
    TBranch *b_event_clean_detector_core;                                                                      //!
    TBranch *b_event_is_bad_batman;                                                                            //!
    TBranch *b_event_number;                                                                                   //!
    TBranch *b_filter_efficiency;                                                                              //!
    TBranch *b_jet_0;                                                                                          //!
    TBranch *b_jet_0_Bottom_DL1_score;                                                                         //!
    TBranch *b_jet_0_Charm_DL1_score;                                                                          //!
    TBranch *b_jet_0_DL1r_FixedCutBEff_70_weight;                                                              //!
    TBranch *b_jet_0_Light_DL1_score;                                                                          //!
    TBranch *b_jet_0_b_tag_quantile;                                                                           //!
    TBranch *b_jet_0_b_tag_score;                                                                              //!
    TBranch *b_jet_0_b_tagged_DL1r_FixedCutBEff_70;                                                            //!
    TBranch *b_jet_0_cleanJet_EC_LooseBad;                                                                     //!
    TBranch *b_jet_0_fjvt;                                                                                     //!
    TBranch *b_jet_0_flavorlabel;                                                                              //!
    TBranch *b_jet_0_flavorlabel_cone;                                                                         //!
    TBranch *b_jet_0_flavorlabel_part;                                                                         //!
    TBranch *b_jet_0_is_Jvt_HS;                                                                                //!
    TBranch *b_jet_0_jvt;                                                                                      //!
    TBranch *b_jet_0_matched_p4;                                                                               //!
    TBranch *b_jet_0_matched_pdgid;                                                                            //!
    TBranch *b_jet_0_origin;                                                                                   //!
    TBranch *b_jet_0_p4;                                                                                       //!
    TBranch *b_jet_0_q;                                                                                        //!
    TBranch *b_jet_0_type;                                                                                     //!
    TBranch *b_jet_0_width;                                                                                    //!
    TBranch *b_jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1r_FixedCutBEff_70;                                     //!
    TBranch *b_jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1r_FixedCutBEff_70;                                     //!
    TBranch *b_jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_B_2_1down_global_effSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_B_2_1down_global_ineffSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_Eigen_B_2_1up_global_effSF_DL1r_FixedCutBEff_70;                                     //!
    TBranch *b_jet_FT_EFF_Eigen_B_2_1up_global_ineffSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1r_FixedCutBEff_70;                                     //!
    TBranch *b_jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1r_FixedCutBEff_70;                                     //!
    TBranch *b_jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_C_2_1down_global_effSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_C_2_1down_global_ineffSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_Eigen_C_2_1up_global_effSF_DL1r_FixedCutBEff_70;                                     //!
    TBranch *b_jet_FT_EFF_Eigen_C_2_1up_global_ineffSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_C_3_1down_global_effSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_C_3_1down_global_ineffSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_Eigen_C_3_1up_global_effSF_DL1r_FixedCutBEff_70;                                     //!
    TBranch *b_jet_FT_EFF_Eigen_C_3_1up_global_ineffSF_DL1r_FixedCutBEff_70;                                   //!
    TBranch *b_jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1r_FixedCutBEff_70;                               //!
    TBranch *b_jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1r_FixedCutBEff_70;                             //!
    TBranch *b_jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1r_FixedCutBEff_70;                               //!
    TBranch *b_jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1r_FixedCutBEff_70;                               //!
    TBranch *b_jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1r_FixedCutBEff_70;                             //!
    TBranch *b_jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1r_FixedCutBEff_70;                               //!
    TBranch *b_jet_FT_EFF_Eigen_Light_2_1down_global_effSF_DL1r_FixedCutBEff_70;                               //!
    TBranch *b_jet_FT_EFF_Eigen_Light_2_1down_global_ineffSF_DL1r_FixedCutBEff_70;                             //!
    TBranch *b_jet_FT_EFF_Eigen_Light_2_1up_global_effSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_Eigen_Light_2_1up_global_ineffSF_DL1r_FixedCutBEff_70;                               //!
    TBranch *b_jet_FT_EFF_Eigen_Light_3_1down_global_effSF_DL1r_FixedCutBEff_70;                               //!
    TBranch *b_jet_FT_EFF_Eigen_Light_3_1down_global_ineffSF_DL1r_FixedCutBEff_70;                             //!
    TBranch *b_jet_FT_EFF_Eigen_Light_3_1up_global_effSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_Eigen_Light_3_1up_global_ineffSF_DL1r_FixedCutBEff_70;                               //!
    TBranch *b_jet_FT_EFF_Eigen_Light_4_1down_global_effSF_DL1r_FixedCutBEff_70;                               //!
    TBranch *b_jet_FT_EFF_Eigen_Light_4_1down_global_ineffSF_DL1r_FixedCutBEff_70;                             //!
    TBranch *b_jet_FT_EFF_Eigen_Light_4_1up_global_effSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_Eigen_Light_4_1up_global_ineffSF_DL1r_FixedCutBEff_70;                               //!
    TBranch *b_jet_FT_EFF_extrapolation_1down_global_effSF_DL1r_FixedCutBEff_70;                               //!
    TBranch *b_jet_FT_EFF_extrapolation_1down_global_ineffSF_DL1r_FixedCutBEff_70;                             //!
    TBranch *b_jet_FT_EFF_extrapolation_1up_global_effSF_DL1r_FixedCutBEff_70;                                 //!
    TBranch *b_jet_FT_EFF_extrapolation_1up_global_ineffSF_DL1r_FixedCutBEff_70;                               //!
    TBranch *b_jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1r_FixedCutBEff_70;                    //!
    TBranch *b_jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1r_FixedCutBEff_70;                  //!
    TBranch *b_jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1r_FixedCutBEff_70;                      //!
    TBranch *b_jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1r_FixedCutBEff_70;                    //!
    TBranch *b_jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT;                                      //!
    TBranch *b_jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT;                                    //!
    TBranch *b_jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT;                                        //!
    TBranch *b_jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT;                                      //!
    TBranch *b_jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT;                                     //!
    TBranch *b_jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT;                                   //!
    TBranch *b_jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT;                                       //!
    TBranch *b_jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT;                                     //!
    TBranch *b_jet_NOMINAL_central_jets_global_effSF_JVT;                                                      //!
    TBranch *b_jet_NOMINAL_central_jets_global_ineffSF_JVT;                                                    //!
    TBranch *b_jet_NOMINAL_forward_jets_global_effSF_JVT;                                                      //!
    TBranch *b_jet_NOMINAL_forward_jets_global_ineffSF_JVT;                                                    //!
    TBranch *b_jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_70;                                                  //!
    TBranch *b_jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_70;                                                //!
    TBranch *b_kfactor;                                                                                        //!
    TBranch *b_lep_0;                                                                                          //!
    TBranch *b_lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose_VarRad;                                          //!
    TBranch *b_lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_FixedRad;                               //!
    TBranch *b_lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_VarRad;                                 //!
    TBranch *b_lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose_VarRad;                                            //!
    TBranch *b_lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_FixedRad;                                 //!
    TBranch *b_lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_VarRad;                                   //!
    TBranch *b_lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose_VarRad;                                           //!
    TBranch *b_lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_FixedRad;                                //!
    TBranch *b_lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_VarRad;                                  //!
    TBranch *b_lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose_VarRad;                                             //!
    TBranch *b_lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_FixedRad;                                  //!
    TBranch *b_lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_VarRad;                                    //!
    TBranch *b_lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium;                                         //!
    TBranch *b_lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium;                                           //!
    TBranch *b_lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium;                                   //!
    TBranch *b_lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium;                                     //!
    TBranch *b_lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium;                                          //!
    TBranch *b_lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium;                                            //!
    TBranch *b_lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium;                                    //!
    TBranch *b_lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium;                                      //!
    TBranch *b_lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA;                                                    //!
    TBranch *b_lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA;                                                      //!
    TBranch *b_lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA;                                                     //!
    TBranch *b_lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA;                                                       //!
    TBranch *b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium; //!
    TBranch *b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;    //!
    TBranch *b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;   //!
    TBranch *b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;      //!
    TBranch *b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium; //!
    TBranch *b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;    //!
    TBranch *b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;   //!
    TBranch *b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;      //!
    TBranch *b_lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium;                            //!
    TBranch *b_lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium;                               //!
    TBranch *b_lep_0_NOMINAL_MuEffSF_IsoLoose_VarRad;                                                          //!
    TBranch *b_lep_0_NOMINAL_MuEffSF_IsoTightTrackOnly_FixedRad;                                               //!
    TBranch *b_lep_0_NOMINAL_MuEffSF_IsoTightTrackOnly_VarRad;                                                 //!
    TBranch *b_lep_0_NOMINAL_MuEffSF_Reco_QualMedium;                                                          //!
    TBranch *b_lep_0_NOMINAL_MuEffSF_TTVA;                                                                     //!
    TBranch *b_lep_0_cluster_eta;                                                                              //!
    TBranch *b_lep_0_cluster_eta_be2;                                                                          //!
    TBranch *b_lep_0_id_bad;                                                                                   //!
    TBranch *b_lep_0_id_charge;                                                                                //!
    TBranch *b_lep_0_id_loose;                                                                                 //!
    TBranch *b_lep_0_id_medium;                                                                                //!
    TBranch *b_lep_0_id_tight;                                                                                 //!
    TBranch *b_lep_0_id_veryloose;                                                                             //!
    TBranch *b_lep_0_iso_HighPtTrackOnly;                                                                      //!
    TBranch *b_lep_0_iso_Loose_FixedRad;                                                                       //!
    TBranch *b_lep_0_iso_Loose_VarRad;                                                                         //!
    TBranch *b_lep_0_iso_PflowLoose_FixedRad;                                                                  //!
    TBranch *b_lep_0_iso_PflowLoose_VarRad;                                                                    //!
    TBranch *b_lep_0_iso_PflowTight_FixedRad;                                                                  //!
    TBranch *b_lep_0_iso_PflowTight_VarRad;                                                                    //!
    TBranch *b_lep_0_iso_TightTrackOnly_FixedRad;                                                              //!
    TBranch *b_lep_0_iso_TightTrackOnly_VarRad;                                                                //!
    TBranch *b_lep_0_iso_Tight_FixedRad;                                                                       //!
    TBranch *b_lep_0_iso_Tight_VarRad;                                                                         //!
    TBranch *b_lep_0_iso_neflowisol20;                                                                         //!
    TBranch *b_lep_0_iso_ptcone20_TightTTVA_pt1000;                                                            //!
    TBranch *b_lep_0_iso_ptcone20_TightTTVA_pt500;                                                             //!
    TBranch *b_lep_0_iso_ptvarcone30_TightTTVA_pt1000;                                                         //!
    TBranch *b_lep_0_iso_ptvarcone30_TightTTVA_pt500;                                                          //!
    TBranch *b_lep_0_iso_topoetcone20;                                                                         //!
    TBranch *b_lep_0_matched;                                                                                  //!
    TBranch *b_lep_0_matched_classifierParticleOrigin;                                                         //!
    TBranch *b_lep_0_matched_classifierParticleType;                                                           //!
    TBranch *b_lep_0_matched_isHad;                                                                            //!
    TBranch *b_lep_0_matched_leptonic_tau_decay;                                                               //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_classifierParticleOrigin;                                      //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_classifierParticleType;                                        //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_invis_p4;                                                      //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_mother_pdgId;                                                  //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_mother_status;                                                 //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_origin;                                                        //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_p4;                                                            //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_pdgId;                                                         //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_pz;                                                            //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_q;                                                             //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_status;                                                        //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_type;                                                          //!
    TBranch *b_lep_0_matched_leptonic_tau_decay_vis_p4;                                                        //!
    TBranch *b_lep_0_matched_mother_pdgId;                                                                     //!
    TBranch *b_lep_0_matched_mother_status;                                                                    //!
    TBranch *b_lep_0_matched_origin;                                                                           //!
    TBranch *b_lep_0_matched_p4;                                                                               //!
    TBranch *b_lep_0_matched_pdgId;                                                                            //!
    TBranch *b_lep_0_matched_q;                                                                                //!
    TBranch *b_lep_0_matched_status;                                                                           //!
    TBranch *b_lep_0_matched_type;                                                                             //!
    TBranch *b_lep_0_muonAuthor;                                                                               //!
    TBranch *b_lep_0_muonType;                                                                                 //!
    TBranch *b_lep_0_origin;                                                                                   //!
    TBranch *b_lep_0_p4;                                                                                       //!
    TBranch *b_lep_0_q;                                                                                        //!
    TBranch *b_lep_0_trk_d0;                                                                                   //!
    TBranch *b_lep_0_trk_d0_sig;                                                                               //!
    TBranch *b_lep_0_trk_p4;                                                                                   //!
    TBranch *b_lep_0_trk_pt_error;                                                                             //!
    TBranch *b_lep_0_trk_pvx_z0;                                                                               //!
    TBranch *b_lep_0_trk_pvx_z0_sig;                                                                           //!
    TBranch *b_lep_0_trk_pvx_z0_sintheta;                                                                      //!
    TBranch *b_lep_0_trk_vx;                                                                                   //!
    TBranch *b_lep_0_trk_vx_v;                                                                                 //!
    TBranch *b_lep_0_trk_z0;                                                                                   //!
    TBranch *b_lep_0_trk_z0_sig;                                                                               //!
    TBranch *b_lep_0_trk_z0_sintheta;                                                                          //!
    TBranch *b_lep_0_type;                                                                                     //!
    TBranch *b_lephad;                                                                                         //!
    TBranch *b_lephad_coll_approx;                                                                             //!
    TBranch *b_lephad_coll_approx_m;                                                                           //!
    TBranch *b_lephad_coll_approx_x0;                                                                          //!
    TBranch *b_lephad_coll_approx_x1;                                                                          //!
    TBranch *b_lephad_cosalpha;                                                                                //!
    TBranch *b_lephad_deta;                                                                                    //!
    TBranch *b_lephad_dphi;                                                                                    //!
    TBranch *b_lephad_dpt;                                                                                     //!
    TBranch *b_lephad_dr;                                                                                      //!
    TBranch *b_lephad_met_bisect;                                                                              //!
    TBranch *b_lephad_met_centrality;                                                                          //!
    TBranch *b_lephad_met_lep0_cos_dphi;                                                                       //!
    TBranch *b_lephad_met_lep1_cos_dphi;                                                                       //!
    TBranch *b_lephad_met_min_dphi;                                                                            //!
    TBranch *b_lephad_met_sum_cos_dphi;                                                                        //!
    TBranch *b_lephad_mt_lep0_met;                                                                             //!
    TBranch *b_lephad_mt_lep1_met;                                                                             //!
    TBranch *b_lephad_p4;                                                                                      //!
    TBranch *b_lephad_qxq;                                                                                     //!
    TBranch *b_lephad_scal_sum_pt;                                                                             //!
    TBranch *b_mc_channel_number;                                                                              //!
    TBranch *b_met_more_met_et_ele;                                                                            //!
    TBranch *b_met_more_met_et_jet;                                                                            //!
    TBranch *b_met_more_met_et_muon;                                                                           //!
    TBranch *b_met_more_met_et_pho;                                                                            //!
    TBranch *b_met_more_met_et_soft;                                                                           //!
    TBranch *b_met_more_met_et_tau;                                                                            //!
    TBranch *b_met_more_met_phi_ele;                                                                           //!
    TBranch *b_met_more_met_phi_jet;                                                                           //!
    TBranch *b_met_more_met_phi_muon;                                                                          //!
    TBranch *b_met_more_met_phi_pho;                                                                           //!
    TBranch *b_met_more_met_phi_soft;                                                                          //!
    TBranch *b_met_more_met_phi_tau;                                                                           //!
    TBranch *b_met_more_met_sumet_ele;                                                                         //!
    TBranch *b_met_more_met_sumet_jet;                                                                         //!
    TBranch *b_met_more_met_sumet_muon;                                                                        //!
    TBranch *b_met_more_met_sumet_pho;                                                                         //!
    TBranch *b_met_more_met_sumet_soft;                                                                        //!
    TBranch *b_met_more_met_sumet_tau;                                                                         //!
    TBranch *b_met_reco_p4;                                                                                    //!
    TBranch *b_met_reco_sig;                                                                                   //!
    TBranch *b_met_reco_sig_tracks;                                                                            //!
    TBranch *b_met_reco_sumet;                                                                                 //!
    TBranch *b_met_truth_p4;                                                                                   //!
    TBranch *b_met_truth_sig;                                                                                  //!
    TBranch *b_met_truth_sig_tracks;                                                                           //!
    TBranch *b_met_truth_sumet;                                                                                //!
    TBranch *b_muTrigMatch_0_HLT_mu20_iloose_L1MU15;                                                           //!
    TBranch *b_muTrigMatch_0_HLT_mu26_ivarmedium;                                                              //!
    TBranch *b_muTrigMatch_0_HLT_mu50;                                                                         //!
    TBranch *b_muTrigMatch_0_trigger_matched;                                                                  //!
    TBranch *b_n_actual_int;                                                                                   //!
    TBranch *b_n_actual_int_cor;                                                                               //!
    TBranch *b_n_avg_int;                                                                                      //!
    TBranch *b_n_avg_int_cor;                                                                                  //!
    TBranch *b_n_bjets_DL1r_FixedCutBEff_70;                                                                   //!
    TBranch *b_n_electrons;                                                                                    //!
    TBranch *b_n_electrons_met;                                                                                //!
    TBranch *b_n_electrons_olr;                                                                                //!
    TBranch *b_n_jets;                                                                                         //!
    TBranch *b_n_jets_30;                                                                                      //!
    TBranch *b_n_jets_40;                                                                                      //!
    TBranch *b_n_jets_bad;                                                                                     //!
    TBranch *b_n_jets_mc_hs;                                                                                   //!
    TBranch *b_n_jets_met;                                                                                     //!
    TBranch *b_n_jets_olr;                                                                                     //!
    TBranch *b_n_muons;                                                                                        //!
    TBranch *b_n_muons_bad;                                                                                    //!
    TBranch *b_n_muons_loose;                                                                                  //!
    TBranch *b_n_muons_medium;                                                                                 //!
    TBranch *b_n_muons_met;                                                                                    //!
    TBranch *b_n_muons_olr;                                                                                    //!
    TBranch *b_n_muons_tight;                                                                                  //!
    TBranch *b_n_muons_veryloose;                                                                              //!
    TBranch *b_n_photons;                                                                                      //!
    TBranch *b_n_photons_met;                                                                                  //!
    TBranch *b_n_photons_olr;                                                                                  //!
    TBranch *b_n_pvx;                                                                                          //!
    TBranch *b_n_taus;                                                                                         //!
    TBranch *b_n_taus_met;                                                                                     //!
    TBranch *b_n_taus_olr;                                                                                     //!
    TBranch *b_n_taus_rnn_loose;                                                                               //!
    TBranch *b_n_taus_rnn_medium;                                                                              //!
    TBranch *b_n_taus_rnn_tight;                                                                               //!
    TBranch *b_n_taus_rnn_veryloose;                                                                           //!
    TBranch *b_n_truth_gluon_jets;                                                                             //!
    TBranch *b_n_truth_jets;                                                                                   //!
    TBranch *b_n_truth_jets_pt20_eta45;                                                                        //!
    TBranch *b_n_truth_quark_jets;                                                                             //!
    TBranch *b_n_vx;                                                                                           //!
    TBranch *b_pmg_truth_weight_FSRHi;                                                                         //!
    TBranch *b_pmg_truth_weight_FSRLo;                                                                         //!
    TBranch *b_pmg_truth_weight_ISRLo;                                                                         //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_0;                                                           //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_1;                                                           //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_10;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_11;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_12;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_13;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_14;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_15;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_16;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_17;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_18;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_19;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_2;                                                           //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_20;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_21;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_22;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_23;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_24;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_25;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_26;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_27;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_28;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_29;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_3;                                                           //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_30;                                                          //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_4;                                                           //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_5;                                                           //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_6;                                                           //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_7;                                                           //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_8;                                                           //!
    TBranch *b_pmg_truth_weight_pdf_signal_weight_9;                                                           //!
    TBranch *b_run_number;                                                                                     //!
    TBranch *b_tau_0;                                                                                          //!
    TBranch *b_tau_0_EleID_AbsDEtaLeadTrk;                                                                     //!
    TBranch *b_tau_0_EleID_AbsDPhiLeadTrk;                                                                     //!
    TBranch *b_tau_0_EleID_AbsEtaLeadTrk;                                                                      //!
    TBranch *b_tau_0_EleID_EMFracAtEMScale;                                                                    //!
    TBranch *b_tau_0_EleID_EMFracFixed;                                                                        //!
    TBranch *b_tau_0_EleID_PSSFraction;                                                                        //!
    TBranch *b_tau_0_EleID_centFrac;                                                                           //!
    TBranch *b_tau_0_EleID_clustersMeanCenterLambda;                                                           //!
    TBranch *b_tau_0_EleID_clustersMeanEMProbability;                                                          //!
    TBranch *b_tau_0_EleID_clustersMeanFirstEngDens;                                                           //!
    TBranch *b_tau_0_EleID_clustersMeanPresamplerFrac;                                                         //!
    TBranch *b_tau_0_EleID_clustersMeanSecondLambda;                                                           //!
    TBranch *b_tau_0_EleID_etHotShotWinOverPtLeadTrk;                                                          //!
    TBranch *b_tau_0_EleID_etOverPtLeadTrk;                                                                    //!
    TBranch *b_tau_0_EleID_hadLeakEt;                                                                          //!
    TBranch *b_tau_0_EleID_hadLeakFracFixed;                                                                   //!
    TBranch *b_tau_0_EleID_isolFrac;                                                                           //!
    TBranch *b_tau_0_EleID_leadTrackProbHT;                                                                    //!
    TBranch *b_tau_0_EleID_secMaxStripEt;                                                                      //!
    TBranch *b_tau_0_EleID_secMaxStripEtOverPtLeadTrk;                                                         //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad;                                                     //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_JetRNNloose;                                                             //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_JetRNNmedium;                                                            //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_JetRNNtight;                                                             //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron;                                                    //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron;                                                   //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_TightEleBDT_electron;                                                    //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_reco;                                                                    //!
    TBranch *b_tau_0_NOMINAL_TauEffSF_selection;                                                               //!
    TBranch *b_tau_0_PanTau_BDTValue_1p0n_vs_1p1n;                                                             //!
    TBranch *b_tau_0_PanTau_BDTValue_1p1n_vs_1pXn;                                                             //!
    TBranch *b_tau_0_PanTau_BDTValue_3p0n_vs_3pXn;                                                             //!
    TBranch *b_tau_0_PanTau_BDTVar_Basic_NNeutralConsts;                                                       //!
    TBranch *b_tau_0_PanTau_BDTVar_Charged_HLV_SumM;                                                           //!
    TBranch *b_tau_0_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt;                                             //!
    TBranch *b_tau_0_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts;                                           //!
    TBranch *b_tau_0_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged;                                      //!
    TBranch *b_tau_0_PanTau_BDTVar_Neutral_HLV_SumM;                                                           //!
    TBranch *b_tau_0_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1;                                            //!
    TBranch *b_tau_0_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2;                                            //!
    TBranch *b_tau_0_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts;                                      //!
    TBranch *b_tau_0_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts;                                            //!
    TBranch *b_tau_0_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed;                                               //!
    TBranch *b_tau_0_PanTau_DecayModeProto;                                                                    //!
    TBranch *b_tau_0_PanTau_isPanTauCandidate;                                                                 //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_LooseEleBDT_electron;                    //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_MediumEleBDT_electron;                   //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_TightEleBDT_electron;                    //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_LooseEleBDT_electron;                      //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_MediumEleBDT_electron;                     //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_TightEleBDT_electron;                      //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDT_electron;                    //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDT_electron;                   //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_TightEleBDT_electron;                    //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDT_electron;                      //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDT_electron;                     //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_TightEleBDT_electron;                      //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT_1down_TauEffSF_selection;                               //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT_1up_TauEffSF_selection;                                 //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1down_TauEffSF_selection;                               //!
    TBranch *b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1up_TauEffSF_selection;                                 //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad;                      //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection;                                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad;                        //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection;                                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco;                                       //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection;                                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco;                                         //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection;                                    //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium;               //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_selection;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium;                 //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_selection;                    //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium;               //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_selection;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium;                 //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_selection;                    //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium;               //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_selection;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium;                 //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_selection;                    //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium;               //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_selection;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium;                 //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_selection;                    //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium;               //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_selection;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium;                 //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_selection;                    //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium;               //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_selection;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium;                 //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_selection;                    //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium;               //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_selection;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium;                 //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_selection;                    //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium;               //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight;                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_selection;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium;                 //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight;                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_selection;                    //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNloose;                              //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNmedium;                             //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNtight;                              //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_selection;                                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNloose;                                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNmedium;                               //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNtight;                                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_selection;                                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNloose;                                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNmedium;                               //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNtight;                                //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_selection;                                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNloose;                                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNmedium;                                 //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNtight;                                  //!
    TBranch *b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_selection;                                    //!
    TBranch *b_tau_0_TauID_ChPiEMEOverCaloEME;                                                                 //!
    TBranch *b_tau_0_TauID_EMPOverTrkSysP;                                                                     //!
    TBranch *b_tau_0_TauID_SumPtTrkFrac;                                                                       //!
    TBranch *b_tau_0_TauID_centFrac;                                                                           //!
    TBranch *b_tau_0_TauID_dRmax;                                                                              //!
    TBranch *b_tau_0_TauID_etOverPtLeadTrk;                                                                    //!
    TBranch *b_tau_0_TauID_innerTrkAvgDist;                                                                    //!
    TBranch *b_tau_0_TauID_ipSigLeadTrk;                                                                       //!
    TBranch *b_tau_0_TauID_mEflowApprox;                                                                       //!
    TBranch *b_tau_0_TauID_massTrkSys;                                                                         //!
    TBranch *b_tau_0_TauID_ptIntermediateAxis;                                                                 //!
    TBranch *b_tau_0_TauID_ptRatioEflowApprox;                                                                 //!
    TBranch *b_tau_0_TauID_trFlightPathSig;                                                                    //!
    TBranch *b_tau_0_allTrk_eta;                                                                               //!
    TBranch *b_tau_0_allTrk_n;                                                                                 //!
    TBranch *b_tau_0_allTrk_phi;                                                                               //!
    TBranch *b_tau_0_allTrk_pt;                                                                                //!
    TBranch *b_tau_0_b_tag_score;                                                                              //!
    TBranch *b_tau_0_b_tagged;                                                                                 //!
    TBranch *b_tau_0_charged_tracks_d0;                                                                        //!
    TBranch *b_tau_0_charged_tracks_d0_sig;                                                                    //!
    TBranch *b_tau_0_charged_tracks_eProbabilityHT;                                                            //!
    TBranch *b_tau_0_charged_tracks_nInnermostPixelHits;                                                       //!
    TBranch *b_tau_0_charged_tracks_nPixelHits;                                                                //!
    TBranch *b_tau_0_charged_tracks_nSCTHits;                                                                  //!
    TBranch *b_tau_0_charged_tracks_p4;                                                                        //!
    TBranch *b_tau_0_charged_tracks_pt_err;                                                                    //!
    TBranch *b_tau_0_charged_tracks_z0;                                                                        //!
    TBranch *b_tau_0_charged_tracks_z0_sig;                                                                    //!
    TBranch *b_tau_0_charged_tracks_z0_sintheta;                                                               //!
    TBranch *b_tau_0_charged_tracks_z0_sintheta_tjva;                                                          //!
    TBranch *b_tau_0_conversion_tracks_p4;                                                                     //!
    TBranch *b_tau_0_decay_mode;                                                                               //!
    TBranch *b_tau_0_ele_bdt_loose_retuned;                                                                    //!
    TBranch *b_tau_0_ele_bdt_medium_retuned;                                                                   //!
    TBranch *b_tau_0_ele_bdt_score_retuned;                                                                    //!
    TBranch *b_tau_0_ele_bdt_score_trans_retuned;                                                              //!
    TBranch *b_tau_0_ele_bdt_tight_retuned;                                                                    //!
    TBranch *b_tau_0_ele_match_lhscore;                                                                        //!
    TBranch *b_tau_0_ele_olr_pass;                                                                             //!
    TBranch *b_tau_0_jet_bdt_score;                                                                            //!
    TBranch *b_tau_0_jet_bdt_score_trans;                                                                      //!
    TBranch *b_tau_0_jet_jvt;                                                                                  //!
    TBranch *b_tau_0_jet_rnn_loose;                                                                            //!
    TBranch *b_tau_0_jet_rnn_medium;                                                                           //!
    TBranch *b_tau_0_jet_rnn_score;                                                                            //!
    TBranch *b_tau_0_jet_rnn_score_trans;                                                                      //!
    TBranch *b_tau_0_jet_rnn_tight;                                                                            //!
    TBranch *b_tau_0_jet_rnn_veryloose;                                                                        //!
    TBranch *b_tau_0_jet_seed_TrackWidthPt1000_wrt_PV;                                                         //!
    TBranch *b_tau_0_jet_seed_TrackWidthPt1000_wrt_TV;                                                         //!
    TBranch *b_tau_0_jet_width;                                                                                //!
    TBranch *b_tau_0_matched;                                                                                  //!
    TBranch *b_tau_0_matched_classifierParticleOrigin;                                                         //!
    TBranch *b_tau_0_matched_classifierParticleType;                                                           //!
    TBranch *b_tau_0_matched_decayVertex;                                                                      //!
    TBranch *b_tau_0_matched_decayVertex_v;                                                                    //!
    TBranch *b_tau_0_matched_decay_mode;                                                                       //!
    TBranch *b_tau_0_matched_isEle;                                                                            //!
    TBranch *b_tau_0_matched_isHadTau;                                                                         //!
    TBranch *b_tau_0_matched_isJet;                                                                            //!
    TBranch *b_tau_0_matched_isMuon;                                                                           //!
    TBranch *b_tau_0_matched_isTau;                                                                            //!
    TBranch *b_tau_0_matched_isTruthMatch;                                                                     //!
    TBranch *b_tau_0_matched_mother_pdgId;                                                                     //!
    TBranch *b_tau_0_matched_mother_status;                                                                    //!
    TBranch *b_tau_0_matched_n_charged;                                                                        //!
    TBranch *b_tau_0_matched_n_charged_pion;                                                                   //!
    TBranch *b_tau_0_matched_n_neutral;                                                                        //!
    TBranch *b_tau_0_matched_n_neutral_pion;                                                                   //!
    TBranch *b_tau_0_matched_origin;                                                                           //!
    TBranch *b_tau_0_matched_p4;                                                                               //!
    TBranch *b_tau_0_matched_p4_vis_charged_track0;                                                            //!
    TBranch *b_tau_0_matched_p4_vis_charged_track1;                                                            //!
    TBranch *b_tau_0_matched_p4_vis_charged_track2;                                                            //!
    TBranch *b_tau_0_matched_pdgId;                                                                            //!
    TBranch *b_tau_0_matched_productionVertex;                                                                 //!
    TBranch *b_tau_0_matched_productionVertex_v;                                                               //!
    TBranch *b_tau_0_matched_pz;                                                                               //!
    TBranch *b_tau_0_matched_q;                                                                                //!
    TBranch *b_tau_0_matched_status;                                                                           //!
    TBranch *b_tau_0_matched_type;                                                                             //!
    TBranch *b_tau_0_matched_vis_charged_p4;                                                                   //!
    TBranch *b_tau_0_matched_vis_neutral_others_p4;                                                            //!
    TBranch *b_tau_0_matched_vis_neutral_p4;                                                                   //!
    TBranch *b_tau_0_matched_vis_neutral_pions_p4;                                                             //!
    TBranch *b_tau_0_matched_vis_p4;                                                                           //!
    TBranch *b_tau_0_n_all_tracks;                                                                             //!
    TBranch *b_tau_0_n_charged_tracks;                                                                         //!
    TBranch *b_tau_0_n_conversion_tracks;                                                                      //!
    TBranch *b_tau_0_n_core_tracks;                                                                            //!
    TBranch *b_tau_0_n_failTrackFilter_tracks;                                                                 //!
    TBranch *b_tau_0_n_fake_tracks;                                                                            //!
    TBranch *b_tau_0_n_isolation_tracks;                                                                       //!
    TBranch *b_tau_0_n_modified_isolation_tracks;                                                              //!
    TBranch *b_tau_0_n_old_tracks;                                                                             //!
    TBranch *b_tau_0_n_passTrkSelectionTight_tracks;                                                           //!
    TBranch *b_tau_0_n_passTrkSelector_tracks;                                                                 //!
    TBranch *b_tau_0_n_unclassified_tracks;                                                                    //!
    TBranch *b_tau_0_n_wide_tracks;                                                                            //!
    TBranch *b_tau_0_origin;                                                                                   //!
    TBranch *b_tau_0_p4;                                                                                       //!
    TBranch *b_tau_0_pass_tst_muonolr;                                                                         //!
    TBranch *b_tau_0_primary_vertex;                                                                           //!
    TBranch *b_tau_0_primary_vertex_v;                                                                         //!
    TBranch *b_tau_0_q;                                                                                        //!
    TBranch *b_tau_0_secondary_vertex;                                                                         //!
    TBranch *b_tau_0_secondary_vertex_v;                                                                       //!
    TBranch *b_tau_0_trk_multi_diag_n_had_tracks;                                                              //!
    TBranch *b_tau_0_trk_multi_diag_n_spu_cv_tracks;                                                           //!
    TBranch *b_tau_0_trk_multi_diag_n_spu_fk_tracks;                                                           //!
    TBranch *b_tau_0_trk_multi_diag_n_spu_pu_tracks;                                                           //!
    TBranch *b_tau_0_trk_multi_diag_n_spu_tracks;                                                              //!
    TBranch *b_tau_0_trk_multi_diag_n_spu_uc_tracks;                                                           //!
    TBranch *b_tau_0_trk_multi_diag_n_spu_ue_tracks;                                                           //!
    TBranch *b_tau_0_trk_multi_diag_n_tau_tracks;                                                              //!
    TBranch *b_tau_0_trk_multi_diag_purity;                                                                    //!
    TBranch *b_tau_0_type;                                                                                     //!
    TBranch *b_tau_tes_alpha_pt_shift;                                                                         //!
    TBranch *b_weight_mc;                                                                                      //!
    TBranch *b_Weight_mc;                                                                                      //!
    TBranch *b_mergedRunNumber;                                                                                //!
    TBranch *b_upsilon;                                                                                        //!
    TBranch *b_isQCR;                                                                                          //!
    TBranch *b_isWCR;                                                                                          //!
    TBranch *b_isWCR_QCD;                                                                                      //!
    TBranch *b_isIQCR_Iso;                                                                                     //!
    TBranch *b_isIQCR_NotIso;                                                                                  //!
    TBranch *b_isTCR;                                                                                          //!
    TBranch *b_isSR;                                                                                           //!
    TBranch *b_isData;                                                                                         //!

    void Init(TChain *tree) override {
        std::lock_guard<std::mutex> lock(_mutex);
        // The Init() function is called when the selector needs to initialize
        // a new tree or chain. Typically here the branch addresses and branch
        // pointers of the tree will be set.
        // It is normally not necessary to make changes to the generated
        // code, but the routine can be extended by the user if needed.
        // Init() will be called many times when running on PROOF
        // (once per file to be processed).

        // Set object pointer
        jet_0_matched_p4 = 0;
        jet_0_p4 = 0;
        lep_0_matched_leptonic_tau_decay_invis_p4 = 0;
        lep_0_matched_leptonic_tau_decay_p4 = 0;
        lep_0_matched_leptonic_tau_decay_vis_p4 = 0;
        lep_0_matched_p4 = 0;
        lep_0_p4 = 0;
        lep_0_trk_p4 = 0;
        lep_0_trk_vx_v = 0;
        lephad_p4 = 0;
        met_reco_p4 = 0;
        met_truth_p4 = 0;
        tau_0_charged_tracks_d0 = 0;
        tau_0_charged_tracks_d0_sig = 0;
        tau_0_charged_tracks_eProbabilityHT = 0;
        tau_0_charged_tracks_nInnermostPixelHits = 0;
        tau_0_charged_tracks_nPixelHits = 0;
        tau_0_charged_tracks_nSCTHits = 0;
        tau_0_charged_tracks_p4 = 0;
        tau_0_charged_tracks_pt_err = 0;
        tau_0_charged_tracks_z0 = 0;
        tau_0_charged_tracks_z0_sig = 0;
        tau_0_charged_tracks_z0_sintheta = 0;
        tau_0_charged_tracks_z0_sintheta_tjva = 0;
        tau_0_conversion_tracks_p4 = 0;
        tau_0_matched_decayVertex_v = 0;
        tau_0_matched_p4 = 0;
        tau_0_matched_p4_vis_charged_track0 = 0;
        tau_0_matched_p4_vis_charged_track1 = 0;
        tau_0_matched_p4_vis_charged_track2 = 0;
        tau_0_matched_productionVertex_v = 0;
        tau_0_matched_vis_charged_p4 = 0;
        tau_0_matched_vis_neutral_others_p4 = 0;
        tau_0_matched_vis_neutral_p4 = 0;
        tau_0_matched_vis_neutral_pions_p4 = 0;
        tau_0_matched_vis_p4 = 0;
        tau_0_p4 = 0;
        tau_0_primary_vertex_v = 0;
        tau_0_secondary_vertex_v = 0;
        isQCR = false;
        isWCR = false;
        isWCR_QCD = false;
        isIQCR_Iso = false;
        isIQCR_NotIso = false;
        isTCR = false;
        isSR = false;
        isData = false;

        // Set branch addresses and branch pointers
        if (!tree) return;
        fChain = tree;
        fChain->SetMakeClass(1);

        fChain->SetBranchAddress("AMI_cross_section", &AMI_cross_section, &b_AMI_cross_section);
        fChain->SetBranchAddress("HLT_mu20_iloose_L1MU15", &HLT_mu20_iloose_L1MU15, &b_HLT_mu20_iloose_L1MU15);
        fChain->SetBranchAddress("HLT_mu26_ivarmedium", &HLT_mu26_ivarmedium, &b_HLT_mu26_ivarmedium);
        fChain->SetBranchAddress("HLT_mu50", &HLT_mu50, &b_HLT_mu50);
        fChain->SetBranchAddress("NOMINAL_pileup_combined_weight", &NOMINAL_pileup_combined_weight, &b_NOMINAL_pileup_combined_weight);
        fChain->SetBranchAddress("NOMINAL_pileup_random_run_number", &NOMINAL_pileup_random_run_number, &b_NOMINAL_pileup_random_run_number);
        fChain->SetBranchAddress("PRW_DATASF_1down_pileup_combined_weight", &PRW_DATASF_1down_pileup_combined_weight,
                                 &b_PRW_DATASF_1down_pileup_combined_weight);
        fChain->SetBranchAddress("PRW_DATASF_1down_pileup_random_run_number", &PRW_DATASF_1down_pileup_random_run_number,
                                 &b_PRW_DATASF_1down_pileup_random_run_number);
        fChain->SetBranchAddress("PRW_DATASF_1up_pileup_combined_weight", &PRW_DATASF_1up_pileup_combined_weight,
                                 &b_PRW_DATASF_1up_pileup_combined_weight);
        fChain->SetBranchAddress("PRW_DATASF_1up_pileup_random_run_number", &PRW_DATASF_1up_pileup_random_run_number,
                                 &b_PRW_DATASF_1up_pileup_random_run_number);
        fChain->SetBranchAddress("cross_section", &cross_section, &b_cross_section);
        fChain->SetBranchAddress("event_clean_EC_LooseBad", &event_clean_EC_LooseBad, &b_event_clean_EC_LooseBad);
        fChain->SetBranchAddress("event_clean_detector_core", &event_clean_detector_core, &b_event_clean_detector_core);
        fChain->SetBranchAddress("event_is_bad_batman", &event_is_bad_batman, &b_event_is_bad_batman);
        fChain->SetBranchAddress("event_number", &event_number, &b_event_number);
        fChain->SetBranchAddress("filter_efficiency", &filter_efficiency, &b_filter_efficiency);
        fChain->SetBranchAddress("jet_0", &jet_0, &b_jet_0);
        fChain->SetBranchAddress("jet_0_Bottom_DL1_score", &jet_0_Bottom_DL1_score, &b_jet_0_Bottom_DL1_score);
        fChain->SetBranchAddress("jet_0_Charm_DL1_score", &jet_0_Charm_DL1_score, &b_jet_0_Charm_DL1_score);
        fChain->SetBranchAddress("jet_0_DL1r_FixedCutBEff_70_weight", &jet_0_DL1r_FixedCutBEff_70_weight, &b_jet_0_DL1r_FixedCutBEff_70_weight);
        fChain->SetBranchAddress("jet_0_Light_DL1_score", &jet_0_Light_DL1_score, &b_jet_0_Light_DL1_score);
        fChain->SetBranchAddress("jet_0_b_tag_quantile", &jet_0_b_tag_quantile, &b_jet_0_b_tag_quantile);
        fChain->SetBranchAddress("jet_0_b_tag_score", &jet_0_b_tag_score, &b_jet_0_b_tag_score);
        fChain->SetBranchAddress("jet_0_b_tagged_DL1r_FixedCutBEff_70", &jet_0_b_tagged_DL1r_FixedCutBEff_70, &b_jet_0_b_tagged_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_0_cleanJet_EC_LooseBad", &jet_0_cleanJet_EC_LooseBad, &b_jet_0_cleanJet_EC_LooseBad);
        fChain->SetBranchAddress("jet_0_fjvt", &jet_0_fjvt, &b_jet_0_fjvt);
        fChain->SetBranchAddress("jet_0_flavorlabel", &jet_0_flavorlabel, &b_jet_0_flavorlabel);
        fChain->SetBranchAddress("jet_0_flavorlabel_cone", &jet_0_flavorlabel_cone, &b_jet_0_flavorlabel_cone);
        fChain->SetBranchAddress("jet_0_flavorlabel_part", &jet_0_flavorlabel_part, &b_jet_0_flavorlabel_part);
        fChain->SetBranchAddress("jet_0_is_Jvt_HS", &jet_0_is_Jvt_HS, &b_jet_0_is_Jvt_HS);
        fChain->SetBranchAddress("jet_0_jvt", &jet_0_jvt, &b_jet_0_jvt);
        fChain->SetBranchAddress("jet_0_matched_p4", &jet_0_matched_p4, &b_jet_0_matched_p4);
        fChain->SetBranchAddress("jet_0_matched_pdgid", &jet_0_matched_pdgid, &b_jet_0_matched_pdgid);
        fChain->SetBranchAddress("jet_0_origin", &jet_0_origin, &b_jet_0_origin);
        fChain->SetBranchAddress("jet_0_p4", &jet_0_p4, &b_jet_0_p4);
        fChain->SetBranchAddress("jet_0_q", &jet_0_q, &b_jet_0_q);
        fChain->SetBranchAddress("jet_0_type", &jet_0_type, &b_jet_0_type);
        fChain->SetBranchAddress("jet_0_width", &jet_0_width, &b_jet_0_width);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_0_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_0_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_0_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_0_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_1_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_1_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_1_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_1_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_2_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_2_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_2_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_2_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_2_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_2_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_2_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_2_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_2_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_B_2_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_B_2_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_B_2_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_0_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_0_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_0_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_0_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_1_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_1_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_1_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_1_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_2_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_2_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_2_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_2_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_2_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_2_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_2_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_2_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_2_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_2_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_2_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_2_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_3_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_3_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_3_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_3_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_3_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_3_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_3_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_3_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_3_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_C_3_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_C_3_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_C_3_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_0_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_0_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_0_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_0_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_1_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_1_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_1_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_1_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_2_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_2_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_2_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_2_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_2_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_2_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_2_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_2_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_2_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_2_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_2_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_2_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_3_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_3_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_3_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_3_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_3_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_3_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_3_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_3_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_3_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_3_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_3_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_3_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_4_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_4_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_4_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_4_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_4_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_4_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_4_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_4_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_4_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_Eigen_Light_4_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_Eigen_Light_4_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_Eigen_Light_4_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_extrapolation_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_extrapolation_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_extrapolation_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_extrapolation_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_extrapolation_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_extrapolation_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_extrapolation_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_extrapolation_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_extrapolation_from_charm_1down_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_extrapolation_from_charm_1down_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_extrapolation_from_charm_1up_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1r_FixedCutBEff_70",
                                 &jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_FT_EFF_extrapolation_from_charm_1up_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT",
                                 &jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT,
                                 &b_jet_JET_JvtEfficiency_1down_central_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT",
                                 &jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT,
                                 &b_jet_JET_JvtEfficiency_1down_central_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT", &jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT,
                                 &b_jet_JET_JvtEfficiency_1up_central_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT",
                                 &jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT,
                                 &b_jet_JET_JvtEfficiency_1up_central_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT",
                                 &jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT,
                                 &b_jet_JET_fJvtEfficiency_1down_forward_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT",
                                 &jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT,
                                 &b_jet_JET_fJvtEfficiency_1down_forward_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT",
                                 &jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT,
                                 &b_jet_JET_fJvtEfficiency_1up_forward_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT",
                                 &jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT,
                                 &b_jet_JET_fJvtEfficiency_1up_forward_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_central_jets_global_effSF_JVT", &jet_NOMINAL_central_jets_global_effSF_JVT,
                                 &b_jet_NOMINAL_central_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_central_jets_global_ineffSF_JVT", &jet_NOMINAL_central_jets_global_ineffSF_JVT,
                                 &b_jet_NOMINAL_central_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_forward_jets_global_effSF_JVT", &jet_NOMINAL_forward_jets_global_effSF_JVT,
                                 &b_jet_NOMINAL_forward_jets_global_effSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_forward_jets_global_ineffSF_JVT", &jet_NOMINAL_forward_jets_global_ineffSF_JVT,
                                 &b_jet_NOMINAL_forward_jets_global_ineffSF_JVT);
        fChain->SetBranchAddress("jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_70", &jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_70,
                                 &b_jet_NOMINAL_global_effSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_70", &jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_70,
                                 &b_jet_NOMINAL_global_ineffSF_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("kfactor", &kfactor, &b_kfactor);
        fChain->SetBranchAddress("lep_0", &lep_0, &b_lep_0);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose_VarRad", &lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose_VarRad,
                                 &b_lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoLoose_VarRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_FixedRad",
                                 &lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_FixedRad,
                                 &b_lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_VarRad",
                                 &lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_VarRad,
                                 &b_lep_0_MUON_EFF_ISO_STAT_1down_MuEffSF_IsoTightTrackOnly_VarRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose_VarRad", &lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose_VarRad,
                                 &b_lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoLoose_VarRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_FixedRad",
                                 &lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_FixedRad,
                                 &b_lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_VarRad",
                                 &lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_VarRad,
                                 &b_lep_0_MUON_EFF_ISO_STAT_1up_MuEffSF_IsoTightTrackOnly_VarRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose_VarRad", &lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose_VarRad,
                                 &b_lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoLoose_VarRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_FixedRad",
                                 &lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_FixedRad,
                                 &b_lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_VarRad",
                                 &lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_VarRad,
                                 &b_lep_0_MUON_EFF_ISO_SYS_1down_MuEffSF_IsoTightTrackOnly_VarRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose_VarRad", &lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose_VarRad,
                                 &b_lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoLoose_VarRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_FixedRad",
                                 &lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_FixedRad,
                                 &b_lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_VarRad",
                                 &lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_VarRad,
                                 &b_lep_0_MUON_EFF_ISO_SYS_1up_MuEffSF_IsoTightTrackOnly_VarRad);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_STAT_1down_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_STAT_1up_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium",
                                 &lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_STAT_LOWPT_1down_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium",
                                 &lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_STAT_LOWPT_1up_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_SYS_1down_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium", &lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_SYS_1up_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium",
                                 &lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_SYS_LOWPT_1down_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium",
                                 &lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_MUON_EFF_RECO_SYS_LOWPT_1up_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA", &lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA,
                                 &b_lep_0_MUON_EFF_TTVA_STAT_1down_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA", &lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA,
                                 &b_lep_0_MUON_EFF_TTVA_STAT_1up_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA", &lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA,
                                 &b_lep_0_MUON_EFF_TTVA_SYS_1down_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA", &lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA,
                                 &b_lep_0_MUON_EFF_TTVA_SYS_1up_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium",
                                 &lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium,
                                 &b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium",
                                 &lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium,
                                 &b_lep_0_MUON_EFF_TrigStatUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium",
                                 &lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium,
                                 &b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium",
                                 &lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium,
                                 &b_lep_0_MUON_EFF_TrigStatUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium",
                                 &lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium,
                                 &b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium",
                                 &lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium,
                                 &b_lep_0_MUON_EFF_TrigSystUncertainty_1down_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium",
                                 &lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium,
                                 &b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium",
                                 &lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium,
                                 &b_lep_0_MUON_EFF_TrigSystUncertainty_1up_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium",
                                 &lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium,
                                 &b_lep_0_NOMINAL_MuEffSF_HLT_mu20_iloose_L1MU15_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium",
                                 &lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium,
                                 &b_lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium);
        fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_IsoLoose_VarRad", &lep_0_NOMINAL_MuEffSF_IsoLoose_VarRad,
                                 &b_lep_0_NOMINAL_MuEffSF_IsoLoose_VarRad);
        fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_IsoTightTrackOnly_FixedRad", &lep_0_NOMINAL_MuEffSF_IsoTightTrackOnly_FixedRad,
                                 &b_lep_0_NOMINAL_MuEffSF_IsoTightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_IsoTightTrackOnly_VarRad", &lep_0_NOMINAL_MuEffSF_IsoTightTrackOnly_VarRad,
                                 &b_lep_0_NOMINAL_MuEffSF_IsoTightTrackOnly_VarRad);
        fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_Reco_QualMedium", &lep_0_NOMINAL_MuEffSF_Reco_QualMedium,
                                 &b_lep_0_NOMINAL_MuEffSF_Reco_QualMedium);
        fChain->SetBranchAddress("lep_0_NOMINAL_MuEffSF_TTVA", &lep_0_NOMINAL_MuEffSF_TTVA, &b_lep_0_NOMINAL_MuEffSF_TTVA);
        fChain->SetBranchAddress("lep_0_cluster_eta", &lep_0_cluster_eta, &b_lep_0_cluster_eta);
        fChain->SetBranchAddress("lep_0_cluster_eta_be2", &lep_0_cluster_eta_be2, &b_lep_0_cluster_eta_be2);
        fChain->SetBranchAddress("lep_0_id_bad", &lep_0_id_bad, &b_lep_0_id_bad);
        fChain->SetBranchAddress("lep_0_id_charge", &lep_0_id_charge, &b_lep_0_id_charge);
        fChain->SetBranchAddress("lep_0_id_loose", &lep_0_id_loose, &b_lep_0_id_loose);
        fChain->SetBranchAddress("lep_0_id_medium", &lep_0_id_medium, &b_lep_0_id_medium);
        fChain->SetBranchAddress("lep_0_id_tight", &lep_0_id_tight, &b_lep_0_id_tight);
        fChain->SetBranchAddress("lep_0_id_veryloose", &lep_0_id_veryloose, &b_lep_0_id_veryloose);
        fChain->SetBranchAddress("lep_0_iso_HighPtTrackOnly", &lep_0_iso_HighPtTrackOnly, &b_lep_0_iso_HighPtTrackOnly);
        fChain->SetBranchAddress("lep_0_iso_Loose_FixedRad", &lep_0_iso_Loose_FixedRad, &b_lep_0_iso_Loose_FixedRad);
        fChain->SetBranchAddress("lep_0_iso_Loose_VarRad", &lep_0_iso_Loose_VarRad, &b_lep_0_iso_Loose_VarRad);
        fChain->SetBranchAddress("lep_0_iso_PflowLoose_FixedRad", &lep_0_iso_PflowLoose_FixedRad, &b_lep_0_iso_PflowLoose_FixedRad);
        fChain->SetBranchAddress("lep_0_iso_PflowLoose_VarRad", &lep_0_iso_PflowLoose_VarRad, &b_lep_0_iso_PflowLoose_VarRad);
        fChain->SetBranchAddress("lep_0_iso_PflowTight_FixedRad", &lep_0_iso_PflowTight_FixedRad, &b_lep_0_iso_PflowTight_FixedRad);
        fChain->SetBranchAddress("lep_0_iso_PflowTight_VarRad", &lep_0_iso_PflowTight_VarRad, &b_lep_0_iso_PflowTight_VarRad);
        fChain->SetBranchAddress("lep_0_iso_TightTrackOnly_FixedRad", &lep_0_iso_TightTrackOnly_FixedRad, &b_lep_0_iso_TightTrackOnly_FixedRad);
        fChain->SetBranchAddress("lep_0_iso_TightTrackOnly_VarRad", &lep_0_iso_TightTrackOnly_VarRad, &b_lep_0_iso_TightTrackOnly_VarRad);
        fChain->SetBranchAddress("lep_0_iso_Tight_FixedRad", &lep_0_iso_Tight_FixedRad, &b_lep_0_iso_Tight_FixedRad);
        fChain->SetBranchAddress("lep_0_iso_Tight_VarRad", &lep_0_iso_Tight_VarRad, &b_lep_0_iso_Tight_VarRad);
        fChain->SetBranchAddress("lep_0_iso_neflowisol20", &lep_0_iso_neflowisol20, &b_lep_0_iso_neflowisol20);
        fChain->SetBranchAddress("lep_0_iso_ptcone20_TightTTVA_pt1000", &lep_0_iso_ptcone20_TightTTVA_pt1000, &b_lep_0_iso_ptcone20_TightTTVA_pt1000);
        fChain->SetBranchAddress("lep_0_iso_ptcone20_TightTTVA_pt500", &lep_0_iso_ptcone20_TightTTVA_pt500, &b_lep_0_iso_ptcone20_TightTTVA_pt500);
        fChain->SetBranchAddress("lep_0_iso_ptvarcone30_TightTTVA_pt1000", &lep_0_iso_ptvarcone30_TightTTVA_pt1000,
                                 &b_lep_0_iso_ptvarcone30_TightTTVA_pt1000);
        fChain->SetBranchAddress("lep_0_iso_ptvarcone30_TightTTVA_pt500", &lep_0_iso_ptvarcone30_TightTTVA_pt500,
                                 &b_lep_0_iso_ptvarcone30_TightTTVA_pt500);
        fChain->SetBranchAddress("lep_0_iso_topoetcone20", &lep_0_iso_topoetcone20, &b_lep_0_iso_topoetcone20);
        fChain->SetBranchAddress("lep_0_matched", &lep_0_matched, &b_lep_0_matched);
        fChain->SetBranchAddress("lep_0_matched_classifierParticleOrigin", &lep_0_matched_classifierParticleOrigin,
                                 &b_lep_0_matched_classifierParticleOrigin);
        fChain->SetBranchAddress("lep_0_matched_classifierParticleType", &lep_0_matched_classifierParticleType,
                                 &b_lep_0_matched_classifierParticleType);
        fChain->SetBranchAddress("lep_0_matched_isHad", &lep_0_matched_isHad, &b_lep_0_matched_isHad);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay", &lep_0_matched_leptonic_tau_decay, &b_lep_0_matched_leptonic_tau_decay);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_classifierParticleOrigin",
                                 &lep_0_matched_leptonic_tau_decay_classifierParticleOrigin,
                                 &b_lep_0_matched_leptonic_tau_decay_classifierParticleOrigin);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_classifierParticleType", &lep_0_matched_leptonic_tau_decay_classifierParticleType,
                                 &b_lep_0_matched_leptonic_tau_decay_classifierParticleType);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_invis_p4", &lep_0_matched_leptonic_tau_decay_invis_p4,
                                 &b_lep_0_matched_leptonic_tau_decay_invis_p4);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_mother_pdgId", &lep_0_matched_leptonic_tau_decay_mother_pdgId,
                                 &b_lep_0_matched_leptonic_tau_decay_mother_pdgId);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_mother_status", &lep_0_matched_leptonic_tau_decay_mother_status,
                                 &b_lep_0_matched_leptonic_tau_decay_mother_status);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_origin", &lep_0_matched_leptonic_tau_decay_origin,
                                 &b_lep_0_matched_leptonic_tau_decay_origin);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_p4", &lep_0_matched_leptonic_tau_decay_p4, &b_lep_0_matched_leptonic_tau_decay_p4);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_pdgId", &lep_0_matched_leptonic_tau_decay_pdgId,
                                 &b_lep_0_matched_leptonic_tau_decay_pdgId);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_pz", &lep_0_matched_leptonic_tau_decay_pz, &b_lep_0_matched_leptonic_tau_decay_pz);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_q", &lep_0_matched_leptonic_tau_decay_q, &b_lep_0_matched_leptonic_tau_decay_q);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_status", &lep_0_matched_leptonic_tau_decay_status,
                                 &b_lep_0_matched_leptonic_tau_decay_status);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_type", &lep_0_matched_leptonic_tau_decay_type,
                                 &b_lep_0_matched_leptonic_tau_decay_type);
        fChain->SetBranchAddress("lep_0_matched_leptonic_tau_decay_vis_p4", &lep_0_matched_leptonic_tau_decay_vis_p4,
                                 &b_lep_0_matched_leptonic_tau_decay_vis_p4);
        fChain->SetBranchAddress("lep_0_matched_mother_pdgId", &lep_0_matched_mother_pdgId, &b_lep_0_matched_mother_pdgId);
        fChain->SetBranchAddress("lep_0_matched_mother_status", &lep_0_matched_mother_status, &b_lep_0_matched_mother_status);
        fChain->SetBranchAddress("lep_0_matched_origin", &lep_0_matched_origin, &b_lep_0_matched_origin);
        fChain->SetBranchAddress("lep_0_matched_p4", &lep_0_matched_p4, &b_lep_0_matched_p4);
        fChain->SetBranchAddress("lep_0_matched_pdgId", &lep_0_matched_pdgId, &b_lep_0_matched_pdgId);
        fChain->SetBranchAddress("lep_0_matched_q", &lep_0_matched_q, &b_lep_0_matched_q);
        fChain->SetBranchAddress("lep_0_matched_status", &lep_0_matched_status, &b_lep_0_matched_status);
        fChain->SetBranchAddress("lep_0_matched_type", &lep_0_matched_type, &b_lep_0_matched_type);
        fChain->SetBranchAddress("lep_0_muonAuthor", &lep_0_muonAuthor, &b_lep_0_muonAuthor);
        fChain->SetBranchAddress("lep_0_muonType", &lep_0_muonType, &b_lep_0_muonType);
        fChain->SetBranchAddress("lep_0_origin", &lep_0_origin, &b_lep_0_origin);
        fChain->SetBranchAddress("lep_0_p4", &lep_0_p4, &b_lep_0_p4);
        fChain->SetBranchAddress("lep_0_q", &lep_0_q, &b_lep_0_q);
        fChain->SetBranchAddress("lep_0_trk_d0", &lep_0_trk_d0, &b_lep_0_trk_d0);
        fChain->SetBranchAddress("lep_0_trk_d0_sig", &lep_0_trk_d0_sig, &b_lep_0_trk_d0_sig);
        fChain->SetBranchAddress("lep_0_trk_p4", &lep_0_trk_p4, &b_lep_0_trk_p4);
        fChain->SetBranchAddress("lep_0_trk_pt_error", &lep_0_trk_pt_error, &b_lep_0_trk_pt_error);
        fChain->SetBranchAddress("lep_0_trk_pvx_z0", &lep_0_trk_pvx_z0, &b_lep_0_trk_pvx_z0);
        fChain->SetBranchAddress("lep_0_trk_pvx_z0_sig", &lep_0_trk_pvx_z0_sig, &b_lep_0_trk_pvx_z0_sig);
        fChain->SetBranchAddress("lep_0_trk_pvx_z0_sintheta", &lep_0_trk_pvx_z0_sintheta, &b_lep_0_trk_pvx_z0_sintheta);
        fChain->SetBranchAddress("lep_0_trk_vx", &lep_0_trk_vx, &b_lep_0_trk_vx);
        fChain->SetBranchAddress("lep_0_trk_vx_v", &lep_0_trk_vx_v, &b_lep_0_trk_vx_v);
        fChain->SetBranchAddress("lep_0_trk_z0", &lep_0_trk_z0, &b_lep_0_trk_z0);
        fChain->SetBranchAddress("lep_0_trk_z0_sig", &lep_0_trk_z0_sig, &b_lep_0_trk_z0_sig);
        fChain->SetBranchAddress("lep_0_trk_z0_sintheta", &lep_0_trk_z0_sintheta, &b_lep_0_trk_z0_sintheta);
        fChain->SetBranchAddress("lep_0_type", &lep_0_type, &b_lep_0_type);
        fChain->SetBranchAddress("lephad", &lephad, &b_lephad);
        fChain->SetBranchAddress("lephad_coll_approx", &lephad_coll_approx, &b_lephad_coll_approx);
        fChain->SetBranchAddress("lephad_coll_approx_m", &lephad_coll_approx_m, &b_lephad_coll_approx_m);
        fChain->SetBranchAddress("lephad_coll_approx_x0", &lephad_coll_approx_x0, &b_lephad_coll_approx_x0);
        fChain->SetBranchAddress("lephad_coll_approx_x1", &lephad_coll_approx_x1, &b_lephad_coll_approx_x1);
        fChain->SetBranchAddress("lephad_cosalpha", &lephad_cosalpha, &b_lephad_cosalpha);
        fChain->SetBranchAddress("lephad_deta", &lephad_deta, &b_lephad_deta);
        fChain->SetBranchAddress("lephad_dphi", &lephad_dphi, &b_lephad_dphi);
        fChain->SetBranchAddress("lephad_dpt", &lephad_dpt, &b_lephad_dpt);
        fChain->SetBranchAddress("lephad_dr", &lephad_dr, &b_lephad_dr);
        fChain->SetBranchAddress("lephad_met_bisect", &lephad_met_bisect, &b_lephad_met_bisect);
        fChain->SetBranchAddress("lephad_met_centrality", &lephad_met_centrality, &b_lephad_met_centrality);
        fChain->SetBranchAddress("lephad_met_lep0_cos_dphi", &lephad_met_lep0_cos_dphi, &b_lephad_met_lep0_cos_dphi);
        fChain->SetBranchAddress("lephad_met_lep1_cos_dphi", &lephad_met_lep1_cos_dphi, &b_lephad_met_lep1_cos_dphi);
        fChain->SetBranchAddress("lephad_met_min_dphi", &lephad_met_min_dphi, &b_lephad_met_min_dphi);
        fChain->SetBranchAddress("lephad_met_sum_cos_dphi", &lephad_met_sum_cos_dphi, &b_lephad_met_sum_cos_dphi);
        fChain->SetBranchAddress("lephad_mt_lep0_met", &lephad_mt_lep0_met, &b_lephad_mt_lep0_met);
        fChain->SetBranchAddress("lephad_mt_lep1_met", &lephad_mt_lep1_met, &b_lephad_mt_lep1_met);
        fChain->SetBranchAddress("lephad_p4", &lephad_p4, &b_lephad_p4);
        fChain->SetBranchAddress("lephad_qxq", &lephad_qxq, &b_lephad_qxq);
        fChain->SetBranchAddress("lephad_scal_sum_pt", &lephad_scal_sum_pt, &b_lephad_scal_sum_pt);
        fChain->SetBranchAddress("mc_channel_number", &mc_channel_number, &b_mc_channel_number);
        fChain->SetBranchAddress("met_more_met_et_ele", &met_more_met_et_ele, &b_met_more_met_et_ele);
        fChain->SetBranchAddress("met_more_met_et_jet", &met_more_met_et_jet, &b_met_more_met_et_jet);
        fChain->SetBranchAddress("met_more_met_et_muon", &met_more_met_et_muon, &b_met_more_met_et_muon);
        fChain->SetBranchAddress("met_more_met_et_pho", &met_more_met_et_pho, &b_met_more_met_et_pho);
        fChain->SetBranchAddress("met_more_met_et_soft", &met_more_met_et_soft, &b_met_more_met_et_soft);
        fChain->SetBranchAddress("met_more_met_et_tau", &met_more_met_et_tau, &b_met_more_met_et_tau);
        fChain->SetBranchAddress("met_more_met_phi_ele", &met_more_met_phi_ele, &b_met_more_met_phi_ele);
        fChain->SetBranchAddress("met_more_met_phi_jet", &met_more_met_phi_jet, &b_met_more_met_phi_jet);
        fChain->SetBranchAddress("met_more_met_phi_muon", &met_more_met_phi_muon, &b_met_more_met_phi_muon);
        fChain->SetBranchAddress("met_more_met_phi_pho", &met_more_met_phi_pho, &b_met_more_met_phi_pho);
        fChain->SetBranchAddress("met_more_met_phi_soft", &met_more_met_phi_soft, &b_met_more_met_phi_soft);
        fChain->SetBranchAddress("met_more_met_phi_tau", &met_more_met_phi_tau, &b_met_more_met_phi_tau);
        fChain->SetBranchAddress("met_more_met_sumet_ele", &met_more_met_sumet_ele, &b_met_more_met_sumet_ele);
        fChain->SetBranchAddress("met_more_met_sumet_jet", &met_more_met_sumet_jet, &b_met_more_met_sumet_jet);
        fChain->SetBranchAddress("met_more_met_sumet_muon", &met_more_met_sumet_muon, &b_met_more_met_sumet_muon);
        fChain->SetBranchAddress("met_more_met_sumet_pho", &met_more_met_sumet_pho, &b_met_more_met_sumet_pho);
        fChain->SetBranchAddress("met_more_met_sumet_soft", &met_more_met_sumet_soft, &b_met_more_met_sumet_soft);
        fChain->SetBranchAddress("met_more_met_sumet_tau", &met_more_met_sumet_tau, &b_met_more_met_sumet_tau);
        fChain->SetBranchAddress("met_reco_p4", &met_reco_p4, &b_met_reco_p4);
        fChain->SetBranchAddress("met_reco_sig", &met_reco_sig, &b_met_reco_sig);
        fChain->SetBranchAddress("met_reco_sig_tracks", &met_reco_sig_tracks, &b_met_reco_sig_tracks);
        fChain->SetBranchAddress("met_reco_sumet", &met_reco_sumet, &b_met_reco_sumet);
        fChain->SetBranchAddress("met_truth_p4", &met_truth_p4, &b_met_truth_p4);
        fChain->SetBranchAddress("met_truth_sig", &met_truth_sig, &b_met_truth_sig);
        fChain->SetBranchAddress("met_truth_sig_tracks", &met_truth_sig_tracks, &b_met_truth_sig_tracks);
        fChain->SetBranchAddress("met_truth_sumet", &met_truth_sumet, &b_met_truth_sumet);
        fChain->SetBranchAddress("muTrigMatch_0_HLT_mu20_iloose_L1MU15", &muTrigMatch_0_HLT_mu20_iloose_L1MU15,
                                 &b_muTrigMatch_0_HLT_mu20_iloose_L1MU15);
        fChain->SetBranchAddress("muTrigMatch_0_HLT_mu26_ivarmedium", &muTrigMatch_0_HLT_mu26_ivarmedium, &b_muTrigMatch_0_HLT_mu26_ivarmedium);
        fChain->SetBranchAddress("muTrigMatch_0_HLT_mu50", &muTrigMatch_0_HLT_mu50, &b_muTrigMatch_0_HLT_mu50);
        fChain->SetBranchAddress("muTrigMatch_0_trigger_matched", &muTrigMatch_0_trigger_matched, &b_muTrigMatch_0_trigger_matched);
        fChain->SetBranchAddress("n_actual_int", &n_actual_int, &b_n_actual_int);
        fChain->SetBranchAddress("n_actual_int_cor", &n_actual_int_cor, &b_n_actual_int_cor);
        fChain->SetBranchAddress("n_avg_int", &n_avg_int, &b_n_avg_int);
        fChain->SetBranchAddress("n_avg_int_cor", &n_avg_int_cor, &b_n_avg_int_cor);
        fChain->SetBranchAddress("n_bjets_DL1r_FixedCutBEff_70", &n_bjets_DL1r_FixedCutBEff_70, &b_n_bjets_DL1r_FixedCutBEff_70);
        fChain->SetBranchAddress("n_electrons", &n_electrons, &b_n_electrons);
        fChain->SetBranchAddress("n_electrons_met", &n_electrons_met, &b_n_electrons_met);
        fChain->SetBranchAddress("n_electrons_olr", &n_electrons_olr, &b_n_electrons_olr);
        fChain->SetBranchAddress("n_jets", &n_jets, &b_n_jets);
        fChain->SetBranchAddress("n_jets_30", &n_jets_30, &b_n_jets_30);
        fChain->SetBranchAddress("n_jets_40", &n_jets_40, &b_n_jets_40);
        fChain->SetBranchAddress("n_jets_bad", &n_jets_bad, &b_n_jets_bad);
        fChain->SetBranchAddress("n_jets_mc_hs", &n_jets_mc_hs, &b_n_jets_mc_hs);
        fChain->SetBranchAddress("n_jets_met", &n_jets_met, &b_n_jets_met);
        fChain->SetBranchAddress("n_jets_olr", &n_jets_olr, &b_n_jets_olr);
        fChain->SetBranchAddress("n_muons", &n_muons, &b_n_muons);
        fChain->SetBranchAddress("n_muons_bad", &n_muons_bad, &b_n_muons_bad);
        fChain->SetBranchAddress("n_muons_loose", &n_muons_loose, &b_n_muons_loose);
        fChain->SetBranchAddress("n_muons_medium", &n_muons_medium, &b_n_muons_medium);
        fChain->SetBranchAddress("n_muons_met", &n_muons_met, &b_n_muons_met);
        fChain->SetBranchAddress("n_muons_olr", &n_muons_olr, &b_n_muons_olr);
        fChain->SetBranchAddress("n_muons_tight", &n_muons_tight, &b_n_muons_tight);
        fChain->SetBranchAddress("n_muons_veryloose", &n_muons_veryloose, &b_n_muons_veryloose);
        fChain->SetBranchAddress("n_photons", &n_photons, &b_n_photons);
        fChain->SetBranchAddress("n_photons_met", &n_photons_met, &b_n_photons_met);
        fChain->SetBranchAddress("n_photons_olr", &n_photons_olr, &b_n_photons_olr);
        fChain->SetBranchAddress("n_pvx", &n_pvx, &b_n_pvx);
        fChain->SetBranchAddress("n_taus", &n_taus, &b_n_taus);
        fChain->SetBranchAddress("n_taus_met", &n_taus_met, &b_n_taus_met);
        fChain->SetBranchAddress("n_taus_olr", &n_taus_olr, &b_n_taus_olr);
        fChain->SetBranchAddress("n_taus_rnn_loose", &n_taus_rnn_loose, &b_n_taus_rnn_loose);
        fChain->SetBranchAddress("n_taus_rnn_medium", &n_taus_rnn_medium, &b_n_taus_rnn_medium);
        fChain->SetBranchAddress("n_taus_rnn_tight", &n_taus_rnn_tight, &b_n_taus_rnn_tight);
        fChain->SetBranchAddress("n_taus_rnn_veryloose", &n_taus_rnn_veryloose, &b_n_taus_rnn_veryloose);
        fChain->SetBranchAddress("n_truth_gluon_jets", &n_truth_gluon_jets, &b_n_truth_gluon_jets);
        fChain->SetBranchAddress("n_truth_jets", &n_truth_jets, &b_n_truth_jets);
        fChain->SetBranchAddress("n_truth_jets_pt20_eta45", &n_truth_jets_pt20_eta45, &b_n_truth_jets_pt20_eta45);
        fChain->SetBranchAddress("n_truth_quark_jets", &n_truth_quark_jets, &b_n_truth_quark_jets);
        fChain->SetBranchAddress("n_vx", &n_vx, &b_n_vx);
        fChain->SetBranchAddress("pmg_truth_weight_FSRHi", &pmg_truth_weight_FSRHi, &b_pmg_truth_weight_FSRHi);
        fChain->SetBranchAddress("pmg_truth_weight_FSRLo", &pmg_truth_weight_FSRLo, &b_pmg_truth_weight_FSRLo);
        fChain->SetBranchAddress("pmg_truth_weight_ISRLo", &pmg_truth_weight_ISRLo, &b_pmg_truth_weight_ISRLo);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_0", &pmg_truth_weight_pdf_signal_weight_0,
                                 &b_pmg_truth_weight_pdf_signal_weight_0);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_1", &pmg_truth_weight_pdf_signal_weight_1,
                                 &b_pmg_truth_weight_pdf_signal_weight_1);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_10", &pmg_truth_weight_pdf_signal_weight_10,
                                 &b_pmg_truth_weight_pdf_signal_weight_10);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_11", &pmg_truth_weight_pdf_signal_weight_11,
                                 &b_pmg_truth_weight_pdf_signal_weight_11);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_12", &pmg_truth_weight_pdf_signal_weight_12,
                                 &b_pmg_truth_weight_pdf_signal_weight_12);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_13", &pmg_truth_weight_pdf_signal_weight_13,
                                 &b_pmg_truth_weight_pdf_signal_weight_13);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_14", &pmg_truth_weight_pdf_signal_weight_14,
                                 &b_pmg_truth_weight_pdf_signal_weight_14);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_15", &pmg_truth_weight_pdf_signal_weight_15,
                                 &b_pmg_truth_weight_pdf_signal_weight_15);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_16", &pmg_truth_weight_pdf_signal_weight_16,
                                 &b_pmg_truth_weight_pdf_signal_weight_16);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_17", &pmg_truth_weight_pdf_signal_weight_17,
                                 &b_pmg_truth_weight_pdf_signal_weight_17);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_18", &pmg_truth_weight_pdf_signal_weight_18,
                                 &b_pmg_truth_weight_pdf_signal_weight_18);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_19", &pmg_truth_weight_pdf_signal_weight_19,
                                 &b_pmg_truth_weight_pdf_signal_weight_19);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_2", &pmg_truth_weight_pdf_signal_weight_2,
                                 &b_pmg_truth_weight_pdf_signal_weight_2);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_20", &pmg_truth_weight_pdf_signal_weight_20,
                                 &b_pmg_truth_weight_pdf_signal_weight_20);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_21", &pmg_truth_weight_pdf_signal_weight_21,
                                 &b_pmg_truth_weight_pdf_signal_weight_21);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_22", &pmg_truth_weight_pdf_signal_weight_22,
                                 &b_pmg_truth_weight_pdf_signal_weight_22);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_23", &pmg_truth_weight_pdf_signal_weight_23,
                                 &b_pmg_truth_weight_pdf_signal_weight_23);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_24", &pmg_truth_weight_pdf_signal_weight_24,
                                 &b_pmg_truth_weight_pdf_signal_weight_24);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_25", &pmg_truth_weight_pdf_signal_weight_25,
                                 &b_pmg_truth_weight_pdf_signal_weight_25);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_26", &pmg_truth_weight_pdf_signal_weight_26,
                                 &b_pmg_truth_weight_pdf_signal_weight_26);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_27", &pmg_truth_weight_pdf_signal_weight_27,
                                 &b_pmg_truth_weight_pdf_signal_weight_27);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_28", &pmg_truth_weight_pdf_signal_weight_28,
                                 &b_pmg_truth_weight_pdf_signal_weight_28);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_29", &pmg_truth_weight_pdf_signal_weight_29,
                                 &b_pmg_truth_weight_pdf_signal_weight_29);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_3", &pmg_truth_weight_pdf_signal_weight_3,
                                 &b_pmg_truth_weight_pdf_signal_weight_3);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_30", &pmg_truth_weight_pdf_signal_weight_30,
                                 &b_pmg_truth_weight_pdf_signal_weight_30);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_4", &pmg_truth_weight_pdf_signal_weight_4,
                                 &b_pmg_truth_weight_pdf_signal_weight_4);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_5", &pmg_truth_weight_pdf_signal_weight_5,
                                 &b_pmg_truth_weight_pdf_signal_weight_5);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_6", &pmg_truth_weight_pdf_signal_weight_6,
                                 &b_pmg_truth_weight_pdf_signal_weight_6);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_7", &pmg_truth_weight_pdf_signal_weight_7,
                                 &b_pmg_truth_weight_pdf_signal_weight_7);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_8", &pmg_truth_weight_pdf_signal_weight_8,
                                 &b_pmg_truth_weight_pdf_signal_weight_8);
        fChain->SetBranchAddress("pmg_truth_weight_pdf_signal_weight_9", &pmg_truth_weight_pdf_signal_weight_9,
                                 &b_pmg_truth_weight_pdf_signal_weight_9);
        fChain->SetBranchAddress("run_number", &run_number, &b_run_number);
        fChain->SetBranchAddress("tau_0", &tau_0, &b_tau_0);
        fChain->SetBranchAddress("tau_0_EleID_AbsDEtaLeadTrk", &tau_0_EleID_AbsDEtaLeadTrk, &b_tau_0_EleID_AbsDEtaLeadTrk);
        fChain->SetBranchAddress("tau_0_EleID_AbsDPhiLeadTrk", &tau_0_EleID_AbsDPhiLeadTrk, &b_tau_0_EleID_AbsDPhiLeadTrk);
        fChain->SetBranchAddress("tau_0_EleID_AbsEtaLeadTrk", &tau_0_EleID_AbsEtaLeadTrk, &b_tau_0_EleID_AbsEtaLeadTrk);
        fChain->SetBranchAddress("tau_0_EleID_EMFracAtEMScale", &tau_0_EleID_EMFracAtEMScale, &b_tau_0_EleID_EMFracAtEMScale);
        fChain->SetBranchAddress("tau_0_EleID_EMFracFixed", &tau_0_EleID_EMFracFixed, &b_tau_0_EleID_EMFracFixed);
        fChain->SetBranchAddress("tau_0_EleID_PSSFraction", &tau_0_EleID_PSSFraction, &b_tau_0_EleID_PSSFraction);
        fChain->SetBranchAddress("tau_0_EleID_centFrac", &tau_0_EleID_centFrac, &b_tau_0_EleID_centFrac);
        fChain->SetBranchAddress("tau_0_EleID_clustersMeanCenterLambda", &tau_0_EleID_clustersMeanCenterLambda,
                                 &b_tau_0_EleID_clustersMeanCenterLambda);
        fChain->SetBranchAddress("tau_0_EleID_clustersMeanEMProbability", &tau_0_EleID_clustersMeanEMProbability,
                                 &b_tau_0_EleID_clustersMeanEMProbability);
        fChain->SetBranchAddress("tau_0_EleID_clustersMeanFirstEngDens", &tau_0_EleID_clustersMeanFirstEngDens,
                                 &b_tau_0_EleID_clustersMeanFirstEngDens);
        fChain->SetBranchAddress("tau_0_EleID_clustersMeanPresamplerFrac", &tau_0_EleID_clustersMeanPresamplerFrac,
                                 &b_tau_0_EleID_clustersMeanPresamplerFrac);
        fChain->SetBranchAddress("tau_0_EleID_clustersMeanSecondLambda", &tau_0_EleID_clustersMeanSecondLambda,
                                 &b_tau_0_EleID_clustersMeanSecondLambda);
        fChain->SetBranchAddress("tau_0_EleID_etHotShotWinOverPtLeadTrk", &tau_0_EleID_etHotShotWinOverPtLeadTrk,
                                 &b_tau_0_EleID_etHotShotWinOverPtLeadTrk);
        fChain->SetBranchAddress("tau_0_EleID_etOverPtLeadTrk", &tau_0_EleID_etOverPtLeadTrk, &b_tau_0_EleID_etOverPtLeadTrk);
        fChain->SetBranchAddress("tau_0_EleID_hadLeakEt", &tau_0_EleID_hadLeakEt, &b_tau_0_EleID_hadLeakEt);
        fChain->SetBranchAddress("tau_0_EleID_hadLeakFracFixed", &tau_0_EleID_hadLeakFracFixed, &b_tau_0_EleID_hadLeakFracFixed);
        fChain->SetBranchAddress("tau_0_EleID_isolFrac", &tau_0_EleID_isolFrac, &b_tau_0_EleID_isolFrac);
        fChain->SetBranchAddress("tau_0_EleID_leadTrackProbHT", &tau_0_EleID_leadTrackProbHT, &b_tau_0_EleID_leadTrackProbHT);
        fChain->SetBranchAddress("tau_0_EleID_secMaxStripEt", &tau_0_EleID_secMaxStripEt, &b_tau_0_EleID_secMaxStripEt);
        fChain->SetBranchAddress("tau_0_EleID_secMaxStripEtOverPtLeadTrk", &tau_0_EleID_secMaxStripEtOverPtLeadTrk,
                                 &b_tau_0_EleID_secMaxStripEtOverPtLeadTrk);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad", &tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad,
                                 &b_tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_JetRNNloose", &tau_0_NOMINAL_TauEffSF_JetRNNloose, &b_tau_0_NOMINAL_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_JetRNNmedium", &tau_0_NOMINAL_TauEffSF_JetRNNmedium, &b_tau_0_NOMINAL_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_JetRNNtight", &tau_0_NOMINAL_TauEffSF_JetRNNtight, &b_tau_0_NOMINAL_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron", &tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron,
                                 &b_tau_0_NOMINAL_TauEffSF_LooseEleBDT_electron);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron", &tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron,
                                 &b_tau_0_NOMINAL_TauEffSF_MediumEleBDT_electron);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_TightEleBDT_electron", &tau_0_NOMINAL_TauEffSF_TightEleBDT_electron,
                                 &b_tau_0_NOMINAL_TauEffSF_TightEleBDT_electron);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_reco", &tau_0_NOMINAL_TauEffSF_reco, &b_tau_0_NOMINAL_TauEffSF_reco);
        fChain->SetBranchAddress("tau_0_NOMINAL_TauEffSF_selection", &tau_0_NOMINAL_TauEffSF_selection, &b_tau_0_NOMINAL_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_PanTau_BDTValue_1p0n_vs_1p1n", &tau_0_PanTau_BDTValue_1p0n_vs_1p1n, &b_tau_0_PanTau_BDTValue_1p0n_vs_1p1n);
        fChain->SetBranchAddress("tau_0_PanTau_BDTValue_1p1n_vs_1pXn", &tau_0_PanTau_BDTValue_1p1n_vs_1pXn, &b_tau_0_PanTau_BDTValue_1p1n_vs_1pXn);
        fChain->SetBranchAddress("tau_0_PanTau_BDTValue_3p0n_vs_3pXn", &tau_0_PanTau_BDTValue_3p0n_vs_3pXn, &b_tau_0_PanTau_BDTValue_3p0n_vs_3pXn);
        fChain->SetBranchAddress("tau_0_PanTau_BDTVar_Basic_NNeutralConsts", &tau_0_PanTau_BDTVar_Basic_NNeutralConsts,
                                 &b_tau_0_PanTau_BDTVar_Basic_NNeutralConsts);
        fChain->SetBranchAddress("tau_0_PanTau_BDTVar_Charged_HLV_SumM", &tau_0_PanTau_BDTVar_Charged_HLV_SumM,
                                 &b_tau_0_PanTau_BDTVar_Charged_HLV_SumM);
        fChain->SetBranchAddress("tau_0_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt", &tau_0_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt,
                                 &b_tau_0_PanTau_BDTVar_Charged_JetMoment_EtDRxTotalEt);
        fChain->SetBranchAddress("tau_0_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts", &tau_0_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts,
                                 &b_tau_0_PanTau_BDTVar_Charged_StdDev_Et_WrtEtAllConsts);
        fChain->SetBranchAddress("tau_0_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged",
                                 &tau_0_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged,
                                 &b_tau_0_PanTau_BDTVar_Combined_DeltaR1stNeutralTo1stCharged);
        fChain->SetBranchAddress("tau_0_PanTau_BDTVar_Neutral_HLV_SumM", &tau_0_PanTau_BDTVar_Neutral_HLV_SumM,
                                 &b_tau_0_PanTau_BDTVar_Neutral_HLV_SumM);
        fChain->SetBranchAddress("tau_0_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1", &tau_0_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1,
                                 &b_tau_0_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_1);
        fChain->SetBranchAddress("tau_0_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2", &tau_0_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2,
                                 &b_tau_0_PanTau_BDTVar_Neutral_PID_BDTValues_BDTSort_2);
        fChain->SetBranchAddress("tau_0_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts",
                                 &tau_0_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts,
                                 &b_tau_0_PanTau_BDTVar_Neutral_Ratio_1stBDTEtOverEtAllConsts);
        fChain->SetBranchAddress("tau_0_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts", &tau_0_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts,
                                 &b_tau_0_PanTau_BDTVar_Neutral_Ratio_EtOverEtAllConsts);
        fChain->SetBranchAddress("tau_0_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed", &tau_0_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed,
                                 &b_tau_0_PanTau_BDTVar_Neutral_Shots_NPhotonsInSeed);
        fChain->SetBranchAddress("tau_0_PanTau_DecayModeProto", &tau_0_PanTau_DecayModeProto, &b_tau_0_PanTau_DecayModeProto);
        fChain->SetBranchAddress("tau_0_PanTau_isPanTauCandidate", &tau_0_PanTau_isPanTauCandidate, &b_tau_0_PanTau_isPanTauCandidate);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_LooseEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_LooseEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_LooseEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_MediumEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_MediumEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_MediumEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_TightEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_TightEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1down_TauEffSF_TightEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_LooseEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_LooseEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_LooseEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_MediumEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_MediumEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_MediumEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_TightEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_TightEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_STAT_1up_TauEffSF_TightEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_LooseEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_MediumEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_TightEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_TightEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1down_TauEffSF_TightEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_LooseEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_MediumEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_TightEleBDT_electron",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_TightEleBDT_electron,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEBDT_SYST_1up_TauEffSF_TightEleBDT_electron);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_STAT_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEELECTRON_EFF_ELEOLR_SYST_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_HadTauEleOLR_tauhad);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_HadTauEleOLR_tauhad);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_ELEOLR_TOTAL_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_reco);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco", &tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_reco);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RECO_TOTAL_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2025_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT2530_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPT3040_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_1PRONGSTATSYSTPTGE40_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2025_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT2530_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPT3040_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_3PRONGSTATSYSTPTGE40_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_HIGHPT_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1down_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNloose",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNloose,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNloose);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNmedium",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNmedium,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNmedium);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNtight",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNtight,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_JetRNNtight);
        fChain->SetBranchAddress("tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_selection",
                                 &tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_selection,
                                 &b_tau_0_TAUS_TRUEHADTAU_EFF_RNNID_SYST_1up_TauEffSF_selection);
        fChain->SetBranchAddress("tau_0_TauID_ChPiEMEOverCaloEME", &tau_0_TauID_ChPiEMEOverCaloEME, &b_tau_0_TauID_ChPiEMEOverCaloEME);
        fChain->SetBranchAddress("tau_0_TauID_EMPOverTrkSysP", &tau_0_TauID_EMPOverTrkSysP, &b_tau_0_TauID_EMPOverTrkSysP);
        fChain->SetBranchAddress("tau_0_TauID_SumPtTrkFrac", &tau_0_TauID_SumPtTrkFrac, &b_tau_0_TauID_SumPtTrkFrac);
        fChain->SetBranchAddress("tau_0_TauID_centFrac", &tau_0_TauID_centFrac, &b_tau_0_TauID_centFrac);
        fChain->SetBranchAddress("tau_0_TauID_dRmax", &tau_0_TauID_dRmax, &b_tau_0_TauID_dRmax);
        fChain->SetBranchAddress("tau_0_TauID_etOverPtLeadTrk", &tau_0_TauID_etOverPtLeadTrk, &b_tau_0_TauID_etOverPtLeadTrk);
        fChain->SetBranchAddress("tau_0_TauID_innerTrkAvgDist", &tau_0_TauID_innerTrkAvgDist, &b_tau_0_TauID_innerTrkAvgDist);
        fChain->SetBranchAddress("tau_0_TauID_ipSigLeadTrk", &tau_0_TauID_ipSigLeadTrk, &b_tau_0_TauID_ipSigLeadTrk);
        fChain->SetBranchAddress("tau_0_TauID_mEflowApprox", &tau_0_TauID_mEflowApprox, &b_tau_0_TauID_mEflowApprox);
        fChain->SetBranchAddress("tau_0_TauID_massTrkSys", &tau_0_TauID_massTrkSys, &b_tau_0_TauID_massTrkSys);
        fChain->SetBranchAddress("tau_0_TauID_ptIntermediateAxis", &tau_0_TauID_ptIntermediateAxis, &b_tau_0_TauID_ptIntermediateAxis);
        fChain->SetBranchAddress("tau_0_TauID_ptRatioEflowApprox", &tau_0_TauID_ptRatioEflowApprox, &b_tau_0_TauID_ptRatioEflowApprox);
        fChain->SetBranchAddress("tau_0_TauID_trFlightPathSig", &tau_0_TauID_trFlightPathSig, &b_tau_0_TauID_trFlightPathSig);
        fChain->SetBranchAddress("tau_0_allTrk_eta", &tau_0_allTrk_eta, &b_tau_0_allTrk_eta);
        fChain->SetBranchAddress("tau_0_allTrk_n", &tau_0_allTrk_n, &b_tau_0_allTrk_n);
        fChain->SetBranchAddress("tau_0_allTrk_phi", &tau_0_allTrk_phi, &b_tau_0_allTrk_phi);
        fChain->SetBranchAddress("tau_0_allTrk_pt", &tau_0_allTrk_pt, &b_tau_0_allTrk_pt);
        fChain->SetBranchAddress("tau_0_b_tag_score", &tau_0_b_tag_score, &b_tau_0_b_tag_score);
        fChain->SetBranchAddress("tau_0_b_tagged", &tau_0_b_tagged, &b_tau_0_b_tagged);
        fChain->SetBranchAddress("tau_0_charged_tracks_d0", &tau_0_charged_tracks_d0, &b_tau_0_charged_tracks_d0);
        fChain->SetBranchAddress("tau_0_charged_tracks_d0_sig", &tau_0_charged_tracks_d0_sig, &b_tau_0_charged_tracks_d0_sig);
        fChain->SetBranchAddress("tau_0_charged_tracks_eProbabilityHT", &tau_0_charged_tracks_eProbabilityHT, &b_tau_0_charged_tracks_eProbabilityHT);
        fChain->SetBranchAddress("tau_0_charged_tracks_nInnermostPixelHits", &tau_0_charged_tracks_nInnermostPixelHits,
                                 &b_tau_0_charged_tracks_nInnermostPixelHits);
        fChain->SetBranchAddress("tau_0_charged_tracks_nPixelHits", &tau_0_charged_tracks_nPixelHits, &b_tau_0_charged_tracks_nPixelHits);
        fChain->SetBranchAddress("tau_0_charged_tracks_nSCTHits", &tau_0_charged_tracks_nSCTHits, &b_tau_0_charged_tracks_nSCTHits);
        fChain->SetBranchAddress("tau_0_charged_tracks_p4", &tau_0_charged_tracks_p4, &b_tau_0_charged_tracks_p4);
        fChain->SetBranchAddress("tau_0_charged_tracks_pt_err", &tau_0_charged_tracks_pt_err, &b_tau_0_charged_tracks_pt_err);
        fChain->SetBranchAddress("tau_0_charged_tracks_z0", &tau_0_charged_tracks_z0, &b_tau_0_charged_tracks_z0);
        fChain->SetBranchAddress("tau_0_charged_tracks_z0_sig", &tau_0_charged_tracks_z0_sig, &b_tau_0_charged_tracks_z0_sig);
        fChain->SetBranchAddress("tau_0_charged_tracks_z0_sintheta", &tau_0_charged_tracks_z0_sintheta, &b_tau_0_charged_tracks_z0_sintheta);
        fChain->SetBranchAddress("tau_0_charged_tracks_z0_sintheta_tjva", &tau_0_charged_tracks_z0_sintheta_tjva,
                                 &b_tau_0_charged_tracks_z0_sintheta_tjva);
        fChain->SetBranchAddress("tau_0_conversion_tracks_p4", &tau_0_conversion_tracks_p4, &b_tau_0_conversion_tracks_p4);
        fChain->SetBranchAddress("tau_0_decay_mode", &tau_0_decay_mode, &b_tau_0_decay_mode);
        fChain->SetBranchAddress("tau_0_ele_bdt_loose_retuned", &tau_0_ele_bdt_loose_retuned, &b_tau_0_ele_bdt_loose_retuned);
        fChain->SetBranchAddress("tau_0_ele_bdt_medium_retuned", &tau_0_ele_bdt_medium_retuned, &b_tau_0_ele_bdt_medium_retuned);
        fChain->SetBranchAddress("tau_0_ele_bdt_score_retuned", &tau_0_ele_bdt_score_retuned, &b_tau_0_ele_bdt_score_retuned);
        fChain->SetBranchAddress("tau_0_ele_bdt_score_trans_retuned", &tau_0_ele_bdt_score_trans_retuned, &b_tau_0_ele_bdt_score_trans_retuned);
        fChain->SetBranchAddress("tau_0_ele_bdt_tight_retuned", &tau_0_ele_bdt_tight_retuned, &b_tau_0_ele_bdt_tight_retuned);
        fChain->SetBranchAddress("tau_0_ele_match_lhscore", &tau_0_ele_match_lhscore, &b_tau_0_ele_match_lhscore);
        fChain->SetBranchAddress("tau_0_ele_olr_pass", &tau_0_ele_olr_pass, &b_tau_0_ele_olr_pass);
        fChain->SetBranchAddress("tau_0_jet_bdt_score", &tau_0_jet_bdt_score, &b_tau_0_jet_bdt_score);
        fChain->SetBranchAddress("tau_0_jet_bdt_score_trans", &tau_0_jet_bdt_score_trans, &b_tau_0_jet_bdt_score_trans);
        fChain->SetBranchAddress("tau_0_jet_jvt", &tau_0_jet_jvt, &b_tau_0_jet_jvt);
        fChain->SetBranchAddress("tau_0_jet_rnn_loose", &tau_0_jet_rnn_loose, &b_tau_0_jet_rnn_loose);
        fChain->SetBranchAddress("tau_0_jet_rnn_medium", &tau_0_jet_rnn_medium, &b_tau_0_jet_rnn_medium);
        fChain->SetBranchAddress("tau_0_jet_rnn_score", &tau_0_jet_rnn_score, &b_tau_0_jet_rnn_score);
        fChain->SetBranchAddress("tau_0_jet_rnn_score_trans", &tau_0_jet_rnn_score_trans, &b_tau_0_jet_rnn_score_trans);
        fChain->SetBranchAddress("tau_0_jet_rnn_tight", &tau_0_jet_rnn_tight, &b_tau_0_jet_rnn_tight);
        fChain->SetBranchAddress("tau_0_jet_rnn_veryloose", &tau_0_jet_rnn_veryloose, &b_tau_0_jet_rnn_veryloose);
        fChain->SetBranchAddress("tau_0_jet_seed_TrackWidthPt1000_wrt_PV", &tau_0_jet_seed_TrackWidthPt1000_wrt_PV,
                                 &b_tau_0_jet_seed_TrackWidthPt1000_wrt_PV);
        fChain->SetBranchAddress("tau_0_jet_seed_TrackWidthPt1000_wrt_TV", &tau_0_jet_seed_TrackWidthPt1000_wrt_TV,
                                 &b_tau_0_jet_seed_TrackWidthPt1000_wrt_TV);
        fChain->SetBranchAddress("tau_0_jet_width", &tau_0_jet_width, &b_tau_0_jet_width);
        fChain->SetBranchAddress("tau_0_matched", &tau_0_matched, &b_tau_0_matched);
        fChain->SetBranchAddress("tau_0_matched_classifierParticleOrigin", &tau_0_matched_classifierParticleOrigin,
                                 &b_tau_0_matched_classifierParticleOrigin);
        fChain->SetBranchAddress("tau_0_matched_classifierParticleType", &tau_0_matched_classifierParticleType,
                                 &b_tau_0_matched_classifierParticleType);
        fChain->SetBranchAddress("tau_0_matched_decayVertex", &tau_0_matched_decayVertex, &b_tau_0_matched_decayVertex);
        fChain->SetBranchAddress("tau_0_matched_decayVertex_v", &tau_0_matched_decayVertex_v, &b_tau_0_matched_decayVertex_v);
        fChain->SetBranchAddress("tau_0_matched_decay_mode", &tau_0_matched_decay_mode, &b_tau_0_matched_decay_mode);
        fChain->SetBranchAddress("tau_0_matched_isEle", &tau_0_matched_isEle, &b_tau_0_matched_isEle);
        fChain->SetBranchAddress("tau_0_matched_isHadTau", &tau_0_matched_isHadTau, &b_tau_0_matched_isHadTau);
        fChain->SetBranchAddress("tau_0_matched_isJet", &tau_0_matched_isJet, &b_tau_0_matched_isJet);
        fChain->SetBranchAddress("tau_0_matched_isMuon", &tau_0_matched_isMuon, &b_tau_0_matched_isMuon);
        fChain->SetBranchAddress("tau_0_matched_isTau", &tau_0_matched_isTau, &b_tau_0_matched_isTau);
        fChain->SetBranchAddress("tau_0_matched_isTruthMatch", &tau_0_matched_isTruthMatch, &b_tau_0_matched_isTruthMatch);
        fChain->SetBranchAddress("tau_0_matched_mother_pdgId", &tau_0_matched_mother_pdgId, &b_tau_0_matched_mother_pdgId);
        fChain->SetBranchAddress("tau_0_matched_mother_status", &tau_0_matched_mother_status, &b_tau_0_matched_mother_status);
        fChain->SetBranchAddress("tau_0_matched_n_charged", &tau_0_matched_n_charged, &b_tau_0_matched_n_charged);
        fChain->SetBranchAddress("tau_0_matched_n_charged_pion", &tau_0_matched_n_charged_pion, &b_tau_0_matched_n_charged_pion);
        fChain->SetBranchAddress("tau_0_matched_n_neutral", &tau_0_matched_n_neutral, &b_tau_0_matched_n_neutral);
        fChain->SetBranchAddress("tau_0_matched_n_neutral_pion", &tau_0_matched_n_neutral_pion, &b_tau_0_matched_n_neutral_pion);
        fChain->SetBranchAddress("tau_0_matched_origin", &tau_0_matched_origin, &b_tau_0_matched_origin);
        fChain->SetBranchAddress("tau_0_matched_p4", &tau_0_matched_p4, &b_tau_0_matched_p4);
        fChain->SetBranchAddress("tau_0_matched_p4_vis_charged_track0", &tau_0_matched_p4_vis_charged_track0, &b_tau_0_matched_p4_vis_charged_track0);
        fChain->SetBranchAddress("tau_0_matched_p4_vis_charged_track1", &tau_0_matched_p4_vis_charged_track1, &b_tau_0_matched_p4_vis_charged_track1);
        fChain->SetBranchAddress("tau_0_matched_p4_vis_charged_track2", &tau_0_matched_p4_vis_charged_track2, &b_tau_0_matched_p4_vis_charged_track2);
        fChain->SetBranchAddress("tau_0_matched_pdgId", &tau_0_matched_pdgId, &b_tau_0_matched_pdgId);
        fChain->SetBranchAddress("tau_0_matched_productionVertex", &tau_0_matched_productionVertex, &b_tau_0_matched_productionVertex);
        fChain->SetBranchAddress("tau_0_matched_productionVertex_v", &tau_0_matched_productionVertex_v, &b_tau_0_matched_productionVertex_v);
        fChain->SetBranchAddress("tau_0_matched_pz", &tau_0_matched_pz, &b_tau_0_matched_pz);
        fChain->SetBranchAddress("tau_0_matched_q", &tau_0_matched_q, &b_tau_0_matched_q);
        fChain->SetBranchAddress("tau_0_matched_status", &tau_0_matched_status, &b_tau_0_matched_status);
        fChain->SetBranchAddress("tau_0_matched_type", &tau_0_matched_type, &b_tau_0_matched_type);
        fChain->SetBranchAddress("tau_0_matched_vis_charged_p4", &tau_0_matched_vis_charged_p4, &b_tau_0_matched_vis_charged_p4);
        fChain->SetBranchAddress("tau_0_matched_vis_neutral_others_p4", &tau_0_matched_vis_neutral_others_p4, &b_tau_0_matched_vis_neutral_others_p4);
        fChain->SetBranchAddress("tau_0_matched_vis_neutral_p4", &tau_0_matched_vis_neutral_p4, &b_tau_0_matched_vis_neutral_p4);
        fChain->SetBranchAddress("tau_0_matched_vis_neutral_pions_p4", &tau_0_matched_vis_neutral_pions_p4, &b_tau_0_matched_vis_neutral_pions_p4);
        fChain->SetBranchAddress("tau_0_matched_vis_p4", &tau_0_matched_vis_p4, &b_tau_0_matched_vis_p4);
        fChain->SetBranchAddress("tau_0_n_all_tracks", &tau_0_n_all_tracks, &b_tau_0_n_all_tracks);
        fChain->SetBranchAddress("tau_0_n_charged_tracks", &tau_0_n_charged_tracks, &b_tau_0_n_charged_tracks);
        fChain->SetBranchAddress("tau_0_n_conversion_tracks", &tau_0_n_conversion_tracks, &b_tau_0_n_conversion_tracks);
        fChain->SetBranchAddress("tau_0_n_core_tracks", &tau_0_n_core_tracks, &b_tau_0_n_core_tracks);
        fChain->SetBranchAddress("tau_0_n_failTrackFilter_tracks", &tau_0_n_failTrackFilter_tracks, &b_tau_0_n_failTrackFilter_tracks);
        fChain->SetBranchAddress("tau_0_n_fake_tracks", &tau_0_n_fake_tracks, &b_tau_0_n_fake_tracks);
        fChain->SetBranchAddress("tau_0_n_isolation_tracks", &tau_0_n_isolation_tracks, &b_tau_0_n_isolation_tracks);
        fChain->SetBranchAddress("tau_0_n_modified_isolation_tracks", &tau_0_n_modified_isolation_tracks, &b_tau_0_n_modified_isolation_tracks);
        fChain->SetBranchAddress("tau_0_n_old_tracks", &tau_0_n_old_tracks, &b_tau_0_n_old_tracks);
        fChain->SetBranchAddress("tau_0_n_passTrkSelectionTight_tracks", &tau_0_n_passTrkSelectionTight_tracks,
                                 &b_tau_0_n_passTrkSelectionTight_tracks);
        fChain->SetBranchAddress("tau_0_n_passTrkSelector_tracks", &tau_0_n_passTrkSelector_tracks, &b_tau_0_n_passTrkSelector_tracks);
        fChain->SetBranchAddress("tau_0_n_unclassified_tracks", &tau_0_n_unclassified_tracks, &b_tau_0_n_unclassified_tracks);
        fChain->SetBranchAddress("tau_0_n_wide_tracks", &tau_0_n_wide_tracks, &b_tau_0_n_wide_tracks);
        fChain->SetBranchAddress("tau_0_origin", &tau_0_origin, &b_tau_0_origin);
        fChain->SetBranchAddress("tau_0_p4", &tau_0_p4, &b_tau_0_p4);
        fChain->SetBranchAddress("tau_0_pass_tst_muonolr", &tau_0_pass_tst_muonolr, &b_tau_0_pass_tst_muonolr);
        fChain->SetBranchAddress("tau_0_primary_vertex", &tau_0_primary_vertex, &b_tau_0_primary_vertex);
        fChain->SetBranchAddress("tau_0_primary_vertex_v", &tau_0_primary_vertex_v, &b_tau_0_primary_vertex_v);
        fChain->SetBranchAddress("tau_0_q", &tau_0_q, &b_tau_0_q);
        fChain->SetBranchAddress("tau_0_secondary_vertex", &tau_0_secondary_vertex, &b_tau_0_secondary_vertex);
        fChain->SetBranchAddress("tau_0_secondary_vertex_v", &tau_0_secondary_vertex_v, &b_tau_0_secondary_vertex_v);
        fChain->SetBranchAddress("tau_0_trk_multi_diag_n_had_tracks", &tau_0_trk_multi_diag_n_had_tracks, &b_tau_0_trk_multi_diag_n_had_tracks);
        fChain->SetBranchAddress("tau_0_trk_multi_diag_n_spu_cv_tracks", &tau_0_trk_multi_diag_n_spu_cv_tracks,
                                 &b_tau_0_trk_multi_diag_n_spu_cv_tracks);
        fChain->SetBranchAddress("tau_0_trk_multi_diag_n_spu_fk_tracks", &tau_0_trk_multi_diag_n_spu_fk_tracks,
                                 &b_tau_0_trk_multi_diag_n_spu_fk_tracks);
        fChain->SetBranchAddress("tau_0_trk_multi_diag_n_spu_pu_tracks", &tau_0_trk_multi_diag_n_spu_pu_tracks,
                                 &b_tau_0_trk_multi_diag_n_spu_pu_tracks);
        fChain->SetBranchAddress("tau_0_trk_multi_diag_n_spu_tracks", &tau_0_trk_multi_diag_n_spu_tracks, &b_tau_0_trk_multi_diag_n_spu_tracks);
        fChain->SetBranchAddress("tau_0_trk_multi_diag_n_spu_uc_tracks", &tau_0_trk_multi_diag_n_spu_uc_tracks,
                                 &b_tau_0_trk_multi_diag_n_spu_uc_tracks);
        fChain->SetBranchAddress("tau_0_trk_multi_diag_n_spu_ue_tracks", &tau_0_trk_multi_diag_n_spu_ue_tracks,
                                 &b_tau_0_trk_multi_diag_n_spu_ue_tracks);
        fChain->SetBranchAddress("tau_0_trk_multi_diag_n_tau_tracks", &tau_0_trk_multi_diag_n_tau_tracks, &b_tau_0_trk_multi_diag_n_tau_tracks);
        fChain->SetBranchAddress("tau_0_trk_multi_diag_purity", &tau_0_trk_multi_diag_purity, &b_tau_0_trk_multi_diag_purity);
        fChain->SetBranchAddress("tau_0_type", &tau_0_type, &b_tau_0_type);
        fChain->SetBranchAddress("tau_tes_alpha_pt_shift", &tau_tes_alpha_pt_shift, &b_tau_tes_alpha_pt_shift);
        fChain->SetBranchAddress("weight_mc", &weight_mc, &b_weight_mc);
        fChain->SetBranchAddress("Weight_mc", &Weight_mc, &b_Weight_mc);
        fChain->SetBranchAddress("mergedRunNumber", &mergedRunNumber, &b_mergedRunNumber);
        fChain->SetBranchAddress("upsilon", &upsilon, &b_upsilon);
        fChain->SetBranchAddress("isQCR", &isQCR, &b_isQCR);
        fChain->SetBranchAddress("isWCR", &isWCR, &b_isWCR);
        fChain->SetBranchAddress("isWCR_QCD", &isWCR_QCD, &b_isWCR_QCD);
        fChain->SetBranchAddress("isIQCR_Iso", &isIQCR_Iso, &b_isIQCR_Iso);
        fChain->SetBranchAddress("isIQCR_NotIso", &isIQCR_NotIso, &b_isIQCR_NotIso);
        fChain->SetBranchAddress("isTCR", &isTCR, &b_isTCR);
        fChain->SetBranchAddress("isSR", &isSR, &b_isSR);
        fChain->SetBranchAddress("isData", &isData, &b_isData);
    }
};
