#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

#include <vector>

#include "AnaObjs.h"
#include "AnaTree.h"

class XamppTreeCompressed : public AnaTree {
public:
    XamppTreeCompressed(TChain *chain) : AnaTree(chain) { Init(this->fChain); }
    AnaObjs getElectrons() {
        AnaObjs eleVec;
        for (unsigned int i = 0; i < this->electrons_pt->size(); i++) {
            int eleID(0);
            if (this->electrons_signal->at(i) == 1) {
                eleID |= EGood;
            }
            if (this->electrons_isol->at(i) == 1) {
                eleID |= EIso;
            }
            AnaObj ele(ELECTRON, (this->electrons_pt->at(i) / 1000), this->electrons_eta->at(i), this->electrons_phi->at(i),
                       (this->electrons_e->at(i) / 1000), this->electrons_charge->at(i), eleID, (this->electrons_MT->at(i) / 1000));
            eleVec.emplace_back(ele);
        }
        return eleVec;
    }

    AnaObjs getMuons() {
        AnaObjs muonVec;
        for (unsigned int i = 0; i < this->muons_pt->size(); i++) {
            int muonID(0);
            if (this->muons_signal->at(i) == 1) {
                muonID |= MuGood;
            }
            if (this->muons_isol->at(i) == 1) {
                muonID |= MuIso;
            }
            AnaObj muon(MUON, (this->muons_pt->at(i) / 1000), this->muons_eta->at(i), this->muons_phi->at(i), (this->muons_e->at(i) / 1000),
                        this->muons_charge->at(i), muonID, (this->muons_MT->at(i) / 1000));
            muonVec.emplace_back(muon);
        }
        return muonVec;
    }

    AnaObjs getTaus() {
        AnaObjs tauVec;
        for (unsigned int i = 0; i < this->taus_pt->size(); i++) {
            int tauID(0);
            switch (this->taus_Quality->at(i)) {
                case 0:
                    tauID |= TauVeryLoose;
                    break;
                case 1:
                    tauID |= TauVeryLoose | TauLoose;
                    break;
                case 2:
                    tauID |= TauVeryLoose | TauLoose | TauMedium;
                    break;
                case 3:
                    tauID |= TauVeryLoose | TauLoose | TauMedium | TauTight;
            }
            if (this->taus_NTrks->at(i) == 1) {
                tauID |= TauOneProng;
            } else {
                tauID |= TauThreeProng;
            }
            if (this->mcChannelNumber != 0) {
                if (this->taus_truthType->at(i) == 10) {
                    tauID |= TauIsTruthTau;
                } else {
                    tauID |= TauIsNotTruthTau;
                }
            } else {
                tauID |= TauIsNotTruthTau;
            }
            if (this->taus_signalID->at(i) == 1) {
                tauID |= TauIsSignal;
            }
            AnaObj tau(TAU, (this->taus_pt->at(i) / 1000), this->taus_eta->at(i), this->taus_phi->at(i), (this->taus_e->at(i) / 1000),
                       this->taus_charge->at(i), tauID, (this->taus_MT->at(i) / 1000));
            tauVec.emplace_back(tau);
        }
        tauVec.setProperties("RNNScore", *(this->taus_RNNJetScore));
        return tauVec;
    }

    AnaObjs getJets() {
        AnaObjs jetVec;
        for (unsigned int i = 0; i < this->jets_pt->size(); i++) {
            int jetID(0);
            if (this->jets_bjet->at(i)) {
                jetID |= GoodBJet;
            }
            // Now the jets_signal in ntuple are all 1 so it's useless
            // if(this->jets_signal->at(i)){
            //	jetID |= JetIsSignal;
            //}

            AnaObj jet(JET, 0, jetID);
            jet.SetPtEtaPhiM(this->jets_pt->at(i) / 1000, this->jets_eta->at(i), this->jets_phi->at(i), this->jets_m->at(i) / 1000);
            jetVec.emplace_back(jet);
        }
        jetVec.setProperties("BTagScore", *(this->jets_BTagScore));
        return jetVec;
    }

    // Declaration of leaf types
    Int_t mcChannelNumber;
    Float_t ColinearMTauTau;
    Float_t CosChi1;
    Float_t CosChi2;
    Double_t EleWeight;
    Double_t EleWeightId;
    Double_t EleWeightIso;
    Double_t EleWeightReco;
    Double_t
        EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0;
    Float_t GenFiltHT;
    Float_t GenFiltMET;
    Double_t GenWeight;
    Double_t GenWeight_LHE_MEWeight;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEW;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEW;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEW;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2;
    Double_t GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEW;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1LO2;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEW;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEW;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2;
    Double_t GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3;
    Double_t GenWeight_LHE_NTrials;
    Double_t GenWeight_LHE_UserHook;
    Double_t GenWeight_LHE_WeightNormalisation;
    Float_t Ht_Jet;
    Float_t Ht_Lep;
    Float_t Ht_Tau;
    Double_t JetWeight;
    Int_t LQ_ncl;
    Float_t MCT;
    Bool_t MET_BiSect;
    Float_t MET_Centrality;
    Float_t MET_CosMinDeltaPhi;
    Float_t MET_LepTau_DeltaPhi;
    Float_t MET_LepTau_LeadingJet_VecSumPt;
    Float_t MET_LepTau_VecSumPhi;
    Float_t MET_LepTau_VecSumPt;
    Float_t MET_SumCosDeltaPhi;
    Float_t MT2_max;
    Float_t MT2_min;
    Float_t Meff;
    Float_t Meff_TauTau;
    Float_t MetTST_met;
    Float_t MetTST_phi;
    Float_t MetTST_sumet;
    Float_t MetTST_OverSqrtHT;
    Float_t MetTST_OverSqrtSumET;
    Float_t MetTST_Significance;
    Float_t MetTST_Significance_Rho;
    Float_t MetTST_Significance_VarL;
    Float_t MetTST_Significance_noPUJets_noSoftTerm;
    Float_t MetTST_Significance_noPUJets_noSoftTerm_Rho;
    Float_t MetTST_Significance_noPUJets_noSoftTerm_VarL;
    Float_t MetTST_Significance_phireso_noPUJets;
    Float_t MetTST_Significance_phireso_noPUJets_Rho;
    Float_t MetTST_Significance_phireso_noPUJets_VarL;
    Double_t MuoWeight;
    Double_t MuoWeightIsol;
    Double_t MuoWeightReco;
    Double_t MuoWeightTTVA;
    Double_t MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40;
    Double_t MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40;
    Double_t MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50;
    Double_t MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50;
    Char_t OS_BaseTauEle;
    Char_t OS_BaseTauMuo;
    Char_t OS_EleEle;
    Char_t OS_MuoMuo;
    Char_t OS_TauEle;
    Char_t OS_TauMuo;
    Char_t OS_TauTau;
    Float_t PTt;
    Float_t RJW_CosThetaStarW;
    Float_t RJW_GammaBetaW;
    std::vector<float> *RJW_JigSawCandidates_eta;
    std::vector<unsigned int> *RJW_JigSawCandidates_frame;
    std::vector<float> *RJW_JigSawCandidates_m;
    std::vector<int> *RJW_JigSawCandidates_pdgId;
    std::vector<float> *RJW_JigSawCandidates_phi;
    std::vector<float> *RJW_JigSawCandidates_pt;
    Float_t RJW_dPhiDecayPlaneW;
    Float_t RJZ_BalMetObj_Z;
    Float_t RJZ_CMS_Mass;
    Float_t RJZ_H01_Tau1;
    Float_t RJZ_H01_Z;
    Float_t RJZ_H11_Tau1;
    Float_t RJZ_H11_Z;
    Float_t RJZ_H21_Tau1;
    Float_t RJZ_H21_Z;
    Float_t RJZ_Ht01_Tau1;
    Float_t RJZ_Ht01_Z;
    Float_t RJZ_Ht11_Tau1;
    Float_t RJZ_Ht11_Z;
    Float_t RJZ_Ht21_Tau1;
    Float_t RJZ_Ht21_Z;
    std::vector<float> *RJZ_JigSawCandidates_CosThetaStar;
    std::vector<float> *RJZ_JigSawCandidates_dPhiDecayPlane;
    std::vector<float> *RJZ_JigSawCandidates_eta;
    std::vector<unsigned int> *RJZ_JigSawCandidates_frame;
    std::vector<float> *RJZ_JigSawCandidates_m;
    std::vector<int> *RJZ_JigSawCandidates_pdgId;
    std::vector<float> *RJZ_JigSawCandidates_phi;
    std::vector<float> *RJZ_JigSawCandidates_pt;
    Float_t RJZ_R_BoostZ;
    Float_t RJZ_cosTheta_Tau1;
    Float_t RJZ_cosTheta_Tau2;
    Float_t RJZ_cosTheta_Z;
    Float_t RJZ_dPhiDecayPlane_Tau1;
    Float_t RJZ_dPhiDecayPlane_Tau2;
    Float_t RJZ_dPhiDecayPlane_Z;
    UInt_t RandomLumiBlockNumber;
    UInt_t RandomRunNumber;
    Int_t SUSYFinalState;
    Double_t TauWeight;
    Double_t TauWeightTrigHLT_tau125_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF;
    Double_t TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;
    Double_t TauWeightTrigHLT_tau160_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau25_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau25_medium1_tracktwoEF;
    Double_t TauWeightTrigHLT_tau25_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;
    Double_t TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF;
    Double_t TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;
    Double_t TauWeightTrigHLT_tau35_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau35_medium1_tracktwoEF;
    Double_t TauWeightTrigHLT_tau35_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;
    Double_t TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau60_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau60_medium1_tracktwoEF;
    Double_t TauWeightTrigHLT_tau60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;
    Double_t TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo;
    Double_t TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF;
    Double_t TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;
    Char_t TrigHLT_e120_lhloose;
    Char_t TrigHLT_e140_lhloose_nod0;
    Char_t TrigHLT_e24_lhmedium_L1EM20VH;
    Char_t TrigHLT_e26_lhtight_nod0_ivarloose;
    Char_t TrigHLT_e60_lhmedium;
    Char_t TrigHLT_e60_lhmedium_nod0;
    Char_t TrigHLT_mu20_iloose_L1MU15;
    Char_t TrigHLT_mu24_ivarmedium;
    Char_t TrigHLT_mu26_ivarmedium;
    Char_t TrigHLT_mu40;
    Char_t TrigHLT_mu50;
    Char_t TrigHLT_noalg_L1J400;
    Char_t TrigHLT_tau125_medium1_tracktwo;
    Char_t TrigHLT_tau160_medium1_tracktwo;
    Char_t TrigHLT_tau160_medium1_tracktwoEF;
    Char_t TrigHLT_tau160_medium1_tracktwoEF_L1TAU100;
    Char_t TrigHLT_tau160_medium1_tracktwo_L1TAU100;
    Char_t TrigHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100;
    Char_t TrigHLT_tau25_medium1_tracktwo;
    Char_t TrigHLT_tau25_medium1_tracktwoEF;
    Char_t TrigHLT_tau25_mediumRNN_tracktwoMVA;
    Char_t TrigHLT_tau35_medium1_tracktwo;
    Char_t TrigHLT_tau35_medium1_tracktwoEF;
    Char_t TrigHLT_tau35_medium1_tracktwoEF_L1TAU12IM;
    Char_t TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
    Char_t TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
    Char_t TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
    Char_t TrigHLT_tau35_mediumRNN_tracktwoMVA;
    Char_t TrigHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM;
    Char_t TrigHLT_tau50_medium1_tracktwo_L1TAU12;
    Char_t TrigHLT_tau60_medium1_tracktwo;
    Char_t TrigHLT_tau60_medium1_tracktwoEF;
    Char_t TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;
    Char_t TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
    Char_t TrigHLT_tau60_mediumRNN_tracktwoMVA;
    Char_t TrigHLT_tau80_medium1_tracktwoEF;
    Char_t TrigHLT_tau80_medium1_tracktwoEF_L1TAU60;
    Char_t TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
    Char_t TrigHLT_tau80_medium1_tracktwo_L1TAU60;
    Char_t TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;
    Char_t TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
    Char_t TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
    Char_t TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60;
    Char_t TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
    Char_t TrigHLT_xe100_pufit_L1XE55;
    Char_t TrigHLT_xe110_mht_L1XE50;
    Char_t TrigHLT_xe110_pufit_L1XE50;
    Char_t TrigHLT_xe110_pufit_L1XE55;
    Char_t TrigHLT_xe110_pufit_xe65_L1XE50;
    Char_t TrigHLT_xe110_pufit_xe70_L1XE50;
    Char_t TrigHLT_xe120_pufit_L1XE50;
    Char_t TrigHLT_xe70;
    Char_t TrigHLT_xe70_mht;
    Char_t TrigHLT_xe70_tc_lcw;
    Char_t TrigHLT_xe90_mht_L1XE50;
    Char_t TrigHLT_xe90_pufit_L1XE50;
    Char_t TrigMatchHLT_e120_lhloose;
    Char_t TrigMatchHLT_e140_lhloose_nod0;
    Char_t TrigMatchHLT_e24_lhmedium_L1EM20VH;
    Char_t TrigMatchHLT_e26_lhtight_nod0_ivarloose;
    Char_t TrigMatchHLT_e60_lhmedium;
    Char_t TrigMatchHLT_e60_lhmedium_nod0;
    Char_t TrigMatchHLT_mu20_iloose_L1MU15;
    Char_t TrigMatchHLT_mu24_ivarmedium;
    Char_t TrigMatchHLT_mu26_ivarmedium;
    Char_t TrigMatchHLT_mu40;
    Char_t TrigMatchHLT_mu50;
    Char_t TrigMatchHLT_tau125_medium1_tracktwo;
    Char_t TrigMatchHLT_tau160_medium1_tracktwo;
    Char_t TrigMatchHLT_tau160_medium1_tracktwoEF;
    Char_t TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100;
    Char_t TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100;
    Char_t TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100;
    Char_t TrigMatchHLT_tau25_medium1_tracktwo;
    Char_t TrigMatchHLT_tau25_medium1_tracktwoEF;
    Char_t TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;
    Char_t TrigMatchHLT_tau35_medium1_tracktwo;
    Char_t TrigMatchHLT_tau35_medium1_tracktwoEF;
    Char_t TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM;
    Char_t TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
    Char_t TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
    Char_t TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
    Char_t TrigMatchHLT_tau35_mediumRNN_tracktwoMVA;
    Char_t TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM;
    Char_t TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;
    Char_t TrigMatchHLT_tau60_medium1_tracktwo;
    Char_t TrigMatchHLT_tau60_medium1_tracktwoEF;
    Char_t TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;
    Char_t TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
    Char_t TrigMatchHLT_tau60_mediumRNN_tracktwoMVA;
    Char_t TrigMatchHLT_tau80_medium1_tracktwoEF;
    Char_t TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60;
    Char_t TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
    Char_t TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;
    Char_t TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;
    Char_t TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
    Char_t TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
    Char_t TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60;
    Char_t TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
    Char_t TrigMatching;
    Float_t TruthMET_met;
    Float_t TruthMET_phi;
    Float_t TruthMET_sumet;
    Float_t VecSumPt_LepTau;
    Int_t Vtx_n;
    Float_t actualInteractionsPerCrossing;
    Float_t averageInteractionsPerCrossing;
    UInt_t backgroundFlags;
    UInt_t bcid;
    UInt_t coreFlags;
    Float_t corr_avgIntPerX;
    std::vector<float> *dilepton_charge;
    std::vector<float> *dilepton_eta;
    std::vector<float> *dilepton_m;
    std::vector<int> *dilepton_pdgId;
    std::vector<float> *dilepton_phi;
    std::vector<float> *dilepton_pt;
    std::vector<int> *electrons_IFFClassType;
    std::vector<float> *electrons_MT;
    std::vector<float> *electrons_charge;
    std::vector<float> *electrons_d0sig;
    std::vector<float> *electrons_e;
    std::vector<float> *electrons_eta;
    std::vector<char> *electrons_isol;
    std::vector<float> *electrons_phi;
    std::vector<float> *electrons_pt;
    std::vector<char> *electrons_signal;
    std::vector<int> *electrons_truthOrigin;
    std::vector<int> *electrons_truthType;
    std::vector<float> *electrons_z0sinTheta;
    Float_t emt_MT2_max;
    Float_t emt_MT2_min;
    ULong64_t eventNumber;
    UInt_t forwardDetFlags;
    std::vector<double> *jets_BTagScore;
    std::vector<float> *jets_Jvt;
    std::vector<int> *jets_NTrks;
    std::vector<char> *jets_bjet;
    std::vector<float> *jets_eta;
    std::vector<char> *jets_isBadTight;
    std::vector<float> *jets_m;
    std::vector<float> *jets_phi;
    std::vector<float> *jets_pt;
    std::vector<char> *jets_signal;
    UInt_t larFlags;
    UInt_t lumiBlock;
    UInt_t lumiFlags;
    Double_t muWeight;
    Float_t mu_density;
    UInt_t muonFlags;
    std::vector<int> *muons_IFFClassType;
    std::vector<float> *muons_MT;
    std::vector<float> *muons_charge;
    std::vector<float> *muons_d0sig;
    std::vector<float> *muons_e;
    std::vector<float> *muons_eta;
    std::vector<char> *muons_isol;
    std::vector<float> *muons_phi;
    std::vector<float> *muons_pt;
    std::vector<char> *muons_signal;
    std::vector<int> *muons_truthOrigin;
    std::vector<int> *muons_truthType;
    std::vector<float> *muons_z0sinTheta;
    Int_t n_BJets;
    Int_t n_BaseElec;
    Int_t n_BaseJets;
    Int_t n_BaseMuon;
    Int_t n_BaseTau;
    Int_t n_SignalElec;
    Int_t n_SignalJets;
    Int_t n_SignalMuon;
    Int_t n_SignalTau;
    Int_t n_TruthJets;
    UInt_t pixelFlags;
    UInt_t runNumber;
    UInt_t sctFlags;
    std::vector<float> *taus_BDTEleScore;
    std::vector<float> *taus_BDTEleScoreSigTrans;
    std::vector<int> *taus_ConeTruthLabelID;
    std::vector<float> *taus_MT;
    std::vector<int> *taus_NTrks;
    std::vector<int> *taus_NTrksJet;
    std::vector<int> *taus_PartonTruthLabelID;
    std::vector<int> *taus_Quality;
    std::vector<float> *taus_RNNJetScore;
    std::vector<float> *taus_RNNJetScoreSigTrans;
    std::vector<char> *taus_TrigMatchHLT_tau125_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau160_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau160_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100;
    std::vector<char> *taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100;
    std::vector<char> *taus_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100;
    std::vector<char> *taus_TrigMatchHLT_tau25_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau25_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;
    std::vector<char> *taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
    std::vector<char> *taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA;
    std::vector<char> *taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM;
    std::vector<char> *taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;
    std::vector<char> *taus_TrigMatchHLT_tau60_medium1_tracktwo;
    std::vector<char> *taus_TrigMatchHLT_tau60_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;
    std::vector<char> *taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;
    std::vector<char> *taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwoEF;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;
    std::vector<char> *taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;
    std::vector<char> *taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60;
    std::vector<char> *taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;
    std::vector<float> *taus_TrkJetWidth;
    std::vector<float> *taus_Width;
    std::vector<float> *taus_charge;
    std::vector<float> *taus_d0;
    std::vector<float> *taus_d0sig;
    std::vector<float> *taus_e;
    std::vector<float> *taus_eta;
    std::vector<float> *taus_phi;
    std::vector<float> *taus_pt;
    std::vector<char> *taus_signalID;
    std::vector<std::vector<float> > *taus_trks_d0;
    std::vector<std::vector<float> > *taus_trks_d0sig;
    std::vector<std::vector<float> > *taus_trks_z0sinTheta;
    std::vector<int> *taus_truthOrigin;
    std::vector<int> *taus_truthType;
    std::vector<float> *taus_z0sinTheta;
    UInt_t tileFlags;
    UInt_t trtFlags;

    // List of branches
    TBranch *b_mcChannelNumber; //!
    TBranch *b_ColinearMTauTau; //!
    TBranch *b_CosChi1;         //!
    TBranch *b_CosChi2;         //!
    TBranch *b_EleWeight;       //!
    TBranch *b_EleWeightId;     //!
    TBranch *b_EleWeightIso;    //!
    TBranch *b_EleWeightReco;   //!
    TBranch *
        b_EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0; //!
    TBranch *b_GenFiltHT;                                                                                                 //!
    TBranch *b_GenFiltMET;                                                                                                //!
    TBranch *b_GenWeight;                                                                                                 //!
    TBranch *b_GenWeight_LHE_MEWeight;                                                                                    //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200;                                                                 //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEW;                                                           //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1;                                                        //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2;                                                     //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3;                                                  //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEW;                                                        //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1;                                                     //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2;                                                  //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3;                                               //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEW;                                                      //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1;                                                   //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2;                                                //!
    TBranch *b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3;                                             //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200;                                                                         //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEW;                                                                   //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1;                                                                //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1LO2;                                                             //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3;                                                          //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEW;                                                                //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1;                                                             //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2;                                                          //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3;                                                       //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEW;                                                              //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1;                                                           //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2;                                                        //!
    TBranch *b_GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3;                                                     //!
    TBranch *b_GenWeight_LHE_NTrials;                                                                                     //!
    TBranch *b_GenWeight_LHE_UserHook;                                                                                    //!
    TBranch *b_GenWeight_LHE_WeightNormalisation;                                                                         //!
    TBranch *b_Ht_Jet;                                                                                                    //!
    TBranch *b_Ht_Lep;                                                                                                    //!
    TBranch *b_Ht_Tau;                                                                                                    //!
    TBranch *b_JetWeight;                                                                                                 //!
    TBranch *b_LQ_ncl;                                                                                                    //!
    TBranch *b_MCT;                                                                                                       //!
    TBranch *b_MET_BiSect;                                                                                                //!
    TBranch *b_MET_Centrality;                                                                                            //!
    TBranch *b_MET_CosMinDeltaPhi;                                                                                        //!
    TBranch *b_MET_LepTau_DeltaPhi;                                                                                       //!
    TBranch *b_MET_LepTau_LeadingJet_VecSumPt;                                                                            //!
    TBranch *b_MET_LepTau_VecSumPhi;                                                                                      //!
    TBranch *b_MET_LepTau_VecSumPt;                                                                                       //!
    TBranch *b_MET_SumCosDeltaPhi;                                                                                        //!
    TBranch *b_MT2_max;                                                                                                   //!
    TBranch *b_MT2_min;                                                                                                   //!
    TBranch *b_Meff;                                                                                                      //!
    TBranch *b_Meff_TauTau;                                                                                               //!
    TBranch *b_MetTST_met;                                                                                                //!
    TBranch *b_MetTST_phi;                                                                                                //!
    TBranch *b_MetTST_sumet;                                                                                              //!
    TBranch *b_MetTST_OverSqrtHT;                                                                                         //!
    TBranch *b_MetTST_OverSqrtSumET;                                                                                      //!
    TBranch *b_MetTST_Significance;                                                                                       //!
    TBranch *b_MetTST_Significance_Rho;                                                                                   //!
    TBranch *b_MetTST_Significance_VarL;                                                                                  //!
    TBranch *b_MetTST_Significance_noPUJets_noSoftTerm;                                                                   //!
    TBranch *b_MetTST_Significance_noPUJets_noSoftTerm_Rho;                                                               //!
    TBranch *b_MetTST_Significance_noPUJets_noSoftTerm_VarL;                                                              //!
    TBranch *b_MetTST_Significance_phireso_noPUJets;                                                                      //!
    TBranch *b_MetTST_Significance_phireso_noPUJets_Rho;                                                                  //!
    TBranch *b_MetTST_Significance_phireso_noPUJets_VarL;                                                                 //!
    TBranch *b_MuoWeight;                                                                                                 //!
    TBranch *b_MuoWeightIsol;                                                                                             //!
    TBranch *b_MuoWeightReco;                                                                                             //!
    TBranch *b_MuoWeightTTVA;                                                                                             //!
    TBranch *b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40;                                                           //!
    TBranch *b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40;                                                              //!
    TBranch *b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50;                                                              //!
    TBranch *b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50;                                                              //!
    TBranch *b_OS_BaseTauEle;                                                                                             //!
    TBranch *b_OS_BaseTauMuo;                                                                                             //!
    TBranch *b_OS_EleEle;                                                                                                 //!
    TBranch *b_OS_MuoMuo;                                                                                                 //!
    TBranch *b_OS_TauEle;                                                                                                 //!
    TBranch *b_OS_TauMuo;                                                                                                 //!
    TBranch *b_OS_TauTau;                                                                                                 //!
    TBranch *b_PTt;                                                                                                       //!
    TBranch *b_RJW_CosThetaStarW;                                                                                         //!
    TBranch *b_RJW_GammaBetaW;                                                                                            //!
    TBranch *b_RJW_JigSawCandidates_eta;                                                                                  //!
    TBranch *b_RJW_JigSawCandidates_frame;                                                                                //!
    TBranch *b_RJW_JigSawCandidates_m;                                                                                    //!
    TBranch *b_RJW_JigSawCandidates_pdgId;                                                                                //!
    TBranch *b_RJW_JigSawCandidates_phi;                                                                                  //!
    TBranch *b_RJW_JigSawCandidates_pt;                                                                                   //!
    TBranch *b_RJW_dPhiDecayPlaneW;                                                                                       //!
    TBranch *b_RJZ_BalMetObj_Z;                                                                                           //!
    TBranch *b_RJZ_CMS_Mass;                                                                                              //!
    TBranch *b_RJZ_H01_Tau1;                                                                                              //!
    TBranch *b_RJZ_H01_Z;                                                                                                 //!
    TBranch *b_RJZ_H11_Tau1;                                                                                              //!
    TBranch *b_RJZ_H11_Z;                                                                                                 //!
    TBranch *b_RJZ_H21_Tau1;                                                                                              //!
    TBranch *b_RJZ_H21_Z;                                                                                                 //!
    TBranch *b_RJZ_Ht01_Tau1;                                                                                             //!
    TBranch *b_RJZ_Ht01_Z;                                                                                                //!
    TBranch *b_RJZ_Ht11_Tau1;                                                                                             //!
    TBranch *b_RJZ_Ht11_Z;                                                                                                //!
    TBranch *b_RJZ_Ht21_Tau1;                                                                                             //!
    TBranch *b_RJZ_Ht21_Z;                                                                                                //!
    TBranch *b_RJZ_JigSawCandidates_CosThetaStar;                                                                         //!
    TBranch *b_RJZ_JigSawCandidates_dPhiDecayPlane;                                                                       //!
    TBranch *b_RJZ_JigSawCandidates_eta;                                                                                  //!
    TBranch *b_RJZ_JigSawCandidates_frame;                                                                                //!
    TBranch *b_RJZ_JigSawCandidates_m;                                                                                    //!
    TBranch *b_RJZ_JigSawCandidates_pdgId;                                                                                //!
    TBranch *b_RJZ_JigSawCandidates_phi;                                                                                  //!
    TBranch *b_RJZ_JigSawCandidates_pt;                                                                                   //!
    TBranch *b_RJZ_R_BoostZ;                                                                                              //!
    TBranch *b_RJZ_cosTheta_Tau1;                                                                                         //!
    TBranch *b_RJZ_cosTheta_Tau2;                                                                                         //!
    TBranch *b_RJZ_cosTheta_Z;                                                                                            //!
    TBranch *b_RJZ_dPhiDecayPlane_Tau1;                                                                                   //!
    TBranch *b_RJZ_dPhiDecayPlane_Tau2;                                                                                   //!
    TBranch *b_RJZ_dPhiDecayPlane_Z;                                                                                      //!
    TBranch *b_RandomLumiBlockNumber;                                                                                     //!
    TBranch *b_RandomRunNumber;                                                                                           //!
    TBranch *b_SUSYFinalState;                                                                                            //!
    TBranch *b_TauWeight;                                                                                                 //!
    TBranch *b_TauWeightTrigHLT_tau125_medium1_tracktwo;                                                                  //!
    TBranch *b_TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwo;                                                          //!
    TBranch *b_TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF;                                                        //!
    TBranch *b_TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;                               //!
    TBranch *b_TauWeightTrigHLT_tau160_medium1_tracktwo;                                                                  //!
    TBranch *b_TauWeightTrigHLT_tau25_medium1_tracktwo;                                                                   //!
    TBranch *b_TauWeightTrigHLT_tau25_medium1_tracktwoEF;                                                                 //!
    TBranch *b_TauWeightTrigHLT_tau25_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;                                        //!
    TBranch *b_TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF;                                                        //!
    TBranch *b_TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;                               //!
    TBranch *b_TauWeightTrigHLT_tau35_medium1_tracktwo;                                                                   //!
    TBranch *b_TauWeightTrigHLT_tau35_medium1_tracktwoEF;                                                                 //!
    TBranch *b_TauWeightTrigHLT_tau35_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;                                        //!
    TBranch *b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;                                                            //!
    TBranch *b_TauWeightTrigHLT_tau60_medium1_tracktwo;                                                                   //!
    TBranch *b_TauWeightTrigHLT_tau60_medium1_tracktwoEF;                                                                 //!
    TBranch *b_TauWeightTrigHLT_tau60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;                                        //!
    TBranch *b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo;                                                            //!
    TBranch *b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF;                                                          //!
    TBranch *b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA;                                 //!
    TBranch *b_TrigHLT_e120_lhloose;                                                                                      //!
    TBranch *b_TrigHLT_e140_lhloose_nod0;                                                                                 //!
    TBranch *b_TrigHLT_e24_lhmedium_L1EM20VH;                                                                             //!
    TBranch *b_TrigHLT_e26_lhtight_nod0_ivarloose;                                                                        //!
    TBranch *b_TrigHLT_e60_lhmedium;                                                                                      //!
    TBranch *b_TrigHLT_e60_lhmedium_nod0;                                                                                 //!
    TBranch *b_TrigHLT_mu20_iloose_L1MU15;                                                                                //!
    TBranch *b_TrigHLT_mu24_ivarmedium;                                                                                   //!
    TBranch *b_TrigHLT_mu26_ivarmedium;                                                                                   //!
    TBranch *b_TrigHLT_mu40;                                                                                              //!
    TBranch *b_TrigHLT_mu50;                                                                                              //!
    TBranch *b_TrigHLT_noalg_L1J400;                                                                                      //!
    TBranch *b_TrigHLT_tau125_medium1_tracktwo;                                                                           //!
    TBranch *b_TrigHLT_tau160_medium1_tracktwo;                                                                           //!
    TBranch *b_TrigHLT_tau160_medium1_tracktwoEF;                                                                         //!
    TBranch *b_TrigHLT_tau160_medium1_tracktwoEF_L1TAU100;                                                                //!
    TBranch *b_TrigHLT_tau160_medium1_tracktwo_L1TAU100;                                                                  //!
    TBranch *b_TrigHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100;                                                             //!
    TBranch *b_TrigHLT_tau25_medium1_tracktwo;                                                                            //!
    TBranch *b_TrigHLT_tau25_medium1_tracktwoEF;                                                                          //!
    TBranch *b_TrigHLT_tau25_mediumRNN_tracktwoMVA;                                                                       //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwo;                                                                            //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwoEF;                                                                          //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwoEF_L1TAU12IM;                                                                //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;                                                     //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;                                  //!
    TBranch *b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;                                                //!
    TBranch *b_TrigHLT_tau35_mediumRNN_tracktwoMVA;                                                                       //!
    TBranch *b_TrigHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM;                                                             //!
    TBranch *b_TrigHLT_tau50_medium1_tracktwo_L1TAU12;                                                                    //!
    TBranch *b_TrigHLT_tau60_medium1_tracktwo;                                                                            //!
    TBranch *b_TrigHLT_tau60_medium1_tracktwoEF;                                                                          //!
    TBranch *b_TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;                                            //!
    TBranch *b_TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;                                                //!
    TBranch *b_TrigHLT_tau60_mediumRNN_tracktwoMVA;                                                                       //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwoEF;                                                                          //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60;                                                                  //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;                                 //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60;                                                                    //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;           //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;                                     //!
    TBranch *b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;                                     //!
    TBranch *b_TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60;                                                               //!
    TBranch *b_TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;                           //!
    TBranch *b_TrigHLT_xe100_pufit_L1XE55;                                                                                //!
    TBranch *b_TrigHLT_xe110_mht_L1XE50;                                                                                  //!
    TBranch *b_TrigHLT_xe110_pufit_L1XE50;                                                                                //!
    TBranch *b_TrigHLT_xe110_pufit_L1XE55;                                                                                //!
    TBranch *b_TrigHLT_xe110_pufit_xe65_L1XE50;                                                                           //!
    TBranch *b_TrigHLT_xe110_pufit_xe70_L1XE50;                                                                           //!
    TBranch *b_TrigHLT_xe120_pufit_L1XE50;                                                                                //!
    TBranch *b_TrigHLT_xe70;                                                                                              //!
    TBranch *b_TrigHLT_xe70_mht;                                                                                          //!
    TBranch *b_TrigHLT_xe70_tc_lcw;                                                                                       //!
    TBranch *b_TrigHLT_xe90_mht_L1XE50;                                                                                   //!
    TBranch *b_TrigHLT_xe90_pufit_L1XE50;                                                                                 //!
    TBranch *b_TrigMatchHLT_e120_lhloose;                                                                                 //!
    TBranch *b_TrigMatchHLT_e140_lhloose_nod0;                                                                            //!
    TBranch *b_TrigMatchHLT_e24_lhmedium_L1EM20VH;                                                                        //!
    TBranch *b_TrigMatchHLT_e26_lhtight_nod0_ivarloose;                                                                   //!
    TBranch *b_TrigMatchHLT_e60_lhmedium;                                                                                 //!
    TBranch *b_TrigMatchHLT_e60_lhmedium_nod0;                                                                            //!
    TBranch *b_TrigMatchHLT_mu20_iloose_L1MU15;                                                                           //!
    TBranch *b_TrigMatchHLT_mu24_ivarmedium;                                                                              //!
    TBranch *b_TrigMatchHLT_mu26_ivarmedium;                                                                              //!
    TBranch *b_TrigMatchHLT_mu40;                                                                                         //!
    TBranch *b_TrigMatchHLT_mu50;                                                                                         //!
    TBranch *b_TrigMatchHLT_tau125_medium1_tracktwo;                                                                      //!
    TBranch *b_TrigMatchHLT_tau160_medium1_tracktwo;                                                                      //!
    TBranch *b_TrigMatchHLT_tau160_medium1_tracktwoEF;                                                                    //!
    TBranch *b_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100;                                                           //!
    TBranch *b_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100;                                                             //!
    TBranch *b_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100;                                                        //!
    TBranch *b_TrigMatchHLT_tau25_medium1_tracktwo;                                                                       //!
    TBranch *b_TrigMatchHLT_tau25_medium1_tracktwoEF;                                                                     //!
    TBranch *b_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;                                                                  //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwo;                                                                       //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwoEF;                                                                     //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM;                                                           //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;                                                //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;                             //!
    TBranch *b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;                                           //!
    TBranch *b_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA;                                                                  //!
    TBranch *b_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM;                                                        //!
    TBranch *b_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;                                                               //!
    TBranch *b_TrigMatchHLT_tau60_medium1_tracktwo;                                                                       //!
    TBranch *b_TrigMatchHLT_tau60_medium1_tracktwoEF;                                                                     //!
    TBranch *b_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;                                       //!
    TBranch *b_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;                                           //!
    TBranch *b_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA;                                                                  //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwoEF;                                                                     //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60;                                                             //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;                            //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;                                                               //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I;      //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;                                //!
    TBranch *b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;                                //!
    TBranch *b_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60;                                                          //!
    TBranch *b_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;                      //!
    TBranch *b_TrigMatching;                                                                                              //!
    TBranch *b_TruthMET_met;                                                                                              //!
    TBranch *b_TruthMET_phi;                                                                                              //!
    TBranch *b_TruthMET_sumet;                                                                                            //!
    TBranch *b_VecSumPt_LepTau;                                                                                           //!
    TBranch *b_Vtx_n;                                                                                                     //!
    TBranch *b_actualInteractionsPerCrossing;                                                                             //!
    TBranch *b_averageInteractionsPerCrossing;                                                                            //!
    TBranch *b_backgroundFlags;                                                                                           //!
    TBranch *b_bcid;                                                                                                      //!
    TBranch *b_coreFlags;                                                                                                 //!
    TBranch *b_corr_avgIntPerX;                                                                                           //!
    TBranch *b_dilepton_charge;                                                                                           //!
    TBranch *b_dilepton_eta;                                                                                              //!
    TBranch *b_dilepton_m;                                                                                                //!
    TBranch *b_dilepton_pdgId;                                                                                            //!
    TBranch *b_dilepton_phi;                                                                                              //!
    TBranch *b_dilepton_pt;                                                                                               //!
    TBranch *b_electrons_IFFClassType;                                                                                    //!
    TBranch *b_electrons_MT;                                                                                              //!
    TBranch *b_electrons_charge;                                                                                          //!
    TBranch *b_electrons_d0sig;                                                                                           //!
    TBranch *b_electrons_e;                                                                                               //!
    TBranch *b_electrons_eta;                                                                                             //!
    TBranch *b_electrons_isol;                                                                                            //!
    TBranch *b_electrons_phi;                                                                                             //!
    TBranch *b_electrons_pt;                                                                                              //!
    TBranch *b_electrons_signal;                                                                                          //!
    TBranch *b_electrons_truthOrigin;                                                                                     //!
    TBranch *b_electrons_truthType;                                                                                       //!
    TBranch *b_electrons_z0sinTheta;                                                                                      //!
    TBranch *b_emt_MT2_max;                                                                                               //!
    TBranch *b_emt_MT2_min;                                                                                               //!
    TBranch *b_eventNumber;                                                                                               //!
    TBranch *b_forwardDetFlags;                                                                                           //!
    TBranch *b_jets_BTagScore;                                                                                            //!
    TBranch *b_jets_Jvt;                                                                                                  //!
    TBranch *b_jets_NTrks;                                                                                                //!
    TBranch *b_jets_bjet;                                                                                                 //!
    TBranch *b_jets_eta;                                                                                                  //!
    TBranch *b_jets_isBadTight;                                                                                           //!
    TBranch *b_jets_m;                                                                                                    //!
    TBranch *b_jets_phi;                                                                                                  //!
    TBranch *b_jets_pt;                                                                                                   //!
    TBranch *b_jets_signal;                                                                                               //!
    TBranch *b_larFlags;                                                                                                  //!
    TBranch *b_lumiBlock;                                                                                                 //!
    TBranch *b_lumiFlags;                                                                                                 //!
    TBranch *b_muWeight;                                                                                                  //!
    TBranch *b_mu_density;                                                                                                //!
    TBranch *b_muonFlags;                                                                                                 //!
    TBranch *b_muons_IFFClassType;                                                                                        //!
    TBranch *b_muons_MT;                                                                                                  //!
    TBranch *b_muons_charge;                                                                                              //!
    TBranch *b_muons_d0sig;                                                                                               //!
    TBranch *b_muons_e;                                                                                                   //!
    TBranch *b_muons_eta;                                                                                                 //!
    TBranch *b_muons_isol;                                                                                                //!
    TBranch *b_muons_phi;                                                                                                 //!
    TBranch *b_muons_pt;                                                                                                  //!
    TBranch *b_muons_signal;                                                                                              //!
    TBranch *b_muons_truthOrigin;                                                                                         //!
    TBranch *b_muons_truthType;                                                                                           //!
    TBranch *b_muons_z0sinTheta;                                                                                          //!
    TBranch *b_n_BJets;                                                                                                   //!
    TBranch *b_n_BaseElec;                                                                                                //!
    TBranch *b_n_BaseJets;                                                                                                //!
    TBranch *b_n_BaseMuon;                                                                                                //!
    TBranch *b_n_BaseTau;                                                                                                 //!
    TBranch *b_n_SignalElec;                                                                                              //!
    TBranch *b_n_SignalJets;                                                                                              //!
    TBranch *b_n_SignalMuon;                                                                                              //!
    TBranch *b_n_SignalTau;                                                                                               //!
    TBranch *b_n_TruthJets;                                                                                               //!
    TBranch *b_pixelFlags;                                                                                                //!
    TBranch *b_runNumber;                                                                                                 //!
    TBranch *b_sctFlags;                                                                                                  //!
    TBranch *b_taus_BDTEleScore;                                                                                          //!
    TBranch *b_taus_BDTEleScoreSigTrans;                                                                                  //!
    TBranch *b_taus_ConeTruthLabelID;                                                                                     //!
    TBranch *b_taus_MT;                                                                                                   //!
    TBranch *b_taus_NTrks;                                                                                                //!
    TBranch *b_taus_NTrksJet;                                                                                             //!
    TBranch *b_taus_PartonTruthLabelID;                                                                                   //!
    TBranch *b_taus_Quality;                                                                                              //!
    TBranch *b_taus_RNNJetScore;                                                                                          //!
    TBranch *b_taus_RNNJetScoreSigTrans;                                                                                  //!
    TBranch *b_taus_TrigMatchHLT_tau125_medium1_tracktwo;                                                                 //!
    TBranch *b_taus_TrigMatchHLT_tau160_medium1_tracktwo;                                                                 //!
    TBranch *b_taus_TrigMatchHLT_tau160_medium1_tracktwoEF;                                                               //!
    TBranch *b_taus_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100;                                                      //!
    TBranch *b_taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100;                                                        //!
    TBranch *b_taus_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100;                                                   //!
    TBranch *b_taus_TrigMatchHLT_tau25_medium1_tracktwo;                                                                  //!
    TBranch *b_taus_TrigMatchHLT_tau25_medium1_tracktwoEF;                                                                //!
    TBranch *b_taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA;                                                             //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwo;                                                                  //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwoEF;                                                                //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM;                                                      //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo;                                           //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM;                        //!
    TBranch *b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50;                                      //!
    TBranch *b_taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA;                                                             //!
    TBranch *b_taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM;                                                   //!
    TBranch *b_taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12;                                                          //!
    TBranch *b_taus_TrigMatchHLT_tau60_medium1_tracktwo;                                                                  //!
    TBranch *b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF;                                                                //!
    TBranch *b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50;                                  //!
    TBranch *b_taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50;                                      //!
    TBranch *b_taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA;                                                             //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF;                                                                //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60;                                                        //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40;                       //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60;                                                          //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I; //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12;                           //!
    TBranch *b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40;                           //!
    TBranch *b_taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60;                                                     //!
    TBranch *b_taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40;                 //!
    TBranch *b_taus_TrkJetWidth;                                                                                          //!
    TBranch *b_taus_Width;                                                                                                //!
    TBranch *b_taus_charge;                                                                                               //!
    TBranch *b_taus_d0;                                                                                                   //!
    TBranch *b_taus_d0sig;                                                                                                //!
    TBranch *b_taus_e;                                                                                                    //!
    TBranch *b_taus_eta;                                                                                                  //!
    TBranch *b_taus_phi;                                                                                                  //!
    TBranch *b_taus_pt;                                                                                                   //!
    TBranch *b_taus_signalID;                                                                                             //!
    TBranch *b_taus_trks_d0;                                                                                              //!
    TBranch *b_taus_trks_d0sig;                                                                                           //!
    TBranch *b_taus_trks_z0sinTheta;                                                                                      //!
    TBranch *b_taus_truthOrigin;                                                                                          //!
    TBranch *b_taus_truthType;                                                                                            //!
    TBranch *b_taus_z0sinTheta;                                                                                           //!
    TBranch *b_tileFlags;                                                                                                 //!
    TBranch *b_trtFlags;                                                                                                  //!

    void Init(TChain *tree) override {
        std::lock_guard<std::mutex> lock(_mutex);

        // Set object pointer
        mcChannelNumber = 0;
        RJW_JigSawCandidates_eta = 0;
        RJW_JigSawCandidates_frame = 0;
        RJW_JigSawCandidates_m = 0;
        RJW_JigSawCandidates_pdgId = 0;
        RJW_JigSawCandidates_phi = 0;
        RJW_JigSawCandidates_pt = 0;
        RJZ_JigSawCandidates_CosThetaStar = 0;
        RJZ_JigSawCandidates_dPhiDecayPlane = 0;
        RJZ_JigSawCandidates_eta = 0;
        RJZ_JigSawCandidates_frame = 0;
        RJZ_JigSawCandidates_m = 0;
        RJZ_JigSawCandidates_pdgId = 0;
        RJZ_JigSawCandidates_phi = 0;
        RJZ_JigSawCandidates_pt = 0;
        dilepton_charge = 0;
        dilepton_eta = 0;
        dilepton_m = 0;
        dilepton_pdgId = 0;
        dilepton_phi = 0;
        dilepton_pt = 0;
        electrons_IFFClassType = 0;
        electrons_MT = 0;
        electrons_charge = 0;
        electrons_d0sig = 0;
        electrons_e = 0;
        electrons_eta = 0;
        electrons_isol = 0;
        electrons_phi = 0;
        electrons_pt = 0;
        electrons_signal = 0;
        electrons_truthOrigin = 0;
        electrons_truthType = 0;
        electrons_z0sinTheta = 0;
        jets_BTagScore = 0;
        jets_Jvt = 0;
        jets_NTrks = 0;
        jets_bjet = 0;
        jets_eta = 0;
        jets_isBadTight = 0;
        jets_m = 0;
        jets_phi = 0;
        jets_pt = 0;
        jets_signal = 0;
        muons_IFFClassType = 0;
        muons_MT = 0;
        muons_charge = 0;
        muons_d0sig = 0;
        muons_e = 0;
        muons_eta = 0;
        muons_isol = 0;
        muons_phi = 0;
        muons_pt = 0;
        muons_signal = 0;
        muons_truthOrigin = 0;
        muons_truthType = 0;
        muons_z0sinTheta = 0;
        taus_BDTEleScore = 0;
        taus_BDTEleScoreSigTrans = 0;
        taus_ConeTruthLabelID = 0;
        taus_MT = 0;
        taus_NTrks = 0;
        taus_NTrksJet = 0;
        taus_PartonTruthLabelID = 0;
        taus_Quality = 0;
        taus_RNNJetScore = 0;
        taus_RNNJetScoreSigTrans = 0;
        taus_TrigMatchHLT_tau125_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau160_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau160_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100 = 0;
        taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100 = 0;
        taus_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100 = 0;
        taus_TrigMatchHLT_tau25_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau25_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM = 0;
        taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50 = 0;
        taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA = 0;
        taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM = 0;
        taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12 = 0;
        taus_TrigMatchHLT_tau60_medium1_tracktwo = 0;
        taus_TrigMatchHLT_tau60_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50 = 0;
        taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50 = 0;
        taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwoEF = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60 = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40 = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60 = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12 = 0;
        taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40 = 0;
        taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60 = 0;
        taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40 = 0;
        taus_TrkJetWidth = 0;
        taus_Width = 0;
        taus_charge = 0;
        taus_d0 = 0;
        taus_d0sig = 0;
        taus_e = 0;
        taus_eta = 0;
        taus_phi = 0;
        taus_pt = 0;
        taus_signalID = 0;
        taus_trks_d0 = 0;
        taus_trks_d0sig = 0;
        taus_trks_z0sinTheta = 0;
        taus_truthOrigin = 0;
        taus_truthType = 0;
        taus_z0sinTheta = 0;

        fChain = tree;

        fChain->SetBranchAddress("mcChannelNumber", &mcChannelNumber, &b_mcChannelNumber);
        fChain->SetBranchAddress("ColinearMTauTau", &ColinearMTauTau, &b_ColinearMTauTau);
        fChain->SetBranchAddress("CosChi1", &CosChi1, &b_CosChi1);
        fChain->SetBranchAddress("CosChi2", &CosChi2, &b_CosChi2);
        fChain->SetBranchAddress("EleWeight", &EleWeight, &b_EleWeight);
        fChain->SetBranchAddress("EleWeightId", &EleWeightId, &b_EleWeightId);
        fChain->SetBranchAddress("EleWeightIso", &EleWeightIso, &b_EleWeightIso);
        fChain->SetBranchAddress("EleWeightReco", &EleWeightReco, &b_EleWeightReco);
        fChain->SetBranchAddress(
            "EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_"
            "nod0",
            &EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0,
            &b_EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0);
        fChain->SetBranchAddress("GenFiltHT", &GenFiltHT, &b_GenFiltHT);
        fChain->SetBranchAddress("GenFiltMET", &GenFiltMET, &b_GenFiltMET);
        fChain->SetBranchAddress("GenWeight", &GenWeight, &b_GenWeight);
        fChain->SetBranchAddress("GenWeight_LHE_MEWeight", &GenWeight_LHE_MEWeight, &b_GenWeight_LHE_MEWeight);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200", &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEW", &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEW,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEW);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1", &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2", &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3",
                                 &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEW", &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEW,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEW);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1", &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2",
                                 &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3",
                                 &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEW", &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEW,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEW);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1", &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2",
                                 &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2);
        fChain->SetBranchAddress("GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3",
                                 &GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3,
                                 &b_GenWeight_LHE_ME_ONLY_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200", &GenWeight_LHE_MUR1_MUF1_PDF303200, &b_GenWeight_LHE_MUR1_MUF1_PDF303200);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEW", &GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEW,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEW);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1", &GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1LO2", &GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1LO2,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1LO2);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3", &GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_ASSEWLO1LO2LO3);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEW", &GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEW,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEW);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1", &GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2", &GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3", &GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_EXPASSEWLO1LO2LO3);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEW", &GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEW,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEW);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1", &GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2", &GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2);
        fChain->SetBranchAddress("GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3", &GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3,
                                 &b_GenWeight_LHE_MUR1_MUF1_PDF303200_MULTIASSEWLO1LO2LO3);
        fChain->SetBranchAddress("GenWeight_LHE_NTrials", &GenWeight_LHE_NTrials, &b_GenWeight_LHE_NTrials);
        fChain->SetBranchAddress("GenWeight_LHE_UserHook", &GenWeight_LHE_UserHook, &b_GenWeight_LHE_UserHook);
        fChain->SetBranchAddress("GenWeight_LHE_WeightNormalisation", &GenWeight_LHE_WeightNormalisation, &b_GenWeight_LHE_WeightNormalisation);
        fChain->SetBranchAddress("Ht_Jet", &Ht_Jet, &b_Ht_Jet);
        fChain->SetBranchAddress("Ht_Lep", &Ht_Lep, &b_Ht_Lep);
        fChain->SetBranchAddress("Ht_Tau", &Ht_Tau, &b_Ht_Tau);
        fChain->SetBranchAddress("JetWeight", &JetWeight, &b_JetWeight);
        fChain->SetBranchAddress("LQ_ncl", &LQ_ncl, &b_LQ_ncl);
        fChain->SetBranchAddress("MCT", &MCT, &b_MCT);
        fChain->SetBranchAddress("MET_BiSect", &MET_BiSect, &b_MET_BiSect);
        fChain->SetBranchAddress("MET_Centrality", &MET_Centrality, &b_MET_Centrality);
        fChain->SetBranchAddress("MET_CosMinDeltaPhi", &MET_CosMinDeltaPhi, &b_MET_CosMinDeltaPhi);
        fChain->SetBranchAddress("MET_LepTau_DeltaPhi", &MET_LepTau_DeltaPhi, &b_MET_LepTau_DeltaPhi);
        fChain->SetBranchAddress("MET_LepTau_LeadingJet_VecSumPt", &MET_LepTau_LeadingJet_VecSumPt, &b_MET_LepTau_LeadingJet_VecSumPt);
        fChain->SetBranchAddress("MET_LepTau_VecSumPhi", &MET_LepTau_VecSumPhi, &b_MET_LepTau_VecSumPhi);
        fChain->SetBranchAddress("MET_LepTau_VecSumPt", &MET_LepTau_VecSumPt, &b_MET_LepTau_VecSumPt);
        fChain->SetBranchAddress("MET_SumCosDeltaPhi", &MET_SumCosDeltaPhi, &b_MET_SumCosDeltaPhi);
        fChain->SetBranchAddress("MT2_max", &MT2_max, &b_MT2_max);
        fChain->SetBranchAddress("MT2_min", &MT2_min, &b_MT2_min);
        fChain->SetBranchAddress("Meff", &Meff, &b_Meff);
        fChain->SetBranchAddress("Meff_TauTau", &Meff_TauTau, &b_Meff_TauTau);
        fChain->SetBranchAddress("MetTST_met", &MetTST_met, &b_MetTST_met);
        fChain->SetBranchAddress("MetTST_phi", &MetTST_phi, &b_MetTST_phi);
        fChain->SetBranchAddress("MetTST_sumet", &MetTST_sumet, &b_MetTST_sumet);
        fChain->SetBranchAddress("MetTST_OverSqrtHT", &MetTST_OverSqrtHT, &b_MetTST_OverSqrtHT);
        fChain->SetBranchAddress("MetTST_OverSqrtSumET", &MetTST_OverSqrtSumET, &b_MetTST_OverSqrtSumET);
        fChain->SetBranchAddress("MetTST_Significance", &MetTST_Significance, &b_MetTST_Significance);
        fChain->SetBranchAddress("MetTST_Significance_Rho", &MetTST_Significance_Rho, &b_MetTST_Significance_Rho);
        fChain->SetBranchAddress("MetTST_Significance_VarL", &MetTST_Significance_VarL, &b_MetTST_Significance_VarL);
        fChain->SetBranchAddress("MetTST_Significance_noPUJets_noSoftTerm", &MetTST_Significance_noPUJets_noSoftTerm,
                                 &b_MetTST_Significance_noPUJets_noSoftTerm);
        fChain->SetBranchAddress("MetTST_Significance_noPUJets_noSoftTerm_Rho", &MetTST_Significance_noPUJets_noSoftTerm_Rho,
                                 &b_MetTST_Significance_noPUJets_noSoftTerm_Rho);
        fChain->SetBranchAddress("MetTST_Significance_noPUJets_noSoftTerm_VarL", &MetTST_Significance_noPUJets_noSoftTerm_VarL,
                                 &b_MetTST_Significance_noPUJets_noSoftTerm_VarL);
        fChain->SetBranchAddress("MetTST_Significance_phireso_noPUJets", &MetTST_Significance_phireso_noPUJets,
                                 &b_MetTST_Significance_phireso_noPUJets);
        fChain->SetBranchAddress("MetTST_Significance_phireso_noPUJets_Rho", &MetTST_Significance_phireso_noPUJets_Rho,
                                 &b_MetTST_Significance_phireso_noPUJets_Rho);
        fChain->SetBranchAddress("MetTST_Significance_phireso_noPUJets_VarL", &MetTST_Significance_phireso_noPUJets_VarL,
                                 &b_MetTST_Significance_phireso_noPUJets_VarL);
        fChain->SetBranchAddress("MuoWeight", &MuoWeight, &b_MuoWeight);
        fChain->SetBranchAddress("MuoWeightIsol", &MuoWeightIsol, &b_MuoWeightIsol);
        fChain->SetBranchAddress("MuoWeightReco", &MuoWeightReco, &b_MuoWeightReco);
        fChain->SetBranchAddress("MuoWeightTTVA", &MuoWeightTTVA, &b_MuoWeightTTVA);
        fChain->SetBranchAddress("MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40", &MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40,
                                 &b_MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40);
        fChain->SetBranchAddress("MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40", &MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40,
                                 &b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu40);
        fChain->SetBranchAddress("MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50", &MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50,
                                 &b_MuoWeightTrigHLT_mu24_ivarmedium_OR_HLT_mu50);
        fChain->SetBranchAddress("MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50", &MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50,
                                 &b_MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50);
        fChain->SetBranchAddress("OS_BaseTauEle", &OS_BaseTauEle, &b_OS_BaseTauEle);
        fChain->SetBranchAddress("OS_BaseTauMuo", &OS_BaseTauMuo, &b_OS_BaseTauMuo);
        fChain->SetBranchAddress("OS_EleEle", &OS_EleEle, &b_OS_EleEle);
        fChain->SetBranchAddress("OS_MuoMuo", &OS_MuoMuo, &b_OS_MuoMuo);
        fChain->SetBranchAddress("OS_TauEle", &OS_TauEle, &b_OS_TauEle);
        fChain->SetBranchAddress("OS_TauMuo", &OS_TauMuo, &b_OS_TauMuo);
        fChain->SetBranchAddress("OS_TauTau", &OS_TauTau, &b_OS_TauTau);
        fChain->SetBranchAddress("PTt", &PTt, &b_PTt);
        fChain->SetBranchAddress("RJW_CosThetaStarW", &RJW_CosThetaStarW, &b_RJW_CosThetaStarW);
        fChain->SetBranchAddress("RJW_GammaBetaW", &RJW_GammaBetaW, &b_RJW_GammaBetaW);
        fChain->SetBranchAddress("RJW_JigSawCandidates_eta", &RJW_JigSawCandidates_eta, &b_RJW_JigSawCandidates_eta);
        fChain->SetBranchAddress("RJW_JigSawCandidates_frame", &RJW_JigSawCandidates_frame, &b_RJW_JigSawCandidates_frame);
        fChain->SetBranchAddress("RJW_JigSawCandidates_m", &RJW_JigSawCandidates_m, &b_RJW_JigSawCandidates_m);
        fChain->SetBranchAddress("RJW_JigSawCandidates_pdgId", &RJW_JigSawCandidates_pdgId, &b_RJW_JigSawCandidates_pdgId);
        fChain->SetBranchAddress("RJW_JigSawCandidates_phi", &RJW_JigSawCandidates_phi, &b_RJW_JigSawCandidates_phi);
        fChain->SetBranchAddress("RJW_JigSawCandidates_pt", &RJW_JigSawCandidates_pt, &b_RJW_JigSawCandidates_pt);
        fChain->SetBranchAddress("RJW_dPhiDecayPlaneW", &RJW_dPhiDecayPlaneW, &b_RJW_dPhiDecayPlaneW);
        fChain->SetBranchAddress("RJZ_BalMetObj_Z", &RJZ_BalMetObj_Z, &b_RJZ_BalMetObj_Z);
        fChain->SetBranchAddress("RJZ_CMS_Mass", &RJZ_CMS_Mass, &b_RJZ_CMS_Mass);
        fChain->SetBranchAddress("RJZ_H01_Tau1", &RJZ_H01_Tau1, &b_RJZ_H01_Tau1);
        fChain->SetBranchAddress("RJZ_H01_Z", &RJZ_H01_Z, &b_RJZ_H01_Z);
        fChain->SetBranchAddress("RJZ_H11_Tau1", &RJZ_H11_Tau1, &b_RJZ_H11_Tau1);
        fChain->SetBranchAddress("RJZ_H11_Z", &RJZ_H11_Z, &b_RJZ_H11_Z);
        fChain->SetBranchAddress("RJZ_H21_Tau1", &RJZ_H21_Tau1, &b_RJZ_H21_Tau1);
        fChain->SetBranchAddress("RJZ_H21_Z", &RJZ_H21_Z, &b_RJZ_H21_Z);
        fChain->SetBranchAddress("RJZ_Ht01_Tau1", &RJZ_Ht01_Tau1, &b_RJZ_Ht01_Tau1);
        fChain->SetBranchAddress("RJZ_Ht01_Z", &RJZ_Ht01_Z, &b_RJZ_Ht01_Z);
        fChain->SetBranchAddress("RJZ_Ht11_Tau1", &RJZ_Ht11_Tau1, &b_RJZ_Ht11_Tau1);
        fChain->SetBranchAddress("RJZ_Ht11_Z", &RJZ_Ht11_Z, &b_RJZ_Ht11_Z);
        fChain->SetBranchAddress("RJZ_Ht21_Tau1", &RJZ_Ht21_Tau1, &b_RJZ_Ht21_Tau1);
        fChain->SetBranchAddress("RJZ_Ht21_Z", &RJZ_Ht21_Z, &b_RJZ_Ht21_Z);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_CosThetaStar", &RJZ_JigSawCandidates_CosThetaStar, &b_RJZ_JigSawCandidates_CosThetaStar);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_dPhiDecayPlane", &RJZ_JigSawCandidates_dPhiDecayPlane, &b_RJZ_JigSawCandidates_dPhiDecayPlane);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_eta", &RJZ_JigSawCandidates_eta, &b_RJZ_JigSawCandidates_eta);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_frame", &RJZ_JigSawCandidates_frame, &b_RJZ_JigSawCandidates_frame);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_m", &RJZ_JigSawCandidates_m, &b_RJZ_JigSawCandidates_m);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_pdgId", &RJZ_JigSawCandidates_pdgId, &b_RJZ_JigSawCandidates_pdgId);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_phi", &RJZ_JigSawCandidates_phi, &b_RJZ_JigSawCandidates_phi);
        fChain->SetBranchAddress("RJZ_JigSawCandidates_pt", &RJZ_JigSawCandidates_pt, &b_RJZ_JigSawCandidates_pt);
        fChain->SetBranchAddress("RJZ_R_BoostZ", &RJZ_R_BoostZ, &b_RJZ_R_BoostZ);
        fChain->SetBranchAddress("RJZ_cosTheta_Tau1", &RJZ_cosTheta_Tau1, &b_RJZ_cosTheta_Tau1);
        fChain->SetBranchAddress("RJZ_cosTheta_Tau2", &RJZ_cosTheta_Tau2, &b_RJZ_cosTheta_Tau2);
        fChain->SetBranchAddress("RJZ_cosTheta_Z", &RJZ_cosTheta_Z, &b_RJZ_cosTheta_Z);
        fChain->SetBranchAddress("RJZ_dPhiDecayPlane_Tau1", &RJZ_dPhiDecayPlane_Tau1, &b_RJZ_dPhiDecayPlane_Tau1);
        fChain->SetBranchAddress("RJZ_dPhiDecayPlane_Tau2", &RJZ_dPhiDecayPlane_Tau2, &b_RJZ_dPhiDecayPlane_Tau2);
        fChain->SetBranchAddress("RJZ_dPhiDecayPlane_Z", &RJZ_dPhiDecayPlane_Z, &b_RJZ_dPhiDecayPlane_Z);
        fChain->SetBranchAddress("RandomLumiBlockNumber", &RandomLumiBlockNumber, &b_RandomLumiBlockNumber);
        fChain->SetBranchAddress("RandomRunNumber", &RandomRunNumber, &b_RandomRunNumber);
        fChain->SetBranchAddress("SUSYFinalState", &SUSYFinalState, &b_SUSYFinalState);
        fChain->SetBranchAddress("TauWeight", &TauWeight, &b_TauWeight);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau125_medium1_tracktwo", &TauWeightTrigHLT_tau125_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau125_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwo", &TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF", &TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF,
                                 &b_TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA",
                                 &TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA,
                                 &b_TauWeightTrigHLT_tau160L1TAU100_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau160_medium1_tracktwo", &TauWeightTrigHLT_tau160_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau160_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwo", &TauWeightTrigHLT_tau25_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwoEF", &TauWeightTrigHLT_tau25_medium1_tracktwoEF,
                                 &b_TauWeightTrigHLT_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau25_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA",
                                 &TauWeightTrigHLT_tau25_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA,
                                 &b_TauWeightTrigHLT_tau25_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF", &TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF,
                                 &b_TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA",
                                 &TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA,
                                 &b_TauWeightTrigHLT_tau35L1TAU12IM_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwo", &TauWeightTrigHLT_tau35_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwoEF", &TauWeightTrigHLT_tau35_medium1_tracktwoEF,
                                 &b_TauWeightTrigHLT_tau35_medium1_tracktwoEF);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau35_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA",
                                 &TauWeightTrigHLT_tau35_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA,
                                 &b_TauWeightTrigHLT_tau35_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo", &TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau60_medium1_tracktwo", &TauWeightTrigHLT_tau60_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau60_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau60_medium1_tracktwoEF", &TauWeightTrigHLT_tau60_medium1_tracktwoEF,
                                 &b_TauWeightTrigHLT_tau60_medium1_tracktwoEF);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA",
                                 &TauWeightTrigHLT_tau60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA,
                                 &b_TauWeightTrigHLT_tau60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo", &TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo,
                                 &b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF", &TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF,
                                 &b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF);
        fChain->SetBranchAddress("TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA",
                                 &TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA,
                                 &b_TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwoEF_OR_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigHLT_e120_lhloose", &TrigHLT_e120_lhloose, &b_TrigHLT_e120_lhloose);
        fChain->SetBranchAddress("TrigHLT_e140_lhloose_nod0", &TrigHLT_e140_lhloose_nod0, &b_TrigHLT_e140_lhloose_nod0);
        fChain->SetBranchAddress("TrigHLT_e24_lhmedium_L1EM20VH", &TrigHLT_e24_lhmedium_L1EM20VH, &b_TrigHLT_e24_lhmedium_L1EM20VH);
        fChain->SetBranchAddress("TrigHLT_e26_lhtight_nod0_ivarloose", &TrigHLT_e26_lhtight_nod0_ivarloose, &b_TrigHLT_e26_lhtight_nod0_ivarloose);
        fChain->SetBranchAddress("TrigHLT_e60_lhmedium", &TrigHLT_e60_lhmedium, &b_TrigHLT_e60_lhmedium);
        fChain->SetBranchAddress("TrigHLT_e60_lhmedium_nod0", &TrigHLT_e60_lhmedium_nod0, &b_TrigHLT_e60_lhmedium_nod0);
        fChain->SetBranchAddress("TrigHLT_mu20_iloose_L1MU15", &TrigHLT_mu20_iloose_L1MU15, &b_TrigHLT_mu20_iloose_L1MU15);
        fChain->SetBranchAddress("TrigHLT_mu24_ivarmedium", &TrigHLT_mu24_ivarmedium, &b_TrigHLT_mu24_ivarmedium);
        fChain->SetBranchAddress("TrigHLT_mu26_ivarmedium", &TrigHLT_mu26_ivarmedium, &b_TrigHLT_mu26_ivarmedium);
        fChain->SetBranchAddress("TrigHLT_mu40", &TrigHLT_mu40, &b_TrigHLT_mu40);
        fChain->SetBranchAddress("TrigHLT_mu50", &TrigHLT_mu50, &b_TrigHLT_mu50);
        fChain->SetBranchAddress("TrigHLT_noalg_L1J400", &TrigHLT_noalg_L1J400, &b_TrigHLT_noalg_L1J400);
        fChain->SetBranchAddress("TrigHLT_tau125_medium1_tracktwo", &TrigHLT_tau125_medium1_tracktwo, &b_TrigHLT_tau125_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_tau160_medium1_tracktwo", &TrigHLT_tau160_medium1_tracktwo, &b_TrigHLT_tau160_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_tau160_medium1_tracktwoEF", &TrigHLT_tau160_medium1_tracktwoEF, &b_TrigHLT_tau160_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_tau160_medium1_tracktwoEF_L1TAU100", &TrigHLT_tau160_medium1_tracktwoEF_L1TAU100,
                                 &b_TrigHLT_tau160_medium1_tracktwoEF_L1TAU100);
        fChain->SetBranchAddress("TrigHLT_tau160_medium1_tracktwo_L1TAU100", &TrigHLT_tau160_medium1_tracktwo_L1TAU100,
                                 &b_TrigHLT_tau160_medium1_tracktwo_L1TAU100);
        fChain->SetBranchAddress("TrigHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100", &TrigHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100,
                                 &b_TrigHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100);
        fChain->SetBranchAddress("TrigHLT_tau25_medium1_tracktwo", &TrigHLT_tau25_medium1_tracktwo, &b_TrigHLT_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_tau25_medium1_tracktwoEF", &TrigHLT_tau25_medium1_tracktwoEF, &b_TrigHLT_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_tau25_mediumRNN_tracktwoMVA", &TrigHLT_tau25_mediumRNN_tracktwoMVA, &b_TrigHLT_tau25_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo", &TrigHLT_tau35_medium1_tracktwo, &b_TrigHLT_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwoEF", &TrigHLT_tau35_medium1_tracktwoEF, &b_TrigHLT_tau35_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwoEF_L1TAU12IM", &TrigHLT_tau35_medium1_tracktwoEF_L1TAU12IM,
                                 &b_TrigHLT_tau35_medium1_tracktwoEF_L1TAU12IM);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo", &TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo,
                                 &b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
                                 &TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM,
                                 &b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
        fChain->SetBranchAddress("TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
                                 &TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
                                 &b_TrigHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigHLT_tau35_mediumRNN_tracktwoMVA", &TrigHLT_tau35_mediumRNN_tracktwoMVA, &b_TrigHLT_tau35_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM", &TrigHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM,
                                 &b_TrigHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM);
        fChain->SetBranchAddress("TrigHLT_tau50_medium1_tracktwo_L1TAU12", &TrigHLT_tau50_medium1_tracktwo_L1TAU12,
                                 &b_TrigHLT_tau50_medium1_tracktwo_L1TAU12);
        fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwo", &TrigHLT_tau60_medium1_tracktwo, &b_TrigHLT_tau60_medium1_tracktwo);
        fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwoEF", &TrigHLT_tau60_medium1_tracktwoEF, &b_TrigHLT_tau60_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50",
                                 &TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50,
                                 &b_TrigHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
                                 &TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
                                 &b_TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigHLT_tau60_mediumRNN_tracktwoMVA", &TrigHLT_tau60_mediumRNN_tracktwoMVA, &b_TrigHLT_tau60_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwoEF", &TrigHLT_tau80_medium1_tracktwoEF, &b_TrigHLT_tau80_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwoEF_L1TAU60", &TrigHLT_tau80_medium1_tracktwoEF_L1TAU60,
                                 &b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",
                                 &TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40,
                                 &b_TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60", &TrigHLT_tau80_medium1_tracktwo_L1TAU60,
                                 &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
                                 &TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I,
                                 &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",
                                 &TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12,
                                 &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
        fChain->SetBranchAddress("TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",
                                 &TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40,
                                 &b_TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
        fChain->SetBranchAddress("TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60", &TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60,
                                 &b_TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60);
        fChain->SetBranchAddress("TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",
                                 &TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40,
                                 &b_TrigHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
        fChain->SetBranchAddress("TrigHLT_xe100_pufit_L1XE55", &TrigHLT_xe100_pufit_L1XE55, &b_TrigHLT_xe100_pufit_L1XE55);
        fChain->SetBranchAddress("TrigHLT_xe110_mht_L1XE50", &TrigHLT_xe110_mht_L1XE50, &b_TrigHLT_xe110_mht_L1XE50);
        fChain->SetBranchAddress("TrigHLT_xe110_pufit_L1XE50", &TrigHLT_xe110_pufit_L1XE50, &b_TrigHLT_xe110_pufit_L1XE50);
        fChain->SetBranchAddress("TrigHLT_xe110_pufit_L1XE55", &TrigHLT_xe110_pufit_L1XE55, &b_TrigHLT_xe110_pufit_L1XE55);
        fChain->SetBranchAddress("TrigHLT_xe110_pufit_xe65_L1XE50", &TrigHLT_xe110_pufit_xe65_L1XE50, &b_TrigHLT_xe110_pufit_xe65_L1XE50);
        fChain->SetBranchAddress("TrigHLT_xe110_pufit_xe70_L1XE50", &TrigHLT_xe110_pufit_xe70_L1XE50, &b_TrigHLT_xe110_pufit_xe70_L1XE50);
        fChain->SetBranchAddress("TrigHLT_xe120_pufit_L1XE50", &TrigHLT_xe120_pufit_L1XE50, &b_TrigHLT_xe120_pufit_L1XE50);
        fChain->SetBranchAddress("TrigHLT_xe70", &TrigHLT_xe70, &b_TrigHLT_xe70);
        fChain->SetBranchAddress("TrigHLT_xe70_mht", &TrigHLT_xe70_mht, &b_TrigHLT_xe70_mht);
        fChain->SetBranchAddress("TrigHLT_xe70_tc_lcw", &TrigHLT_xe70_tc_lcw, &b_TrigHLT_xe70_tc_lcw);
        fChain->SetBranchAddress("TrigHLT_xe90_mht_L1XE50", &TrigHLT_xe90_mht_L1XE50, &b_TrigHLT_xe90_mht_L1XE50);
        fChain->SetBranchAddress("TrigHLT_xe90_pufit_L1XE50", &TrigHLT_xe90_pufit_L1XE50, &b_TrigHLT_xe90_pufit_L1XE50);
        fChain->SetBranchAddress("TrigMatchHLT_e120_lhloose", &TrigMatchHLT_e120_lhloose, &b_TrigMatchHLT_e120_lhloose);
        fChain->SetBranchAddress("TrigMatchHLT_e140_lhloose_nod0", &TrigMatchHLT_e140_lhloose_nod0, &b_TrigMatchHLT_e140_lhloose_nod0);
        fChain->SetBranchAddress("TrigMatchHLT_e24_lhmedium_L1EM20VH", &TrigMatchHLT_e24_lhmedium_L1EM20VH, &b_TrigMatchHLT_e24_lhmedium_L1EM20VH);
        fChain->SetBranchAddress("TrigMatchHLT_e26_lhtight_nod0_ivarloose", &TrigMatchHLT_e26_lhtight_nod0_ivarloose,
                                 &b_TrigMatchHLT_e26_lhtight_nod0_ivarloose);
        fChain->SetBranchAddress("TrigMatchHLT_e60_lhmedium", &TrigMatchHLT_e60_lhmedium, &b_TrigMatchHLT_e60_lhmedium);
        fChain->SetBranchAddress("TrigMatchHLT_e60_lhmedium_nod0", &TrigMatchHLT_e60_lhmedium_nod0, &b_TrigMatchHLT_e60_lhmedium_nod0);
        fChain->SetBranchAddress("TrigMatchHLT_mu20_iloose_L1MU15", &TrigMatchHLT_mu20_iloose_L1MU15, &b_TrigMatchHLT_mu20_iloose_L1MU15);
        fChain->SetBranchAddress("TrigMatchHLT_mu24_ivarmedium", &TrigMatchHLT_mu24_ivarmedium, &b_TrigMatchHLT_mu24_ivarmedium);
        fChain->SetBranchAddress("TrigMatchHLT_mu26_ivarmedium", &TrigMatchHLT_mu26_ivarmedium, &b_TrigMatchHLT_mu26_ivarmedium);
        fChain->SetBranchAddress("TrigMatchHLT_mu40", &TrigMatchHLT_mu40, &b_TrigMatchHLT_mu40);
        fChain->SetBranchAddress("TrigMatchHLT_mu50", &TrigMatchHLT_mu50, &b_TrigMatchHLT_mu50);
        fChain->SetBranchAddress("TrigMatchHLT_tau125_medium1_tracktwo", &TrigMatchHLT_tau125_medium1_tracktwo,
                                 &b_TrigMatchHLT_tau125_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_tau160_medium1_tracktwo", &TrigMatchHLT_tau160_medium1_tracktwo,
                                 &b_TrigMatchHLT_tau160_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_tau160_medium1_tracktwoEF", &TrigMatchHLT_tau160_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_tau160_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100", &TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100,
                                 &b_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100);
        fChain->SetBranchAddress("TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100", &TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100,
                                 &b_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100);
        fChain->SetBranchAddress("TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100", &TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100,
                                 &b_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100);
        fChain->SetBranchAddress("TrigMatchHLT_tau25_medium1_tracktwo", &TrigMatchHLT_tau25_medium1_tracktwo, &b_TrigMatchHLT_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_tau25_medium1_tracktwoEF", &TrigMatchHLT_tau25_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_tau25_mediumRNN_tracktwoMVA", &TrigMatchHLT_tau25_mediumRNN_tracktwoMVA,
                                 &b_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo", &TrigMatchHLT_tau35_medium1_tracktwo, &b_TrigMatchHLT_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwoEF", &TrigMatchHLT_tau35_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM", &TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo",
                                 &TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
                                 &TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
                                 &TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
                                 &b_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_mediumRNN_tracktwoMVA", &TrigMatchHLT_tau35_mediumRNN_tracktwoMVA,
                                 &b_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM", &TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM,
                                 &b_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM);
        fChain->SetBranchAddress("TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12", &TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12,
                                 &b_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12);
        fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwo", &TrigMatchHLT_tau60_medium1_tracktwo, &b_TrigMatchHLT_tau60_medium1_tracktwo);
        fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwoEF", &TrigMatchHLT_tau60_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_tau60_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50",
                                 &TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50,
                                 &b_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
                                 &TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
                                 &b_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("TrigMatchHLT_tau60_mediumRNN_tracktwoMVA", &TrigMatchHLT_tau60_mediumRNN_tracktwoMVA,
                                 &b_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwoEF", &TrigMatchHLT_tau80_medium1_tracktwoEF,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwoEF);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60", &TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",
                                 &TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60", &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
                                 &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",
                                 &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",
                                 &TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40,
                                 &b_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60", &TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60,
                                 &b_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60);
        fChain->SetBranchAddress("TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",
                                 &TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40,
                                 &b_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
        fChain->SetBranchAddress("TrigMatching", &TrigMatching, &b_TrigMatching);
        fChain->SetBranchAddress("TruthMET_met", &TruthMET_met, &b_TruthMET_met);
        fChain->SetBranchAddress("TruthMET_phi", &TruthMET_phi, &b_TruthMET_phi);
        fChain->SetBranchAddress("TruthMET_sumet", &TruthMET_sumet, &b_TruthMET_sumet);
        fChain->SetBranchAddress("VecSumPt_LepTau", &VecSumPt_LepTau, &b_VecSumPt_LepTau);
        fChain->SetBranchAddress("Vtx_n", &Vtx_n, &b_Vtx_n);
        fChain->SetBranchAddress("actualInteractionsPerCrossing", &actualInteractionsPerCrossing, &b_actualInteractionsPerCrossing);
        fChain->SetBranchAddress("averageInteractionsPerCrossing", &averageInteractionsPerCrossing, &b_averageInteractionsPerCrossing);
        fChain->SetBranchAddress("backgroundFlags", &backgroundFlags, &b_backgroundFlags);
        fChain->SetBranchAddress("bcid", &bcid, &b_bcid);
        fChain->SetBranchAddress("coreFlags", &coreFlags, &b_coreFlags);
        fChain->SetBranchAddress("corr_avgIntPerX", &corr_avgIntPerX, &b_corr_avgIntPerX);
        fChain->SetBranchAddress("dilepton_charge", &dilepton_charge, &b_dilepton_charge);
        fChain->SetBranchAddress("dilepton_eta", &dilepton_eta, &b_dilepton_eta);
        fChain->SetBranchAddress("dilepton_m", &dilepton_m, &b_dilepton_m);
        fChain->SetBranchAddress("dilepton_pdgId", &dilepton_pdgId, &b_dilepton_pdgId);
        fChain->SetBranchAddress("dilepton_phi", &dilepton_phi, &b_dilepton_phi);
        fChain->SetBranchAddress("dilepton_pt", &dilepton_pt, &b_dilepton_pt);
        fChain->SetBranchAddress("electrons_IFFClassType", &electrons_IFFClassType, &b_electrons_IFFClassType);
        fChain->SetBranchAddress("electrons_MT", &electrons_MT, &b_electrons_MT);
        fChain->SetBranchAddress("electrons_charge", &electrons_charge, &b_electrons_charge);
        fChain->SetBranchAddress("electrons_d0sig", &electrons_d0sig, &b_electrons_d0sig);
        fChain->SetBranchAddress("electrons_e", &electrons_e, &b_electrons_e);
        fChain->SetBranchAddress("electrons_eta", &electrons_eta, &b_electrons_eta);
        fChain->SetBranchAddress("electrons_isol", &electrons_isol, &b_electrons_isol);
        fChain->SetBranchAddress("electrons_phi", &electrons_phi, &b_electrons_phi);
        fChain->SetBranchAddress("electrons_pt", &electrons_pt, &b_electrons_pt);
        fChain->SetBranchAddress("electrons_signal", &electrons_signal, &b_electrons_signal);
        fChain->SetBranchAddress("electrons_truthOrigin", &electrons_truthOrigin, &b_electrons_truthOrigin);
        fChain->SetBranchAddress("electrons_truthType", &electrons_truthType, &b_electrons_truthType);
        fChain->SetBranchAddress("electrons_z0sinTheta", &electrons_z0sinTheta, &b_electrons_z0sinTheta);
        fChain->SetBranchAddress("emt_MT2_max", &emt_MT2_max, &b_emt_MT2_max);
        fChain->SetBranchAddress("emt_MT2_min", &emt_MT2_min, &b_emt_MT2_min);
        fChain->SetBranchAddress("eventNumber", &eventNumber, &b_eventNumber);
        fChain->SetBranchAddress("forwardDetFlags", &forwardDetFlags, &b_forwardDetFlags);
        fChain->SetBranchAddress("jets_BTagScore", &jets_BTagScore, &b_jets_BTagScore);
        fChain->SetBranchAddress("jets_Jvt", &jets_Jvt, &b_jets_Jvt);
        fChain->SetBranchAddress("jets_NTrks", &jets_NTrks, &b_jets_NTrks);
        fChain->SetBranchAddress("jets_bjet", &jets_bjet, &b_jets_bjet);
        fChain->SetBranchAddress("jets_eta", &jets_eta, &b_jets_eta);
        fChain->SetBranchAddress("jets_isBadTight", &jets_isBadTight, &b_jets_isBadTight);
        fChain->SetBranchAddress("jets_m", &jets_m, &b_jets_m);
        fChain->SetBranchAddress("jets_phi", &jets_phi, &b_jets_phi);
        fChain->SetBranchAddress("jets_pt", &jets_pt, &b_jets_pt);
        fChain->SetBranchAddress("jets_signal", &jets_signal, &b_jets_signal);
        fChain->SetBranchAddress("larFlags", &larFlags, &b_larFlags);
        fChain->SetBranchAddress("lumiBlock", &lumiBlock, &b_lumiBlock);
        fChain->SetBranchAddress("lumiFlags", &lumiFlags, &b_lumiFlags);
        fChain->SetBranchAddress("muWeight", &muWeight, &b_muWeight);
        fChain->SetBranchAddress("mu_density", &mu_density, &b_mu_density);
        fChain->SetBranchAddress("muonFlags", &muonFlags, &b_muonFlags);
        fChain->SetBranchAddress("muons_IFFClassType", &muons_IFFClassType, &b_muons_IFFClassType);
        fChain->SetBranchAddress("muons_MT", &muons_MT, &b_muons_MT);
        fChain->SetBranchAddress("muons_charge", &muons_charge, &b_muons_charge);
        fChain->SetBranchAddress("muons_d0sig", &muons_d0sig, &b_muons_d0sig);
        fChain->SetBranchAddress("muons_e", &muons_e, &b_muons_e);
        fChain->SetBranchAddress("muons_eta", &muons_eta, &b_muons_eta);
        fChain->SetBranchAddress("muons_isol", &muons_isol, &b_muons_isol);
        fChain->SetBranchAddress("muons_phi", &muons_phi, &b_muons_phi);
        fChain->SetBranchAddress("muons_pt", &muons_pt, &b_muons_pt);
        fChain->SetBranchAddress("muons_signal", &muons_signal, &b_muons_signal);
        fChain->SetBranchAddress("muons_truthOrigin", &muons_truthOrigin, &b_muons_truthOrigin);
        fChain->SetBranchAddress("muons_truthType", &muons_truthType, &b_muons_truthType);
        fChain->SetBranchAddress("muons_z0sinTheta", &muons_z0sinTheta, &b_muons_z0sinTheta);
        fChain->SetBranchAddress("n_BJets", &n_BJets, &b_n_BJets);
        fChain->SetBranchAddress("n_BaseElec", &n_BaseElec, &b_n_BaseElec);
        fChain->SetBranchAddress("n_BaseJets", &n_BaseJets, &b_n_BaseJets);
        fChain->SetBranchAddress("n_BaseMuon", &n_BaseMuon, &b_n_BaseMuon);
        fChain->SetBranchAddress("n_BaseTau", &n_BaseTau, &b_n_BaseTau);
        fChain->SetBranchAddress("n_SignalElec", &n_SignalElec, &b_n_SignalElec);
        fChain->SetBranchAddress("n_SignalJets", &n_SignalJets, &b_n_SignalJets);
        fChain->SetBranchAddress("n_SignalMuon", &n_SignalMuon, &b_n_SignalMuon);
        fChain->SetBranchAddress("n_SignalTau", &n_SignalTau, &b_n_SignalTau);
        fChain->SetBranchAddress("n_TruthJets", &n_TruthJets, &b_n_TruthJets);
        fChain->SetBranchAddress("pixelFlags", &pixelFlags, &b_pixelFlags);
        fChain->SetBranchAddress("runNumber", &runNumber, &b_runNumber);
        fChain->SetBranchAddress("sctFlags", &sctFlags, &b_sctFlags);
        fChain->SetBranchAddress("taus_BDTEleScore", &taus_BDTEleScore, &b_taus_BDTEleScore);
        fChain->SetBranchAddress("taus_BDTEleScoreSigTrans", &taus_BDTEleScoreSigTrans, &b_taus_BDTEleScoreSigTrans);
        fChain->SetBranchAddress("taus_ConeTruthLabelID", &taus_ConeTruthLabelID, &b_taus_ConeTruthLabelID);
        fChain->SetBranchAddress("taus_MT", &taus_MT, &b_taus_MT);
        fChain->SetBranchAddress("taus_NTrks", &taus_NTrks, &b_taus_NTrks);
        fChain->SetBranchAddress("taus_NTrksJet", &taus_NTrksJet, &b_taus_NTrksJet);
        fChain->SetBranchAddress("taus_PartonTruthLabelID", &taus_PartonTruthLabelID, &b_taus_PartonTruthLabelID);
        fChain->SetBranchAddress("taus_Quality", &taus_Quality, &b_taus_Quality);
        fChain->SetBranchAddress("taus_RNNJetScore", &taus_RNNJetScore, &b_taus_RNNJetScore);
        fChain->SetBranchAddress("taus_RNNJetScoreSigTrans", &taus_RNNJetScoreSigTrans, &b_taus_RNNJetScoreSigTrans);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau125_medium1_tracktwo", &taus_TrigMatchHLT_tau125_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_tau125_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_medium1_tracktwo", &taus_TrigMatchHLT_tau160_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_tau160_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_medium1_tracktwoEF", &taus_TrigMatchHLT_tau160_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_tau160_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100", &taus_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100,
                                 &b_taus_TrigMatchHLT_tau160_medium1_tracktwoEF_L1TAU100);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100", &taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100,
                                 &b_taus_TrigMatchHLT_tau160_medium1_tracktwo_L1TAU100);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100", &taus_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100,
                                 &b_taus_TrigMatchHLT_tau160_mediumRNN_tracktwoMVA_L1TAU100);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau25_medium1_tracktwo", &taus_TrigMatchHLT_tau25_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau25_medium1_tracktwoEF", &taus_TrigMatchHLT_tau25_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_tau25_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA", &taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA,
                                 &b_taus_TrigMatchHLT_tau25_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo", &taus_TrigMatchHLT_tau35_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwoEF", &taus_TrigMatchHLT_tau35_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM", &taus_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwoEF_L1TAU12IM);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo",
                                 &taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM",
                                 &taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_L1TAU20IM_2TAU12IM);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
                                 &taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
                                 &b_taus_TrigMatchHLT_tau35_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA", &taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA,
                                 &b_taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM", &taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM,
                                 &b_taus_TrigMatchHLT_tau35_mediumRNN_tracktwoMVA_L1TAU12IM);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12", &taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12,
                                 &b_taus_TrigMatchHLT_tau50_medium1_tracktwo_L1TAU12);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwo", &taus_TrigMatchHLT_tau60_medium1_tracktwo,
                                 &b_taus_TrigMatchHLT_tau60_medium1_tracktwo);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwoEF", &taus_TrigMatchHLT_tau60_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50",
                                 &taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50,
                                 &b_taus_TrigMatchHLT_tau60_medium1_tracktwoEF_tau25_medium1_tracktwoEF_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50",
                                 &taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50,
                                 &b_taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA", &taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA,
                                 &b_taus_TrigMatchHLT_tau60_mediumRNN_tracktwoMVA);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwoEF", &taus_TrigMatchHLT_tau80_medium1_tracktwoEF,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60", &taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40",
                                 &taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60", &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR-TAU20ITAU12I",
                                 &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau35_medium1_tracktwo_L1TAU12IM_L1TAU60_DR_TAU20ITAU12I);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12",
                                 &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40",
                                 &taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40,
                                 &b_taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau60_medium1_tracktwo_L1TAU40);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60", &taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60,
                                 &b_taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60);
        fChain->SetBranchAddress("taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40",
                                 &taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40,
                                 &b_taus_TrigMatchHLT_tau80_mediumRNN_tracktwoMVA_L1TAU60_tau60_mediumRNN_tracktwoMVA_L1TAU40);
        fChain->SetBranchAddress("taus_TrkJetWidth", &taus_TrkJetWidth, &b_taus_TrkJetWidth);
        fChain->SetBranchAddress("taus_Width", &taus_Width, &b_taus_Width);
        fChain->SetBranchAddress("taus_charge", &taus_charge, &b_taus_charge);
        fChain->SetBranchAddress("taus_d0", &taus_d0, &b_taus_d0);
        fChain->SetBranchAddress("taus_d0sig", &taus_d0sig, &b_taus_d0sig);
        fChain->SetBranchAddress("taus_e", &taus_e, &b_taus_e);
        fChain->SetBranchAddress("taus_eta", &taus_eta, &b_taus_eta);
        fChain->SetBranchAddress("taus_phi", &taus_phi, &b_taus_phi);
        fChain->SetBranchAddress("taus_pt", &taus_pt, &b_taus_pt);
        fChain->SetBranchAddress("taus_signalID", &taus_signalID, &b_taus_signalID);
        fChain->SetBranchAddress("taus_trks_d0", &taus_trks_d0, &b_taus_trks_d0);
        fChain->SetBranchAddress("taus_trks_d0sig", &taus_trks_d0sig, &b_taus_trks_d0sig);
        fChain->SetBranchAddress("taus_trks_z0sinTheta", &taus_trks_z0sinTheta, &b_taus_trks_z0sinTheta);
        fChain->SetBranchAddress("taus_truthOrigin", &taus_truthOrigin, &b_taus_truthOrigin);
        fChain->SetBranchAddress("taus_truthType", &taus_truthType, &b_taus_truthType);
        fChain->SetBranchAddress("taus_z0sinTheta", &taus_z0sinTheta, &b_taus_z0sinTheta);
        fChain->SetBranchAddress("tileFlags", &tileFlags, &b_tileFlags);
        fChain->SetBranchAddress("trtFlags", &trtFlags, &b_trtFlags);

        // auto newTree = fChain->CloneTree(0);
        fChain->SetBranchStatus("GenWeight_LHE_pdfset_eq_2*", 0);
        fChain->SetBranchStatus("GenWeight_LHE_PDF_set__eq__*", 0);
        fChain->SetBranchStatus("GenWeight_LHE_MUR*_MUF*_PDF2*", 0);
        fChain->SetBranchStatus("GenWeight_LHE_muR__eq*", 0);
        fChain->SetBranchStatus("GenWeight_LHE_2muR_*", 0);
        fChain->SetBranchStatus("GenWeight_LHE_2muF_*", 0);
        fChain->SetBranchStatus("GenWeight_LHE_0p5muR_*", 0);
        fChain->SetBranchStatus("GenWeight_LHE_0p5muF_*", 0);
        fChain->SetBranchStatus("GenWeight_LHE_muR_eq*", 0);
        fChain->SetBranchStatus("GenWeight_LHE_muF_eq*", 0);
        fChain->SetBranchStatus("GenWeight_LHE_nnlops-*", 0);
        fChain->SetBranchStatus("GenWeight_LHE_mt*", 0);
    }
};
