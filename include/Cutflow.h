#pragma once
#include <TFile.h>
#include <TH1.h>
#include <TTree.h>
#include <math.h>

#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <iomanip>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "AnaOutputHandler.h"
#include "Hist.h"
#include "Utils.h"

// Will store all of the information we needed for a single cut
class CutIterator {
public:
    double sum;
    double err;
    std::string name;
    double rate;
    bool passed;
    std::function<bool()> cut;
    bool willFillHist;

    // All single cut contains N-1 plot for this cut, plots *before* this cut and other plots *after* this cut.
    Hist* hist_N_1;
    Hist* hist_beforeCut;
    TDirectory* histDir;
    std::vector<Hist*> hists;
    std::vector<std::function<double()>> histExtraWei;

    CutIterator(std::function<bool()> cut, bool histOpt = false) {
        sum = 0;
        err = 0;
        rate = 100;
        passed = true;
        this->cut = cut;
        hist_N_1 = nullptr;
        hist_beforeCut = nullptr;
        histDir = 0;
        this->willFillHist = histOpt;
    };

    // Optional: Set Hist directory
    void setDirectory(TDirectory* dir) { this->histDir = dir; }
    // Set N_1 plot for this cut
    void set_N_1_Hist(std::string histname, int nBin, double binStart, double binEnd, std::function<double()> fillPosition,
                      bool useOverflow = false) {
        if (willFillHist) {
            hist_N_1 = new Hist(histname, nBin, binStart, binEnd, fillPosition, useOverflow);
            hist_N_1->SetDirectory(histDir);
        }
    }
    // Set before cut plot for this cut
    void set_Before_Hist(std::string histname, int nBin, double binStart, double binEnd, std::function<double()> fillPosition,
                         bool useOverflow = false) {
        if (willFillHist) {
            hist_beforeCut = new Hist(histname, nBin, binStart, binEnd, fillPosition, useOverflow);
            hist_beforeCut->SetDirectory(histDir);
        }
    }
    // Add other hists if you want to use
    void addHist(TH1* hist, std::function<double()> fillPosition, bool useOverflow = false, std::function<double()> sysWei = NULL) {
        if (willFillHist) {
            auto newhist = new Hist(hist, fillPosition, useOverflow);
            newhist->SetDirectory(histDir);
            hists.emplace_back(newhist);
            if (sysWei == NULL) {
                histExtraWei.emplace_back([&] { return 1; });
            } else {
                histExtraWei.emplace_back(sysWei);
            }
        }
    }

    void addHist(std::string histname, int nBin, double binStart, double binEnd, std::function<double()> fillPosition, bool useOverflow = false,
                 std::function<double()> sysWei = NULL) {
        if (willFillHist) {
            auto newhist = new Hist(histname, nBin, binStart, binEnd, fillPosition, useOverflow);
            newhist->SetDirectory(histDir);
            hists.emplace_back(newhist);
            if (sysWei == NULL) {
                histExtraWei.emplace_back([&] { return 1; });
            } else {
                histExtraWei.emplace_back(sysWei);
            }
        }
    }

    void addHist(std::string histname, int nBin, double binStart, double binEnd, std::function<std::vector<double>()> fillPosition,
                 bool useOverflow = false, std::function<double()> sysWei = NULL) {
        if (willFillHist) {
            auto newhist = new Hist(histname, nBin, binStart, binEnd, fillPosition, useOverflow);
            newhist->SetDirectory(histDir);
            hists.emplace_back(newhist);
            if (sysWei == NULL) {
                histExtraWei.emplace_back([&] { return 1; });
            } else {
                histExtraWei.emplace_back(sysWei);
            }
        }
    }
    // add a 2D hist fill with vector
    void addHist(std::string histname, int nBinX, double binStartX, double binEndX, int nBinY, double binStartY, double binEndY,
                 std::function<std::pair<std::vector<double>, std::vector<double>>()> fillPosition, bool useOverflow = false) {
        if (willFillHist) {
            auto newhist = new Hist(histname, nBinX, binStartX, binEndX, nBinY, binStartY, binEndY, fillPosition, useOverflow);
            newhist->SetDirectory(histDir);
            hists.emplace_back(newhist);
            histExtraWei.emplace_back([&] { return 1; });
        }
    }
    // add a 2D hist
    void addHist(std::string histname, int nBinX, double binStartX, double binEndX, int nBinY, double binStartY, double binEndY,
                 std::function<std::pair<double, double>()> fillPosition, bool useOverflow = false) {
        if (willFillHist) {
            auto newhist = new Hist(histname, nBinX, binStartX, binEndX, nBinY, binStartY, binEndY, fillPosition, useOverflow);
            newhist->SetDirectory(histDir);
            hists.emplace_back(newhist);
            histExtraWei.emplace_back([&] { return 1; });
        }
    }

    ~CutIterator() {
        SafeDelete(hist_N_1);
        SafeDelete(hist_beforeCut);
        for (auto&& hist : hists) {
            SafeDelete(hist);
        }
    };
};

class Cutflow {
public:
    static const bool SHOW_RATIO = true;
    static const bool NO_RATIO = false;

    static const bool SHOW_HIST = true;
    static const bool NO_HIST = false;

    static const int CSV = 0;
    static const int LATEX = 1;

    // If the program will deal with many files, you can set a name (like filename) to distinguish them
    Cutflow(std::shared_ptr<AnaOutputHandler> oFile, bool willFillHist = SHOW_HIST, std::string histDirName = "") {
        this->willFillHist = willFillHist;
        this->outTree = nullptr;
        this->willFillTree = false;

        // check if str end with ".root"
        std::string name = oFile->getName();
        std::string s = ".root";
        std::string::size_type idx = name.rfind(s);
        if (idx != std::string::npos && name.size() == idx + s.size()) { // if yes
            Name = name.erase(idx, name.size());
        } else { // if not
            Name = name;
        }
        histFolder = (histDirName == "") ? oFile->rootDir() : oFile->Dir(histDirName);
    }
    ~Cutflow() {}

    void checkCutName(std::string cutname);
    // Here we return Object CutIterator so we can further do something for this cut step. Such as adding more histogram
    std::shared_ptr<CutIterator> registerCut(std::string cutname, std::function<bool()> cut);
    std::shared_ptr<CutIterator> registerCut(std::string cutname, std::function<bool()> cut, std::string histname, int nBin, double binStart,
                                             double binEnd, std::function<double()> fillPosition, bool useOverflow = false);

    void setWeight(std::function<double()> weight) { this->weight = weight; }
    void setName(std::string name) { this->Name = name; }
    void setFillTree(TTree* tree) {
        this->outTree = tree;
        this->willFillTree = true;
    }

    void startCut();
    void PrintOut(bool CoutRatio = NO_RATIO, int OutputStyle = CSV);
    void PrintOut(std::string name, bool CoutRatio = NO_RATIO, int OutputStyle = CSV);

private:
    void PrintOutCsv(std::string name, bool CoutRatio = NO_RATIO);
    void PrintOutLatex(std::string name, bool CoutRatio = NO_RATIO);

    std::vector<std::shared_ptr<CutIterator>> CutQueue;
    std::string Name;
    std::function<double()> weight;
    std::function<bool()> finalCut;
    TTree* outTree;
    TDirectory* histFolder;
    bool willFillHist;
    bool willFillTree;
    std::mutex _lock; // for asyn
};
