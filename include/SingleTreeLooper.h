#pragma once
#include <TChain.h>

#include <algorithm>
#include <functional>
#include <memory>
#include <string>
#include <vector>

#include "AnaConfigReader.h"
#include "AnaOutputHandler.h"
#include "AnaTree.h"
#include "Cutflow.h"
#include "GRLDB.h"
#include "MetaDB.h"
#include "Region.h"
#include "Runnable.h"
#include "Timer.h"
#include "Utils.h"

class SingleTreeLooper : public Runnable {
public:
    using Cutflow_ptr = std::shared_ptr<Cutflow>;
    using Region_ptr = std::shared_ptr<Region>;
    using GRLDB_ptr = std::shared_ptr<GRLDB>;
    using json = nlohmann::json;

    TChain* anaChain;
    std::map<std::string, double> Var;

    SingleTreeLooper() {
        flag_weightOverRidden = false;
        mGRLDB = nullptr;
    };

    SingleTreeLooper(std::string chainName, std::shared_ptr<AnaOutputHandler> output) {
        flag_weightOverRidden = false;
        mGRLDB = nullptr;
        setAnaChain(chainName);
        this->output = output;
    };

    inline std::string getOutName() { return output->getName(); }
    void setAnaChain(std::string chainName) {
        // lock area
        std::lock_guard<std::mutex> lock{_lock};
        this->_currentTreeName = chainName;
        this->anaChain = new TChain(chainName.c_str());
        for (const auto& input : AnaConfigReader::Instance().getInputs()) {
            anaChain->Add(input.c_str());
        }
    }
    inline void setWeight(std::function<double()> weight) {
        if (!flag_weightOverRidden) {
            this->weight = weight;
        }
    }
    inline TTree* newTree(std::string name) {
        this->allocTrees.emplace_back(name);
        return output->newTree(name);
    }
    Cutflow_ptr addCutflow(bool willFillHist = Cutflow::SHOW_HIST, std::string histDirName = "") {
        Cutflow_ptr cutflow(new Cutflow(output, willFillHist, histDirName));
        cutflow->setWeight(this->weight);
        cutflows.emplace_back(cutflow);
        return cutflow;
    }
    Cutflow_ptr addCutflow(const char* histDirName_c_str) {
        std::string histDirName(histDirName_c_str);
        Cutflow_ptr cutflow(new Cutflow(output, Cutflow::SHOW_HIST, histDirName));
        cutflow->setWeight(this->weight);
        cutflows.emplace_back(cutflow);
        return cutflow;
    }
    Region_ptr addRegion(std::function<bool()> cut, std::string histDirName = "") {
        Region_ptr region(new Region(cut, output, histDirName));
        region->setWeight(this->weight);
        regions.emplace_back(region);
        return region;
    }
    Region_ptr addRegion(std::string regionName, std::function<bool()> cut, std::string histDirName = "") {
        Region_ptr region(new Region(regionName, cut, output, histDirName));
        region->setWeight(this->weight);
        regions.emplace_back(region);
        return region;
    }
    Region_ptr addRegion(int combineMethod, Region_ptr oldRegion, std::function<bool()> cut, std::string histDirName = "") {
        Region_ptr region = oldRegion->combine(combineMethod, cut, histDirName);
        regions.emplace_back(region);
        return region;
    }

    void fillRegions() {
        for (auto&& region : regions) {
            region->fill();
        }
    }
    void fillCutflows() {
        for (auto&& cutflow : cutflows) {
            cutflow->startCut();
        }
    }
    TTree* cloneCurrentAnaTree(std::string newname = "", Long64_t nentries = 0, Option_t* option = "") {
        std::string tName = (newname == "") ? _currentTreeName : newname;
        allocTrees.emplace_back(tName);
        return output->newTree(this->anaChain, newname, nentries, option);
    }
    inline std::string currentTreeName() { return _currentTreeName; }
    void run() override {
        LOG(INFO) << "**********Start cutting tree " << this->_currentTreeName << "*********";
        // check if the weight is overridden and set the weight if so
        this->weightOverRidden = AnaConfigReader::Instance().getOverriddenWeight();
        if (this->weightOverRidden != 0) { // 0 is the default value which won't set exception wei
            this->weight = [&] { return weightOverRidden; };
            flag_weightOverRidden = true;
        }
        // get the GRL DB
        std::string GRLFile = AnaConfigReader::Instance().GRL();
        if (GRLFile != "") {
            mGRLDB = GRLDB_ptr(new GRLDB());
            mGRLDB->loadGRLFile(GRLFile);
        }
        loop(); // Todo list: Try to split it to two func and do what simpleAnalysis do
        LOG(INFO) << "**********End of cutting " << this->_currentTreeName << ", cleaning all objs********";
    }

    std::vector<Region_ptr> getRegions() { return this->regions; }

    std::vector<Cutflow_ptr> getCutflows() { return this->cutflows; }

    virtual void loop() = 0;

    virtual ~SingleTreeLooper() {
        std::lock_guard<std::mutex> lock{_lock};
        // delete chain
        SafeDelete(anaChain);
        // write and delete alloc trees
        output->Write();
        for (auto tree : allocTrees) {
            output->deleteTree(tree);
        }
    }

private:
    std::mutex _lock; // for asyn

    std::function<double()> weight;
    std::vector<Region_ptr> regions;
    std::vector<Cutflow_ptr> cutflows;
    std::vector<std::string> allocTrees;

    double weightOverRidden;
    std::shared_ptr<AnaOutputHandler> output;
    std::string _currentTreeName;
    GRLDB_ptr mGRLDB;
    bool flag_weightOverRidden;
};
