#pragma once

//#include <RooStats/RooStatsUtils.h>
#include <TLorentzVector.h>

#include "AnaObjs.h"
#include "mt2_bisect.h"

/*******************************************************************
***Some functions are steal and modify from SimpleAnalysis package**
*****https://gitlab.cern.ch/atlas-phys-susy-wg/SimpleAnalysis*******
********************************************************************/

class PhyUtils {
public:
    PhyUtils();
    /*
     *Notice: For functions like xxMin(), xxMax(), one should provide an AnaObjs collection with all objs you want to
     *cal in it. For example, if you want to calculate  MT2Max for tau container, you can use MT2Max(tauContainers,
     *met); If you want calculate MT2Max for tau and electron container, you can ues MT2Max(tauContainers +
     *eleContainers, met);
     */
    // DeltaR, DeltaPhi
    //	static inline double deltaR(double eta1, double phi1, double eta2, double phi2); -> disabled because it is
    // recommended to use rapidity not eta to calculate the deltaR
    static inline double deltaR(const TLorentzVector& v1, const TLorentzVector& v2);
    static inline double deltaPhi(double phi1, double phi2);
    static inline double deltaPhi(const TLorentzVector& o1, const TLorentzVector& o2);
    // overlapRemoval
    static AnaObjs overlapRemoval(const AnaObjs& cands, const AnaObjs& others, float deltaR, int passId = 0);
    static AnaObjs overlapRemoval(const AnaObjs& cands, const AnaObjs& others, std::function<float(const AnaObj&, const AnaObj&)> radiusFunc,
                                  int passId = 0);

    // MCT,MT,Mll
    static inline double calcMCT(double Et1, double pt1, double phi1, double Et2, double pt2, double phi2);
    static inline double calcMCT(const TLorentzVector& v1, const TLorentzVector& v2);
    static inline double calcMT(double pt1, double phi1, double pt2, double phi2);
    static inline double calcMT(const TLorentzVector& lepton, const TLorentzVector& met);
    static inline double calcMll(double pt1, double eta1, double phi1, double E1, double pt2, double eta2, double phi2, double E2);

    static inline double calcMll(TLorentzVector tlv1, TLorentzVector tlv2);

    // arxiv:1401.1235
    static std::vector<double> RecoMll(TLorentzVector tlv1, TLorentzVector tlv2, TLorentzVector met);
    static AnaObj RecoW(TLorentzVector lep, TLorentzVector neu, double wmass = 80.379);

    static double calcMTmin(const AnaObjs& cands, const AnaObj& met);

    // MT2
    static double MT2(double pt1, double eta1, double phi1, double E1, double pt2, double eta2, double phi2, double E2, double missET,
                      double missphi);
    static double MT2(double pt1, double eta1, double phi1, double E1, double pt2, double eta2, double phi2, double E2, double pxmiss, double pymiss,
                      double minv);
    static double MT2(const TLorentzVector& o1, const TLorentzVector& o2, const TLorentzVector& met, double minv = 0);
    static double MT2Max(const AnaObjs& cands, const TLorentzVector& met, double minv = 0);

    // Sphericity related
    static TVectorD calcEigenValues(const AnaObjs& objs);
    static float aplanarity(const AnaObjs& objs);
    static float sphericity(const AnaObjs& objs);
    static float transSphericity(const AnaObjs& objs);

    // Deconstructed Transverse Mass
    // arXiv:1409.2868v1
    static double DeMTCos(double mT0, const TLorentzVector& lepton, const TLorentzVector& met);
    static double DeMTQ(double mT0, const TLorentzVector& lepton, const TLorentzVector& met);

    // Top Tagger related
    static bool toptag0j(TLorentzVector v1l, TLorentzVector v2l, Double_t met, Double_t evt_MET_phi);
    static bool toptag2j(double ptjcut, double meffcut, TLorentzVector v1l, TLorentzVector v2l, TLorentzVector v1j, TLorentzVector v2j, Double_t met,
                         int iopt, Double_t evt_MET_phi);

    static int GetTopPairs(double ptjcut, double meffcut, int njetscan, TLorentzVector v1l, TLorentzVector v2l, std::vector<TLorentzVector> jets,
                           double_t met, double_t evt_met_phi);
    static int GetTopPairs(double ptjcut, double meffcut, int njetscan, TLorentzVector v1l, TLorentzVector v2l, std::vector<AnaObj> jets,
                           double_t met, double_t evt_met_phi);

    // Find two VBF jets by eta_jet1*eta_jet2 < 0 && delta_eta > x , return one index
    static std::vector<std::pair<int, int> > GetVBFjetPair(const AnaObjs& jets, double dEta);

    // Calculate Zn
    static double calZn(double signalExp, double backgroundExp, double relativeBkgUncert);
    // Get run2 MC compaign
    static inline int getCompaign(unsigned int runNumber);

    // Other useful utils
    static inline double calcPoissonCLLower(double q, double obs);
    static inline double calcPoissonCLUpper(double q, double obs);

    ~PhyUtils();

private:
};

/****************************** Inline functions *******************/

/********************************************************
 ******************DeltaR and DeltaPhi*******************
 ********************************************************/
double PhyUtils::deltaPhi(double phi1, double phi2) {
    double diffphi = fabs(phi1 - phi2);
    return (diffphi > M_PI) ? (2 * M_PI - diffphi) : diffphi;
}

double PhyUtils::deltaPhi(const TLorentzVector& v1, const TLorentzVector& v2) {
    double diffphi = fabs(v1.Phi() - v2.Phi());
    return (diffphi > M_PI) ? (2 * M_PI - diffphi) : diffphi;
}
/*
double PhyUtils::deltaR(double eta1, double phi1, double eta2, double phi2){
    double diffphi = fabs(phi1 - phi2);
    while (diffphi > M_PI) diffphi = 2*M_PI - diffphi;
    return sqrt(pow((eta1 - eta2) ,2) + pow(diffphi ,2));
}
*/
double PhyUtils::deltaR(const TLorentzVector& v1, const TLorentzVector& v2) {
    double diffphi = fabs(v1.Phi() - v2.Phi());
    while (diffphi > M_PI) diffphi = fabs(2 * M_PI - diffphi);
    return sqrt(pow((v1.Rapidity() - v2.Rapidity()), 2) + pow(diffphi, 2));
    ;
}

/********************************************************
 ************************MCT, MT, Mll********************
 ********************************************************/
double PhyUtils::calcMCT(double Et1, double pt1, double phi1, double Et2, double pt2, double phi2) {
    double px1 = pt1 * cos(phi1);
    double py1 = pt1 * sin(phi1);
    double px2 = pt2 * cos(phi2);
    double py2 = pt2 * sin(phi2);
    double mCT = pow(Et1 + Et2, 2) - pow(px1 - px2, 2) - pow(py1 - py2, 2);
    return (mCT >= 0.) ? sqrt(mCT) : sqrt(-mCT);
}

double PhyUtils::calcMCT(const TLorentzVector& v1, const TLorentzVector& v2) {
    double mCT = pow(v1.Et() + v2.Et(), 2) - pow(v1.Px() - v2.Px(), 2) - pow(v1.Py() - v2.Py(), 2);
    return (mCT >= 0.) ? sqrt(mCT) : sqrt(-mCT);
}

double PhyUtils::calcMT(double pt1, double phi1, double pt2, double phi2) {
    double mT = 2 * pt1 * pt2 * (1 - TMath::Cos(phi1 - phi2));
    return (mT >= 0.) ? sqrt(mT) : sqrt(-mT);
}

double PhyUtils::calcMT(const TLorentzVector& lepton, const TLorentzVector& met) {
    double mT = 2 * lepton.Et() * met.Et() - 2 * lepton.Pt() * met.Pt() * TMath::Cos(lepton.Phi() - met.Phi());
    return (mT >= 0.) ? sqrt(mT) : sqrt(-mT);
}

double PhyUtils::calcMll(double pt1, double eta1, double phi1, double E1, double pt2, double eta2, double phi2, double E2) {
    TLorentzVector tlv1, tlv2;
    tlv1.SetPtEtaPhiE(pt1, eta1, phi1, E1);
    tlv2.SetPtEtaPhiE(pt2, eta2, phi2, E2);
    return (tlv1 + tlv2).M();
}

double PhyUtils::calcMll(TLorentzVector tlv1, TLorentzVector tlv2) { return (tlv1 + tlv2).M(); }

int PhyUtils::getCompaign(unsigned int runNumber) {
    if (runNumber > 341649)
        return 18;
    else if (runNumber > 320000)
        return 17;
    else
        return 16;
}

double PhyUtils::calcPoissonCLLower(double q, double obs) {
    if (obs > 0) {
        double a = (1. - q) / 2.;
        return TMath::ChisquareQuantile(a, 2. * obs) / 2.;
    }
    return 0;
}

double PhyUtils::calcPoissonCLUpper(double q, double obs) {
    if (obs > 0) {
        double a = 1 - (1. - q) / 2.;
        return TMath::ChisquareQuantile(a, 2. * (obs + 1.)) / 2.;
    }
    return 0;
}
