#pragma once

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>

// using namespace std;

// Header file for the classes stored in the TTree if any.
#include <vector>

class AnaTree {
public:
    TChain* fChain;
    AnaTree(TChain* chain) { this->fChain = chain; }
    Long64_t GetEntries() {
        std::unique_lock<std::mutex> lock{_mutex};
        Long64_t num = fChain->GetEntries();
        return num;
    }
    Int_t GetEntry(Long64_t i) { return fChain->GetEntry(i); }
    std::string GetName() { return (std::string)fChain->GetName(); }
    virtual void SetBranchStatus(const char* bname, Bool_t stat = 1, UInt_t* found = 0) { this->fChain->SetBranchStatus(bname, stat, found); }
    virtual void Init(TChain* chain) = 0;
    virtual ~AnaTree() {}
    std::mutex _mutex; // lock for pool state switching
};
