#pragma once

#include <TFile.h>
#include <TH1.h>
#include <TTree.h>

#include <fstream>
#include <functional>
#include <iostream>
#include <map>
#include <mutex>
#include <string>
#include <unordered_map>
#include <vector>

class AnaOutputHandler {
public:
    AnaOutputHandler(std::string name, bool doNtuple = false) : _doNtuple(doNtuple) {
        this->outName = name;
        _oFile = new TFile(name.c_str(), "RECREATE");
    };
    inline std::string getName() { return this->outName; }
    inline TDirectory* rootDir() { return this->_oFile; }
    // Function to fetch the created dir using its name. If the dir hasn't being created, a new empty dir with the query
    // name will be created
    TDirectory* Dir(std::string name);
    // Create a new empty tree with the name
    TTree* newTree(std::string name);
    /* Use a exist tree to clone tree and add to the output. Could only be used to create new tree
     * Cnnnot be used to refer the exist tree!
     * Could use the newname option to rename the created tree.
     * In the nentries option, -1 means copy entries, 0(default) means don't copy entries
     * 'option' option is the extra option used for clone tree*/
    TTree* newTree(TTree* tree, std::string newname = "", Long64_t nentries = 0, Option_t* option = "");
    void deleteTree(std::string name);
    // Frtch the tree using its name
    TTree* getTree(std::string name);
    void Write();
    virtual ~AnaOutputHandler();

private:
    TFile* _oFile;
    std::map<std::string, TDirectory*> _oDirs;
    std::map<std::string, TTree*> _oTrees;
    bool _doNtuple;
    std::string outName;
    std::mutex _lock; // for asyn

    static std::vector<std::string> variationNames;
    static std::vector<float> variationValues;
    static std::function<std::string(const std::vector<double>&)> _outFunc;
};
