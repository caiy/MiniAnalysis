#include "AnaObjs.h"
#include "GRLDB.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

/* Slimmer for W-CR and W-VR QCD contribution*/

//#include "XamppTree.h"
DefineLooper(C1N2OS_W_QCD_Slimmer, XamppTree);

void C1N2OS_W_QCD_Slimmer::loop() {
    std::string outFullName = getOutName();
    std::string treeName = currentTreeName();
    // std::string outTree = Utils::splitStrBy(outFullName,'.')[0] + "_Nominal";
    // std::string outTree = treeName.replace(treeName.find("Staus"), 5, Utils::splitStrBy(outFullName,'.')[0]);
    LOG(INFO) << "currentTree name: " << treeName;

    // auto oTree = cloneCurrentAnaTree(outTree);
    auto oTree = cloneCurrentAnaTree();

    auto mCutflow = addCutflow();
    //	Cutflow* mCutflow = nullptr;
    //	if(treeName == "Staus_Nominal"){
    //		mCutflow = addCutflow();
    //	}else{
    //		mCutflow = addCutflow(Cutflow::NO_HIST);
    //	}
    // define cuts
    Var["tau1Pt"] = 0;
    Var["tau1Mt"] = 0;
    Var["mu1Pt"] = 0;
    Var["mu1Mt"] = 0;
    Var["dPhitt"] = 0;
    Var["dRtm"] = 0;
    Var["mt12tm"] = 0;
    Var["meff_tau"] = 0;
    Var["Mtm_12"] = 0;
    Var["MT2"] = 0;
    Var["met_sig"] = 0;
    Var["met_sig_tj"] = 0;
    Var["evt_MET"] = 0;

    Var["dPhijj"] = 0;
    Var["dRjj"] = 0;
    Var["dEtajj"] = 0;
    Var["EtajEtaj"] = 0;
    Var["jet1Pt"] = 0;
    Var["jet2Pt"] = 0;
    Var["jet1Eta"] = 0;
    Var["jet2Eta"] = 0;
    Var["jet1Mt"] = 0;
    Var["jet2Mt"] = 0;
    Var["fabs_jet1Eta"] = 0;
    Var["fabs_jet2Eta"] = 0;
    Var["bVeto"] = 0;
    Var["bNumber"] = 0;
    Var["totalWeight"] = 1;

    Var["n_signal_jets"] = 0;

    int nTightTau(0), nMuons(0), nTaus(0), Ljet_n(0), mergedRunNumber(0), mlumiBlock(0), nJet(0), Nemu(0), topTagger(0);
    bool muTriger, OS2TM, zVeto, mllCut, mt2Cut, noemu;

    oTree->Branch("Weight_mc", &Var["totalWeight"]);
    oTree->Branch("mergedRunNumber", &mergedRunNumber);
    oTree->Branch("mergedlumiBlock", &mlumiBlock);
    //	oTree->Branch("tau1Pt", &Var["tau1Pt"]);
    //	oTree->Branch("tau2Pt", &Var["tau2Pt"]);
    //	oTree->Branch("tau1Mt", &Var["tau1Mt"]);
    //	oTree->Branch("tau2Mt", &Var["tau2Mt"]);
    //	oTree->Branch("dRtt", &Var["dRtt"]);
    //	oTree->Branch("dPhitt", &Var["dPhitt"]);
    //	oTree->Branch("mt12tau", &Var["mt12tau"]);
    //	oTree->Branch("Mll_tt", &Var["Mtt_12"]);
    //	oTree->Branch("meff_tt", &Var["meff_tau"]);
    //	oTree->Branch("MT2", &Var["MT2"]);
    //
    //	oTree->Branch("jet1Pt", &Var["jet1Pt"]);
    //	oTree->Branch("jet2Pt", &Var["jet2Pt"]);
    //	oTree->Branch("jet1Eta", &Var["jet1Eta"]);
    //	oTree->Branch("jet2Eta", &Var["jet2Eta"]);
    //	oTree->Branch("jet1Mt", &Var["jet1Mt"]);
    //	oTree->Branch("jet2Mt", &Var["jet2Mt"]);
    //	oTree->Branch("dRjj", &Var["dRjj"]);
    //	oTree->Branch("dPhijj", &Var["dPhijj"]);
    //	oTree->Branch("dEtajj", &Var["dEtajj"]);
    //	oTree->Branch("EtajEtaj", &Var["EtajEtaj"]);
    //	oTree->Branch("Absjet1Eta", &Var["fabs_jet1Eta"]);
    //	oTree->Branch("ABsjet2Eta", &Var["fabs_jet2Eta"]);
    //
    //	oTree->Branch("LJet_n", &Ljet_n);
    //	oTree->Branch("BJet_n", &Var["bNumber"]);
    //	oTree->Branch("Evt_MET", &Var["evt_MET"]);
    //	oTree->Branch("Evt_METSIG", &Var["met_sig"]);
    //	oTree->Branch("Evt_METSIG_WithTauJet", &Var["met_sig_tj"]);

    mCutflow->setWeight([&] { return Var["totalWeight"]; });
    mCutflow->setFillTree(oTree);
    //将m_Tree导入Cutflow里的outTree
    mCutflow->registerCut("baseline", [&] { return fabs(Var["totalWeight"]) < 100; });
    mCutflow->registerCut("singleMuonTrig", [&] { return muTriger; });

    mCutflow->registerCut("topTagger", [&] { return topTagger == 0; });
    mCutflow->registerCut("bVeto", [&] { return Var["bVeto"]; });

    mCutflow->registerCut("nStau ==1", [&] { return nTaus == 1; });
    mCutflow->registerCut("nSmuon ==1 ", [&] { return nMuons == 1; });
    mCutflow->registerCut("SStm", [&] { return !OS2TM; });

    mCutflow->registerCut("tau1Pt > 50", [&] { return Var["tau1Pt"] > 50; });
    mCutflow->registerCut("mu1Pt > 40", [&] { return Var["mu1Pt"] > 40; });

    mCutflow->registerCut("MET > 60", [&] { return Var["evt_MET"] > 60; });
    mCutflow->registerCut(" mu1Mt < 140", [&] { return Var["mu1Mt"] < 140; });

    // mCutflow->registerCut("M_tm > 80", [&] {return Var["Mtm_12"] > 80; } );

    // mCutflow->registerCut("dRtm < 2.5", [&] {return Var["dRtm"] < 2.5; } );

    mCutflow->registerCut("40 < MT2_tm", [&] { return 40 < Var["MT2"]; });
    // mCutflow->registerCut("MT2_tm < 70", [&] {return  MT2_tm < 70;},"MT2_70", 20, 0, 100, [&] {return MT2_tm; });

    auto lastCut = mCutflow->registerCut("the END", [&] { return true; });

    //	lastCut->addHist("jet2pt", 30, 0, 600, [&] {return Var["jet2Pt"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("absjet1eta", 20, 0, 4, [&] {return Var["fabs_jet1Eta"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("absjet2eta", 20, 0, 4, [&] {return Var["fabs_jet2Eta"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("DRjj", 28, 0, 7, [&] {return Var["dRjj"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("DPhijj", 17, 0, 3.4, [&] {return Var["dPhijj"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("DEtajj", 20, 0, 10, [&] {return Var["dEtajj"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("EtajEtaj", 40, -10, 10, [&] {return Var["EtajEtaj"]; }, Hist::USE_OVERFLOW);

    // Loop Start
    Long64_t nentries = tree->GetEntries();
    for (Long64_t jentry = 0; jentry < nentries; jentry++) {
        tree->GetEntry(jentry);
        if (0 == jentry % 100000) std::cout << jentry << " entry of " << nentries << "entries" << std::endl;

        Var["tau1Pt"] = 0;
        Var["tau1Mt"] = 0;
        Var["mu1Pt"] = 0;
        Var["mu1Mt"] = 0;
        Var["dPhitt"] = 999;
        Var["dRtm"] = 999;
        Var["mt12tm"] = -999;
        Var["meff_tau"] = -999;
        Var["Mtm_12"] = -999;
        Var["MT2"] = -999;
        Var["met_sig"] = -1;
        Var["met_sig_tj"] = -1;
        Var["evt_MET"] = -1;

        Var["dPhijj"] = 0;
        Var["dRjj"] = 0;
        Var["dEtajj"] = 0;
        Var["EtajEtaj"] = 0;
        Var["jet1Pt"] = 0;
        Var["jet2Pt"] = 0;
        Var["jet1Eta"] = 0;
        Var["jet2Eta"] = 0;
        Var["fabs_jet1Eta"] = 0;
        Var["fabs_jet2Eta"] = 0;
        Var["totalWeight"] = 1;
        Var["jet1Mt"] = 0;
        Var["jet2Mt"] = 0;
        nTightTau = 0;
        nMuons = 0;
        nTaus = 0;
        Ljet_n = 0;
        mergedRunNumber = 0;
        nJet = 0;
        muTriger = false;
        Var["bVeto"] = false;
        OS2TM = false, zVeto = false, noemu = true;
        Var["bNumber"] = 0;
        Var["evt_MET"] = tree->MetTST_met / 1000;
        topTagger = 1;

        if (tree->mcChannelNumber == 0) {
            Var["totalWeight"] = 1;
            mergedRunNumber = tree->runNumber;
            mlumiBlock = tree->lumiBlock;
        } else {
            mergedRunNumber = tree->RandomRunNumber;
            mlumiBlock = tree->RandomLumiBlockNumber;
        }

        if (tree->mcChannelNumber != 0) {
            double extraWei = tree->GenWeight * tree->muWeight * tree->TauWeight * tree->EleWeight * tree->MuoWeight * tree->JetWeight;
            Var["totalWeight"] =
                -1 * MetaDB::Instance().getWeight(PhyUtils::getCompaign(mergedRunNumber), tree->mcChannelNumber, tree->SUSYFinalState, extraWei);
        }

        ///*******************************Event construct *********************/
        AnaObjs eleVec = tree->getElectrons();
        AnaObjs muonVec = tree->getMuons();
        AnaObjs tauVec = tree->getTaus();
        AnaObjs jetVec = tree->getJets();

        Var["bVeto"] = (tree->n_BJets == 0); // b-veto
        Var["bNumber"] = tree->n_BJets;      // b-veto

        // Trigger
        //		GRLDB mGRLDB;
        //		mGRLDB.loadGRLFile("/publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/run2Ditau/include/ditauMETGRL.xml");
        //		bool useOldtrigger(true);
        //		if(mergedRunNumber >= 325713 && mergedRunNumber <=340453){  // 17 data/MC
        //			if(tree->mcChannelNumber != 0){
        //				if(!mGRLDB.checkInGRL(mergedRunNumber,tree->RandomLumiBlockNumber)){
        //					useOldtrigger = false;
        //				}
        //			} else{
        //				if(!mGRLDB.checkInGRL(mergedRunNumber,tree->lumiBlock)){
        //					useOldtrigger = false;
        //				}
        //			}
        //		}
        // e if(( mergedRunNumber >= 348885 )){}
        // d else if( mergedRunNumber >= 325713 && mergedRunNumber <= 340453 ){}
        // a else if( mergedRunNumber <= 311481){}
        // if(( mergedRunNumber >= 348885 )) {
        //	muonVec = muonVec.passTrig(( tree->TrigHLT_mu20_iloose_L1MU15 ), *(tree->TrigMatchHLT_mu20_iloose_L1MU15), {50} );
        //	Var["totalWeight"] *= tree->MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40;
        //} else {
        //	muonVec = muonVec.passTrig(( tree->TrigHLT_mu26_ivarmedium ), *(tree->TrigMatchHLT_mu26_ivarmedium), {50} );
        //	Var["totalWeight"] *= tree->MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50;
        //}
        auto temptauVec = tauVec.filterObjects(0, 100, TauMedium | TauIsSignal);
        auto tempmuonVec = muonVec.filterObjects(0, 100, MuGood | MuIso);

        if (((tree->TrigHLT_mu26_ivarmedium && tree->TrigMatchHLT_mu26_ivarmedium) ||
             (tree->TrigHLT_mu20_iloose_L1MU15 && tree->TrigMatchHLT_mu20_iloose_L1MU15)) &&
            tempmuonVec.size() >= 1 && temptauVec.size() >= 1) {
            muTriger = true;
            if (tree->mcChannelNumber != 0) {
                if (tree->TrigHLT_mu26_ivarmedium && tree->TrigMatchHLT_mu26_ivarmedium) {
                    Var["totalWeight"] *= tree->MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50;
                } else {
                    Var["totalWeight"] *= tree->MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40;
                }
            }
        }

        //		if( mergedRunNumber > 348885 ){
        //			tauVec = tauVec.passTrig(( tree->TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40 ),
        //*(tree->taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40), {95,75} );
        //		}
        ////		else if( !useOldtrigger ){
        ////			tauVec = tauVec.passTrig(( tree->TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50 ),
        ///*(tree->taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50),{75,40} ); /		}
        //		else{
        //			tauVec = tauVec.passTrig(( tree->TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12),
        //*(tree->taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12 ) , {95,65}); 			if(tree->mcChannelNumber
        //!= 0){ 				Var["totalWeight"] *= tree->TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo *
        // tree->TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;
        //			}
        //		}
        //		tauTriger = (!(tauVec.size()==0));
        // Taus
        tauVec = tauVec.filterObjects(0, 100, TauMedium);
        nTaus = tauVec.size();
        muonVec = muonVec.filterObjects(0, 100, MuGood | MuIso);
        nMuons = muonVec.size();

        if (nTaus >= 1 and nMuons >= 1) {
            auto tau = tauVec[0];
            auto muon = muonVec[0];
            // offline PT select
            TLorentzVector METVec;
            METVec.SetPtEtaPhiE(Var["evt_MET"], 0, tree->MetTST_phi, Var["evt_MET"]);
            OS2TM = tau.isOS(muon);
            Var["tau1Pt"] = tau.Pt();
            Var["tau1Mt"] = tau.Mt();
            Var["mu1Pt"] = muon.Pt();
            Var["mu1Mt"] = muon.Mt();
            Var["dRtm"] = PhyUtils::deltaR(tau, muon);
            //			Var["dPhitt"] = PhyUtils::deltaPhi(tau1, tau2);
            Var["Mtm_12"] = PhyUtils::calcMll(tau, muon);
            //			Var["meff_tau"] = tau1.Pt() + tau2.Pt() + Var["evt_MET"];
            //			Var["mct"] = PhyUtils::calcMCT(tau1, tau2);
            Var["MT2"] = PhyUtils::MT2(tau, muon, METVec);
            Var["mt12tm"] = Var["tau1Mt"] + Var["mu1Mt"];
            //			Var["met_sig"] = Var["evt_MET"] / (sqrt( Var["tau1Pt"] + Var["tau2Pt"]));
            topTagger = PhyUtils::GetTopPairs(20., 100., 30, tau, muon, jetVec, tree->MetTST_met / 1000, tree->MetTST_phi);
        }
        // if (Mtt_12 > 120000) zVeto = true;

        // Jets

        fillRegions();
        fillCutflows();
    }
    for (auto cut : getCutflows()) {
        cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
    }
}
