#include "AnaObjs.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "easylogging++.h"
#include "xTFwTree.h"

DefineLooper(TES_Slimmer, xTFwTree);

void TES_Slimmer::loop() {
    std::string outFullName = getOutName();
    std::string treeName = currentTreeName();
    // "Staus" length is 5
    // std::string outTree = treeName.replace(treeName.find("Staus"), 5, Utils::splitStrBy(outFullName, '.')[0]);
    // LOG(DEBUG) << "currentTree name: " << treeName << ", outTree name: " << outTree;

    auto oTree = cloneCurrentAnaTree();

    Cutflow_ptr mCutflow = nullptr;
    if (treeName.find("NOMINAL") != std::string::npos) {
        mCutflow = addCutflow();
    } else {
        mCutflow = addCutflow(Cutflow::NO_HIST);
    }
    // for cut
    Var["totalWeight"] = 1;
    Var["bJetNum"] = 0;
    Var["MET"] = 0;
    Var["MET_sig"] = 0;
    Var["upsilon"] = -2;
    Var["tau_0_allTrk_pt"] = 0;
    Var["Mll_lephad"] = 0;
    bool isQCR = false;
    bool isWCR = false;
    bool isTCR = false;
    bool isSR = false;
    bool isData = false;
    int Njet(0), NTightTau(0), mRunnum(0), mlumiBlock(0), nTruthTau, nTotal, nMedTau;

    if (outFullName.find("data") != std::string::npos || outFullName.find("Data") != std::string::npos) {
        isData = true;
    }

    // set mc channel number to 0 initially to seperate data and mc
    tree->mc_channel_number == 0;
    oTree->Branch("Weight_mc", &Var["totalWeight"]);
    oTree->Branch("upsilon", &Var["upsilon"]);
    oTree->Branch("isQCR", &isQCR);
    oTree->Branch("isWCR", &isWCR);
    oTree->Branch("isTCR", &isTCR);
    oTree->Branch("isSR", &isSR);
    oTree->Branch("isData", &isData);

    mCutflow->setWeight([&] { return Var["totalWeight"]; });

    mCutflow->setFillTree(oTree);

    mCutflow->registerCut("baseline", [&] { return true; });
    mCutflow->registerCut("either region", [&] { return isQCR || isWCR || isTCR || isSR; });
    auto preselect = mCutflow->registerCut("The END", [&] { return true; });
    preselect->addHist(
        "dNTracks", 5, 0, 5, [&] { return tree->tau_0_n_all_tracks - tree->tau_0_n_charged_tracks - tree->tau_0_n_conversion_tracks; },
        Hist::USE_OVERFLOW);

    Region_ptr QCR = addRegion([&] { return isQCR; }, "QCR");
    QCR->setWeight([&] { return Var["totalWeight"]; });
    // QCR->addHist("Mll", 10, 0, 100, [&] { return Var["Mll_lephad"] ; }, Hist::USE_OVERFLOW);

    Region_ptr WCR = addRegion([&] { return isWCR; }, "WCR");
    WCR->setWeight([&] { return Var["totalWeight"]; });
    // WCR->addHist("Mll", 10, 0, 100, [&] { return Var["Mll_lephad"] ; }, Hist::USE_OVERFLOW);

    Region_ptr TCR = addRegion([&] { return isTCR; }, "TCR");
    TCR->setWeight([&] { return Var["totalWeight"]; });
    // TCR->addHist("Mll", 10, 0, 100, [&] { return Var["Mll_lephad"] ; }, Hist::USE_OVERFLOW);

    Region_ptr SR = addRegion([&] { return isSR; }, "SR");
    SR->setWeight([&] { return Var["totalWeight"]; });
    // SR->addHist("Mll", 10, 0, 100, [&] { return Var["Mll_lephad"] ; }, Hist::USE_OVERFLOW);

    // Start the loop!
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);

        Var["upsilon"] = -2;
        Var["tau_0_allTrk_pt"] = 0;
        Var["Mll_lephad"] = 0;

        isQCR = false;
        isWCR = false;
        isTCR = false;
        isSR = false;

        // Weight setting
        if (isData) {
            Var["totalWeight"] = 1;
        } else if (tree->mc_channel_number != 0) {
            // weight_mc * (Xsection * Kfactor * FilEff) * lumi / totw
            // Var["totalWeight"] = tree->weight_mc * MetaDB::Instance().getXsection( tree->mc_channel_number , 0 ) * tree->kfactor *
            // tree->filter_efficiency * MetaDB::Instance().getLumi( MCcompaign ) / MetaDB::Instance().getNnorm( MCcompaign , tree->mc_channel_number,
            // 0);
            // Var["totalWeight"] = tree->weight_mc * tree->cross_section * tree->kfactor * tree->filter_efficiency * MetaDB::Instance().getLumi(
            // MCcompaign ) / MetaDB::Instance().getNnorm( MCcompaign , tree->mc_channel_number, 0);
            int MCcompaign = PhyUtils::getCompaign(tree->run_number);
            Var["totalWeight"] = MetaDB::Instance().getWeight(MCcompaign, tree->mc_channel_number, 0, tree->weight_mc);
            // Var["totalWeight"] = MetaDB::Instance().getWeight(PhyUtils::getCompaign(mergedRunNumber), tree->mcChannelNumber, tree->SUSYFinalState,
            // tree->weight_mc);
        }

        Var["bJetNum"] = tree->n_bjets_DL1r_FixedCutBEff_70;
        Var["MET"] = tree->met_reco_p4->Et();
        Var["sum_cos_dPhi"] = tree->lephad_met_sum_cos_dphi;
        Var["lep0_Mt"] = tree->lephad_mt_lep0_met;
        Var["is_OS"] = (tree->lephad_qxq == -1);

        auto charged_tracks_p4 = tree->tau_0_charged_tracks_p4;
        auto conversion_tracks_p4 = tree->tau_0_conversion_tracks_p4;
        TLorentzVector allTrk_p4;
        for (auto i : *charged_tracks_p4) {
            allTrk_p4 = allTrk_p4 + i;
        }
        for (auto i : *conversion_tracks_p4) {
            allTrk_p4 = allTrk_p4 + i;
        }
        Var["tau_0_allTrk_pt"] = allTrk_p4.Pt();
        Var["pt_calib"] = tree->tau_0_p4->Pt();
        Var["upsilon"] = 2. * Var["tau_0_allTrk_pt"] / Var["pt_calib"] - 1.;
        Var["Mll_lephad"] = tree->lephad_p4->M();
        Var["tau0_Eta"] = fabs(tree->tau_0_p4->Eta());

        Var["TauSelection"] = (tree->tau_0_jet_rnn_loose == 1 && tree->tau_0_p4->Pt() > 20 && abs(tree->tau_0_q) == 1 &&
                               (tree->tau_0_n_charged_tracks == 1 || tree->tau_0_n_charged_tracks == 3) &&
                               ((Var["tau0_Eta"] < 1.37) || (1.52 < Var["tau0_Eta"] && Var["tau0_Eta"] < 2.47)));

        bool passMuTrig = false;
        if (tree->run_number <= 284484) {
            if ((tree->HLT_mu20_iloose_L1MU15 == 1 && tree->lep_0_p4->Pt() > 21 && tree->muTrigMatch_0_HLT_mu20_iloose_L1MU15 == 1) ||
                (tree->HLT_mu50 == 1 && tree->lep_0_p4->Pt() > 52.5 && tree->muTrigMatch_0_HLT_mu50))
                passMuTrig = true;
        } else if (tree->run_number >= 297730) {
            if ((tree->HLT_mu26_ivarmedium == 1 && tree->lep_0_p4->Pt() > 27.3 && tree->muTrigMatch_0_HLT_mu26_ivarmedium == 1) ||
                (tree->HLT_mu50 == 1 && tree->lep_0_p4->Pt() > 52.5 && tree->muTrigMatch_0_HLT_mu50))
                passMuTrig = true;
        }
        Var["MuonSelection"] = (tree->lep_0_id_medium == 1 && fabs(tree->lep_0_p4->Eta()) < 2.5 && passMuTrig);

        Var["is_iso"] = tree->lep_0_iso_TightTrackOnly_VarRad;

        if (Var["TauSelection"] && Var["MuonSelection"]) {
            LOG(DEBUG) << "iso:" << Var["is_iso"] << " bjet:" << Var["bJetNum"] << " lep0mt:" << Var["lep0_Mt"] << " dphi:" << Var["sum_cos_dPhi"]
                       << " met:" << Var["MET"];
            if (Var["is_iso"] && Var["bJetNum"] == 0 && Var["lep0_Mt"] < 50 &&
                Var["sum_cos_dPhi"] > -0.15) { // && Var["is_OS"] We need SR_ss for QCD estimation!!!!!!!!!!!!!!!!!
                isSR = true;
                LOG(DEBUG) << "isSR";
            }

            if (Var["is_iso"] && Var["bJetNum"] == 0 && Var["lep0_Mt"] > 60 && Var["MET"] > 30) {
                isWCR = true;
                LOG(DEBUG) << "isWCR";
            }

            if (!Var["is_iso"] && Var["bJetNum"] == 0) {
                isQCR = true;
                LOG(DEBUG) << "isQCR";
            }

            if (Var["is_iso"] && Var["bJetNum"] >= 1 && Var["lep0_Mt"] > 60) {
                isTCR = true;
                LOG(DEBUG) << "isTCR";
            }
        }

        fillRegions();
        fillCutflows();
    }
    // Print out for region, cutflows if needed
    if (treeName.find("NOMINAL") != std::string::npos) {
        for (auto cut : getCutflows()) {
            cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
        }
    }
}
