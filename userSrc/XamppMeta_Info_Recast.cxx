#include "SingleTreeRunner.h"
#include "XamppMetaTree.h"

DefineLooper(XamppMeta_Info_Recast, XamppMetaTree);

// There is no any infos to seperate the a,d,e in Xampp MetadataTree, so you should split the inputs by yourself!
void XamppMeta_Info_Recast::loop() {
    // Wrap the total loop in a if because we will only loop MetaDataTree and in case user loop other trees
    if (currentTreeName() == "MetaDataTree") {
        nlohmann::json jOut;
        std::string MCcompaign;
        std::string outName = getOutName();
        if (outName.find("16a") != std::string::npos) {
            MCcompaign = "16";
        } else if (outName.find("16d") != std::string::npos) {
            MCcompaign = "17";
        } else {
            MCcompaign = "18";
        }
        setWeight([&] { return 1; });
        auto oTree = cloneCurrentAnaTree();
        Var["m_xsec"] = -1;
        Var["m_filterEff"] = -1;
        Var["m_kFactor"] = -1;
        oTree->SetBranchAddress("xSection", &Var["m_xsec"]);
        oTree->SetBranchAddress("FilterEfficiency", &Var["m_filterEff"]);
        oTree->SetBranchAddress("kFactor", &Var["m_kFactor"]);
        auto mCutflow = addCutflow();
        mCutflow->setFillTree(oTree);
        mCutflow->registerCut("baseline", [&] { return true; });
        mCutflow->registerCut("baseline2", [&] { return tree->xSection > -999; });
        mCutflow->registerCut("The END", [&] { return true; });

        for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
            getEntry(jentry);

            std::string number = "999999"; // std::to_string(tree->mcChannelNumber);
            std::string process = std::to_string(tree->ProcessID);
            Var["m_xsec"] = tree->xSection;
            Var["m_filterEff"] = tree->FilterEfficiency;
            Var["m_kFactor"] = tree->kFactor;

            bool hasValue = !(jOut["Nnorm"][process][number][MCcompaign].empty());

            if (!hasValue) {
                jOut["xSection"][process][number] = "ChangeThisValue"; // Var["m_xsec"];
                jOut["kFactor"][number] = 1.0;                         // Var["m_kFactor"];
                jOut["filterEff"][number] = 1.0;                       // Var["m_filterEff"];
                jOut["Nnorm"][process][number][MCcompaign] = tree->TotalSumW;
                jOut["lumi"]["16"] = 36207.680256;
                jOut["lumi"]["17"] = 44307.39456;
                jOut["lumi"]["18"] = 58450.120704;
            } else {
                double OldSumW = jOut["Nnorm"][process][number][MCcompaign];
                jOut["Nnorm"][process][number][MCcompaign] = tree->TotalSumW + OldSumW;
            }
        }

        std::string outJsonName = outName + ".json";
        std::ofstream o(outJsonName);
        o << std::setw(4) << jOut << std::endl;
    }
}
