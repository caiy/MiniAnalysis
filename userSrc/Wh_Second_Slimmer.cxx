#include "AnaObjs.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

DefineLooper(Wh_Second_Slimmer, XamppTree);

void Wh_Second_Slimmer::loop() {
    std::string outFullName = getOutName();
    auto treeName = currentTreeName();

    tree->SetBranchStatus("GenWeight_LHE*", 0);

    auto oTree = cloneCurrentAnaTree();

    Cutflow_ptr mCutflow = nullptr;
    if (treeName.find("Nominal") != std::string::npos) {
        mCutflow = addCutflow();
    } else {
        mCutflow = addCutflow(Cutflow::NO_HIST);
    }
    // for cut
    Var["totalWeight"] = 1;

    mCutflow->setWeight([&] { return Var["totalWeight"]; });

    mCutflow->setFillTree(oTree);

    mCutflow->registerCut("baseline", [&] { return true; });
    mCutflow->registerCut("the end", [&] { return true; });

    // Start the loop!
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);

        Var["totalWeight"] = tree->Weight_mc;

        fillRegions();
        fillCutflows();
    }
    // Print out for region, cutflows if needed
    if (treeName.find("Nominal") != std::string::npos) {
        for (auto cut : getCutflows()) {
            cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
        }
    }
}
