#include "SingleTreeRunner.h"
#include "SubTauTree.h"
#include "easylogging++.h"

DefineLooper(SubTau_MCSlimmer, SubTauTree);

void SubTau_MCSlimmer::loop() {
    auto oTree = cloneCurrentAnaTree();

    auto mCutflow = addCutflow(Cutflow::NO_HIST);
    Var["totalWeight"] = 1;
    Var["TauSelection"] = 0;
    Var["MuonSelection"] = 0;
    Var["BaseSelection"] = 0;
    mCutflow->setWeight([&] { return Var["totalWeight"]; }); // Currently only to slim, don't care about weight
    mCutflow->setFillTree(oTree);

    mCutflow->registerCut("baseline", [&] { return true; });
    mCutflow->registerCut("TauSelection", [&] { return Var["TauSelection"]; });
    mCutflow->registerCut("MuonSelection", [&] { return Var["MuonSelection"]; });
    mCutflow->registerCut("BaseSelection", [&] { return Var["BaseSelection"]; });
    mCutflow->registerCut("The END", [&] { return true; });

    // Start the loop!
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);

        Var["BaseSelection"] = false;
        Var["MuonSelection"] = false;
        Var["TauSelection"] = false;

        Var["MuonSelection"] = (tree->lep_0_id_medium == 1 && fabs(tree->lep_0_p4->Eta()) < 2.5 &&
                                ((tree->HLT_mu26_ivarmedium == 1 && tree->lep_0_p4->Pt() > 27.3 && tree->muTrigMatch_0_HLT_mu26_ivarmedium == 1) ||
                                 (tree->HLT_mu50 == 1 && tree->lep_0_p4->Pt() > 52.5 && tree->muTrigMatch_0_HLT_mu50)));

        double tauEta = tree->tau_0_p4->Eta();
        double DPhi = fabs(tree->tau_0_p4->Phi() - tree->lep_0_p4->Phi());
        if (DPhi > M_PI) DPhi = 2 * M_PI - DPhi;
        bool isOS = (tree->lephad_qxq == -1);

        Var["TauSelection"] = (tree->tau_0_jet_bdt_score_trans > 0.005 && tree->tau_0_p4->Pt() > 20 && abs(tree->tau_0_q) == 1 &&
                               (tree->tau_0_n_charged_tracks == 1 || tree->tau_0_n_charged_tracks == 3) &&
                               ((fabs(tauEta) < 1.37) || (1.52 < fabs(tauEta) && fabs(tauEta) < 2.47)));
        bool SRcut = (tree->lep_0_iso_FCTightTrackOnly_FixedRad && tree->n_bjets == 0 && tree->lephad_mt_lep0_met < 50 &&
                      tree->lephad_p4->M() < 120 && tree->lephad_p4->M() > 45 && DPhi > 2.4 && tree->lephad_met_sum_cos_dphi > -0.15 && isOS);
        bool SRSScut =
            (tree->lep_0_iso_FCTightTrackOnly_FixedRad && tree->n_bjets == 0 && tree->lephad_mt_lep0_met < 50 && tree->lephad_p4->M() < 120 &&
             tree->lephad_p4->M() > 45 && DPhi > 2.4 && tree->lephad_met_sum_cos_dphi > -0.15 && !isOS && currentTreeName() == "NOMINAL");
        bool WCRCut =
            (tree->lep_0_iso_FCTightTrackOnly_FixedRad && tree->n_bjets == 0 && tree->lephad_mt_lep0_met > 60 && tree->met_reco_sumet > 20 &&
             tree->lephad_p4->M() < 120 && tree->lephad_p4->M() > 45 && tree->lephad_met_sum_cos_dphi < 0 && currentTreeName() == "NOMINAL");
        bool QCRCut =
            (!tree->lep_0_iso_FCTightTrackOnly_FixedRad && tree->n_bjets == 0 && tree->lephad_mt_lep0_met < 50 && tree->lephad_p4->M() < 120 &&
             tree->lephad_p4->M() > 45 && DPhi > 2.4 && tree->lephad_met_sum_cos_dphi > -0.15 && currentTreeName() == "NOMINAL");
        bool topCR = (tree->n_bjets != 0 && tree->lep_0_iso_FCTightTrackOnly_FixedRad && tree->lephad_mt_lep0_met > 40 && tree->met_reco_sumet > 30);
        bool slimCut = (SRcut || SRSScut || WCRCut || QCRCut);
        Var["BaseSelection"] = (Var["MuonSelection"] && Var["TauSelection"] && slimCut);
        fillRegions();
        fillCutflows();
    }
    // Print out for region, cutflows if needed
}
