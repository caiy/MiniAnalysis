#include "AnaObjs.h"
#include "GRLDB.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

/* QCD sample slimmer */

//#include "XamppTree.h"
DefineLooper(C1N2OS_QCD_Slimmer, XamppTree);

void C1N2OS_QCD_Slimmer::loop() {
    std::string outFullName = getOutName();
    std::string treeName = currentTreeName();
    // std::string outTree = Utils::splitStrBy(outFullName,'.')[0] + "_Nominal";
    std::string outTree = treeName.replace(treeName.find("Staus"), 5, Utils::splitStrBy(outFullName, '.')[0]);
    LOG(INFO) << "currentTree name: " << treeName << "outTree name: " << outTree;

    auto oTree = cloneCurrentAnaTree(outTree);

    auto mCutflow = addCutflow();
    //	Cutflow* mCutflow = nullptr;
    //	if(treeName == "Staus_Nominal"){
    //		mCutflow = addCutflow();
    //	}else{
    //		mCutflow = addCutflow(Cutflow::NO_HIST);
    //	}
    // define cuts
    Var["tau1Pt"] = 0;
    Var["tau1Mt"] = 0;
    Var["tau2Pt"] = 0;
    Var["tau2Mt"] = 0;
    Var["dPhitt"] = 0;
    Var["dRtt"] = 0;
    Var["mt12tau"] = 0;
    Var["meff_tau"] = 0;
    Var["Mtt_12"] = 0;
    Var["MT2"] = 0;
    Var["met_sig"] = 0;
    Var["met_sig_tj"] = 0;
    Var["evt_MET"] = 0;

    Var["dPhijj"] = 0;
    Var["dRjj"] = 0;
    Var["dEtajj"] = 0;
    Var["EtajEtaj"] = 0;
    Var["jet1Pt"] = 0;
    Var["jet2Pt"] = 0;
    Var["jet1Eta"] = 0;
    Var["jet2Eta"] = 0;
    Var["jet1Mt"] = 0;
    Var["jet2Mt"] = 0;
    Var["fabs_jet1Eta"] = 0;
    Var["fabs_jet2Eta"] = 0;
    Var["bVeto"] = 0;
    Var["bNumber"] = 0;
    Var["totalWeight"] = 1;

    Var["n_signal_jets"] = 0;

    int nTightTau(0), nTaus(0), Ljet_n(0), mergedRunNumber(0), mlumiBlock(0), nJet(0), Nemu(0);
    bool tauTriger, OS2Tau, zVeto, mllCut, mt2Cut, noemu;

    oTree->Branch("Weight_mc", &Var["totalWeight"]);
    oTree->Branch("mergedRunNumber", &mergedRunNumber);
    oTree->Branch("mergedlumiBlock", &mlumiBlock);
    //	oTree->Branch("tau1Pt", &Var["tau1Pt"]);
    //	oTree->Branch("tau2Pt", &Var["tau2Pt"]);
    //	oTree->Branch("tau1Mt", &Var["tau1Mt"]);
    //	oTree->Branch("tau2Mt", &Var["tau2Mt"]);
    //	oTree->Branch("dRtt", &Var["dRtt"]);
    //	oTree->Branch("dPhitt", &Var["dPhitt"]);
    //	oTree->Branch("mt12tau", &Var["mt12tau"]);
    //	oTree->Branch("Mll_tt", &Var["Mtt_12"]);
    //	oTree->Branch("meff_tt", &Var["meff_tau"]);
    //	oTree->Branch("MT2", &Var["MT2"]);
    //
    //	oTree->Branch("jet1Pt", &Var["jet1Pt"]);
    //	oTree->Branch("jet2Pt", &Var["jet2Pt"]);
    //	oTree->Branch("jet1Eta", &Var["jet1Eta"]);
    //	oTree->Branch("jet2Eta", &Var["jet2Eta"]);
    //	oTree->Branch("jet1Mt", &Var["jet1Mt"]);
    //	oTree->Branch("jet2Mt", &Var["jet2Mt"]);
    //	oTree->Branch("dRjj", &Var["dRjj"]);
    //	oTree->Branch("dPhijj", &Var["dPhijj"]);
    //	oTree->Branch("dEtajj", &Var["dEtajj"]);
    //	oTree->Branch("EtajEtaj", &Var["EtajEtaj"]);
    //	oTree->Branch("Absjet1Eta", &Var["fabs_jet1Eta"]);
    //	oTree->Branch("ABsjet2Eta", &Var["fabs_jet2Eta"]);
    //
    //	oTree->Branch("LJet_n", &Ljet_n);
    //	oTree->Branch("BJet_n", &Var["bNumber"]);
    //	oTree->Branch("Evt_MET", &Var["evt_MET"]);
    //	oTree->Branch("Evt_METSIG", &Var["met_sig"]);
    //	oTree->Branch("Evt_METSIG_WithTauJet", &Var["met_sig_tj"]);

    mCutflow->setWeight([&] { return Var["totalWeight"]; });
    mCutflow->setFillTree(oTree);
    //将m_Tree导入Cutflow里的outTree
    mCutflow->registerCut("baseline", [&] { return fabs(Var["totalWeight"]) < 1000; });
    //	mCutflow->registerCut("noemu", [&] {return noemu; },"noemu",3, 0, 3, [&] {return Nemu; });
    //	mCutflow->registerCut("bVeto",[&] {return Var["bVeto"];},"bTag",2,0,2,[&]{return Var["bNumber"];});
    //	mCutflow->registerCut("tau trigger", [&] {return tauTriger; });
    //	mCutflow->registerCut("MET>130 GeV", [&] {return Var["evt_MET"] >= 130; });
    //	mCutflow->registerCut(">= 2 medium tau", [&] {return nTaus >= 2; });
    //	mCutflow->registerCut("OS", [&] {return (OS2Tau); });
    //	mCutflow->registerCut("Njets==1or2", [&] {return (nJet == 1 || nJet == 2 ) ; });
    //	mCutflow->registerCut("z/hVeto(100Gev)", [&] {return Var["Mtt_12"] >= 100; });
    //	mCutflow->registerCut("mt1+mt2>100GeV", [&] {return Var["mt12tau"] >= 100; });
    //	mCutflow->registerCut("DPhitt>0.8", [&] {return Var["dPhitt"] >= 0.8; });

    auto lastCut = mCutflow->registerCut("the END", [&] { return true; });

    //	lastCut->addHist("mll_12", 50 , 0, 1000, [&]{return Var["Mtt_12"] ;}, Hist::USE_OVERFLOW);
    //	lastCut->addHist("MT12", 40, 0, 1000, [&] {return Var["mt12tau"] ;}, Hist::USE_OVERFLOW);
    //	lastCut->addHist("mT2", 20 , 0, 200, [&] {return Var["MT2"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("meff_tau", 20, 0, 1000, [&] {return Var["meff_tau"];}, Hist::USE_OVERFLOW);
    //	lastCut->addHist("DRtt", 28, 0, 7, [&] {return Var["dRtt"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("tau1pt", 35, 0, 700, [&] {return Var["tau1Pt"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("tau2pt", 20, 0, 400, [&] {return Var["tau2Pt"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("tau1Mt", 50, 0, 1000, [&] {return Var["tau1Mt"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("tau2Mt", 30, 0, 600, [&] {return Var["tau2Mt"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("DPhitt", 17, 0, 3.4, [&] {return Var["dPhitt"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("Evt_MET", 40, 0, 800, [&] {return Var["evt_MET"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("MET_sig", 20, 0, 50, [&] {return Var["met_sig"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("met_sig_tj", 20, 0, 50, [&] {return Var["met_sig_tj"];}, Hist::USE_OVERFLOW);

    //	lastCut->addHist("NSignalJet", 10, 0, 10, [&] {return Var["n_signal_jets"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("jet1pt", 40, 0, 1000, [&] {return Var["jet1Pt"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("jet2pt", 30, 0, 600, [&] {return Var["jet2Pt"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("absjet1eta", 20, 0, 4, [&] {return Var["fabs_jet1Eta"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("absjet2eta", 20, 0, 4, [&] {return Var["fabs_jet2Eta"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("DRjj", 28, 0, 7, [&] {return Var["dRjj"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("DPhijj", 17, 0, 3.4, [&] {return Var["dPhijj"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("DEtajj", 20, 0, 10, [&] {return Var["dEtajj"]; }, Hist::USE_OVERFLOW);
    //	lastCut->addHist("EtajEtaj", 40, -10, 10, [&] {return Var["EtajEtaj"]; }, Hist::USE_OVERFLOW);

    // Loop Start
    Long64_t nentries = tree->GetEntries();
    for (Long64_t jentry = 0; jentry < nentries; jentry++) {
        tree->GetEntry(jentry);
        if (0 == jentry % 100000) std::cout << jentry << " entry of " << nentries << "entries" << std::endl;

        Var["tau1Pt"] = 0;
        Var["tau1Mt"] = 0;
        Var["tau2Pt"] = 0;
        Var["tau2Mt"] = 0;
        Var["dPhitt"] = 999;
        Var["dRtt"] = 999;
        Var["mt12tau"] = -999;
        Var["meff_tau"] = -999;
        Var["Mtt_12"] = -999;
        Var["MT2"] = -999;
        Var["met_sig"] = -1;
        Var["met_sig_tj"] = -1;
        Var["evt_MET"] = -1;

        Var["dPhijj"] = 0;
        Var["dRjj"] = 0;
        Var["dEtajj"] = 0;
        Var["EtajEtaj"] = 0;
        Var["jet1Pt"] = 0;
        Var["jet2Pt"] = 0;
        Var["jet1Eta"] = 0;
        Var["jet2Eta"] = 0;
        Var["fabs_jet1Eta"] = 0;
        Var["fabs_jet2Eta"] = 0;
        Var["totalWeight"] = 1;
        Var["jet1Mt"] = 0;
        Var["jet2Mt"] = 0;
        nTightTau = 0;
        nTaus = 0;
        Ljet_n = 0;
        mergedRunNumber = 0;
        nJet = 0;
        tauTriger = false;
        Var["bVeto"] = false;
        OS2Tau = false, zVeto = false, noemu = true;
        Var["bNumber"] = 0;
        Var["evt_MET"] = tree->MetTST_met / 1000;

        if (tree->mcChannelNumber == 0) {
            Var["totalWeight"] = 1;
            mergedRunNumber = tree->runNumber;
            mlumiBlock = tree->lumiBlock;
        } else {
            mergedRunNumber = tree->RandomRunNumber;
            mlumiBlock = tree->RandomLumiBlockNumber;
        }

        if (tree->mcChannelNumber != 0) {
            double extraWei = tree->GenWeight * tree->muWeight * tree->TauWeight * tree->EleWeight * tree->MuoWeight * tree->JetWeight;
            Var["totalWeight"] =
                -1 * MetaDB::Instance().getWeight(PhyUtils::getCompaign(mergedRunNumber), tree->mcChannelNumber, tree->SUSYFinalState, extraWei);
        }

        ///*******************************Event construct *********************/
        AnaObjs eleVec = tree->getElectrons();
        AnaObjs muonVec = tree->getMuons();
        AnaObjs tauVec = tree->getTaus();
        AnaObjs jetVec = tree->getJets();

        Var["bVeto"] = (tree->n_BJets == 0); // b-veto
        Var["bNumber"] = tree->n_BJets;      // b-veto
        noemu = ((tree->n_SignalMuon + tree->n_SignalElec) == 0);
        Nemu = tree->n_SignalMuon + tree->n_SignalElec;

        // Trigger
        //		GRLDB mGRLDB;
        //		mGRLDB.loadGRLFile("/publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/run2Ditau/include/ditauMETGRL.xml");
        //		bool useOldtrigger(true);
        //		if(mergedRunNumber >= 325713 && mergedRunNumber <=340453){  // 17 data/MC
        //			if(tree->mcChannelNumber != 0){
        //				if(!mGRLDB.checkInGRL(mergedRunNumber,tree->RandomLumiBlockNumber)){
        //					useOldtrigger = false;
        //				}
        //			} else{
        //				if(!mGRLDB.checkInGRL(mergedRunNumber,tree->lumiBlock)){
        //					useOldtrigger = false;
        //				}
        //			}
        //		}
        // e if(( mergedRunNumber >= 348885 )){}
        // d else if( mergedRunNumber >= 325713 && mergedRunNumber <= 340453 ){}
        // a else if( mergedRunNumber <= 311481){}
        //
        //		if( mergedRunNumber > 348885 ){
        //			tauVec = tauVec.passTrig(( tree->TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40 ),
        //*(tree->taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40), {95,75} );
        //		}
        ////		else if( !useOldtrigger ){
        ////			tauVec = tauVec.passTrig(( tree->TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50 ),
        ///*(tree->taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50),{75,40} ); /		}
        //		else{
        //			tauVec = tauVec.passTrig(( tree->TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12),
        //*(tree->taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12 ) , {95,65}); 			if(tree->mcChannelNumber
        //!= 0){ 				Var["totalWeight"] *= tree->TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo *
        // tree->TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;
        //			}
        //		}
        //		tauTriger = (!(tauVec.size()==0));
        // Taus
        tauVec = tauVec.filterObjects(0, 100, TauMedium);
        nTaus = tauVec.size();
        if (nTaus >= 2) {
            auto tau1 = tauVec[0];
            auto tau2 = tauVec[1];
            // offline PT select
            TLorentzVector METVec;
            METVec.SetPtEtaPhiE(Var["evt_MET"], 0, tree->MetTST_phi, Var["evt_MET"]);
            OS2Tau = tau1.isOS(tauVec[1]);
            Var["tau1Pt"] = tau1.Pt();
            Var["tau1Mt"] = tau1.Mt();
            Var["tau2Pt"] = tau2.Pt();
            Var["tau2Mt"] = tau2.Mt();
            //			Var["dRtt"] = PhyUtils::deltaR(tau1, tau2);
            //			Var["dPhitt"] = PhyUtils::deltaPhi(tau1, tau2);
            //			Var["Mtt_12"] = PhyUtils::calcMll(tau1, tau2);
            //			Var["meff_tau"] = tau1.Pt() + tau2.Pt() + Var["evt_MET"];
            //			Var["mct"] = PhyUtils::calcMCT(tau1, tau2);
            //			Var["MT2"] = PhyUtils::MT2(tau1,tau2,METVec);
            //			Var["mt12tau"] = Var["tau1Mt"] + Var["tau2Mt"];
            //			Var["met_sig"] = Var["evt_MET"] / (sqrt( Var["tau1Pt"] + Var["tau2Pt"]));
        }
        // if (Mtt_12 > 120000) zVeto = true;

        // Jets
        double sumJet(0);
        // jetVec = jetVec.filterObjects(0,100,JetIsSignal);
        // nJet = jetVec.size();
        tree->jets_signal->size();
        Var["n_signal_jets"] = nJet;
        if (nJet >= 2) {
            auto jet1 = jetVec[0];
            auto jet2 = jetVec[1];
            Var["jet1Pt"] = jet1.Pt();
            Var["jet2Pt"] = jet2.Pt();
            Var["jet1Eta"] = jet1.Eta();
            Var["jet2Eta"] = jet2.Eta();
            Var["jet1Mt"] = jet1.lorentzMt();
            Var["jet2Mt"] = jet2.lorentzMt();
            //			Var["dRjj"] = PhyUtils::deltaR(jet1, jet2);
            //			Var["dPhijj"] = PhyUtils::deltaPhi(jet1, jet2);
            //			Var["dEtajj"] = fabs(Var["jet1Eta"]-Var["jet2Eta"]);
            //			Var["EtajEtaj"] = Var["jet1Eta"] * Var["jet2Eta"];
            //			Var["fabs_jet1Eta"] = fabs(Var["jet1Eta"]);
            //			Var["fabs_jet2Eta"] = fabs(Var["jet2Eta"]);
        }
        // met_sig_tj = (MetTST_met) / (sqrt( tau1Pt + tau2Pt + sumJet ));

        fillRegions();
        fillCutflows();
    }
    for (auto cut : getCutflows()) {
        cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
    }
}
