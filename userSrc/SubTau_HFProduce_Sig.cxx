#include <iostream>

#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "SubTauTree.h"
#include "easylogging++.h"

DefineLooper(SubTau_HFProduce_Sig, SubTauTree);

void SubTau_HFProduce_Sig::loop() {
    // register vars
    Var["upsilon"] = 0;
    Var["NTrack"] = 0;
    Var["Mu"] = 0;
    Var["TauPt"] = 0;
    Var["TauEta"] = 0;
    Var["MuPt"] = 0;
    Var["MuEta"] = 0;
    Var["LepHadVisMass"] = 0;
    Var["isData"] = 0;
    Var["isW"] = 0;
    Var["isLep"] = 0;
    Var["WCR"] = 0;
    Var["WCRSS"] = 0;
    Var["SR"] = 0;
    Var["SRSS"] = 0;
    Var["tauID"] = 0;
    Var["QCR"] = 0;
    Var["QCRSS"] = 0;
    Var["TruthMode"] = 0;
    Var["RecoMode"] = 0;
    Var["Weight"] = 0;
    Var["QCDWeight"] = 0;
    Var["isReco1"] = 0;
    Var["isReco2"] = 0;
    Var["isReco3"] = 0;
    Var["isReco4"] = 0;
    Var["isReco5"] = 0;
    Var["isQCRReco1"] = 0;
    Var["isQCRReco2"] = 0;
    Var["isQCRReco3"] = 0;
    Var["isQCRReco4"] = 0;
    Var["isQCRReco5"] = 0;
    Var["W_SS_TF_up"] = 1;
    Var["W_SS_TF_dw"] = 1;
    // tauID setting. 0: nocut, 1: loose, 2: medium, 3: tight
    Var["tauIDCheckBox"] = 0;
    if (AnaConfigReader::Instance().readKey<std::string>("tauID") != NULL) {
        std::string IDChoice = AnaConfigReader::Instance().readKey<std::string>("tauID");
        if (IDChoice == "loose") {
            LOG(INFO) << "Selected tauID: loose";
            Var["tauIDCheckBox"] = 1;
        } else if (IDChoice == "medium") {
            LOG(INFO) << "Selected tauID: medium";
            Var["tauIDCheckBox"] = 2;
        } else if (IDChoice == "tight") {
            LOG(INFO) << "Selected tauID: tight";
            Var["tauIDCheckBox"] = 3;
        } else if (IDChoice == "RNNloose") {
            LOG(INFO) << "Selected tauID: RNN loose";
            Var["tauIDCheckBox"] = 4;
        } else if (IDChoice == "RNNmedium") {
            LOG(INFO) << "Selected tauID: RNN medium";
            Var["tauIDCheckBox"] = 5;
        } else if (IDChoice == "RNNtight") {
            LOG(INFO) << "Selected tauID: RNN tight";
            Var["tauIDCheckBox"] = 6;
        } else {
            LOG(INFO) << "Selected tauID: nocut";
        }

    } else {
        LOG(INFO) << "Selected tauID: nocut";
    }

    // truth1 template
    auto truth1 = addCutflow();
    auto tree_truth1 = cloneCurrentAnaTree("Truth1_" + currentTreeName());
    // Variable setting
    tree_truth1->Branch("upsilon", &Var["upsilon"]);
    tree_truth1->Branch("Ntrack", &Var["NTrack"]);
    tree_truth1->Branch("Weight_total", &Var["Weight"]);
    // channel setting
    tree_truth1->Branch("isReco1", &Var["isReco1"]);
    tree_truth1->Branch("isReco2", &Var["isReco2"]);
    tree_truth1->Branch("isReco3", &Var["isReco3"]);
    tree_truth1->Branch("isReco4", &Var["isReco4"]);
    tree_truth1->Branch("isReco5", &Var["isReco5"]);
    tree_truth1->Branch("isQCRReco1", &Var["isQCRReco1"]);
    tree_truth1->Branch("isQCRReco2", &Var["isQCRReco2"]);
    tree_truth1->Branch("isQCRReco3", &Var["isQCRReco3"]);
    tree_truth1->Branch("isQCRReco4", &Var["isQCRReco4"]);
    tree_truth1->Branch("isQCRReco5", &Var["isQCRReco5"]);
    // weight setting
    truth1->setWeight([&] { return Var["Weight"]; });
    truth1->setFillTree(tree_truth1);
    // simple cut
    truth1->registerCut("baseline", [&] { return Var["SR"] == 1 && Var["isW"] == 0 && Var["isData"] == 0; });
    truth1->registerCut("is truth recon1", [&] { return Var["TruthMode"] == 0; });
    truth1->registerCut("the END", [&] { return true; });

    // truth2 template
    auto truth2 = addCutflow();
    auto tree_truth2 = cloneCurrentAnaTree("Truth2_" + currentTreeName());
    // Fit Variable setting
    tree_truth2->Branch("upsilon", &Var["upsilon"]);
    tree_truth2->Branch("Ntrack", &Var["NTrack"]);
    tree_truth2->Branch("Weight_total", &Var["Weight"]);
    // channel setting
    tree_truth2->Branch("isReco1", &Var["isReco1"]);
    tree_truth2->Branch("isReco2", &Var["isReco2"]);
    tree_truth2->Branch("isReco3", &Var["isReco3"]);
    tree_truth2->Branch("isReco4", &Var["isReco4"]);
    tree_truth2->Branch("isReco5", &Var["isReco5"]);
    tree_truth2->Branch("isQCRReco1", &Var["isQCRReco1"]);
    tree_truth2->Branch("isQCRReco2", &Var["isQCRReco2"]);
    tree_truth2->Branch("isQCRReco3", &Var["isQCRReco3"]);
    tree_truth2->Branch("isQCRReco4", &Var["isQCRReco4"]);
    tree_truth2->Branch("isQCRReco5", &Var["isQCRReco5"]);
    // weight setting
    truth2->setWeight([&] { return Var["Weight"]; });
    truth2->setFillTree(tree_truth2);
    // simple cut
    truth2->registerCut("baseline", [&] { return Var["SR"] == 1 && Var["isW"] == 0 && Var["isData"] == 0; });
    truth2->registerCut("is truth recon2", [&] { return Var["TruthMode"] == 1; });
    truth2->registerCut("the END", [&] { return true; });

    // truth3 template
    auto truth3 = addCutflow();
    auto tree_truth3 = cloneCurrentAnaTree("Truth3_" + currentTreeName());
    // Fit Variable setting
    tree_truth3->Branch("upsilon", &Var["upsilon"]);
    tree_truth3->Branch("Ntrack", &Var["NTrack"]);
    tree_truth3->Branch("Weight_total", &Var["Weight"]);
    // channel setting
    tree_truth3->Branch("isReco1", &Var["isReco1"]);
    tree_truth3->Branch("isReco2", &Var["isReco2"]);
    tree_truth3->Branch("isReco3", &Var["isReco3"]);
    tree_truth3->Branch("isReco4", &Var["isReco4"]);
    tree_truth3->Branch("isReco5", &Var["isReco5"]);
    tree_truth3->Branch("isQCRReco1", &Var["isQCRReco1"]);
    tree_truth3->Branch("isQCRReco2", &Var["isQCRReco2"]);
    tree_truth3->Branch("isQCRReco3", &Var["isQCRReco3"]);
    tree_truth3->Branch("isQCRReco4", &Var["isQCRReco4"]);
    tree_truth3->Branch("isQCRReco5", &Var["isQCRReco5"]);
    // weight setting
    truth3->setWeight([&] { return Var["Weight"]; });
    truth3->setFillTree(tree_truth3);
    // simple cut
    truth3->registerCut("baseline", [&] { return Var["SR"] == 1 && Var["isW"] == 0 && Var["isData"] == 0; });
    truth3->registerCut("is truth recon3", [&] { return Var["TruthMode"] == 2; });
    truth3->registerCut("the END", [&] { return true; });

    // truth4 template
    auto truth4 = addCutflow();
    auto tree_truth4 = cloneCurrentAnaTree("Truth4_" + currentTreeName());
    // Fit Variable setting
    tree_truth4->Branch("upsilon", &Var["upsilon"]);
    tree_truth4->Branch("Ntrack", &Var["NTrack"]);
    tree_truth4->Branch("Weight_total", &Var["Weight"]);
    // channel setting
    tree_truth4->Branch("isReco1", &Var["isReco1"]);
    tree_truth4->Branch("isReco2", &Var["isReco2"]);
    tree_truth4->Branch("isReco3", &Var["isReco3"]);
    tree_truth4->Branch("isReco4", &Var["isReco4"]);
    tree_truth4->Branch("isReco5", &Var["isReco5"]);
    tree_truth4->Branch("isQCRReco1", &Var["isQCRReco1"]);
    tree_truth4->Branch("isQCRReco2", &Var["isQCRReco2"]);
    tree_truth4->Branch("isQCRReco3", &Var["isQCRReco3"]);
    tree_truth4->Branch("isQCRReco4", &Var["isQCRReco4"]);
    tree_truth4->Branch("isQCRReco5", &Var["isQCRReco5"]);
    // weight setting
    truth4->setWeight([&] { return Var["Weight"]; });
    truth4->setFillTree(tree_truth4);
    // simple cut
    truth4->registerCut("baseline", [&] { return Var["SR"] == 1 && Var["isW"] == 0 && Var["isData"] == 0; });
    truth4->registerCut("is truth recon4", [&] { return Var["TruthMode"] == 3; });
    truth4->registerCut("the END", [&] { return true; });

    // truth5 template
    auto truth5 = addCutflow();
    auto tree_truth5 = cloneCurrentAnaTree("Truth5_" + currentTreeName());
    // Fit Variable setting
    tree_truth5->Branch("upsilon", &Var["upsilon"]);
    tree_truth5->Branch("Ntrack", &Var["NTrack"]);
    tree_truth5->Branch("Weight_total", &Var["Weight"]);
    // channel setting
    tree_truth5->Branch("isReco1", &Var["isReco1"]);
    tree_truth5->Branch("isReco2", &Var["isReco2"]);
    tree_truth5->Branch("isReco3", &Var["isReco3"]);
    tree_truth5->Branch("isReco4", &Var["isReco4"]);
    tree_truth5->Branch("isReco5", &Var["isReco5"]);
    tree_truth5->Branch("isQCRReco1", &Var["isQCRReco1"]);
    tree_truth5->Branch("isQCRReco2", &Var["isQCRReco2"]);
    tree_truth5->Branch("isQCRReco3", &Var["isQCRReco3"]);
    tree_truth5->Branch("isQCRReco4", &Var["isQCRReco4"]);
    tree_truth5->Branch("isQCRReco5", &Var["isQCRReco5"]);
    // weight setting
    truth5->setWeight([&] { return Var["Weight"]; });
    truth5->setFillTree(tree_truth5);
    // simple cut
    truth5->registerCut("baseline", [&] { return Var["SR"] == 1 && Var["isW"] == 0 && Var["isData"] == 0; });
    truth5->registerCut("is truth recon5", [&] { return Var["TruthMode"] == 4; });
    truth5->registerCut("the END", [&] { return true; });

    // Lep template
    auto lep = addCutflow();
    auto tree_lep = cloneCurrentAnaTree("Lep_" + currentTreeName());
    // Fit Variable setting
    tree_lep->Branch("upsilon", &Var["upsilon"]);
    tree_lep->Branch("Ntrack", &Var["NTrack"]);
    tree_lep->Branch("Weight_total", &Var["Weight"]);
    // channel setting
    tree_lep->Branch("isReco1", &Var["isReco1"]);
    tree_lep->Branch("isReco2", &Var["isReco2"]);
    tree_lep->Branch("isReco3", &Var["isReco3"]);
    tree_lep->Branch("isReco4", &Var["isReco4"]);
    tree_lep->Branch("isReco5", &Var["isReco5"]);
    tree_lep->Branch("isQCRReco1", &Var["isQCRReco1"]);
    tree_lep->Branch("isQCRReco2", &Var["isQCRReco2"]);
    tree_lep->Branch("isQCRReco3", &Var["isQCRReco3"]);
    tree_lep->Branch("isQCRReco4", &Var["isQCRReco4"]);
    tree_lep->Branch("isQCRReco5", &Var["isQCRReco5"]);
    // weight setting
    lep->setWeight([&] { return Var["Weight"]; });
    lep->setFillTree(tree_lep);
    // simple cut
    lep->registerCut("baseline", [&] { return Var["SR"] == 1 && Var["isW"] == 0 && Var["isData"] == 0; });
    lep->registerCut("is Lep", [&] { return Var["isLep"] == 1; });
    lep->registerCut("the END", [&] { return true; });

    // Trees Only apply to NOMINAL, need to remove extra branch for data tree
    if (currentTreeName() == "NOMINAL") {
        // Data template
        auto Data = addCutflow();
        auto tree_Data = cloneCurrentAnaTree("Data_" + currentTreeName());
        // Fit Variable setting
        tree_Data->Branch("upsilon", &Var["upsilon"]);
        tree_Data->Branch("Ntrack", &Var["NTrack"]);
        tree_Data->Branch("Weight_total", &Var["Weight"]);
        // channel setting
        tree_Data->Branch("isReco1", &Var["isReco1"]);
        tree_Data->Branch("isReco2", &Var["isReco2"]);
        tree_Data->Branch("isReco3", &Var["isReco3"]);
        tree_Data->Branch("isReco4", &Var["isReco4"]);
        tree_Data->Branch("isReco5", &Var["isReco5"]);
        tree_Data->Branch("isQCRReco1", &Var["isQCRReco1"]);
        tree_Data->Branch("isQCRReco2", &Var["isQCRReco2"]);
        tree_Data->Branch("isQCRReco3", &Var["isQCRReco3"]);
        tree_Data->Branch("isQCRReco4", &Var["isQCRReco4"]);
        tree_Data->Branch("isQCRReco5", &Var["isQCRReco5"]);

        // weight setting
        Data->setWeight([&] { return Var["Weight"]; });
        Data->setFillTree(tree_Data);
        // simple cut
        Data->registerCut("baseline", [&] { return Var["SR"] == 1 && Var["isW"] == 0; });
        Data->registerCut("is Data", [&] { return Var["isData"] == 1; });
        Data->registerCut("the END", [&] { return true; });

        // W template, using Data in WCR to represent, need TF in HF
        auto W = addCutflow();
        auto tree_W = cloneCurrentAnaTree("W_" + currentTreeName());
        // Fit Variable setting
        tree_W->Branch("upsilon", &Var["upsilon"]);
        tree_W->Branch("Ntrack", &Var["NTrack"]);
        tree_W->Branch("Weight_total", &Var["Weight"]);
        // channel setting
        tree_W->Branch("isReco1", &Var["isReco1"]);
        tree_W->Branch("isReco2", &Var["isReco2"]);
        tree_W->Branch("isReco3", &Var["isReco3"]);
        tree_W->Branch("isReco4", &Var["isReco4"]);
        tree_W->Branch("isReco5", &Var["isReco5"]);
        tree_W->Branch("isQCRReco1", &Var["isQCRReco1"]);
        tree_W->Branch("isQCRReco2", &Var["isQCRReco2"]);
        tree_W->Branch("isQCRReco3", &Var["isQCRReco3"]);
        tree_W->Branch("isQCRReco4", &Var["isQCRReco4"]);
        tree_W->Branch("isQCRReco5", &Var["isQCRReco5"]);

        // weight setting
        W->setWeight([&] { return Var["Weight"]; });
        W->setFillTree(tree_W);
        // simple cut
        W->registerCut("baseline", [&] { return Var["WCR"] == 1; });
        W->registerCut("is Data", [&] { return Var["isData"] == 1; });
        W->registerCut("the END", [&] { return true; });

        // Normal QCD template, W part at another program, need TF in HF
        auto QCD_noW = addCutflow();
        auto tree_QCD_noW = cloneCurrentAnaTree("QCD_" + currentTreeName());
        // Fit Variable setting
        tree_QCD_noW->Branch("upsilon", &Var["upsilon"]);
        tree_QCD_noW->Branch("Ntrack", &Var["NTrack"]);
        tree_QCD_noW->Branch("Weight_total", &Var["QCDWeight"]);
        // channel setting
        tree_QCD_noW->Branch("isReco1", &Var["isReco1"]);
        tree_QCD_noW->Branch("isReco2", &Var["isReco2"]);
        tree_QCD_noW->Branch("isReco3", &Var["isReco3"]);
        tree_QCD_noW->Branch("isReco4", &Var["isReco4"]);
        tree_QCD_noW->Branch("isReco5", &Var["isReco5"]);
        tree_QCD_noW->Branch("isQCRReco1", &Var["isQCRReco1"]);
        tree_QCD_noW->Branch("isQCRReco2", &Var["isQCRReco2"]);
        tree_QCD_noW->Branch("isQCRReco3", &Var["isQCRReco3"]);
        tree_QCD_noW->Branch("isQCRReco4", &Var["isQCRReco4"]);
        tree_QCD_noW->Branch("isQCRReco5", &Var["isQCRReco5"]);

        // Syst seting
        tree_QCD_noW->Branch("jet_uncert_W_TF_SS_up", &Var["W_SS_TF_up"]);
        tree_QCD_noW->Branch("jet_uncert_W_TF_SS_dw", &Var["W_SS_TF_dw"]);
        // weight setting
        QCD_noW->setWeight([&] { return Var["QCDWeight"]; });
        QCD_noW->setFillTree(tree_QCD_noW);
        // simple cut
        QCD_noW->registerCut("baseline", [&] { return Var["SRSS"] == 1; });
        QCD_noW->registerCut("is not W", [&] { return Var["isW"] == 0; });
        QCD_noW->registerCut("the END", [&] { return true; });
    }

    // Start the loop!
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);
        Var["upsilon"] = (2 * tree->tau_0_allTrk_pt - tree->tau_0_p4->Pt()) / tree->tau_0_p4->Pt();
        Var["NTrack"] = tree->tau_0_n_charged_tracks + tree->tau_0_n_conversion_tracks + tree->tau_0_n_isolation_tracks;
        Var["Mu"] = tree->n_avg_int_cor;
        Var["TauPt"] = tree->tau_0_p4->Pt();
        Var["TauEta"] = fabs(tree->tau_0_p4->Eta());
        Var["MuPt"] = tree->lep_0_p4->Pt();
        Var["MuEta"] = fabs(tree->lep_0_p4->Eta());
        Var["LepHadVisMass"] = tree->lephad_p4->M();
        Var["isData"] = (tree->mc_channel_number == 0);
        Var["isW"] = (tree->mc_channel_number >= 361100 && tree->mc_channel_number <= 361105);
        Var["isLep"] = (tree->tau_0_truth_isEle || tree->tau_0_truth_isMuon);
        Var["TruthMode"] = tree->tau_0_truth_decay_mode;
        Var["RecoMode"] = tree->tau_0_decay_mode;

        if (Var["tauIDCheckBox"] == 1) {
            Var["tauID"] = (tree->tau_0_jet_bdt_loose == 1 && tree->tau_0_jet_bdt_medium == 0);
        } else if (Var["tauIDCheckBox"] == 2) {
            Var["tauID"] = (tree->tau_0_jet_bdt_medium == 1 && tree->tau_0_jet_bdt_tight == 0);
        } else if (Var["tauIDCheckBox"] == 3) {
            Var["tauID"] = (tree->tau_0_jet_bdt_tight == 1);
        } else if (Var["tauIDCheckBox"] == 4) {
            Var["tauID"] = (tree->tau_0_jet_rnn_loose == 1 && tree->tau_0_jet_rnn_medium == 0);
        } else if (Var["tauIDCheckBox"] == 5) {
            Var["tauID"] = (tree->tau_0_jet_rnn_medium == 1 && tree->tau_0_jet_rnn_tight == 0);
        } else if (Var["tauIDCheckBox"] == 6) {
            Var["tauID"] = (tree->tau_0_jet_rnn_tight == 1);
        } else {
            Var["tauID"] = (true);
        }

        Var["isReco1"] = tree->tau_0_decay_mode == 0;
        Var["isReco2"] = tree->tau_0_decay_mode == 1;
        Var["isReco3"] = tree->tau_0_decay_mode == 2;
        Var["isReco4"] = tree->tau_0_decay_mode == 3;
        Var["isReco5"] = tree->tau_0_decay_mode == 4;
        // Weight and compaign setting
        if (tree->mc_channel_number == 0) {
            Var["Weight"] = 1;
        } else {
            double extraWei = tree->weight_mc * tree->NOMINAL_pileup_combined_weight * tree->jet_NOMINAL_forward_jets_global_effSF_JVT *
                              tree->jet_NOMINAL_forward_jets_global_ineffSF_JVT * tree->jet_NOMINAL_central_jets_global_effSF_JVT *
                              tree->jet_NOMINAL_central_jets_global_ineffSF_JVT * tree->tau_0_NOMINAL_TauEffSF_HadTauEleOLR_tauhad *
                              tree->tau_0_NOMINAL_TauEffSF_VeryLooseLlhEleOLR_electron * tree->jet_NOMINAL_global_ineffSF_MV2c10 *
                              tree->jet_NOMINAL_global_effSF_MV2c10 * tree->lep_0_NOMINAL_MuEffSF_Reco_QualMedium * tree->lep_0_NOMINAL_MuEffSF_TTVA *
                              tree->tau_0_NOMINAL_TauEffSF_reco * tree->lep_0_NOMINAL_MuEffSF_IsoFCTightTrackOnly_FixedRad *
                              tree->lep_0_NOMINAL_MuEffSF_HLT_mu26_ivarmedium_OR_HLT_mu50_QualMedium_IsoNone;
            Var["Weight"] =
                MetaDB::Instance().getWeight(PhyUtils::getCompaign(tree->NOMINAL_pileup_random_run_number), tree->mc_channel_number, 0, extraWei);
        }
        Var["QCDWeight"] = (Var["isData"] == 1) ? ((double)Var["Weight"]) : ((double)Var["Weight"] * -1);

        bool MuonSelection = (tree->lep_0_id_medium == 1 && Var["MuEta"] < 2.5 &&
                              ((tree->HLT_mu26_ivarmedium == 1 && tree->lep_0_p4->Pt() > 27.3 && tree->muTrigMatch_0_HLT_mu26_ivarmedium == 1) ||
                               (tree->HLT_mu50 == 1 && tree->lep_0_p4->Pt() > 52.5 && tree->muTrigMatch_0_HLT_mu50)));

        double DPhi = fabs(tree->tau_0_p4->Phi() - tree->lep_0_p4->Phi());
        if (DPhi > M_PI) DPhi = 2 * M_PI - DPhi;

        bool TauSelection = (Var["tauID"] && tree->tau_0_jet_bdt_score_trans > 0.005 && Var["TauPt"] > 20 && abs(tree->tau_0_q) == 1 &&
                             (tree->tau_0_n_charged_tracks == 1 || tree->tau_0_n_charged_tracks == 3) &&
                             ((Var["TauEta"] < 1.37) || (1.52 < Var["TauEta"] && Var["TauEta"] < 2.47)));
        bool isOS = (tree->lephad_qxq == -1);

        Var["SR"] = (MuonSelection && tree->lep_0_iso_FCTightTrackOnly_FixedRad && TauSelection && tree->n_bjets == 0 &&
                     tree->lephad_mt_lep0_met < 50 && tree->lephad_p4->M() < 120 && tree->lephad_p4->M() > 45 && DPhi > 2.4 &&
                     tree->lep_0_p4->Pt() < 40 && tree->lephad_met_sum_cos_dphi > -0.15 && isOS);
        Var["SRSS"] = (MuonSelection && tree->lep_0_iso_FCTightTrackOnly_FixedRad && TauSelection && tree->n_bjets == 0 &&
                       tree->lephad_mt_lep0_met < 50 && tree->lephad_p4->M() < 120 && tree->lephad_p4->M() > 45 && DPhi > 2.4 &&
                       tree->lep_0_p4->Pt() < 40 && tree->lephad_met_sum_cos_dphi > -0.15 && !isOS);
        Var["QCR"] = (MuonSelection && !tree->lep_0_iso_FCTightTrackOnly_FixedRad && TauSelection && tree->n_bjets == 0 &&
                      tree->lephad_mt_lep0_met < 50 && tree->lephad_p4->M() < 120 && tree->lephad_p4->M() > 45 && DPhi > 2.4 &&
                      tree->lep_0_p4->Pt() < 40 && tree->lephad_met_sum_cos_dphi > -0.15 && isOS);
        Var["QCRSS"] = (MuonSelection && !tree->lep_0_iso_FCTightTrackOnly_FixedRad && TauSelection && tree->n_bjets == 0 &&
                        tree->lephad_mt_lep0_met < 50 && tree->lephad_p4->M() < 120 && tree->lephad_p4->M() > 45 && DPhi > 2.4 &&
                        tree->lep_0_p4->Pt() < 40 && tree->lephad_met_sum_cos_dphi > -0.15 && !isOS);
        Var["WCR"] =
            (MuonSelection && tree->lep_0_iso_FCTightTrackOnly_FixedRad && TauSelection && tree->n_bjets == 0 && tree->lephad_mt_lep0_met > 60 &&
             tree->met_reco_sumet > 20 && tree->lephad_p4->M() < 120 && tree->lephad_p4->M() > 45 && tree->lephad_met_sum_cos_dphi < 0 && isOS);
        Var["WCRSS"] =
            (MuonSelection && tree->lep_0_iso_FCTightTrackOnly_FixedRad && TauSelection && tree->n_bjets == 0 && tree->lephad_mt_lep0_met > 60 &&
             tree->met_reco_sumet > 20 && tree->lephad_p4->M() < 120 && tree->lephad_p4->M() > 45 && tree->lephad_met_sum_cos_dphi < 0 && !isOS);
        fillRegions();
        fillCutflows();
    }
    // Print out for region, cutflows if needed
}
