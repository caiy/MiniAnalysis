#include "AnaObjs.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

DefineLooper(C1N2OS_FullJERPart2, XamppTree);

void C1N2OS_FullJERPart2::loop() {
    std::string outFullName = getOutName();
    std::string currentName = currentTreeName();
    currentName.replace(currentName.find("__2"), 3, "__1");
    auto oTree = cloneCurrentAnaTree(currentName);

    auto mCutflow = addCutflow();
    // define cuts
    mCutflow->setWeight([&] { return -1 * tree->Weight_mc; });
    mCutflow->setFillTree(oTree);
    mCutflow->registerCut("baseline", [&] { return true; });
    mCutflow->registerCut("OS", [&] { return true; });

    // Loop Start
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);
        tree->Weight_mc *= -1;

        fillRegions();
        fillCutflows();
    }
    for (auto cut : getCutflows()) {
        cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
    }
}
