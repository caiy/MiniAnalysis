#include "AnaObjs.h"
#include "GRLDB.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

/* Slimmer for SR, without QCD */

//#include "XamppTree.h"
DefineLooper(C1N2OS_SR_Slimmer_Simplified, XamppTree);

void C1N2OS_SR_Slimmer_Simplified::loop() {
    std::string outFullName = getOutName();
    std::string treeName = currentTreeName();
    LOG(INFO) << "currentTree name: " << treeName;

    // auto oTree = cloneCurrentAnaTree(outTree);
    auto oTree = cloneCurrentAnaTree();

    Cutflow_ptr mCutflow = addCutflow();

    Var["tau1Pt"] = 0;
    Var["tau1Mt"] = 0;
    Var["tau2Pt"] = 0;
    Var["tau2Mt"] = 0;
    Var["dPhitt"] = 0;
    Var["dRtt"] = 0;
    Var["mt12tau"] = 0;
    Var["meff_tau"] = 0;
    Var["Mtt_12"] = 0;
    Var["MT2"] = 0;
    Var["met_sig"] = 0;
    Var["met_sig_tj"] = 0;
    Var["evt_MET"] = 0;

    Var["dPhijj"] = 0;
    Var["dRjj"] = 0;
    Var["dEtajj"] = 0;
    Var["EtajEtaj"] = 0;
    Var["jet1Pt"] = 0;
    Var["jet2Pt"] = 0;
    Var["jet1Eta"] = 0;
    Var["jet2Eta"] = 0;
    Var["jet1Mt"] = 0;
    Var["jet2Mt"] = 0;
    Var["fabs_jet1Eta"] = 0;
    Var["fabs_jet2Eta"] = 0;
    Var["bVeto"] = 0;
    Var["bNumber"] = 0;
    Var["totalWeight"] = 1;

    Var["n_signal_jets"] = 0;

    int nTightTau(0), nTaus(0), Ljet_n(0), mergedRunNumber(0), mlumiBlock(0), nJet(0), Nemu(0);
    bool tauTriger, OS2Tau, zVeto, mllCut, mt2Cut, noemu;

    oTree->Branch("Weight_mc", &Var["totalWeight"]);
    oTree->Branch("mergedRunNumber", &mergedRunNumber);
    oTree->Branch("mergedlumiBlock", &mlumiBlock);

    mCutflow->setWeight([&] { return Var["totalWeight"]; });
    mCutflow->setFillTree(oTree);
    mCutflow->registerCut("baseline", [&] { return fabs(Var["totalWeight"]) < 1000; });
    mCutflow->registerCut(">= 2 medium tau", [&] { return nTaus >= 2; });
    mCutflow->registerCut(
        "bVeto", [&] { return Var["bVeto"]; }, "bTag", 2, 0, 2, [&] { return Var["bNumber"]; });
    mCutflow->registerCut("MET>60 GeV", [&] { return Var["evt_MET"] >= 60; });
    mCutflow->registerCut("MT2>70 GeV", [&] { return Var["MT2"] >= 70; });

    auto lastCut = mCutflow->registerCut("the END", [&] { return true; });

    // Loop Start
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);

        Var["tau1Pt"] = 0;
        Var["tau1Mt"] = 0;
        Var["tau2Pt"] = 0;
        Var["tau2Mt"] = 0;
        Var["dPhitt"] = 999;
        Var["dRtt"] = 999;
        Var["mt12tau"] = -999;
        Var["meff_tau"] = -999;
        Var["Mtt_12"] = 0;
        Var["MT2"] = 0;
        Var["met_sig"] = -1;
        Var["met_sig_tj"] = -1;
        Var["evt_MET"] = 0;

        Var["dPhijj"] = 0;
        Var["dRjj"] = 0;
        Var["dEtajj"] = 0;
        Var["EtajEtaj"] = 0;
        Var["jet1Pt"] = 0;
        Var["jet2Pt"] = 0;
        Var["jet1Eta"] = 0;
        Var["jet2Eta"] = 0;
        Var["fabs_jet1Eta"] = 0;
        Var["fabs_jet2Eta"] = 0;
        Var["totalWeight"] = 1;
        Var["jet1Mt"] = 0;
        Var["jet2Mt"] = 0;
        nTightTau = 0;
        nTaus = 0;
        Ljet_n = 0;
        mergedRunNumber = 0;
        nJet = 0;
        tauTriger = false;
        Var["bVeto"] = false;
        OS2Tau = false, zVeto = false, noemu = true;
        Var["bNumber"] = 0;
        Var["evt_MET"] = tree->MetTST_met / 1000;

        if (tree->mcChannelNumber == 0) {
            Var["totalWeight"] = 1;
            mergedRunNumber = tree->runNumber;
            mlumiBlock = tree->lumiBlock;
        } else {
            mergedRunNumber = tree->RandomRunNumber;
            mlumiBlock = tree->RandomLumiBlockNumber;
        }

        // if (tree->mcChannelNumber != 0) {
        double extraWei = tree->GenWeight * tree->muWeight * tree->TauWeight * tree->EleWeight * tree->MuoWeight * tree->JetWeight;
        Var["totalWeight"] = MetaDB::Instance().getWeight(PhyUtils::getCompaign(mergedRunNumber), 999999, 0, extraWei);
        //}

        ///*******************************Event construct *********************/
        AnaObjs eleVec = tree->getElectrons();
        AnaObjs muonVec = tree->getMuons();
        AnaObjs tauVec = tree->getTaus();
        AnaObjs jetVec = tree->getJets();

        Var["bVeto"] = (tree->n_BJets == 0); // b-veto
        Var["bNumber"] = tree->n_BJets;      // b-veto
        noemu = ((tree->n_SignalMuon + tree->n_SignalElec) == 0);
        Nemu = tree->n_SignalMuon + tree->n_SignalElec;

        // Trigger
        //		GRLDB mGRLDB;
        //		mGRLDB.loadGRLFile("/publicfs/atlas/atlasnew/SUSY/users/zhucz/framework/run2Ditau/include/ditauMETGRL.xml");
        //		bool useOldtrigger(true);
        //		if(mergedRunNumber >= 325713 && mergedRunNumber <=340453){  // 17 data/MC
        //			if(tree->mcChannelNumber != 0){
        //				if(!mGRLDB.checkInGRL(mergedRunNumber,tree->RandomLumiBlockNumber)){
        //					useOldtrigger = false;
        //				}
        //			} else{
        //				if(!mGRLDB.checkInGRL(mergedRunNumber,tree->lumiBlock)){
        //					useOldtrigger = false;
        //				}
        //			}
        //		}
        // e if(( mergedRunNumber >= 348885 )){}
        // d else if( mergedRunNumber >= 325713 && mergedRunNumber <= 340453 ){}
        // a else if( mergedRunNumber <= 311481){}
        //
        //		if( mergedRunNumber > 348885 ){
        //			tauVec = tauVec.passTrig(( tree->TrigHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40 ),
        //*(tree->taus_TrigMatchHLT_tau80_medium1_tracktwoEF_L1TAU60_tau60_medium1_tracktwoEF_L1TAU40), {95,75} );
        //		}
        //		else if( !useOldtrigger ){
        //			tauVec = tauVec.passTrig(( tree->TrigHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50 ),
        //*(tree->taus_TrigMatchHLT_tau60_medium1_tracktwo_tau25_medium1_tracktwo_xe50),{75,40} );
        //		}
        //		else{
        //			tauVec = tauVec.passTrig(( tree->TrigHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12),
        //*(tree->taus_TrigMatchHLT_tau80_medium1_tracktwo_L1TAU60_tau50_medium1_tracktwo_L1TAU12 ) , {95,65}); 			if(tree->mcChannelNumber
        //!= 0){ 				Var["totalWeight"] *= tree->TauWeightTrigHLT_tau80L1TAU60_medium1_tracktwo *
        // tree->TauWeightTrigHLT_tau50L1TAU12_medium1_tracktwo;
        //			}
        //		}
        //		tauTriger = (!(tauVec.size()==0));
        // Taus
        tauVec = tauVec.filterObjects(0, 100, TauMedium);
        nTaus = tauVec.size();
        // auto tightTauVec = tauVec.filterObjects(0,100,TauTight);
        // nTightTau = tightTauVec.size();
        if (nTaus >= 2) {
            auto tau1 = tauVec[0];
            auto tau2 = tauVec[1];
            // offline PT select
            TLorentzVector METVec;
            METVec.SetPtEtaPhiE(Var["evt_MET"], 0, tree->MetTST_phi, Var["evt_MET"]);
            OS2Tau = tau1.isOS(tauVec[1]);
            Var["tau1Pt"] = tau1.Pt();
            Var["tau1Mt"] = tau1.Mt();
            Var["tau2Pt"] = tau2.Pt();
            Var["tau2Mt"] = tau2.Mt();
            //			Var["dRtt"] = PhyUtils::deltaR(tau1, tau2);
            //			Var["dPhitt"] = PhyUtils::deltaPhi(tau1, tau2);
            Var["Mtt_12"] = PhyUtils::calcMll(tau1, tau2);
            //			Var["meff_tau"] = tau1.Pt() + tau2.Pt() + Var["evt_MET"];
            //			Var["mct"] = PhyUtils::calcMCT(tau1, tau2);
            Var["MT2"] = PhyUtils::MT2Max(tauVec, METVec);
            //			Var["mt12tau"] = Var["tau1Mt"] + Var["tau2Mt"];
            //			Var["met_sig"] = Var["evt_MET"] / (sqrt( Var["tau1Pt"] + Var["tau2Pt"]));
        }

        fillRegions();
        fillCutflows();
    }
    for (auto cut : getCutflows()) {
        cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
    }
}
