#include "AnaObjs.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

DefineLooper(Wh_First_Slimmer_Simplified, XamppTree);

void Wh_First_Slimmer_Simplified::loop() {
    std::string outFullName = getOutName();
    std::string treeName = currentTreeName();
    // "Staus" length is 5
    std::string outTree = treeName.replace(treeName.find("Staus"), 5, Utils::splitStrBy(outFullName, '.')[0]);
    LOG(DEBUG) << "currentTree name: " << treeName << ", outTree name: " << outTree;

    auto oTree = cloneCurrentAnaTree(outTree);

    Cutflow_ptr mCutflow = nullptr;
    if (treeName.find("Nominal") != std::string::npos) {
        mCutflow = addCutflow();
    } else {
        mCutflow = addCutflow(Cutflow::NO_HIST);
    }
    // for cut
    Var["totalWeight"] = 1;
    Var["1Lepton"] = 0;
    Var["nLepton"] = 0;
    Var["bVeto"] = 0;
    Var["2MediumTau"] = 0;
    Var["OS2Tau"] = 0;
    // for histo
    Var["dRtt"] = 999;
    Var["dPhitt"] = 999;
    Var["tau1Pt"] = 0;
    Var["tau2Pt"] = 0;
    Var["lepPt"] = 0;
    Var["lepEta"] = 0;
    Var["Ht_Tau"] = 0;
    Var["Ht_Lep"] = 0;
    Var["Ht_Jet"] = 0;
    Var["mT2"] = 0;
    Var["MET"] = 0;
    Var["pretauPt"] = 0;
    Var["Mtt"] = 0;
    Var["lepMt"] = 0;
    Var["tau1Mt"] = 0;
    Var["tau2Mt"] = 0;
    Var["meff"] = 0;
    Var["METmeff"] = 0;
    Var["dRLepTwoTau"] = 999;
    Var["dPhiLepTwoTau"] = 999;
    Var["dRt1MET"] = 999;
    Var["dRt2MET"] = 999;
    Var["dRlepMET"] = 999;
    Var["dR2TauMET"] = 999;
    Var["dPhit1MET"] = 999;
    Var["dPhit2MET"] = 999;
    Var["dPhilepMET"] = 999;
    Var["dPhi2TauMET"] = 999;
    Var["TwotauPt"] = 0;
    Var["MET_sig"] = 0;
    Var["MET_sig_Lep"] = 0;
    Var["mct"] = 0;
    Var["MTsum"] = 0;
    int Njet(0), NTightTau(0), mRunnum(0), mlumiBlock(0), nTruthTau, nTotal, nMedTau;

    // set mc channel number to 0 initially to seperate data and mc
    tree->mcChannelNumber = 0;
    // clean conflict tree

    oTree->Branch("mergedRunNumber", &mRunnum);
    oTree->Branch("mergedlumiBlock", &mlumiBlock);
    oTree->Branch("Weight_mc", &Var["totalWeight"]);
    oTree->Branch("dRtt", &Var["dRtt"]);
    oTree->Branch("dPhitt", &Var["dPhitt"]);
    oTree->Branch("MTsum", &Var["MTsum"]);
    oTree->Branch("METmeff", &Var["METmeff"]);
    oTree->Branch("TwotauPt", &Var["TwotauPt"]);
    oTree->Branch("dRLepTwoTau", &Var["dRLepTwoTau"]);
    oTree->Branch("dPhiLepTwoTau", &Var["dPhiLepTwoTau"]);
    oTree->Branch("dRLepTwoTau", &Var["dRLepTwoTau"]);
    oTree->Branch("dPhiLepTwoTau", &Var["dPhiLepTwoTau"]);
    oTree->Branch("dRt1MET", &Var["dRt1MET"]);
    oTree->Branch("dRt2MET", &Var["dRt2MET"]);
    oTree->Branch("dRlepMET", &Var["dRlepMET"]);
    oTree->Branch("dR2TauMET", &Var["dR2TauMET"]);
    oTree->Branch("dPhit1MET", &Var["dPhit1MET"]);
    oTree->Branch("dPhit2MET", &Var["dPhit2MET"]);
    oTree->Branch("dPhilepMET", &Var["dPhilepMET"]);
    oTree->Branch("dPhi2TauMET", &Var["dPhi2TauMET"]);
    oTree->Branch("tau1Pt", &Var["tau1Pt"]);
    oTree->Branch("tau2Pt", &Var["tau2Pt"]);
    oTree->Branch("lepPt", &Var["lepPt"]);
    oTree->Branch("mT2", &Var["mT2"]);
    oTree->Branch("MET", &Var["MET"]);
    oTree->Branch("Mtt", &Var["Mtt"]);
    oTree->Branch("lepMt", &Var["lepMt"]);
    oTree->Branch("tau1Mt", &Var["tau1Mt"]);
    oTree->Branch("tau2Mt", &Var["tau2Mt"]);
    oTree->Branch("MET_sig", &Var["MET_sig"]);
    oTree->Branch("meff", &Var["meff"]);
    oTree->Branch("mct", &Var["mct"]);
    oTree->Branch("MET_sig_Lep", &Var["MET_sig_Lep"]);
    oTree->Branch("Njet", &Njet);
    oTree->Branch("NTightTau", &NTightTau);

    mCutflow->setWeight([&] { return Var["totalWeight"]; });

    mCutflow->setFillTree(oTree);

    mCutflow->registerCut("baseline", [&] { return true; });
    auto total2 = mCutflow->registerCut("(tau + lep)>=2", [&] { return nTotal >= 2; });
    total2->addHist(
        "nTruthTau1", 4, 0, 4, [&] { return nTruthTau; }, Hist::USE_OVERFLOW);
    auto medium2 = mCutflow->registerCut(">=2MediumTau", [&] { return Var["2MediumTau"]; });
    medium2->addHist(
        "tau1Pt1", 20, 0, 200, [&] { return Var["tau1Pt"]; }, Hist::USE_OVERFLOW);
    medium2->addHist(
        "nLep1", 5, 0, 5, [&] { return Var["nLepton"]; }, Hist::USE_OVERFLOW);
    medium2->addHist(
        "nTruthTau2", 4, 0, 4, [&] { return nTruthTau; }, Hist::USE_OVERFLOW);
    auto lepCut = mCutflow->registerCut("1Lepton", [&] { return Var["1Lepton"]; });
    lepCut->addHist(
        "nTruthTau3", 4, 0, 4, [&] { return nTruthTau; }, Hist::USE_OVERFLOW);
    // mCutflow->registerCut("bVeto",[&] {return Var["bVeto"];},"bTag",2,0,2,[&]{return !Var["bVeto"];});
    mCutflow->registerCut(
        "dPhitt < 3", [&] { return Var["dPhitt"] < 3; }, "dPhitt", 20, 0, 4, [&] { return Var["dPhitt"]; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "40 < Mtt < 160", [&] { return Var["Mtt"] > 0 && Var["Mtt"] < 160; }, "Mtt", 20, 0, 400, [&] { return Var["Mtt"]; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "mT2 > 30", [&] { return Var["mT2"] > 0; }, "mT2", 20, 0, 200, [&] { return Var["mT2"]; }, Hist::USE_OVERFLOW);
    auto preselect = mCutflow->registerCut("The END", [&] { return true; });
    preselect->addHist(
        "nLep", 5, 0, 5, [&] { return Var["nLepton"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "MET_sig", 20, 0, 40, [&] { return Var["MET_sig"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "MET_sig_Lep", 20, 0, 40, [&] { return Var["MET_sig_Lep"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "tau1Pt", 20, 0, 200, [&] { return Var["tau1Pt"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "tau2Pt", 20, 0, 200, [&] { return Var["tau2Pt"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "lepPt", 20, 0, 200, [&] { return Var["lepPt"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "lepEta", 20, 0, 4, [&] { return Var["lepEta"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "dRtt", 20, 0, 4, [&] { return Var["dRtt"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "Ht_Tau", 20, 0, 400, [&] { return Var["Ht_Tau"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "Ht_Lep", 20, 0, 400, [&] { return Var["Ht_Lep"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "Ht_Jet", 20, 0, 400, [&] { return Var["Ht_Jet"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "MET", 20, 0, 200, [&] { return Var["MET"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "METmeff", 20, 0, 1, [&] { return Var["METmeff"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "MTsum", 20, 0, 200, [&] { return Var["MTsum"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "TwotauPt", 20, 0, 300, [&] { return Var["TwotauPt"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "dRLepTwoTau", 20, 0, 4, [&] { return Var["dRLepTwoTau"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "dPhiLepTwoTau", 20, 0, 4, [&] { return Var["dPhiLepTwoTau"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "lepMt", 20, 0, 400, [&] { return Var["lepMt"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "tau1Mt", 20, 0, 400, [&] { return Var["tau1Mt"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "tau2Mt", 20, 0, 200, [&] { return Var["tau2Mt"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "meff", 20, 0, 600, [&] { return Var["meff"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "mct", 20, 0, 200, [&] { return Var["mct"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "NTightTau", 4, 0, 4, [&] { return NTightTau; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "NMedTau", 4, 0, 4, [&] { return nMedTau; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "Njet", 6, 0, 5, [&] { return Njet; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "dRt1MET", 20, 0, 4, [&] { return Var["dRt1MET"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "dRt2MET", 20, 0, 4, [&] { return Var["dRt2MET"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "dRlepMET", 20, 0, 4, [&] { return Var["dRlepMET"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "dR2TauMET", 20, 0, 4, [&] { return Var["dR2TauMET"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "dPhit1MET", 20, 0, 4, [&] { return Var["dPhit1MET"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "dPhit2MET", 20, 0, 4, [&] { return Var["dPhit2MET"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "dPhilepMET", 20, 0, 4, [&] { return Var["dPhilepMET"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "dPhi2TauMET", 20, 0, 4, [&] { return Var["dPhi2TauMET"]; }, Hist::USE_OVERFLOW);

    // Start the loop!
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);

        Var["1Lepton"] = false;
        Var["bVeto"] = false;
        Var["2MediumTau"] = false;
        Var["OS2Tau"] = false;
        Var["MET_sig"] = (tree->MetTST_met / 1000) / sqrt(tree->Ht_Tau / 1000 + tree->Ht_Lep / 1000 + tree->Ht_Jet / 1000);
        Var["MET_sig_Lep"] = (tree->MetTST_met / 1000) / sqrt(tree->Ht_Tau / 1000 + tree->Ht_Lep / 1000);
        Var["nLepton"] = 0;
        Var["dRtt"] = 999;
        Var["dPhitt"] = 999;
        Var["tau1Pt"] = 0;
        Var["pretauPt"] = 0;
        if (tree->taus_pt->size() >= 1) Var["pretauPt"] = (tree->taus_pt->at(0)) / 1000;
        Var["tau2Pt"] = 0;
        Var["lepPt"] = 0;
        Var["lepEta"] = 0;
        Var["Ht_Tau"] = tree->Ht_Tau / 1000;
        Var["Ht_Lep"] = tree->Ht_Lep / 1000;
        Var["Ht_Jet"] = tree->Ht_Jet / 1000;
        Var["mT2"] = 0;
        Var["MET"] = tree->MetTST_met / 1000;
        Var["Mtt"] = 0;
        Var["lepMt"] = 0;
        Var["tau1Mt"] = 0;
        Var["tau2Mt"] = 0;
        Var["meff"] = 0;
        Var["mct"] = 0;
        Njet = 0, NTightTau = 0;
        Var["METmeff"] = 0;
        Var["dRLepTwoTau"] = 999;
        Var["dPhiLepTwoTau"] = 999;
        Var["TwotauPt"] = 0;
        Var["MTsum"] = 0;
        Var["dRt1MET"] = 999;
        Var["dRt2MET"] = 999;
        Var["dRlepMET"] = 999;
        Var["dR2TauMET"] = 999;
        Var["dPhit1MET"] = 999;
        Var["dPhit2MET"] = 999;
        Var["dPhilepMET"] = 999;
        Var["dPhi2TauMET"] = 999;

        int MCcompaign;
        // Weight setting
        if (tree->mcChannelNumber == 0) {
            Var["totalWeight"] = 1;
            mRunnum = tree->runNumber;
            mlumiBlock = tree->lumiBlock;
        } else {
            mRunnum = tree->RandomRunNumber;
            mlumiBlock = tree->RandomLumiBlockNumber;
        }
        // if(tree->mcChannelNumber != 0){
        double extraWei = tree->GenWeight * tree->muWeight * tree->TauWeight * tree->EleWeight * tree->MuoWeight * tree->JetWeight;
        Var["totalWeight"] = MetaDB::Instance().getWeight(PhyUtils::getCompaign(mRunnum), 999999, 0, extraWei);
        //}
        /*******************************Event construct *********************/
        AnaObjs eleVec = tree->getElectrons();
        AnaObjs muonVec = tree->getMuons();
        AnaObjs tauVec = tree->getTaus();

        bool oneBaseLep = ((eleVec.size() + muonVec.size()) == 1);
        // Jet is not important so we only count bjet and total jet num
        Var["bVeto"] = tree->n_BJets == 0;

        /********************************Now start the preselect! ****************/
        // trigger
        // Ele, temporary no trig match vec, do the total match
        eleVec =
            eleVec.passTrig((mRunnum < 290000 && tree->TrigHLT_e24_lhmedium_L1EM20VH && tree->TrigMatchHLT_e24_lhmedium_L1EM20VH), 25) +
            eleVec.passTrig((mRunnum < 290000 && tree->TrigHLT_e60_lhmedium && tree->TrigMatchHLT_e60_lhmedium), 61) +
            eleVec.passTrig((mRunnum < 290000 && tree->TrigHLT_e120_lhloose && tree->TrigMatchHLT_e120_lhloose), 121) +
            eleVec.passTrig((mRunnum >= 290000 && tree->TrigHLT_e26_lhtight_nod0_ivarloose && tree->TrigMatchHLT_e26_lhtight_nod0_ivarloose), 27) +
            eleVec.passTrig((mRunnum >= 290000 && tree->TrigHLT_e60_lhmedium_nod0 && tree->TrigMatchHLT_e60_lhmedium_nod0), 61) +
            eleVec.passTrig((mRunnum >= 290000 && tree->TrigHLT_e140_lhloose_nod0 && tree->TrigMatchHLT_e140_lhloose_nod0), 141);

        if (eleVec.size() > 0 && tree->mcChannelNumber != 0) {
            Var["totalWeight"] *=
                tree->EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0;
        }
        // muon, same as ele
        if (mRunnum < 290000) { // 2015
            muonVec = muonVec.passTrig(tree->TrigHLT_mu20_iloose_L1MU15 && tree->TrigMatchHLT_mu20_iloose_L1MU15, 21) +
                      muonVec.passTrig(tree->TrigHLT_mu50 && tree->TrigMatchHLT_mu50, 52.5);
            if (muonVec.size() > 0 && tree->mcChannelNumber != 0) Var["totalWeight"] *= tree->MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40;
        } else {
            muonVec = muonVec.passTrig(tree->TrigHLT_mu26_ivarmedium && tree->TrigMatchHLT_mu26_ivarmedium, 27.3) +
                      muonVec.passTrig(tree->TrigHLT_mu50 && tree->TrigMatchHLT_mu50, 52.5);
            if (muonVec.size() > 0 && tree->mcChannelNumber != 0) Var["totalWeight"] *= tree->MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50;
        }
        // start select the target ID objs and trigger offline requirement
        // electron
        eleVec = eleVec.filterObjects(0, 100, EIsoGood);
        // muon. Should filter events with Eta < 2.5 to avoid mismodeling
        muonVec = muonVec.filterObjects(0, 2.5, MuIsoGood);

        // tau, select medium ones
        tauVec = tauVec.filterObjects(0, 100, TauMedium);
        nTruthTau = (tauVec.filterObjects(0, 100, TauIsTruthTau)).size();

        // Only select truth taus for MC Bkg because only they will apply FF
        if (tree->mcChannelNumber != 0 && (treeName.find("Data_") == std::string::npos || treeName.find("Wh_") == std::string::npos ||
                                           treeName.find("Sig_") == std::string::npos)) {
            Var["MCtruth"] = (nTruthTau >= 1);
        }

        auto tighttauVec = tauVec.filterObjects(0, 100, TauTight);
        Njet = tree->jets_signal->size();

        auto lepVec = eleVec + muonVec;
        Var["nLepton"] = lepVec.size();
        Var["1Lepton"] = (oneBaseLep && lepVec.size() == 1);
        Var["2MediumTau"] = (tauVec.size() >= 2);
        nTotal = lepVec.size() + tauVec.size();
        nMedTau = tauVec.size();
        if (tauVec.size() >= 2) {
            Var["tau1Pt"] = tauVec[0].Pt();
        }
        NTightTau = tighttauVec.size();
        if (Var["2MediumTau"] && Var["1Lepton"]) {
            auto tau1 = tauVec[0];
            auto tau2 = tauVec[1];
            auto lep = lepVec[0];
            auto TwoTau = tau1 + tau2;
            auto SumObj = TwoTau + lep;
            Var["MTsum"] = SumObj.Mt();
            Var["TwotauPt"] = TwoTau.Pt();
            TLorentzVector METVec;
            METVec.SetPtEtaPhiE(Var["MET"], 0, tree->MetTST_phi, Var["MET"]);
            Var["OS2Tau"] = tau1.isOS(tauVec[1]);
            Var["tau1Pt"] = tau1.Pt();
            Var["tau1Mt"] = tau1.Mt();
            Var["tau2Pt"] = tau2.Pt();
            Var["tau2Mt"] = tau2.Mt();
            Var["lepPt"] = lep.Pt();
            Var["lepEta"] = fabs(lep.Eta());
            Var["lepMt"] = lep.Mt();
            Var["dRtt"] = PhyUtils::deltaR(tau1, tau2);
            Var["dRt1MET"] = PhyUtils::deltaR(tau1, METVec);
            Var["dRt2MET"] = PhyUtils::deltaR(tau2, METVec);
            Var["dRlepMET"] = PhyUtils::deltaR(lep, METVec);
            Var["dR2TauMET"] = PhyUtils::deltaR(TwoTau, METVec);
            Var["dPhitt"] = PhyUtils::deltaPhi(tau1, tau2);
            Var["dRLepTwoTau"] = PhyUtils::deltaR(TwoTau, lep);
            Var["dPhiLepTwoTau"] = PhyUtils::deltaPhi(TwoTau, lep);
            Var["dPhit1MET"] = PhyUtils::deltaPhi(tau1, METVec);
            Var["dPhit2MET"] = PhyUtils::deltaPhi(tau2, METVec);
            Var["dPhilepMET"] = PhyUtils::deltaPhi(lep, METVec);
            Var["dPhi2TauMET"] = PhyUtils::deltaPhi(TwoTau, METVec);
            Var["Mtt"] = PhyUtils::calcMll(tau1, tau2);
            Var["meff"] = tau1.Pt() + tau2.Pt() + lep.Pt() + Var["MET"];
            Var["METmeff"] = Var["MET"] / Var["meff"];
            Var["mct"] = PhyUtils::calcMCT(tau1, tau2);
            Var["mT2"] = PhyUtils::MT2Max(tauVec + lepVec, METVec);
        }

        fillRegions();
        fillCutflows();
    }
    // Print out for region, cutflows if needed
    if (treeName.find("Nominal") != std::string::npos) {
        for (auto cut : getCutflows()) {
            cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
        }
    }
}
