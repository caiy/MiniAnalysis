#include "AnaObjs.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

DefineLooper(Wh_TCR, XamppTree);

void Wh_TCR::loop() {
    Var["lightBDM_score"] = 0;
    Var["Weight"] = 0;
    Var["MCtruth"] = true;
    Var["bVeto"] = 0;
    Var["nBjets"] = 0;
    Var["Njets"] = 0;
    Var["OS2Tau"] = false;
    bool addFF = true;
    tree->mcChannelNumber = 0;

    std::string treeName = currentTreeName();
    std::string outTag = Utils::splitStrBy(currentTreeName(), 'N')[0];
    std::string treeTag = Utils::splitStrBy(currentTreeName(), '_').back();
    if (treeName.find("p0") != std::string::npos) {
        treeName.replace(treeName.find("p0"), 2, "");
        treeName.replace(treeName.find("p0"), 2, "");
    }
    if (treeName.find("p5") != std::string::npos) {
        treeName.replace(treeName.find("p5"), 2, ".5");
        treeName.replace(treeName.find("p5"), 2, ".5");
    }

    LOG(INFO) << treeName;
    auto oTree = cloneCurrentAnaTree(treeName);

    Bool_t isSRlow = false;
    Bool_t isSRhigh = false;
    Bool_t isTCRlow = true;
    Bool_t isTCRhigh = false;
    Bool_t isTVRlow = false;
    Bool_t isTVRhigh = false;
    Bool_t isBosonCRlow = false;
    Bool_t isBosonCRhigh = false;
    Bool_t isBosonVRlow = false;
    Bool_t isBosonVRhigh = false;
    double FF_up = 1, FF_dw = 1;

    oTree->Branch("isSRlow", &isSRlow);
    oTree->Branch("isSRhigh", &isSRhigh);
    oTree->Branch("isTCRlow", &isTCRlow);
    oTree->Branch("isTCRhigh", &isTCRhigh);
    oTree->Branch("isTVRlow", &isTVRlow);
    oTree->Branch("isTVRhigh", &isTVRhigh);
    oTree->Branch("isBosonCRlow", &isBosonCRlow);
    oTree->Branch("isBosonCRhigh", &isBosonCRhigh);
    oTree->Branch("isBosonVRlow", &isBosonVRlow);
    oTree->Branch("isBosonVRhigh", &isBosonVRhigh);
    if (treeName.find("QCD_Nominal") == std::string::npos) {
        oTree->Branch("Weight_mc_FF_up", &FF_up);
        oTree->Branch("Weight_mc_FF_dw", &FF_dw);
    }

    setWeight([&] { return Var["Weight"]; });
    Cutflow_ptr mCutflow = nullptr;
    if (treeName.find("Nominal") != std::string::npos) {
        mCutflow = addCutflow();
    } else {
        mCutflow = addCutflow(Cutflow::NO_HIST);
    }
    mCutflow->setName(currentTreeName());
    mCutflow->setFillTree(oTree);
    mCutflow->registerCut("baseline", [&] { return true; });
    mCutflow->registerCut("MC truth", [&] { return Var["MCtruth"]; });
    mCutflow->registerCut(
        "bTag", [&] { return Var["nBjets"] < 3 && Var["nBjets"] > 0; }, outTag + "nBjets", 5, 0, 5, [&] { return Var["nBjets"]; });
    mCutflow->registerCut(
        "OS", [&] { return true; }, outTag + "OS2Tau", 2, 0, 2, [&] { return !Var["OS2Tau"]; });
    mCutflow->registerCut(
        "mT2 > 50", [&] { return tree->mT2 > 20; }, outTag + "mT2", 20, 0, 200, [&] { return tree->mT2; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "mT2 < 90", [&] { return tree->mT2 < 80; }, outTag + "mT2high", 20, 0, 200, [&] { return tree->mT2; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "mll > 60", [&] { return tree->Mtt > 40; }, outTag + "Mtt_low", 20, 0, 200, [&] { return tree->Mtt; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "mll < 120", [&] { return tree->Mtt < 160; }, outTag + "Mtt_high", 20, 0, 200, [&] { return tree->Mtt; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "tau1Pt > 70", [&] { return tree->tau1Pt > 0; }, outTag + "tau1Pt", 20, 0, 200, [&] { return tree->tau1Pt; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "tau2Pt > 30", [&] { return tree->tau2Pt > 0; }, outTag + "tau2Pt", 20, 0, 200, [&] { return tree->tau2Pt; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "NTightTau >=2", [&] { return tree->NTightTau >= 0; }, outTag + "NTightTau", 4, 0, 4, [&] { return tree->NTightTau; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "dRtt < 3", [&] { return tree->dRtt < 100; }, outTag + "dRtt", 20, 0, 4, [&] { return tree->dRtt; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "meff > 100", [&] { return tree->meff > 0; }, outTag + "meff", 30, 0, 600, [&] { return tree->meff; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "MtLep > 100", [&] { return tree->lepMt > 0; }, outTag + "lepMt", 20, 0, 200, [&] { return tree->lepMt; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "MET > 80", [&] { return tree->MET > 0; }, outTag + "MET", 20, 0, 200, [&] { return tree->MET; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "tau1Mt > 150", [&] { return tree->tau1Mt > 0; }, outTag + "tau1Mt", 20, 0, 300, [&] { return tree->tau1Mt; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "MTsum > 250", [&] { return tree->MTsum > 250; }, outTag + "MTsum", 60, 100, 700, [&] { return tree->MTsum; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "mCT > 40", [&] { return tree->mct > 0; }, outTag + "mCT", 20, 0, 200, [&] { return tree->mct; }, Hist::USE_OVERFLOW);
    auto theEnd = mCutflow->registerCut("the end", [&] { return true; });
    // theEnd->addHist(
    //    outTag + "Mtt_final", 8, 50, 130, [&] { return tree->Mtt; }, Hist::USE_OVERFLOW);
    // theEnd->addHist(
    //    outTag + "dPhitt_final", 20, 0, 4, [&] { return tree->dPhitt; }, Hist::USE_OVERFLOW);
    // theEnd->addHist(
    //    outTag + "dRl2t_final", 20, 0, 4, [&] { return tree->dRLepTwoTau; }, Hist::USE_OVERFLOW);
    // theEnd->addHist(
    //    outTag + "Njet_final", 8, 0, 8, [&] { return Var["Njets"]; }, Hist::USE_OVERFLOW);
    // theEnd->addHist(
    //    outTag + "MTsum_final", 60, 100, 700, [&] { return tree->MTsum; }, Hist::USE_OVERFLOW);
    // theEnd->addHist(
    //    outTag + "tau2Mt_final", 20, 0, 200, [&] { return tree->tau2Mt; }, Hist::USE_OVERFLOW);
    // theEnd->addHist(
    //    outTag + "tau2Pt_final", 20, 0, 200, [&] { return tree->tau2Pt; }, Hist::USE_OVERFLOW);
    // theEnd->addHist(
    //    outTag + "tau1Pt_final", 16, 40, 200, [&] { return tree->tau1Pt; }, Hist::USE_OVERFLOW);
    // theEnd->addHist(
    //    outTag + "lepPt_final", 20, 0, 200, [&] { return tree->lepPt; }, Hist::USE_OVERFLOW);
    // theEnd->addHist(
    //    outTag + "lepMt_final", 20, 0, 200, [&] { return tree->lepMt; }, Hist::USE_OVERFLOW);
    // theEnd->addHist(
    //    outTag + "mT2_final", 14, 20, 90, [&] { return tree->mT2; }, Hist::USE_OVERFLOW);
    // theEnd->addHist(
    //    outTag + "NTightTau", 4, 0, 4, [&] { return tree->NTightTau; }, Hist::USE_OVERFLOW);
    // theEnd->addHist(
    //    outTag + "TwotauPt", 20, 0, 200, [&] { return tree->TwotauPt; }, Hist::USE_OVERFLOW);

    // Start the loop!
    Long64_t nentries = tree->GetEntries();
    for (Long64_t jentry = 0; jentry < tree->GetEntries(); jentry++) {
        tree->GetEntry(jentry);
        if (0 == jentry % 100000) LOG(INFO) << jentry << " entry of " << nentries << " entries";

        Var["MCtruth"] = true;
        Var["bVeto"] = tree->n_BJets == 0;
        Var["nBjets"] = tree->n_BJets;
        Var["Njets"] = tree->getJets().size();
        auto taus = tree->getTaus();
        Var["OS2Tau"] = taus[0].isOS(taus[1]);
        if (addFF && tree->mcChannelNumber != 0 && (outTag.find("Data_") == std::string::npos || outTag.find("Wh_") == std::string::npos)) {
            AnaObjs tauVec = tree->getTaus();
            int nTruthTau = (tauVec.filterObjects(0, 100, TauIsTruthTau)).size();
            Var["MCtruth"] = (nTruthTau >= 1);
        }
        Var["Weight"] = tree->Weight_mc;
        FF_up = tree->Weight_mc;
        FF_dw = tree->Weight_mc;

        fillCutflows();
    }
    // Print out for region, cutflows if needed
    if (treeTag == "Nominal") {
        int i = 0;
        for (auto c : getCutflows()) {
            c->PrintOut("cutflow" + std::to_string(i));
            i++;
        }
    }
}
