#include "AnaObjs.h"
#include "PhyUtils.h"
#include "SingleTreeRunner.h"
#include "XamppTree.h"
#include "easylogging++.h"

DefineLooper(Wh_CutflowCheck, XamppTree);

void Wh_CutflowCheck::loop() {
    std::string outFullName = getOutName();
    std::string outTree = Utils::splitStrBy(outFullName, '.')[0] + "_Nominal";

    auto oTree = cloneCurrentAnaTree(outTree);
    Var["totalWeight"] = 1;
    setWeight([&] { return Var["totalWeight"]; });

    auto mCutflow = addCutflow();
    // for cut
    Var["1Lepton"] = 0;
    Var["nLepton"] = 0;
    Var["bVeto"] = 0;
    Var["2MediumTau"] = 0;
    Var["1MediumTau"] = 0;
    Var["OS2Tau"] = 0;
    // for histo
    Var["dRtt"] = 999;
    Var["dPhitt"] = 999;
    Var["tau1Pt"] = 0;
    Var["tau2Pt"] = 0;
    Var["lepPt"] = 0;
    Var["elePt"] = 0;
    Var["muonPt"] = 0;
    Var["lepEta"] = 0;
    Var["Ht_Tau"] = 0;
    Var["Ht_Lep"] = 0;
    Var["Ht_Jet"] = 0;
    Var["mT2"] = 0;
    Var["MET"] = 0;
    Var["pretauPt"] = 0;
    Var["Mtt"] = 0;
    Var["lepMt"] = 0;
    Var["tau1Mt"] = 0;
    Var["tau2Mt"] = 0;
    Var["meff"] = 0;
    Var["mct"] = 0;
    Var["dPhitl"] = 999;
    Var["maxJetDL1r"] = 0;
    int Njet(0), NTightTau(0), mRunnum(0), mlumiBlock(0), nTruthTau, nTotal, nMedTau;

    // set mc channel number to 0 initially to seperate data and mc
    tree->mcChannelNumber = 0;
    // clean conflict tree

    oTree->Branch("mergedRunNumber", &mRunnum);
    oTree->Branch("mergedlumiBlock", &mlumiBlock);
    oTree->Branch("Weight_mc", &Var["totalWeight"]);
    oTree->Branch("dRtt", &Var["dRtt"]);
    oTree->Branch("dPhitt", &Var["dPhitt"]);
    oTree->Branch("tau1Pt", &Var["tau1Pt"]);
    oTree->Branch("tau2Pt", &Var["tau2Pt"]);
    oTree->Branch("lepPt", &Var["lepPt"]);
    oTree->Branch("mT2", &Var["mT2"]);
    oTree->Branch("MET", &Var["MET"]);
    oTree->Branch("Mtt", &Var["Mtt"]);
    oTree->Branch("lepMt", &Var["lepMt"]);
    oTree->Branch("tau1Mt", &Var["tau1Mt"]);
    oTree->Branch("tau2Mt", &Var["tau2Mt"]);
    oTree->Branch("meff", &Var["meff"]);
    oTree->Branch("mct", &Var["mct"]);
    oTree->Branch("maxJetDL1r", &Var["maxJetDL1r"]);
    oTree->Branch("Njet", &Njet);
    oTree->Branch("NTightTau", &NTightTau);

    mCutflow->setFillTree(oTree);

    mCutflow->registerCut("baseline", [&] { return true; });
    mCutflow->registerCut(">=1 tau && == 1 lep", [&] { return tree->taus_pt->size() >= 1 && (tree->n_SignalElec == 1 || tree->n_SignalMuon == 1); });
    auto lepCut = mCutflow->registerCut("1Lepton", [&] { return Var["1Lepton"]; });
    lepCut->addHist(
        "nTruthTau3", 4, 0, 4, [&] { return nTruthTau; }, Hist::USE_OVERFLOW);
    lepCut->addHist(
        "lepPt1", 20, 0, 200, [&] { return Var["lepPt"]; }, Hist::USE_OVERFLOW);
    lepCut->addHist(
        "MET1", 20, 0, 200, [&] { return Var["MET"]; }, Hist::USE_OVERFLOW);
    lepCut->addHist(
        "lepEta1", 20, 0, 4, [&] { return Var["lepEta"]; }, Hist::USE_OVERFLOW);
    lepCut->addHist(
        "lepMt1", 20, 0, 400, [&] { return Var["lepMt"]; }, Hist::USE_OVERFLOW);
    auto medium1 = mCutflow->registerCut(">=1MediumTau", [&] { return Var["1MediumTau"]; });
    medium1->addHist(
        "tau1Pt", 20, 0, 200, [&] { return Var["tau1Pt"]; }, Hist::USE_OVERFLOW);
    medium1->addHist(
        "lepPt", 20, 0, 200, [&] { return Var["lepPt"]; }, Hist::USE_OVERFLOW);
    medium1->addHist(
        "elePt", 20, 0, 200, [&] { return Var["elePt"]; }, Hist::USE_OVERFLOW);
    medium1->addHist(
        "muonPt", 20, 0, 200, [&] { return Var["muonPt"]; }, Hist::USE_OVERFLOW);
    medium1->addHist(
        "MET", 20, 0, 200, [&] { return Var["MET"]; }, Hist::USE_OVERFLOW);
    medium1->addHist(
        "lepEta", 20, 0, 4, [&] { return Var["lepEta"]; }, Hist::USE_OVERFLOW);
    medium1->addHist(
        "lepMt", 20, 0, 400, [&] { return Var["lepMt"]; }, Hist::USE_OVERFLOW);
    medium1->addHist(
        "tau1Mt", 20, 0, 400, [&] { return Var["tau1Mt"]; }, Hist::USE_OVERFLOW);
    medium1->addHist(
        "dPhitl", 20, 0, 4, [&] { return Var["dPhitl"]; }, Hist::USE_OVERFLOW);
    auto medium2 = mCutflow->registerCut(">=2MediumTau", [&] { return Var["2MediumTau"]; });
    medium2->addHist(
        "tau1Pt1", 20, 0, 200, [&] { return Var["tau1Pt"]; }, Hist::USE_OVERFLOW);
    medium2->addHist(
        "nLep1", 5, 0, 5, [&] { return Var["nLepton"]; }, Hist::USE_OVERFLOW);
    medium2->addHist(
        "nTruthTau2", 4, 0, 4, [&] { return nTruthTau; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "dPhitt < 3", [&] { return Var["dPhitt"] < 3; }, "dPhitt", 20, 0, 4, [&] { return Var["dPhitt"]; }, Hist::USE_OVERFLOW);
    mCutflow->registerCut(
        "bVeto", [&] { return Var["bVeto"]; }, "bTag", 2, 0, 2, [&] { return !Var["bVeto"]; });
    mCutflow->registerCut("OS2Tau", [&] { return Var["OS2Tau"]; });
    mCutflow->registerCut(
        "mT2 > 30", [&] { return Var["mT2"] > 30; }, "mT2", 20, 0, 200, [&] { return Var["mT2"]; }, Hist::USE_OVERFLOW);
    auto preselect = mCutflow->registerCut("The END", [&] { return true; });
    preselect->addHist(
        "nLep", 5, 0, 5, [&] { return Var["nLepton"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "tau2Pt", 20, 0, 200, [&] { return Var["tau2Pt"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "dRtt", 20, 0, 4, [&] { return Var["dRtt"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "Ht_Tau", 20, 0, 400, [&] { return Var["Ht_Tau"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "Ht_Lep", 20, 0, 400, [&] { return Var["Ht_Lep"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "Ht_Jet", 20, 0, 400, [&] { return Var["Ht_Jet"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "Mtt", 20, 0, 400, [&] { return Var["Mtt"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "tau2Mt", 20, 0, 200, [&] { return Var["tau2Mt"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "meff", 20, 0, 600, [&] { return Var["meff"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "mct", 20, 0, 200, [&] { return Var["mct"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "NTightTau", 4, 0, 4, [&] { return NTightTau; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "NMedTau", 4, 0, 4, [&] { return nMedTau; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "maxJetDL1r", 60, -10, 5, [&] { return Var["maxJetDL1r"]; }, Hist::USE_OVERFLOW);
    preselect->addHist(
        "Njet", 6, 0, 5, [&] { return Njet; }, Hist::USE_OVERFLOW);

    // Start the loop!
    for (Long64_t jentry = 0; jentry < getEntries(); jentry++) {
        getEntry(jentry);

        Var["1Lepton"] = false;
        Var["bVeto"] = false;
        Var["2MediumTau"] = false;
        Var["OS2Tau"] = false;
        Var["nLepton"] = 0;
        Var["dRtt"] = 999;
        Var["dPhitt"] = 999;
        Var["dPhitl"] = 999;
        Var["tau1Pt"] = 0;
        Var["pretauPt"] = 0;
        if (tree->taus_pt->size() >= 1) Var["pretauPt"] = (tree->taus_pt->at(0)) / 1000;
        Var["tau2Pt"] = 0;
        Var["lepPt"] = 0;
        Var["elePt"] = -1;
        Var["muonPt"] = -1;
        Var["lepEta"] = 0;
        Var["Ht_Tau"] = tree->Ht_Tau / 1000;
        Var["Ht_Lep"] = tree->Ht_Lep / 1000;
        Var["Ht_Jet"] = tree->Ht_Jet / 1000;
        Var["mT2"] = 0;
        Var["MET"] = tree->MetTST_met / 1000;
        Var["Mtt"] = 0;
        Var["lepMt"] = 0;
        Var["tau1Mt"] = 0;
        Var["tau2Mt"] = 0;
        Var["meff"] = 0;
        Var["mct"] = 0;
        Var["maxJetDL1r"] = -10; // Usually DL1r is from -6 to 10, don't let the initial value too far away from the range
        Njet = 0, NTightTau = 0;

        int MCcompaign;
        // Weight setting
        if (tree->mcChannelNumber == 0) {
            Var["totalWeight"] = 1;
            mRunnum = tree->runNumber;
            mlumiBlock = tree->lumiBlock;
        } else {
            mRunnum = tree->RandomRunNumber;
            mlumiBlock = tree->RandomLumiBlockNumber;
        }
        if (tree->mcChannelNumber != 0) {
            double extraWei = tree->GenWeight * tree->muWeight * tree->TauWeight * tree->EleWeight * tree->MuoWeight * tree->JetWeight;
            Var["totalWeight"] = MetaDB::Instance().getWeight(PhyUtils::getCompaign(mRunnum), tree->mcChannelNumber, tree->SUSYFinalState, extraWei);
        }

        /*******************************Event construct *********************/
        AnaObjs eleVec = tree->getElectrons();
        AnaObjs muonVec = tree->getMuons();
        AnaObjs tauVec = tree->getTaus();

        bool oneBaseLep = ((eleVec.size() + muonVec.size()) == 1);

        // Jet is not important so we only count bjet and total jet num
        Var["bVeto"] = tree->n_BJets == 0;
        for (int i_jet = 0; i_jet < tree->jets_BTagScore->size(); i_jet++) {
            if (Var["maxJetDL1r"] < tree->jets_BTagScore->at(i_jet)) {
                Var["maxJetDL1r"] = tree->jets_BTagScore->at(i_jet);
            }
        } // end of jet loop

        /********************************Now start the preselect! ****************/
        // trigger
        // Ele, temporary no trig match vec, do the total match
        eleVec =
            eleVec.passTrig((mRunnum < 290000 && tree->TrigHLT_e24_lhmedium_L1EM20VH && tree->TrigMatchHLT_e24_lhmedium_L1EM20VH), 25) +
            eleVec.passTrig((mRunnum < 290000 && tree->TrigHLT_e60_lhmedium && tree->TrigMatchHLT_e60_lhmedium), 61) +
            eleVec.passTrig((mRunnum < 290000 && tree->TrigHLT_e120_lhloose && tree->TrigMatchHLT_e120_lhloose), 121) +
            eleVec.passTrig((mRunnum >= 290000 && tree->TrigHLT_e26_lhtight_nod0_ivarloose && tree->TrigMatchHLT_e26_lhtight_nod0_ivarloose), 27) +
            eleVec.passTrig((mRunnum >= 290000 && tree->TrigHLT_e60_lhmedium_nod0 && tree->TrigMatchHLT_e60_lhmedium_nod0), 61) +
            eleVec.passTrig((mRunnum >= 290000 && tree->TrigHLT_e140_lhloose_nod0 && tree->TrigMatchHLT_e140_lhloose_nod0), 141);

        if (eleVec.size() > 0 && tree->mcChannelNumber != 0) {
            Var["totalWeight"] *=
                tree->EleWeightTrig_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_OR_e26_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0;
        }
        // muon, same as ele
        if (mRunnum < 290000) { // 2015
            muonVec = muonVec.passTrig(tree->TrigHLT_mu20_iloose_L1MU15 && tree->TrigMatchHLT_mu20_iloose_L1MU15, 21) +
                      muonVec.passTrig(tree->TrigHLT_mu50 && tree->TrigMatchHLT_mu50, 52.5);
            if (muonVec.size() > 0 && tree->mcChannelNumber != 0) Var["totalWeight"] *= tree->MuoWeightTrigHLT_mu20_iloose_L1MU15_OR_HLT_mu40;
        } else {
            muonVec = muonVec.passTrig(tree->TrigHLT_mu26_ivarmedium && tree->TrigMatchHLT_mu26_ivarmedium, 27.3) +
                      muonVec.passTrig(tree->TrigHLT_mu50 && tree->TrigMatchHLT_mu50, 52.5);
            if (muonVec.size() > 0 && tree->mcChannelNumber != 0) Var["totalWeight"] *= tree->MuoWeightTrigHLT_mu26_ivarmedium_OR_HLT_mu50;
        }
        // start select the target ID objs and trigger offline requirement
        // electron
        eleVec = eleVec.filterObjects(0, 100, EIsoGood);
        // muon. Should filter events with Eta < 2.5 to avoid mismodeling
        muonVec = muonVec.filterObjects(0, 2.5, MuIsoGood);

        // tau, select medium ones
        nTruthTau = (tauVec.filterObjects(0, 100, TauIsTruthTau)).size();
        tauVec = tauVec.filterObjects(0, 100, TauMedium);
        auto tighttauVec = tauVec.filterObjects(0, 100, TauTight);

        auto lepVec = eleVec + muonVec;
        Var["nLepton"] = lepVec.size();
        Var["1Lepton"] = (lepVec.size() == 1 && oneBaseLep);
        Var["2MediumTau"] = (tauVec.size() >= 2);
        Var["1MediumTau"] = (tauVec.size() >= 1);
        nTotal = lepVec.size() + tauVec.size();
        nMedTau = tauVec.size();
        if (tauVec.size() >= 1 && Var["1Lepton"]) {
            Var["tau1Pt"] = tauVec[0].Pt();
            Var["tau1Mt"] = tauVec[0].Mt();
            Var["dPhitl"] = PhyUtils::deltaPhi(tauVec[0], lepVec[0]);
        }
        NTightTau = tighttauVec.size();
        if (Var["1Lepton"]) {
            auto lep = lepVec[0];
            Var["lepPt"] = lep.Pt();
            if (eleVec.size() > 0) {
                Var["elePt"] = lep.Pt();
            } else {
                Var["muonPt"] = lep.Pt();
            }
            Var["lepEta"] = fabs(lep.Eta());
            Var["lepMt"] = lep.Mt();
        }
        if (Var["2MediumTau"] && Var["1Lepton"]) {
            auto tau1 = tauVec[0];
            auto tau2 = tauVec[1];
            auto lep = lepVec[0];
            TLorentzVector METVec;
            METVec.SetPtEtaPhiE(Var["MET"], 0, tree->MetTST_phi, Var["MET"]);
            Var["OS2Tau"] = tau1.isOS(tauVec[1]);
            Var["tau2Pt"] = tau2.Pt();
            Var["tau2Mt"] = tau2.Mt();
            Var["dRtt"] = PhyUtils::deltaR(tau1, tau2);
            Var["dPhitt"] = PhyUtils::deltaPhi(tau1, tau2);
            Var["Mtt"] = PhyUtils::calcMll(tau1, tau2);
            Var["meff"] = tau1.Pt() + tau2.Pt() + lep.Pt() + Var["MET"];
            Var["mct"] = PhyUtils::calcMCT(tau1, tau2);
            Var["mT2"] = PhyUtils::MT2Max(tauVec + lepVec, METVec);
        }

        fillRegions();
        fillCutflows();
    }
    // Print out for region, cutflows if needed
    for (auto cut : getCutflows()) {
        cut->PrintOut("cutflow_" + Utils::splitStrBy(outFullName, '.')[0]);
    }
}
