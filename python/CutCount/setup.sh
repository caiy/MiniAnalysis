#setupATLAS
#lsetup "lcgenv -p LCG_95a x86_64-slc6-gcc62-opt pip"
#source /cvmfs/sft.cern.ch/lcg/views/LCG_97rc4python3/x86_64-centos7-gcc8-opt/setup.sh
#source /cvmfs/sft.cern.ch/lcg/views/LCG_97apython3_ATLAS_1/x86_64-centos7-gcc8-opt/setup.sh
#source /cvmfs/sft.cern.ch/lcg/views/LCG_97/x86_64-centos7-gcc8-opt/setup.sh
pip3 install Cython
pip3 install --user numpy==1.17.3
pip3 install --user uproot pandas
pip3 install tqdm

python setup.py build_ext --inplace
