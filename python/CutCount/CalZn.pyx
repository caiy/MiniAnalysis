from libc.math cimport log,sqrt

def calcZn(double bkg , double bkgerr2 , double sig ):
	
	cdef double n,dll,nsigma
	
	if bkg <= 0:
		return 0.0
	else:
		n = bkg + sig
		dll = n * log(( n * (bkg + bkgerr2)) / (bkg * bkg + n * bkgerr2)) - \
			((bkg * bkg) / (bkgerr2)) * log((bkg * bkg + n * bkgerr2) / (bkg * (bkg + bkgerr2)));
		nsigma = sqrt( 2 * dll );
		return nsigma
