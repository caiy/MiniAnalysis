import argparse
import numpy as np
import pandas as pd
import sampleMap as sam
import timeit
import time
import json
import itertools
import logging
from tqdm import tqdm

from CalZn import calcZn

### can change Zn definition
#def calcZn( bkg , bkgerr2 , sig ):
#	if bkg <= 0:
#		return 0
#	else:
#		n = bkg + sig
#		dll = n * np.log(( n * (bkg + bkgerr2)) / (bkg * bkg + n * bkgerr2)) - \
#			((bkg * bkg) / (bkgerr2)) * np.log((bkg * bkg + n * bkgerr2) / (bkg * (bkg + bkgerr2)));
#		nsigma = np.sqrt( 2 * dll );
#		return nsigma


if __name__ == "__main__":
	
	parser = argparse.ArgumentParser(description='Cut and Count , need one config file')
	parser.add_argument('-i','--input', action='store', 
								help='input config json file ')
	parser.add_argument('-f','--filter', action='store', 
								help='filter Zn below this level (default = 0)')
	parser.add_argument('-o','--output', action='store', 
								help='output csv name (default = system time) ')
	args = vars(parser.parse_args())

	time_now = time.strftime("%Y%m%d-%H%M%S")


	if not args['input'] :
		input_file = "config.json"
	else:
		input_file = args['input']
	with open(input_file) as f:
		json_config = json.load(f)

	if not args['filter'] :
		zn_filter_value = 0
	else:
		zn_filter_value = float(args['filter'])
	
	if not args['output'] :
		output_file = time_now + "_" + str(zn_filter_value) + ".csv"
	elif len(args['output']) > 4 and (args['output'])[-4:] == ".csv" :
		output_file = args['output']
	else:
		output_file = args['output'] + ".csv"

	# start to load config file
	relative_sys_err2 = json_config["sys_uncertainty"] * json_config["sys_uncertainty"]
	weight_leaf = json_config['weight']
	root_path = json_config['filepath']
	weight_cut = json_config['weight_cut']

	#input branch info including weight
	leaf_name_list = [weight_leaf]
	# weight > weight_value
	moreless_list = [">"]  
	cuts_list_list = [[weight_cut]]
	for x in json_config['variables']:
		leaf_name_list.append( x['branchname'] )
		moreless_list.append( x['more_or_less'] )
		cuts_list_list.append( x['cuts'] )

	#input BKG files
	bkgfile_list = []
	bkgtree_list = []
	for x in json_config['bkgfile']:
		bkgfile_list.append( x['filename'] )
		bkgtree_list.append( x['treename'] )
	
	#input SIG files
	sigfile_list = []
	sigtree_list = []
	for x in json_config['sigfile']:
		sigfile_list.append( x['filename'] )
		sigtree_list.append( x['treename'] )

	
	print('Options loaded. Load root files now')
	# This is the start. Join sekai wo ooi ni mori ageru tameno suzumiya haruhi no dan !
	start = timeit.default_timer()
	

	calc = np.vectorize(calcZn)
	# bkg sampleMaps
	sam_bkg = []
	for i in range( len(bkgfile_list) ):
		
		_root_name = bkgfile_list[i]
		_tree_name = bkgtree_list[i]

		a = sam.sampleMap( _tree_name )
		a.loadRootFile( root_path , _root_name , _tree_name , leaf_name_list)
		a.loadMoreLess( moreless_list)
		sam_bkg.append(a)
	

	# sig sampleMaps
	sam_sig = []
	for i in range( len(sigfile_list) ):
		
		_root_name = sigfile_list[i]
		_tree_name = sigtree_list[i]

		a = sam.sampleMap( _tree_name )
		a.loadRootFile( root_path , _root_name , _tree_name , leaf_name_list)
		a.loadMoreLess( moreless_list)
		sam_sig.append(a)


	# all possible cut combinations (Permutation)
	cut_combination_2Darr = np.array( list(itertools.product(*cuts_list_list)) )
	N_cut_combination = len(cut_combination_2Darr)

	stop = timeit.default_timer()
	print('All root files loaded, start CutCount now. Time: ', stop - start) 
	print('start BKG')
	# Bkg cuts
	bkg_nom_err_list = []
	bkg_name_list = []
	bkg_sum_nom = np.zeros(N_cut_combination)
	bkg_sum_err = np.zeros(N_cut_combination)
	bkg_sum_err2 = np.zeros(N_cut_combination)
	

	for i in tqdm( range(len(sam_bkg)) ):
		one_sam = sam_bkg[i]
		_tmp_cont = np.apply_along_axis( one_sam.applyOneCut , axis = 1 , arr = cut_combination_2Darr ) 
		# return _tmp_cont[:,0] = nomial , _tmp_cont[:,1] = err^2
		bkg_nom_err_list.append(_tmp_cont[:,0])
		bkg_nom_err_list.append( np.sqrt(_tmp_cont[:,1]) )
		bkg_name_list.append( one_sam.getName() )
		bkg_name_list.append( one_sam.getName()+"_err" )
		bkg_sum_nom = np.add( bkg_sum_nom , _tmp_cont[:,0] )
		bkg_sum_err2 = np.add( bkg_sum_err2 , np.add( _tmp_cont[:,1] , relative_sys_err2 * np.square(_tmp_cont[:,0]) ) )
	
	bkg_sum_err = np.sqrt(bkg_sum_err2)


	# signal cuts and zn calculation
	sig_nom_err_list = []
	sig_name_list = []
	zn_cal_list = []
	zn_name_list = []
	
	print('Start Signal and Zn')
	for i in tqdm( range(len(sam_sig)) ):
		one_sam = sam_sig[i]
		_tmp_cont = np.apply_along_axis( one_sam.applyOneCutNoErr , axis = 1 , arr = cut_combination_2Darr ) 
		sig_nom_err_list.append(_tmp_cont)
		sig_name_list.append( one_sam.getName() )

		_result = np.zeros(N_cut_combination)
		_result = np.apply_along_axis( calc, 0, bkg_sum_nom, bkg_sum_err2, _tmp_cont )
		#for i,(x,y,z) in enumerate(zip( bkg_sum_nom , bkg_sum_err2 , _tmp_cont )):
		#	_result[i] = calcZn(x,y,z)
		zn_cal_list.append(_result)
		zn_name_list.append( one_sam.getName()+"_Zn" )


	stop = timeit.default_timer()
	print('Analysis ended, preparing for output csv file. Time: ', stop - start) 
	
	# make final Cut-Bkg-bkgErr-totalBkg-totalBkgErr-Sig-Zn Map
	CBSZ = []
	CBSZ_name = []
	for i in range(len(cut_combination_2Darr[0])):
		CBSZ.append( np.array(cut_combination_2Darr[:,i]) )
		CBSZ_name.append( leaf_name_list[i] + " " + moreless_list[i] )

	CBSZ = CBSZ + bkg_nom_err_list + [ bkg_sum_nom , bkg_sum_err ] + sig_nom_err_list + zn_cal_list 
	CBSZ_name = CBSZ_name + bkg_name_list + ["Total BKG","Total BKG err"] + sig_name_list + zn_name_list
	CBSZ_dict = dict( zip(CBSZ_name,CBSZ) )

	# now make Cuts-Bkg-Sig-Zn DataFrame
	CBSZ_pd = pd.DataFrame( CBSZ_dict )
	if len(zn_name_list)!=0 and zn_filter_value>0:
		for i in range(len(zn_name_list)):
			CBSZ_pd = CBSZ_pd[ CBSZ_pd[zn_name_list[i] ] > zn_filter_value ]

	#you can change the format here!
	CBSZ_pd.to_csv(output_file , float_format='%.5f')
	

#	threads = []
#	t = threading.Thread(target=print(a.applyOneCut(var_cut_list0)), args=(1,))
#	threads.append(t)
#	t.start()
	
	#This is the end / Hold your breath and count to ten 
	stop = timeit.default_timer()
	print('All things are done. Cut&Count Time: ', stop - start) 
	#/ Feel the Earth move, and then / Hear my heart burst again /

