#Systematic("ZjetsRenormTheo", configMgr.weights,1.+0.232,1.-0.155 , "user","userOverallSys")#9.11

import os
import csv
import numpy

def str_abs(i):
    if i[0] == '-' :
        return i[1:-1]
    else:
        return "+"+i

def str_positive(i):
    if i[0] != '-' and i[0] != '+' :
        return "+"+i
    else:
        return i

input_path = 'C1C1_C1N2_OS_Theoretical_Uncertainties_Sum.CSV'
bkg_name_start_end = [ ['Higgs',2,11] , ['MultiBoson',14,25] , ['Top',28,43] , ['Z',46,57] , ['W',60,71]]

reader = csv.reader( open(input_path,"r"), delimiter="," )
sys_list = list(reader)

total_region_num = len(sys_list[0])

for name_start_end in bkg_name_start_end :
    bkg_name = name_start_end[0]
    start_step = name_start_end[1]
    end_step = name_start_end[2]
    print( '    '+bkg_name+'Theo = {}' )
    for col_num in range(1,total_region_num) :
        region_name = sys_list[0][col_num]
        print( '    addBkgTheoUncert('+bkg_name+'Theo, "'+bkg_name+'_Staus'+'", "'+region_name+'", '+region_name+'set) ' )

    for line_num in range( start_step, end_step, 2) :
        var_name = sys_list[line_num][0]
        
        for col_num in range(1,total_region_num) :
            
            region_name = sys_list[0][col_num]
            
            str_up = sys_list[line_num][col_num]
            str_dw = sys_list[line_num+1][col_num]
            if str_up == str_dw and str_up[0] != "-" :
                str_dw = "-" + str_dw
                str_up = "+" + str_up

            var_up = str_positive(str_up) #str_abs(sys_list[line_num][col_num])
            var_dw = str_positive(str_dw) #str_abs(sys_list[line_num+1][col_num])
            
            print( '    '+bkg_name+'Theo["'+bkg_name+var_name+"_"+region_name+'"] ' + '= Systematic( "'+bkg_name+"_"+var_name+'_Theo", configMgr.weights, (1.'+var_up+'), (1'+var_dw+'), "user", "userOverallSys" )' )
        

