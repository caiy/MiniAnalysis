#Systematic("ZjetsRenormTheo", configMgr.weights,1.+0.232,1.-0.155 , "user","userOverallSys")#9.11

import os, re

with open('Z.result','r') as f:
    lines = f.readlines()
    regionMatch = re.compile(r'.+Region (.+):')
    SystNames = ['ZjetsRenorFacto', 'ZjetsReno', 'ZjetsFacto', 'ZjetsCkkw', 'ZjetsQsf']
    nextDw = True
    VarCount = 0
    systDw = None
    systUp = None
    Region = None
    for line in lines:
        results = regionMatch.match(line)
        if results != None:
            Region = results.group(1).strip()
            VarCount = 0
            nextDw = True
        else:
            SystVar = line.split(' ')[3]
            if nextDw:
                systDw = SystVar.strip()
                nextDw = False
            else:
                systUp = SystVar.strip()
                print 'Zjets["'+SystNames[VarCount]+'_'+Region+'"] = Systematic("'+SystNames[VarCount]+'", configMgr.weights,1.-'+systDw+',1.+'+systUp+',"user","userOverallSys")'
                nextDw = True
                VarCount = VarCount + 1

