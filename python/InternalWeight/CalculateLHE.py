import numpy as np
import pandas as pd
import json
import uproot
import warnings
import sys

JsonFile="./Link.json"
with open(JsonFile) as f:
    _LinkDB = json.load(f)
    NNORM_DB = _LinkDB["Nnorm"]
    VARIATION_DB = _LinkDB["link"]

GenDBfile="./generatorDB.json"
with open(GenDBfile) as f:
    GEN_DB = json.load(f)

ROOT_FILE_PATH = "/publicfs/atlas/atlasnew/SUSY/users/caiyc/C1C1C1N2/mergeROOT/merged/"
ROOT_FILE_PATH = "/publicfs/atlas/atlasnew/SUSY/users/caiyc/C1C1C1N2/LooseCutForInternalWeight/SR/"
PRE_ROOT_FILE_PATH = "/publicfs/atlas/atlasnew/SUSY/users/caiyc/C1C1C1N2/LooseCutForInternalWeight/"
VARIATION_NAME = ""
REGION_NAME = ''
SAMPLE_GEN = ''


def initPD(root_path, root_name, tree_name , branch_name = []):
    with uproot.open(root_path+root_name) as root_file:
        _var_pandas = root_file[tree_name].pandas.df(branch_name)
    return _var_pandas


# get Variation weight
def get_var_weight(PDMap):
    
    run_number = PDMap['mergedRunNumber']
    
    mc_compaign = ""
    if run_number >= 348885 :
        mc_compaign = "18"
    elif run_number >= 325713 :
        mc_compaign = "17"
    else :
        mc_compaign = "16"
    
    mcChannel = PDMap['mcChannelNumber']
    weight = PDMap['Weight_mc']
    if VARIATION_NAME == "Nominal_Inclusive":
        branch_name = "GenWeight"
    else:
        branch_name = "GenWeight_LHE_" + VARIATION_NAME
    var_GenWeight = PDMap[branch_name]
    nom_GenWeight = PDMap['GenWeight']

    if SAMPLE_GEN != "DSID" :
        if len(VARIATION_DB[SAMPLE_GEN]) == 1:
            process_ID = '0'
            warnings.warn( SAMPLE_GEN + " only has Nominal_Inclusive")
        else:
            if VARIATION_NAME in VARIATION_DB[SAMPLE_GEN]:
                process_ID = VARIATION_DB[SAMPLE_GEN][VARIATION_NAME]
            else:
                raise KeyError( SAMPLE_GEN + " does not have any matched variation : " + VARIATION_NAME )
    else:
        if len(VARIATION_DB[str(mcChannel)]) == 1:
            process_ID = '0'
            warnings.warn( str(mcChannel) + " only has Nominal_Inclusive")
        else:
            if VARIATION_NAME in VARIATION_DB[str(mcChannel)]:
                process_ID = VARIATION_DB[str(mcChannel)][VARIATION_NAME]
            else:
                raise KeyError(str(mcChannel) + " does not have any matched variation : " + VARIATION_NAME )

    nom_sumWeight = NNORM_DB["0"][str(mcChannel)][mc_compaign]
    try:
        var_sumWeight = NNORM_DB[process_ID][str(mcChannel)][mc_compaign]
    except KeyError:
        return weight
    return weight * (nom_sumWeight/var_sumWeight) * (var_GenWeight/nom_GenWeight)


def get_nom_weight(PDMap):
    weight = PDMap['Weight_mc']
    return weight


# genDict return str to find sumWeight # GenDB return variation Name
def control_dic(sample_name, var_name):
    genDict = {
            'W' : 'Sherpa221', 
            'Z' : 'Sherpa221',
            'MultiBoson' : 'Sherpa',
            'ttbar': 'PowHegPy8_ttbar',
            'ttV' : 'MG5',
            'ttX_multiTop' : 'DSID',
            'ttX_tZ' : 'DSID',
            'ttX_ttX' : 'DSID',
            'SingleTop_S_Chan' : 'DSID',
            'SingleTop_T_Chan' : 'DSID',
            'SingleTop_Wt_Chan' : 'DSID',
            'VBFHiggs' : 'VBFHiggs',
            'VHiggs' : 'VHiggs',
            'ggHiggs' : 'ggHiggs',
            'ttHiggs' : 'ttHiggs'
            }
    _genDict = {
            'W' : 'Sherpa', 
            'Z' : 'Sherpa',
            'MultiBoson' : 'Sherpa',
            'SingleTop_S_Chan' : 'PowHegPy8_SingleTop',
            'SingleTop_T_Chan' : 'PowHegPy8_SingleTop_t_chan',
            'SingleTop_Wt_Chan' : 'PowHegPy8_SingleTop',
            'ttbar': 'PowHegPy8_ttbar',
            'ttV' : 'MG5_aMCatNLO_Py8',
            'ttX_multiTop' : 'OnlyNominal',
            'ttX_tZ' : 'MG5Py8',
            'ttX_ttX' : 'OnlyNominal',
            'VBFHiggs' : 'VBFHiggs',
            'VHiggs' : 'VHiggs',
            'ggHiggs' : 'ggHiggs',
            'ttHiggs' : 'ttHiggs'
            }
    gen_name = _genDict[sample_name]
    return genDict[sample_name] , GEN_DB[gen_name ][var_name]


def main( root_file_name_list, sample_name_dic, tree_name, region_name, var_name_list):
    
    total_nom_weight_dic = {'nomWeight' : 0.0}
    total_var_weight_dic = {}
    for i in var_name_list:
        total_var_weight_dic['varWeight'+i] = 0.0
    
    ps_nom_weight_dic = {'nomWeight' : 0.0}
    ps_var_weight_dic = {}
    for i in var_name_list:
        ps_var_weight_dic['varWeight'+i] = 0.0
    
    total_raw_event_num = 0

    global VARIATION_NAME,REGION_NAME,SAMPLE_GEN
    
    REGION_NAME = region_name

    for sample_name in root_file_name_list:
        root_file_name = sample_name_dic[sample_name]
        
        variation_name_list = []
        variation_branch_list = []
        sample_gen_list = []
        for i in var_name_list:
                a,b = control_dic(sample_name, i)
                variation_name_list.append(b)
                sample_gen_list.append(a)
                if b == "Nominal_Inclusive" :
                    variation_branch_list.append("GenWeight")
                else:
                    variation_branch_list.append("GenWeight_LHE_" + b)

        branchList = [REGION_NAME, "mcChannelNumber" ,"mergedRunNumber" ,"Weight_mc","GenWeight"] + variation_branch_list
        re_branch_list = []
        for i in branchList:
            if i not in re_branch_list:
                re_branch_list.append(i)
        PDMap_total = initPD(ROOT_FILE_PATH, root_file_name, tree_name, re_branch_list)
        preselection_PD = initPD(PRE_ROOT_FILE_PATH, root_file_name, tree_name, re_branch_list)

        PDMap = PDMap_total.loc[PDMap_total[REGION_NAME]].copy()
        #print(PDMap)

        raw_event_num = len(PDMap)
        PDMap['nomWeight'] = PDMap.apply(get_nom_weight, axis=1)
        preselection_PD['nomWeight'] = preselection_PD.apply(get_nom_weight, axis=1)
        total_nom_weight_dic[ 'nomWeight' ] += PDMap[ 'nomWeight' ].sum()
        ps_nom_weight_dic[ 'nomWeight' ] += preselection_PD[ 'nomWeight' ].sum()

        if raw_event_num == 0:
            print(sample_name + " has 0 Raw events in Region " + region_name)
        else:
            for i in range(len(variation_name_list)):
                #print(i,len(variation_name_list))
                VARIATION_NAME = variation_name_list[i]
                SAMPLE_GEN = sample_gen_list[i]
                PDMap[ 'varWeight'+var_name_list[i] ] = PDMap.apply(get_var_weight, axis=1)
                total_var_weight_dic[ 'varWeight'+var_name_list[i] ] += PDMap[ 'varWeight'+var_name_list[i] ].sum()
                preselection_PD[ 'varWeight'+var_name_list[i] ] = preselection_PD.apply(get_var_weight, axis=1)
                ps_var_weight_dic[ 'varWeight'+var_name_list[i] ] += preselection_PD[ 'varWeight'+var_name_list[i] ].sum()
            total_raw_event_num = total_raw_event_num + raw_event_num

    for i in var_name_list:
        systematic_val = 0
        if total_nom_weight_dic[ 'nomWeight' ] != 0 :
            systematic_val = ( total_var_weight_dic[ 'varWeight'+i ] - total_nom_weight_dic[ 'nomWeight' ] ) / total_nom_weight_dic[ 'nomWeight' ] 
        print(" * ", i ,",", total_raw_event_num,",", format(total_nom_weight_dic[ 'nomWeight' ], '.3f'),",", format(total_var_weight_dic[ 'varWeight'+i ], '.3f'),",", format( abs(systematic_val), '.3f') )
        print(" * ", i ,",", total_raw_event_num,",", format(ps_nom_weight_dic[ 'nomWeight' ], '.3f'),",", format(ps_var_weight_dic[ 'varWeight'+i ], '.3f'),",", format( abs( ( ps_var_weight_dic[ 'varWeight'+i ] - ps_nom_weight_dic[ 'nomWeight' ] ) / ps_nom_weight_dic[ 'nomWeight' ] ) , '.3f') )


if __name__ == "__main__":
    
    RegionNameS = ["isSR4","isWCR","isWVR","isTVR1","isTVR2","isZVR1","isZVR2","isBosonVR1","isBosonVR2","isQCDCR_SR1","isQCDCR_SR2","isQCDCR_SR3","isQCDCR_SR4"]
    RegionNameS = ["isSR1","isSR2","isSR3","isSR4","isWCR","isWVR","isTVR1","isTVR2","isZVR1","isZVR2","isBosonVR1","isBosonVR2"]
    #RegionNameS = ["isWCR","isWVR"]
    VarNameS_test = ["FSRfac_20", "FSRfac_05"]
    VarNameS_test = ["Var3cUp", "Var3cDown", "FSRfac_20", "FSRfac_05"]
    VarNameS_test = ["05_05", "20_20", "05_10", "20_10", "10_05", "10_20"]
    VarNameS_test = ["PDF_alpha_up","PDF_alpha_down","PDF_alternative_1","PDF_alternative_2"]
    #VarNameS_test = ["PDFset_00","PDFset_01","PDFset_02","PDFset_03","PDFset_04","PDFset_05","PDFset_06","PDFset_07","PDFset_08","PDFset_09","PDFset_10","PDFset_11","PDFset_12","PDFset_13","PDFset_14","PDFset_15","PDFset_16","PDFset_17","PDFset_18","PDFset_19","PDFset_20","PDFset_21","PDFset_22","PDFset_23","PDFset_24","PDFset_25","PDFset_26","PDFset_27","PDFset_28","PDFset_29","PDFset_30"]
    VarNameS_test = ["PDF_alpha_up","PDF_alpha_down", "PDFset_00", "PDFset_01", "PDFset_02", "PDFset_03", "PDFset_04", "PDFset_05", "PDFset_06", "PDFset_07", "PDFset_08", "PDFset_09", "PDFset_10", "PDFset_11", "PDFset_12", "PDFset_13", "PDFset_14", "PDFset_15", "PDFset_16", "PDFset_17", "PDFset_18", "PDFset_19", "PDFset_20", "PDFset_21", "PDFset_22", "PDFset_23", "PDFset_24", "PDFset_25", "PDFset_26", "PDFset_27", "PDFset_28", "PDFset_29", "PDFset_30", "PDFset_31", "PDFset_32", "PDFset_33", "PDFset_34", "PDFset_35", "PDFset_36", "PDFset_37", "PDFset_38", "PDFset_39", "PDFset_40", "PDFset_41", "PDFset_42", "PDFset_43", "PDFset_44", "PDFset_45", "PDFset_46", "PDFset_47", "PDFset_48", "PDFset_49", "PDFset_50", "PDFset_51", "PDFset_52", "PDFset_53", "PDFset_54", "PDFset_55", "PDFset_56", "PDFset_57", "PDFset_58", "PDFset_59", "PDFset_60", "PDFset_61", "PDFset_62", "PDFset_63", "PDFset_64", "PDFset_65", "PDFset_66", "PDFset_67", "PDFset_68", "PDFset_69", "PDFset_70", "PDFset_71", "PDFset_72", "PDFset_73", "PDFset_74", "PDFset_75", "PDFset_76", "PDFset_77", "PDFset_78", "PDFset_79", "PDFset_80", "PDFset_81", "PDFset_82", "PDFset_83", "PDFset_84", "PDFset_85", "PDFset_86", "PDFset_87", "PDFset_88", "PDFset_89", "PDFset_90", "PDFset_91", "PDFset_92", "PDFset_93", "PDFset_94", "PDFset_95", "PDFset_96", "PDFset_97", "PDFset_98", "PDFset_99" ]
    VarNameS = ["05_05", "20_20", "05_10", "20_10", "10_05", "10_20", "Var3cUp", "Var3cDown", "FSRfac_20", "FSRfac_05"]
    VarNameS_boson = ["05_05", "20_20", "05_10", "20_10", "10_05", "10_20", "ckkwWeight_dw", "ckkwWeight_up", "facWeight_dw", "facWeight_up", "renormWeight_dw", "renormWeight_up", "qsfWeight_dw", "qsfWeight_up"]
    VarNameS_boson = ["05_05", "20_20", "05_10", "20_10", "10_05", "10_20", "ckkwWeight_dw", "ckkwWeight_up", "qsfWeight_dw", "qsfWeight_up"]

    SampleTreeNameDic = {
            'W' : 'W_Staus_Nominal', 
            'Z' : 'Z_Staus_Nominal',
            'MultiBoson' : 'MultiBoson_Staus_Nominal',
            'Top' : 'Top_Staus_Nominal',
            'Higgs' : 'Higgs_Staus_Nominal'
            }
    SampleRootNameDic = {
            'W' : 'W.root', 
            'Z' : 'Z.root',
            'MultiBoson' : 'MultiBoson.root',
            'ttbar': 'Top.ttbar.root',
            'ttV' : 'Top.ttV.root',
            'ttX_multiTop' : 'Top.ttX_M.root',
            'ttX_tZ' : 'Top.ttX_Z.root',
            'ttX_ttX' : 'Top.ttX_X.root',
            'SingleTop_S_Chan' : 'Top.SingleTopS.root',
            'SingleTop_T_Chan' : 'Top.SingleTopT.root',
            'SingleTop_Wt_Chan' : 'Top.SingleTopWt.root',
            'VBFHiggs' : 'Higgs.VBFHiggs.root',
            'VHiggs' : 'Higgs.VHiggs.root',
            'ggHiggs' : 'Higgs.ggHiggs.root',
            'ttHiggs' : 'Higgs.ttHiggs.root'
            }

    SampleGroupDic = {
            'Top' : ['ttbar', 'ttV', 'ttX_multiTop', 'ttX_tZ', 'ttX_ttX', 'SingleTop_S_Chan', 'SingleTop_T_Chan', 'SingleTop_Wt_Chan'],
            'MultiBoson' : ['MultiBoson'],
            'Z' : ['Z'],
            'W' : ['W'],
            'Higgs' : ['VHiggs', 'VBFHiggs', 'ggHiggs', 'ttHiggs']
            }
    
    r = sys.argv[1]
    SelectedSample = "Z"
    print( "  the Sample " + SelectedSample + " in Region " + r + ":")
    main(root_file_name_list = SampleGroupDic[SelectedSample], sample_name_dic = SampleRootNameDic , tree_name = SampleTreeNameDic[SelectedSample] , region_name = r, var_name_list = VarNameS_boson )
    print(' * Done * ')
