#include "AnaRunner.h"

std::map<std::string, std::function<AnaRunner*()>>& getAnaCollection() {
    static std::map<std::string, std::function<AnaRunner*()>> GLOBAL_ANALYSIS_COLLECTION;
    return GLOBAL_ANALYSIS_COLLECTION;
}

std::vector<std::string> getAnaList() {
    std::vector<std::string> anaList;
    for (auto&& it : getAnaCollection()) {
        anaList.emplace_back(it.first);
    }
    return anaList;
}

AnaRunner* getNewAnaRunner(std::string analysisName) {
    auto iter = getAnaCollection().find(analysisName);
    if (iter != getAnaCollection().end()) {
        return iter->second();
    }
    return nullptr;
}
