#include "PhyUtils.h"

PhyUtils::PhyUtils() {}

PhyUtils::~PhyUtils() {}

AnaObjs PhyUtils::overlapRemoval(const AnaObjs& cands, const AnaObjs& others, std::function<float(const AnaObj&, const AnaObj&)> radiusFunc,
                                 int passId) {
    AnaObjs reducedList;

    for (const auto& cand : cands) {
        bool overlap = false;
        for (const auto& other : others) {
            if (cand.DeltaR(other) < radiusFunc(cand, other) && cand != other && cand.passID(passId)) {
                overlap = true;
                break;
            }
        }
        if (!overlap) reducedList.emplace_back(cand);
    }
    return reducedList;
}
AnaObjs PhyUtils::overlapRemoval(const AnaObjs& cands, const AnaObjs& others, float deltaR, int passId) {
    return overlapRemoval(
        cands, others, [deltaR](const AnaObj&, const AnaObj&) { return deltaR; }, passId);
}

AnaObj PhyUtils::RecoW(TLorentzVector lep, TLorentzVector neu, double wmass) {
    double lep_px = lep.Px();
    double lep_py = lep.Py();
    double lep_pz = lep.Pz();
    double lep_e = lep.E();
    double neu_px = neu.Px();
    double neu_py = neu.Py();
    double neu_pz = 0;
    double cross_term = (wmass * wmass + 2 * lep_px * neu_px + 2 * lep_py * neu_py);

    double a = 4 * lep_px * lep_px + 4 * lep_py * lep_py;
    double b = (-4) * lep_pz * cross_term;
    double c = 4 * lep_e * lep_e * (neu_px * neu_px + neu_py * neu_py) - cross_term * cross_term;

    double bb_4ac = b * b - 4 * a * c;
    if (bb_4ac < 0) {
        neu_pz = -0.5 * b / a;
    } else {
        neu_pz = -0.5 * b / a - 0.5 * sqrt(bb_4ac) / a;
    }

    TLorentzVector NeuTlv;
    NeuTlv.SetPxPyPzE(neu_px, neu_py, neu_pz, sqrt(neu_px * neu_px + neu_py * neu_py + neu_pz * neu_pz));
    AnaObj newNeu(MET, NeuTlv, 0);
    return newNeu;
}

std::vector<double> PhyUtils::RecoMll(TLorentzVector tlv1, TLorentzVector tlv2, TLorentzVector met) {
    AnaObjs twoNeu;
    std::vector<double> chi;

    double pT1_x = tlv1.Px();
    double pT1_y = tlv1.Py();
    double pT1_z = tlv1.Pz();
    double pT2_x = tlv2.Px();
    double pT2_y = tlv2.Py();
    double PT2_z = tlv2.Pz();
    double met_x = met.Px();
    double met_y = met.Py();
    double chi1 = (met_x * pT2_y - met_y * pT2_x) / (pT1_x * pT2_y - pT1_y * pT2_x);
    double chi2 = (met_x * pT1_y - met_y * pT1_x) / (pT1_y * pT2_x - pT1_x * pT2_y);

    chi.emplace_back(chi1);
    chi.emplace_back(chi2);
    // TLorentzVector Neu1Tlv, Neu2Tlv;
    // Neu1Tlv.SetPxPyPzE(chi1 * tlv1.Px(), chi1 * tlv1.Py(), chi1 * tlv1.Pz(),
    //                   chi1 * sqrt(tlv1.Px() * tlv1.Px() + tlv1.Py() * tlv1.Py() + tlv1.Pz() * tlv1.Pz()));
    // Neu2Tlv.SetPxPyPzE(chi2 * tlv2.Px(), chi2 * tlv2.Py(), chi2 * tlv2.Pz(),
    //                   chi2 * sqrt(tlv2.Px() * tlv2.Px() + tlv2.Py() * tlv2.Py() + tlv2.Pz() * tlv2.Pz()));
    //// Neu2Tlv.SetPtEtaPhiM( chi2*tlv2.Pt(), tlv2.Eta(), tlv2.Phi(), 0 );

    // AnaObj Neu1(MET, Neu1Tlv, 0);
    // AnaObj Neu2(MET, Neu2Tlv, 0);

    // Neu1.setProperty("chi", chi1);
    // Neu2.setProperty("chi", chi2);

    // twoNeu.emplace_back(Neu1);
    // twoNeu.emplace_back(Neu2);

    // return twoNeu;
    return chi;
}

double PhyUtils::calcMTmin(const AnaObjs& cands, const AnaObj& met) {
    double mtmin = 0;
    if (cands.size() < 1) {
        return 0;
    }
    for (unsigned int i = 0; i < cands.size(); i++) {
        double mtTmp = calcMT(cands[i], met);
        if (mtmin < mtTmp) {
            mtmin = mtTmp;
        }
    }
    return mtmin;
}

/********************************************************
 ***************************MT2**************************
 ********************************************************/
double PhyUtils::MT2(const TLorentzVector& o1, const TLorentzVector& o2, const TLorentzVector& met, double minv) {
    double pa[3] = {o1.M(), o1.Px(), o1.Py()};
    double pb[3] = {o2.M(), o2.Px(), o2.Py()};
    double pmiss[3] = {0, met.Px(), met.Py()};
    double mn = minv;
    mt2_bisect::mt2 mt2_event;
    mt2_event.set_momenta(pa, pb, pmiss);
    mt2_event.set_mn(mn);
    return mt2_event.get_mt2();
}

double PhyUtils::MT2(double pt1, double eta1, double phi1, double E1, double pt2, double eta2, double phi2, double E2, double missET,
                     double missphi) {
    double pxmiss = missET * cos(missphi);
    double pymiss = missET * sin(missphi);
    return MT2(pt1, eta1, phi1, E1, pt2, eta2, phi2, E2, pxmiss, pymiss, 0);
}

double PhyUtils::MT2(double pt1, double eta1, double phi1, double E1, double pt2, double eta2, double phi2, double E2, double pxmiss, double pymiss,
                     double minv) {
    TLorentzVector tlv_1;
    TLorentzVector tlv_2;
    tlv_1.SetPtEtaPhiE(pt1, eta1, phi1, E1);
    tlv_2.SetPtEtaPhiE(pt2, eta2, phi2, E2);
    double pa[3] = {tlv_1.M(), tlv_1.Px(), tlv_1.Py()};
    double pb[3] = {tlv_2.M(), tlv_2.Px(), tlv_2.Py()};
    double pmiss[3] = {0, pxmiss, pymiss};
    double mn = minv;
    mt2_bisect::mt2 mt2_event;
    mt2_event.set_momenta(pa, pb, pmiss);
    mt2_event.set_mn(mn);
    return mt2_event.get_mt2();
}

double PhyUtils::MT2Max(const AnaObjs& cands, const TLorentzVector& met, double minv) {
    double mt2 = 0;
    if (cands.size() < 2) {
        return 0;
    }
    for (unsigned int i = 1; i < cands.size(); i++) {
        //		auto cands_i = cands[i];
        for (unsigned int j = 0; j < i; j++) {
            //			auto cands_j = cands[j];
            double mt2Tmp = MT2(cands[i], cands[j], met, minv);
            if (mt2 < mt2Tmp) {
                mt2 = mt2Tmp;
            }
        }
    }
    return mt2;
}

TVectorD PhyUtils::calcEigenValues(const AnaObjs& objs) {
    TMatrixDSym momTensor(3);
    TVectorD eigenValues(3);
    momTensor.Zero();
    double norm = 0;
    for (const auto& o : objs) {
        momTensor(0, 0) += o.Px() * o.Px();
        momTensor(0, 1) += o.Px() * o.Py();
        momTensor(0, 2) += o.Px() * o.Pz();
        momTensor(1, 0) += o.Py() * o.Px();
        momTensor(1, 1) += o.Py() * o.Py();
        momTensor(1, 2) += o.Py() * o.Pz();
        momTensor(2, 0) += o.Pz() * o.Px();
        momTensor(2, 1) += o.Pz() * o.Py();
        momTensor(2, 2) += o.Pz() * o.Pz();
        norm += o.Vect().Mag2();
    }
    momTensor *= 1. / norm;
    momTensor.EigenVectors(eigenValues);
    return eigenValues;
}

float PhyUtils::aplanarity(const AnaObjs& objs) {
    if (objs.size() < 2) return 0;
    TVectorD eigenValues = calcEigenValues(objs);
    return 1.5 * eigenValues(2);
}

float PhyUtils::sphericity(const AnaObjs& objs) {
    if (objs.size() < 2) return 0;
    TVectorD eigenValues = calcEigenValues(objs);
    return 1.5 * (eigenValues(1) + eigenValues(2));
}

float PhyUtils::transSphericity(const AnaObjs& objs) {
    if (objs.size() < 2) return 0;
    TVectorD eigenValues = calcEigenValues(objs);
    return 2 * eigenValues(1) / (eigenValues(0) + eigenValues(1));
}

/**************************************************************
 * *******************Top Tagger Related **********************
 * ***********************************************************/
bool PhyUtils::toptag0j(TLorentzVector v1l, TLorentzVector v2l, Double_t met, Double_t evt_MET_phi) {
    const Double_t wmass = 80400.;
    TLorentzVector vecMet;
    vecMet.SetPtEtaPhiM(met, 0, evt_MET_phi, 0);
    TLorentzVector vecAll = v1l + v2l + vecMet;

    double mctll = calcMCT(v1l, v2l);

    double rr = vecAll.Pt() / (2. * wmass);
    double fact = rr + sqrt(1 + rr * rr);

    // cout << "mctll " << mctll << " cut value " << wmass*fact << std::endl;
    return mctll < wmass * fact ? true : false;
}

bool PhyUtils::toptag2j(double ptjcut, double meffcut, TLorentzVector v1l, TLorentzVector v2l, TLorentzVector v1j, TLorentzVector v2j, Double_t met,
                        int iopt, Double_t evt_MET_phi) {
    //
    static const Double_t tmass = 172.5;
    static const Double_t mljcut = 155.; // The max mass l+j could get from a top decay
    static const Double_t mctjcut = 137.;
    //
    TLorentzVector vjb[2];
    double ml1j[2];
    double ml2j[2];
    //
    double metx = met * cos(evt_MET_phi);
    double mety = met * sin(evt_MET_phi);
    double ptl1 = v1l.Pt();
    double ptl2 = v2l.Pt();
    double ptj1 = v1j.Pt();
    double ptj2 = v2j.Pt();
    double pxus = v1l.Pt() * cos(v1l.Phi()) + v2l.Pt() * cos(v2l.Phi()) + metx;
    double pyus = v1l.Pt() * sin(v1l.Phi()) + v2l.Pt() * sin(v2l.Phi()) + mety;
    double mefftop = ptj1 + ptj2 + ptl1 + ptl2;

    if (ptj2 < ptjcut) return false;
    if (iopt == 0 && mefftop < meffcut) return false;

    // First part:
    //-------------
    vjb[0] = v1j;
    vjb[1] = v2j;
    double mctjj = calcMCT(vjb[0], vjb[1]);
    double pxusjl = vjb[0].Pt() * cos(vjb[0].Phi()) + vjb[1].Pt() * cos(vjb[1].Phi()) + pxus;
    double pyusjl = vjb[0].Pt() * sin(vjb[0].Phi()) + vjb[1].Pt() * sin(vjb[1].Phi()) + pyus;
    double rrj = sqrt(pxusjl * pxusjl + pyusjl * pyusjl) / (2. * tmass);
    double factj = rrj + sqrt(1 + rrj * rrj);

    bool imct = mctjj < mctjcut * factj ? true : false;
    if (iopt == 1) return imct;

    // Second part:
    //--------------
    for (int i = 0; i < 2; ++i) {
        ml1j[i] = (v1l + vjb[i]).M();
        ml2j[i] = (v2l + vjb[i]).M();
    }
    int ncou = 0;
    int icou1[2];
    int icou2[2];
    for (int i = 0; i < 2; ++i) {
        for (int j = 0; j < 2; ++j) {
            if (i != j) {
                if (ml1j[i] < mljcut && ml2j[j] < mljcut) {
                    icou1[ncou] = i;
                    icou2[ncou] = j;
                    ncou++;
                }
            }
        }
    }
    bool imjl = ncou > 0 ? true : false;

    // Third part: finally mct(ql,ql) for each coupling passing the mjl cut
    //-------------exploit the dependence of mct((jl),(jl)) on the maximum
    //             mass^2 of the two jl combinations
    int ngcou = 0;
    for (int i = 0; i < ncou; ++i) {
        int ij1 = icou1[i];
        int ij2 = icou2[i];
        TLorentzVector vtot1 = vjb[ij1] + v1l;
        TLorentzVector vtot2 = vjb[ij2] + v2l;
        double mctjl = calcMCT(vtot1, vtot2);
        double mm1 = (vjb[ij1] + v1l).M();
        double mm2 = (vjb[ij2] + v2l).M();
        double mmax2 = mm1 > mm2 ? (mm1 * mm1) : (mm2 * mm2);
        double upl = mmax2 / (tmass) + (tmass);
        // cout << " i " << " mctjl " << mctjl << " mmax2 " << mmax2 <<
        //" upl " << upl << endl;
        if (mctjl < upl * factj) ngcou++;
    }

    bool imctjl = ngcou > 0 ? true : false;
    // cout << " ll " << imctll << " ct " << imct << " jl " << imjl <<
    //" ctlj " << imctjl << endl;

    return imct & imjl & imctjl;
}

int PhyUtils::GetTopPairs(double ptjcut, double meffcut, int njetscan, TLorentzVector v1l, TLorentzVector v2l, std::vector<TLorentzVector> jets,
                          Double_t met, Double_t evt_MET_phi) {
    // iopt = 0 - paper-like algorithm
    //      = 1 - consider only combinations including the most energetic jet
    int iopt = 0;

    //        bool itopll = toptag0j(v1l, v2l, met, evt_MET_phi);
    //        if(!itopll) return 0;

    bool itop = false;
    int npairs = 0;
    int njet = jets.size();
    if (njet < njetscan) njetscan = njet;

    for (int i1 = 0; i1 < njetscan - 1; ++i1) {
        if (i1 == 0 || iopt == 0) {
            for (int i2 = i1 + 1; i2 < njetscan; ++i2) {
                TLorentzVector v1j = jets.at(i1);
                TLorentzVector v2j = jets.at(i2);
                itop = toptag2j(ptjcut, meffcut, v1l, v2l, v1j, v2j, met, iopt, evt_MET_phi);
                if (itop) npairs++;
            }
        }
    }
    return npairs;
}

int PhyUtils::GetTopPairs(double ptjcut, double meffcut, int njetscan, TLorentzVector v1l, TLorentzVector v2l, std::vector<AnaObj> jets, Double_t met,
                          Double_t evt_MET_phi) {
    // iopt = 0 - paper-like algorithm
    //      = 1 - consider only combinations including the most energetic jet
    int iopt = 0;

    //        bool itopll = toptag0j(v1l, v2l, met, evt_MET_phi);
    //        if(!itopll) return 0;

    bool itop = false;
    int npairs = 0;
    int njet = jets.size();
    if (njet < njetscan) njetscan = njet;

    for (int i1 = 0; i1 < njetscan - 1; ++i1) {
        if (i1 == 0 || iopt == 0) {
            for (int i2 = i1 + 1; i2 < njetscan; ++i2) {
                auto v1j = jets.at(i1);
                auto v2j = jets.at(i2);
                itop = toptag2j(ptjcut, meffcut, v1l, v2l, v1j, v2j, met, iopt, evt_MET_phi);
                if (itop) npairs++;
            }
        }
    }
    return npairs;
}

double PhyUtils::calZn(double sig, double nb, double relativeBkgUncert) {
    double Err = relativeBkgUncert * nb;
    double E2 = Err * Err;
    double n = sig + nb;
    double dll = n * TMath::Log(((sig + nb) * (nb + E2)) / (nb * nb + (sig + nb) * E2)) -
                 ((nb * nb) / (E2)) * TMath::Log((nb * nb + (sig + nb) * E2) / (nb * (nb + E2)));
    double nsigma = TMath::Sqrt(2. * dll);
    return nsigma;
}
/*
double PhyUtils::calZn(double signalExp, double backgroundExp, double relativeBkgUncert)
{
    double mainInf = signalExp + backgroundExp;  //Given
    double tau = 1. / backgroundExp / (relativeBkgUncert*relativeBkgUncert);
    double auxiliaryInf = backgroundExp * tau;  //Given

    double P_Bi = TMath::BetaIncomplete(1. / (1. + tau), mainInf, auxiliaryInf + 1);
    return RooStats::PValueToSignificance(P_Bi);
}
*/

double PhyUtils::DeMTCos(double mT0 /*GeV*/, const TLorentzVector& lepton, const TLorentzVector& met) { return TMath::Cos(lepton.Phi() - met.Phi()); }
double PhyUtils::DeMTQ(double mT0 /*GeV*/, const TLorentzVector& lepton, const TLorentzVector& met) {
    return (1 - (mT0 * mT0) / (2 * lepton.Et() * met.Et()));
}

std::vector<std::pair<int, int> > PhyUtils::GetVBFjetPair(const AnaObjs& jets, double dEta) {
    std::vector<std::pair<int, int> > pairs;

    if (jets.size() < 2) return {};

    std::map<double, int> eta_njet; // map{eta,order}
    for (int i = 0; i < jets.size(); i++) {
        eta_njet[jets.at(i).Eta()] = i;
    }

    // the last element is max_eta element;
    // auto map_at = eta_njet.end();
    // map_at--;
    // double max_eta = map_at->first;
    // int max_num = map_at->second;

    for (auto start = eta_njet.begin(); start != eta_njet.end(); start++) {
        double min_eta = start->first;
        int min_num = start->second;

        auto end_position = eta_njet.end();
        end_position--;

        for (auto end = end_position; end != start; end--) {
            if (end->first - min_eta > dEta && end->first * min_eta < 0) {
                pairs.push_back(std::make_pair(end->second, min_num));
            } else {
                break;
            }
        }
    }

    // if (max_eta - min_eta > dEta && max_eta * min_eta < 0) return std::make_pair(max_num, min_num);

    return pairs;
}
